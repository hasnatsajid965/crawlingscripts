qa_override("[E1473228885]", "Some properties have lot size and some not.");
qa_override("[E2431535312]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="property-image">[\s\t\n ]*?<a href="([^\"]*)"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<span class="current">[^\<]*</span><a href="([^\"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://indominicana.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="col-sm-8">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<p>Alquiler: <span>[a-zA-Z\$ ]*([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<p>Alquiler: <span>([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, '<p>Venta: <span>[a-zA-Z\$ ]*([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<p>Venta: <span>([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value === "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, '<p><strong>Superficie solar:</strong>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
		units = get_unique_regex_match(html, '<p><strong>Superficie solar:</strong>[ 0-9\,\. ]*([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, '<p><strong>Superficie solar:</strong>[ 0-9\,\. ]*[ a-zA-Z\,\. ]*<sup>([0-9]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(units) && isDefined(mls.LotSize.value)) {
			if (units !== "undefinedundefined") {
				mls.LotSize.areaUnits = units;
			}
		}
		mls.LivingArea.value = get_unique_regex_match(html, 'p><strong>Superficie construida:</strong>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
		units = get_unique_regex_match(html, 'p><strong>Superficie construida:</strong>[ 0-9\,\. ]*([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, 'p><strong>Superficie construida:</strong>[ 0-9\,\. ]*[ a-zA-Z\,\. ]*<sup>([0-9]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(units) && isDefined(mls.LotSize.value)) {
			if (units !== "undefinedundefined") {
				mls.LivingArea.areaUnits = units;
			}
		}
		mls.Bedrooms = get_unique_regex_match(html, 'p><strong>Habitaciones:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<p><strong>Ba[^\:]*os:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, '<p><strong>Parqueos:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, '<p><strong>A[^\:]*o construcci[^\:]*n:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<h4>Facilidades</h4>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Aire Acondicionado") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, ' <h4>Descripci[^\<]*n</h4>(.+?)<!--', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<p class="nombre">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.Brokerage.Name == undefined) {
			mls.Brokerage.Name = [];
		}
		mls.office.PhoneNumber = get_next_regex_match(html, 0, '<i class="fa fa-phone"></i> Oficina:([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.office.PhoneNumber == undefined) {
			mls.office.PhoneNumber = [];
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || mls.ListPrice.value == 1) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.MlsId = get_unique_regex_match(html, '<span class="label">Property Reference:</span>([^<]*)</div> ', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Property Type:</span>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '"latitude":([^,]*),"longitude":([^,]*),"markername"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"latitude":([^,]*),"longitude":([^,]*),"markername"', 2, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		mls.Address.StateOrProvince.value = get_unique_regex_match(html, "<p><strong>Provincia:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		cityValue = get_unique_regex_match(html, "<strong>Ciudad:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			cityValue = cityValue.split(",");
			if (isDefined(cityValue[0])) {
				mls.Address.City.value = cityValue[0];
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-flickity-lazyload="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}

}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();

		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=apartamentos&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=apartamentos&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// building en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=edificios&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// building en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=hoteles&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// building en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=hoteles&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// building en rent
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=edificios&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// building en rent
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=hoteles&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=casas&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=penthouses&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=townhouses&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=casas&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=penthouses&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=townhouses&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Office
		{
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=oficinas&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=oficinas&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=solares&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=terrenos&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=solares&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=terrenos&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=fincas&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=naves&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=comeriales&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=sale&type%5B%5D=negocios&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=fincas&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=naves&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=negocios&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://indominicana.com/propiedades?status=rent&type%5B%5D=comeriales&rooms=&parking=&pmin=&pmax=&action=Search+for&refcode=", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
