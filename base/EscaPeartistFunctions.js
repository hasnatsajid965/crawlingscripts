qa_override("[E1473228885]", "Some of the properties have lot size and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<h4><a href="([^\"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<li class="roundright"><a href=\'([^\']*)\'><i class="fa fa-angle-right"></i>', 1, KDONOTNOTIFYERROR);
		checkRepeat.push(relativeLink);
		if (isDefined(checkRepeat)) {
			for (i = 0; i < checkRepeat.length; i++) {
				if (isDefined(checkRepeat[i - 1])) {
					if (checkRepeat[i - 1] == checkRepeat[i]) {
						return cumulatedCount;
					}
				}
			}
		}
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.LotSize.value = get_unique_regex_match(html, '<strong>Property Land Size:</strong>([ 0-9\.\, ]*)[a-zA-Z]*<sup>[0-9]', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<strong>Property Land Size:</strong>[ 0-9\.\, ]*([a-zA-Z]*)<sup>[0-9]', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, '<strong>Property Land Size:</strong>[ 0-9\.\, ]*[a-zA-Z]*<sup>([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="entry-title entry-prop">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="price_area"><span class="price_label price_label_before"></span>[ \$ ]*([0-9\,\. ]*)<span class="price_label">[a-zA-Z]*</span>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="price_area"><span class="price_label price_label_before"></span>[ \$ ]*[0-9\,\. ]*<span class="price_label">([a-zA-Z]*)</span>', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, '<span class="price_area"><span class="price_label price_label_before"></span>([ \$ ]*)[0-9\,\. ]*<span class="price_label">[a-zA-Z]*</span>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.Bedrooms = get_unique_regex_match(html, '<strong>Bedrooms:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<strong>Bathrooms:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, '<strong>Year Built:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<i class="fa fa-check"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("view")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="wpestate_property_description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, 'My details[ ]*?</div>[\s\t\n ]*?<h3><a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<i class="fa fa-phone"></i><a href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.WebsiteURL = get_unique_regex_match(html, '<i class="fa fa-desktop"></i><a href="[^\"]*" target="_blank">([^<]*)', 1, KDONOTNOTIFYERROR);
		var phoneNum = get_unique_regex_match(html, '<i class="fa fa-mobile"></i><a href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(phoneNum)) {
			mls.Brokerage.office.PhoneNumber = phoneNum;
		}
		mls.MlsId = get_unique_regex_match(html, '<strong>Property Id :</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, '<div class="property_categs">[\s\t\n ]*?<a href="[^"]*" rel="tag">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>City:</strong>[\s\t\n ]*?<a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_next_regex_match(html, 0, "<div class=\"listing_detail col-md-4\"><strong>Country:</strong>[\\s\\t\\n ]*?<a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		mls.Address.StreetAdditionalInfo = get_next_regex_match(html, 0, "<strong>Address:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, "<strong>State/Province:</strong>[\\s\\t\\n ]*?<a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '"general_latitude":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '"general_longitude":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<div class="item" style="background-image:url[(]([^\"]*)[)]">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}