qa_override("[E2592408747]", "There is no city on this website.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			el1 = virtual_browser_find_one(browser, "(//div[@class='grid-details']/parent::a)[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//a[@data-selenium-id='propertySearch-pager-nextClick']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						if (virtual_browser_click_element(browser, array[element], KDONOTNOTIFYERROR)) {
							wait(6000);
							html = virtual_browser_html(browser);
							tracer = 0;
							actualCount = 0;
							page++;
							foundIt = true;
							break;
						} else {
							return cumulatedCount;
						}
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		var element
		var imageCount = 0;
		var index = 1;
		element = virtual_browser_find_one(browser, "(//img[@class='container-item'])[2]", KDONOTNOTIFYERROR);
		var obj = JSON.parse(get_list_empty_variable("photo"));
		obj.MediaURL = virtual_browser_element_attribute(element, "src");
		obj.MediaOrderNumber = imageCount;
		if (mls.Photos.photo == undefined) {
			mls.Photos.photo = [];
		}
		mls.Photos.photo.push(obj);
		imageCount++;
		element = virtual_browser_find_one(browser, "(//div[@class='price'])[1]", KDONOTNOTIFYERROR);
		if (element != undefined) {
			var price = virtual_browser_element_text(element);
			mls.ListPrice.value = get_next_regex_match(price, 0, '<!---->[\s\t\n ]*?([�])([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(price, 0, '<!---->[\s\t\n ]*?([�])([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		element = virtual_browser_find_one(browser, "//div[@class='country']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Address.Country.value = virtual_browser_element_text(element);
		}
		index = 1;
		do {
			element = virtual_browser_find_one(browser, "//div[@class='agent-name']", KDONOTNOTIFYERROR);
			if ((isDefined(element)) && (virtual_browser_element_text(element).length > 0)) {
				mls.Brokerage.Name = virtual_browser_element_text(element);
				break;
			}
			index++;
		} while (isDefined(element));
		element = virtual_browser_find_one(browser, "//h1[@class='address']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.ListingTitle = virtual_browser_element_text(element);
		}
		if (isDefined(mls.ListingTitle)) {
			var address = mls.ListingTitle.split(",");
			if (isDefined(address[1])) {
				mls.Address.City.value = address[1].trim();
			}
		}
		if (mls.ListingTitle == "" || isUndefined(mls.ListingTitle) || mls.ListingTitle == null) {
			return analyzeOnePublication_return_unreachable;
		}
		element = virtual_browser_find_one(browser, "//div[@class='contact-agent-name']", KDONOTNOTIFYERROR);
		if ((isDefined(element)) && (virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR))) {
			element = virtual_browser_find_one(browser, "(//div[@class='panel contact-telephone'])[2]", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Brokerage.Phone = virtual_browser_element_text(element);
			}
		}
		element = virtual_browser_find_one(browser, "//li[@class='bed']/div", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Bedrooms = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "//li[@class='bath']/div", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Bathrooms = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "//li[@class='parking']/div", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.NumParkingSpaces = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "(//div[@class='features-title']//following-sibling::span)[1]", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.PropertyType.value = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "(//h2[@class='short-description']//following-sibling::p)[1]", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.ListingDescription = virtual_browser_element_text(element);
		}
		if (isUndefined(mls.ListingDescription) || mls.ListingDescription == null || mls.ListingDescription == "" && mls.ListingTitle == null || isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
			return analyzeOnePublication_return_unreachable;
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}