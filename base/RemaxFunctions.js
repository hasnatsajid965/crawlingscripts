

// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[@class='LinkImage'])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//i[@class='page-next']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KNOTIFYERROR)) {
						loadedMore = false;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		notAvailable = get_unique_regex_match(html, '<div id="LeftMain" PlaceholderDisplayMode="Display">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			return analyzeOnePublication_return_unreachable;
		}
		freq = get_unique_regex_match(html, '<div class="key-price-gran">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(freq)) {
			if (freq.includes("Mensual")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Tama?o de lote [(]([^\<]*)[)]</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^<]*)', 2, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Tama?o de lote [(]([^\<]*)[)]</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, '[^\<]*rea Construida en ([^\<]*)</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^<]*)', 2, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, '[^\<]*rea Construida en ([^\<]*)</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^<]*)', 1, KDONOTNOTIFYERROR);

		}
		mls.Bedrooms = get_unique_regex_match(html, 'Num. de Dormitorios:</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Ba[^\:]*os:</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'A[^\<]*o Construcci[^\<]*n</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Parking Spaces</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="data-item-value">[\s\t\n ]*?<span dir="ltr">([^\<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<i class="tick"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div id="ListingFullLeft_ctl01_DescriptionDivShort" itemprop="description" class="desc-short fts-mark">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<div id="ListingFullLeft_ct[^\"]*_DescriptionDivShort" itemprop="description" class="desc-short fts-mark">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Phone = "512-600-0767/011-506-2787-0226";
		mls.Brokerage.Name = "Costa Rica Real Estate Service Dominical";
		mls.MlsId = get_unique_regex_match(html, 'ID: <span itemprop="productID">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<h2 itemprop="name" class="fts-mark">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_next_regex_match(html, 0, '<div class="col-xs-12 key-address fts-mark">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		if (mls.ListingTitle.includes("VENTA") && mls.ListingTitle.includes("EDIFICIO")) {
			mls.NonMLSListingData.category = getCategory(KEDIFICIOSVENTAS);
		}
		mls.ListPrice.value = get_unique_regex_match(html, '<a dir="ltr" itemprop="price" content="[^\"]*" onclick="[^\"]*" href="[^\"]*">([0-9\.\, ]*)([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<a dir="ltr" itemprop="price" content="[^\"]*" onclick="[^\"]*" href="[^\"]*">([0-9\.\, ]*)([a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var dVideo = get_unique_regex_match(html, '<iframe width="[^\"]*" height="[^\"]*" src="([^\"]*)" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>', 1, KDONOTNOTIFYERROR);
		if (isDefined(dVideo)) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "var lat = ([^\;]*);", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "var lng = ([^\;]*);", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_next_regex_match(html, 0, "<span itemprop='addressCountry'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, "<span itemprop='addressRegion'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = mls.Address.City.value.replace(",&nbsp;", "").trim();
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img class="sp-image" src="[^\"]*" alt="[^\"]*" data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.remax.co" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}