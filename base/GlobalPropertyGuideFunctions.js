qa_override("[E1473228885]", "There is no lot size on this website.");
qa_override("[E2592408747]", "Some properties have city and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			// *[@class='btn_tmc']/parent::a
			// wait(4000);
			el1 = virtual_browser_find_one(browser, "(//*[@class='btn_tmc']/parent::a)[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				finalUrl = virtual_browser_element_attribute(el1, "href");
				resArray.push(finalUrl);
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "(//a[@class='prevnext'])[1]", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		var notAvailable = get_unique_regex_match(html, '<h1 class="blue main_title_2">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			if (notAvailable == "Property Results") {
				return analyzeOnePublication_return_innactive;
			}
		}
		notAvailable = get_unique_regex_match(html, '<a href="/search" class="button_oops2">[\\s\\t\\n ]*?<span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (notAvailable.includes('TRY')) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, ' <div id="full_desc">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0,
				"<strong style=\"margin-top:6px;\" >Price:</strong><br />[\s\t\n ]*?<ul style=\"margin-bottom:6px;\" >[\s\t\n ]*?<li>[^\<]*</li>[\s\t\n ]*?<li>[^\<]*</li>[\s\t\n ]*?<li>([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0,
				"<strong style=\"margin-top:6px;\" >Price:</strong><br />[\s\t\n ]*?<ul style=\"margin-bottom:6px;\" >[\s\t\n ]*?<li>[^\<]*</li>[\s\t\n ]*?<li>[^\<]*</li>[\s\t\n ]*?<li>([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<span class=\"f-left nomt7 hundredh1\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "var latitude=([^\;]*);", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "var longitude=([^\;]*);", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Bedrooms:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Bathrooms:([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.MlsId = get_next_regex_match(html, 0, "<strong>Property ID:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_next_regex_match(html, 0, "Categories:</strong>[\s\t\n ]*?<ul style=\"margin-bottom:6px;\" >[\s\t\n ]*?<li>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Global Property Guide";
		mls.Brokerage.Email = "office.manager@globalpropertyguide.com";
		mls.Location.Directions = get_next_regex_match(html, 0, "<strong>Location:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var tokens = mls.Location.Directions.split(",");
			if (isDefined(tokens[0])) {
				mls.Address.Country.value = tokens[0].trim();
			}
			if (isDefined(tokens[1])) {
				mls.Address.StateOrProvince.value = tokens[1].trim();
			}
			if (isDefined(tokens[2])) {
				mls.Address.City.value = tokens[2].trim();
			}
		}
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryText;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "#imgMain:([^\']*)'[\]],", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}