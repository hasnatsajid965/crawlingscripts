qa_override("[E2592408747]", "There is no city on this website.");
qa_override("[E219240453]", "I have implemented frequency condition on the basis of description.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var forListing;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)" class="underline underline1">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a class="active" href="[^\"]*">[^\<]*</a>[\\s\\t\\n ]*?<a href="([^\"]*)" class="next">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		checkRepeat.push(tracer);
		if (isDefined(checkRepeat)) {
			for (i = 0; i < checkRepeat.length; i++) {
				if (i > 1) {
					print("tracer repeat value..." + checkRepeat[i]);
					if (checkRepeat[i] == tracer) {
						return cumulatedCount;
					}
				}
			}
		}
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.LotSize.value = get_unique_regex_match(html,
				'Lot size:</div>[\s\t\n ]*?<div class="clearfix"></div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="details-parameters-cont">[\s\t\n ]*?<div class="details-parameters-val" style="width:100%!important;">([0-9\,\. ]*)([a-zA-Z\.\, ]*)', 1,
				KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html,
				'Lot size:</div>[\s\t\n ]*?<div class="clearfix"></div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="details-parameters-cont">[\s\t\n ]*?<div class="details-parameters-val" style="width:100%!important;">([0-9\,\. ]*)([a-zA-Z\.\, ]*)', 2,
				KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms</div>[\s\t\n ]*?<div class="details-parameters-val">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms</div>[\s\t\n ]*?<div class="details-parameters-val">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, 'Stories/Floors</div>[\s\t\n ]*?<div class="details-parameters-val">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'Year built</div>[\s\t\n ]*?<div class="details-parameters-val">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<div class="details-parameters-price" onclick="[^\"]*">[\s\t\n ]*?([\$ ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<div class="details-parameters-price" onclick="[^\"]*">[\s\t\n ]*?([\$ ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<strong>Listing ID:</strong>[^\<]*<br /><br />(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_innactive;
		}
		var features = get_all_regex_matched(html, '<i class="jfont">&#xe815;</i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<h3 class="title-negative-margin text-center">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<strong>Listing ID:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="details-title pull-left">[\s\t\n ]*?<h3>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Garage capacity</div>[\s\t\n ]*?<div class="details-parameters-val">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || mls.ListPrice.value <= 1) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isDefined(mls.ListPrice.value)) {
			var listPrice = mls.ListPrice.value.replace(",", "").trim();
			if (listPrice < 99000) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "mapInitGoogle[(]([^\,]*),([^\,]*),", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "mapInitGoogle[(]([^\,]*),([^\,]*),", 2, KDONOTNOTIFYERROR);
		var address = get_next_regex_match(html, 0, "<div class=\"details-title pull-left\">[\s\t\n ]*?<h3>[^\<]*<span class=\"special-color\">[^\<]*</span></h3>[\s\t\n ]*?<br />[\s\t\n ]*?<h4>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(address)) {
			address = address.split(",");
			if (isDefined(address[1])) {
				mls.Address.City.value = address[1].trim();
			}
			if (isDefined(address[2])) {
				mls.Address.Country.value = address[2].trim();
			}
		}
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-background="([^\"]*)" class="swiper-lazy slide-bg">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}