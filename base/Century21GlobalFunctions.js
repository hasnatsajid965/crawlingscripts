// qa_override("[E4063587116]", "Some of the properties have description and some not");
qa_override("[E4230575792]", "Some properties have images and some not.");
qa_override("[E1554112646]", "I am overriding this error because images are crawling but showing me as an error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, " <a class=\"search-result-photo\" href=\"([^\"]*)\">", category, "https://www.century21global.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\" aria-label=\"Next\">Next", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.century21global.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}


// Analyze one publication

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (html.includes("Listing is no longer available.")) {	
			return analyzeOnePublication_return_innactive;	
		}
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListPrice.value = get_unique_regex_match(html, "<div style=\"margin-bottom:15px;\">([\\n\\s]*)<h2>([\\n\\s]*)<span dir=\"ltr\">([^(0-9)]*)([^\<]*)</span> ([A-Za-z]*)</h2>", 4, KDONOTNOTIFYERROR);
		currencyCodeCheck = get_unique_regex_match(html, "<div style=\"margin-bottom:15px;\">([\\n\\s]*)<h2>([\\n\\s]*)<span dir=\"ltr\">([^(0-9)]*)([^\<]*)</span> ([A-Za-z]*)</h2>", 3, KDONOTNOTIFYERROR);
//				+ get_next_regex_match(html, 0, "<div style=\"margin-bottom:15px;\">[\\s\\t\\n ]*?<h2>[\\s\\t\\n ]*?<span dir=\"ltr\">[\$][0-9\,\.]*</span>([ a-zA-Z ]*)</h2>", 1, KDONOTNOTIFYERROR);
		if (currencyCodeCheck !== "undefinedundefined") {
			mls.ListPrice.currencyCode = get_unique_regex_match(html, "<div style=\"margin-bottom:15px;\">([\\n\\s]*)<h2>([\\n\\s]*)<span dir=\"ltr\">([^(0-9)]*)([^\<]*)</span> ([A-Za-z]*)</h2>", 5, KDONOTNOTIFYERROR);
//					+ get_next_regex_match(html, 0, "<div style=\"margin-bottom:15px;\">[\\s\\t\\n ]*?<h2>[\\s\\t\\n ]*?<span dir=\"ltr\">[\$][0-9\,\.]*</span>([ a-zA-Z ]*)</h2>", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) && mls.ListPrice.currencyCode == "undefinedundefined") {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"symbol\">[\s\t\n ]*?<span dir=\"ltr\">([\$ ]*)([0-9\,\.]*)</span>[\s\t\n ]*?[a-zA-Z]*</span>", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"symbol\">[\s\t\n ]*?<span dir=\"ltr\">[\$ ]*[0-9\,\.]*</span>[\s\t\n ]*?([a-zA-Z]*)</span>", 1, KDONOTNOTIFYERROR)
					+ get_next_regex_match(html, 0, "<span class=\"symbol\">[\s\t\n ]*?<span dir=\"ltr\">([\$ ]*)[0-9\,\.]*</span>[\s\t\n ]*?[a-zA-Z]*</span>", 1, KDONOTNOTIFYERROR);
		}

		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isDefined(mls.ListPrice.value)) {
			mls.ListingTitle = mls.ListPrice.value + " " + mls.ListPrice.currencyCode;
		}
		var description = "";
		var description2 = "";
		description = get_unique_regex_match(html, '<div class="remarksSection clear-background" id="propertyRemarksSection">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		description2 = get_unique_regex_match(html, '<p class="seo-paragraph">([^\<]*) ', 1, KDONOTNOTIFYERROR);

		mls.ListingDescription = description2 + " " + description;
		mls.LotSize.value = get_unique_regex_match(html, "<div>Land size:([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<div>Land size:([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_next_regex_match(html, 0, "<div>([^\<]*) bedrooms</div>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "<div>([^\<]*)full baths,[^\<]*</div>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<a class=\"agent-office-link\" href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<br />Phone:([^\<]*)", 1, KDONOTNOTIFYERROR);
		cityValue = get_unique_regex_match(html, "<div class=\"pull-left detail-data-primary\">([\\n\\s]*)([\\s\\S]*)<br/>([^\<]*)<br/>([^\<]*)</div>", 3, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			cityValue = cityValue.split(",");
		}
		if (isDefined(cityValue[1])) {
			mls.Address.City.value = cityValue[1].trim();
		}
		mls.Address.Country.value = get_unique_regex_match(html, 'country: \'([^\']*)\'', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		mls.Location.Latitude = get_unique_regex_match(html, 'data-lat="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'data-lng="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<li><img alt=\"property photo\" rel=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.century21global.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
