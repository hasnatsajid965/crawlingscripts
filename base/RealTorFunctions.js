qa_override("[E2431535312]", "Some of the properties have description and some not.");
qa_override("[E1473228885]", "Some of the properties have lot size and some not.");
qa_override("[E1554112646]", "I am overrriding this error because it is crawling images but showing proportion image error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"btn listing-link button\" href=\"([^\"]*)\">", category, "https://www.realtor.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" class=\"next active\" title=\'Next\' rel=\"next\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.realtor.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.PropertyType.value = get_unique_regex_match(html, "Property Type</strong>[\\s\\t\\n ]*?<span class=\"value\">[\\s\\t\\n ]*?<span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<p class=\"exchanged-price specified\" title=\"([a-zA-Z\$ ]*)([0-9\,\.]*)\">", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p class=\"exchanged-price specified\" title=\"([a-zA-Z\$ ]*)([0-9\,\.]*)\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<p class=\"exchanged-price specified\" title=\"([a-zA-Z\$ ]*)([0-9\,\.]*)([^\"]*)\">", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p class=\"exchanged-price specified\" title=\"([a-zA-Z\$ ]*)([0-9\,\.]*)([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		}
		mls.MlsId = get_next_regex_match(html, 0, "Property ID: <span class=\"listing-id\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, "<h1 class=\"seo-address\" data-property=\"Property\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<p class="desc">Details :-(.+?)</p>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, '<p class="desc">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		}
		if (mls.ListingDescription.includes("/")) {
			mls.ListingDescription = mls.ListingDescription.replace("Details:-MORE INFO HERE:", "").trim();
		}
		if (mls.ListingDescription.includes("]")) {
			mls.ListingDescription = mls.ListingDescription.slice(0, -1);
			mls.ListingDescription = mls.ListingDescription.slice(0, -1);
			mls.ListingDescription = mls.ListingDescription.slice(0, -1);
		}
		// list_price = get_next_regex_match(html, 0, "</dt>[\s\t\n ]*?<dd><span
		// class=\"tag price\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		list_price = get_next_regex_match(html, 0, "<span class=\"font-size-large font-normal\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (list_price.includes("/mo")) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		} else {
			list_price = get_next_regex_match(html, 0, "<p class=\"exchanged-price specified\" title=\"([a-zA-Z\$ ]*)([0-9\,\.]*)([^\"]*)\">", 3, KDONOTNOTIFYERROR);
			if (list_price.includes("month")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}

		var features = get_all_regex_matched(html, '<strong class="attr">([^\<]*)</strong>[\\s\\t\\n ]*?<span class="value"><i class="icon icon-ok">', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Swimming Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Bedrooms = get_next_regex_match(html, 0, "<strong>([^\<]*)</strong> bedrooms</span>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "<strong>([^\<]*)</strong> bathrooms</span>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<strong class=\"lister-name agent-name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "class=\"phone phone_ellipsis\" title=\"Click to reveal phone number\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Land Size</strong>[\s\t\n ]*?<span class='value'>[\s\t\n ]*?([0-9\,\.]+)[\s\t\n ]*?([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Land Size</strong>[\s\t\n ]*?<span class='value'>[\s\t\n ]*?([0-9\,\.]+)[\s\t\n ]*?([^\<]*)", 2, KDONOTNOTIFYERROR);
		cityValue = get_next_regex_match(html, 0, "Address</strong>[\\s\\t\\n ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			commas = (cityValue.match(/,/g) || []).length;
			if (commas == 2) {
				cityValue = cityValue.split(",");
				if (isDefined(cityValue[2])) {
					mls.Address.City.value = cityValue[2].trim();
					if (mls.Address.City.value.includes("request")) {
						mls.Address.City.value = cityValue[1].trim();
					}
				}
			}
			if (commas == 3) {
				cityValue = cityValue.split(",");
				if (isDefined(cityValue[3])) {
					mls.Address.City.value = cityValue[3].trim();
					if (mls.Address.City.value.includes("request")) {
						mls.Address.City.value = cityValue[2].trim();
					}
				}
			}
		}
		mls.Address.Country.value = get_next_regex_match(html, 0, "<div class=\"current-country\">[\\s\\t\\n ]*?<i class=\"flag flag-32 flag-32-[^\"]*\"></i>[\\s\\t\\n ]*?<span class=\"name\" title=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<li class=\"listing-photo img\">[\\s\\t\\n ]*?<img src=\'([^\']*)\'", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("130x98", "668x501");

			if (obj.MediaURL.startsWith("//")) {
				obj.MediaURL = "https:" + obj.MediaURL;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
