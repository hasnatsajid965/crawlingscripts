qa_override("[E2633803259]", "There is no lot size data on this website.");

// crawlForPublications crawl-mode: Regular-Expression Crawling

function crawlCategory(url, category, stopword) {

	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			var reg_exp = "";
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"photo-hover\" href=\"([^\ ]*)\">", category, "https://www.primelocation.com", 1, KDONOTNOTIFYERROR, false)) > 0) {

				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href=\"([^\ ]*)\"([\\n\\s]*)>Next</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = "https://www.primelocation.com" + relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		
		// LISTING PRICE -------------------------------------------------------

		reg_exp = "<span class=\"price\">([\\n\\s]*)([^(0-9)]*)([^\<]*)";

		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, "\"price\": \"([0-9.]*)", 1, KDONOTNOTIFYERROR);
			print("price_number_value: "+ price_number_value);
			if (isDefined(price_number_value)) {
            mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_unique_regex_match(html, "\"currency_code\":\"([^\"]*)", 1, KDONOTNOTIFYERROR);
			}
			
        }print("list price value : " + mls.ListPrice.currencyCode);
		
		// LISTING TITLE -------------------------------------------------------
		//<div class="b72558b0"><h1 class="_64bb5b3b">House Available For Sale at Phul Ghulab Road Abbottabad</h1><div class="cbcd1b2b" aria-label="Property header">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</div></div>

		reg_exp = "<h1 class=\"listing-details-h1\">([\\s\\S]*)</h1>";
		
		if (isDefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, reg_exp, 1, KNOTIFYERROR);
		}
		
		// LISTING DESCRIPTION -------------------------------------------------------
		//<div class="_066bb126"><h3 class="_95f4723e">Description</h3><div class="_892154cd _6c5bbfd9" style="max-height:110px"><div><div class="_2015cd68" aria-label="Property description"><div dir="auto"><span class="_2a806e1e">Phul Ghulab Abbottabad<br />Please call for more details. Be a lucky buyer to own a House in Abbottabad. Make the most of your purchase with this opportunity. Ideally located on Phul Ghulab Road, this is a rare and golden real estate opportunity. With an area spanning 1350  Square Feet, this will prove to be an ideal place for your family. Plug in to our listings and you'll find out that it has various properties that come at a reasonable price tag such as Rs 14,000,000 in this case. You simply cannot say no to this House. Better yet, it comes with tonnes of exciting features that distinguish it from other properties. <br />Don't hesitate to express your concerns, call us.</span></div></div></div>

		reg_exp = "<h3>Property description</h3>([\\n\\s]*)([\\s\\S]*)([\\n\\s]*)</div>([\\n\\s]*)</div>([\\n\\s]*)";

		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR)
		}
				
		// BEDROOMS -------------------------------------------------------
		//<li aria-label="Property detail beds"><span class="_3af7fa95">Bedroom(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<span class=\"num-icon num-beds\" title=\"([^\"]*)\"><span class=\"interface\"></span>([0-9.]*)</span>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		}
		
		// BATHROOMS -------------------------------------------------------
		//<li aria-label="Property detail baths"><span class="_3af7fa95">Bath(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<span class=\"num-icon num-baths\" title=\"([^\"]*)\"><span class=\"interface\"></span>([0-9.]*)</span>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
        }
        
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<h1 class=\"listing-details-h1\">([\\s\\S]*)</h1>";

 		if (isDefined(mls.PropertyType.value)) {
            title = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
            title = title.toLowerCase();
            if (title.includes("house")) {
                mls.PropertyType.value = "House";
                mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
            }
            if (title.includes("Apartment")) {
                mls.PropertyType.value = "House";
                mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
			}
		}
		
		// BROKERAGE NAME -------------------------------------------------------
		//<span class="_725b3e64">Arif Khan</span></div><div class="_6f510616"><a href="/Profile/Abbottabad-Fahad_Real_Estate-179977-1.html" class="_23d69e6d" title="Fahad Real Estate">Agency profile</a></div>
		reg_exp = "<h4>Marketed by</h4>([\\n\\s]*)<img data-src=\"([^\ ]*)\"([\\n\\s]*)alt=\"([^\"]*)\" class=\"agent_logo lazy\" >";
		if (isDefined(mls.Brokerage.Name)) {
			mls.Brokerage.Name = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		
		// BROKERAGE Phone No. -------------------------------------------------------
		//<div class="_9ff5faa9 _02e9ff75 _27fee0c9">0800-ZAMEEN (92633)</div><div class="_9ff5faa9 _02e9ff75 _27fee0c9">(+92) 42 3256 0445</div>
		reg_exp = "<span class=\"phone\"><a href=\"([^\"]*)\" data-ga-category=\"([^\"]*)\" data-ga-action=\"Call\" data-ga-label=\"([^\"]*)\" class=\"agent_phone\">([^\<]*)</a> \\*</span>";
		if (isDefined(mls.Brokerage.Phone)) {
			mls.Brokerage.Phone = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);

		}
		
		
		cityValue = mls.ListingTitle;
		if (isDefined(cityValue)) {
			commas = (cityValue.match(/,/g) || []).length;
			if (commas == 2) {
				cityValue = cityValue.split(",");
				if (isDefined(cityValue[1])) {
					mls.Address.City.value = cityValue[1].trim();
				}
			}
			if (commas == 3) {
				cityValue = cityValue.split(",");
				if (isDefined(cityValue[2])) {
					mls.Address.City.value = cityValue[2].trim();
				}
			}
		}
		
		mls.Address.Country.value = countryText;
				
		//<picture class="_219b7e0a"><source type="image/webp" srcSet="https://media.zameen.com/thumbnails/85464870-120x90.webp"/><img role="presentation" alt="2 " title="2 " src="https://media.zameen.com/thumbnails/85464870-120x90.jpeg"/></picture>
		reg_exp = "<meta property=\"og:image\" content=\"([^\"]*)\"/>";
		images = get_all_regex_matched(html, reg_exp, 1);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
				mls.Photos.photo.push(obj);
							imageCount++;
				
		});
		
		
		
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}