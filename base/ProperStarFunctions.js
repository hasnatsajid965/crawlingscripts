
// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {

	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"shadow-wrap\"><a class=\"link advert-vendors-link\" href=\"([^\"]*)\">", category, "https://www.properstar.co.uk", 1, KDONOTNOTIFYERROR, false)) > 0) {

				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<link data-react-helmet=\"true\" rel=\"next\" href=\"([^\"]*)\"/>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		
		// LISTING BROKER CODE -------------------------------------------------------
		//<span itemscope="" itemProp="itemListElement" itemType="http://schema.org/ListItem" dir="auto"><span itemProp="name" class="c5051fb4">House 23517282</span><meta itemProp="position" content="4"/></span>
		var reg_exp = "\"reference\"\\:\"([^\"]*)\"";
		var code_part1 = get_next_regex_match(html, 0, reg_exp, 1, KDONOTNOTIFYERROR);
		if (isDefined(code_part1)){
			if(mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code_part1);
			mls.NonMLSListingData.brokerCodes.push("Ref:" + code_part1);
		}

		


		// LISTING RENT MATCHING	
		reg_exp = "<h6 class=\"heading description-title\" dir=\"auto\">([^\<]*)</h6>";
		forRent = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);

		if (isDefined(forRent)) {
			if (forRent.includes("rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} 
		else {
			forRent = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("rent")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}

		/*
		forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-122 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (get_next_regex_match(html, 0, "<i id=\"houzez-print\" class=\"fa fa-print\"", 0, KDONOTNOTIFYERROR) != undefined) {
			var pos = get_last_regex_match_end_pos();
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}*/
		
		// LISTING PRICE -------------------------------------------------------

		//<div><div class="_126656cb "><div class="c4fc20ba"><span class="e63a6bfb" aria-label="Currency">PKR</span><span class="_14bafbc4"></span><span class="_105b8a67" aria-label="Price">1.4 Crore</span></div></div>

		reg_exp = "<h1 class=\"heading price\" dir=\"auto\"><span>([^0-9.]*)([^\<]*)</span></h1>";

		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		
			print("price_number_value: "+ price_number_value);
			mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
			
		
		// LISTING TITLE -------------------------------------------------------
		//<div class="b72558b0"><h1 class="_64bb5b3b">House Available For Sale at Phul Ghulab Road Abbottabad</h1><div class="cbcd1b2b" aria-label="Property header">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</div></div>

		reg_exp = "<title data-react-helmet=\"true\">([^\<]*)</title>";
		
		if (isDefined(mls.ListingTitle)) {
             var title = get_unique_regex_match(html, reg_exp, 1, KNOTIFYERROR);
             mls.ListingTitle = title.replace('- Properstar', '');
		}
		
		
		
		// LISTING DESCRIPTION -------------------------------------------------------
		//<div class="_066bb126"><h3 class="_95f4723e">Description</h3><div class="_892154cd _6c5bbfd9" style="max-height:110px"><div><div class="_2015cd68" aria-label="Property description"><div dir="auto"><span class="_2a806e1e">Phul Ghulab Abbottabad<br />Please call for more details. Be a lucky buyer to own a House in Abbottabad. Make the most of your purchase with this opportunity. Ideally located on Phul Ghulab Road, this is a rare and golden real estate opportunity. With an area spanning 1350  Square Feet, this will prove to be an ideal place for your family. Plug in to our listings and you'll find out that it has various properties that come at a reasonable price tag such as Rs 14,000,000 in this case. You simply cannot say no to this House. Better yet, it comes with tonnes of exciting features that distinguish it from other properties. <br />Don't hesitate to express your concerns, call us.</span></div></div></div>

		reg_exp = "\"descriptionFull\"\\:\\[\\{\"language\"\\:\"en\"\\,\"text\"\:\"([\\s\\S]*)\",\"original\"\\:true\\}\\]";

		if (isDefined(mls.ListingDescription)) {
            description = get_next_regex_match(html, 2, reg_exp, 1, KDONOTNOTIFYERROR);
            mls.ListingDescription = "<p>" + description.substring(0, description.indexOf("\",\"original\":true")) + "</p>";
		}
		
		// LAND SIZE -------------------------------------------------------
		//<li aria-label="Property detail area"><span class="_3af7fa95">Area</span><span class="_812aa185" aria-label="Value"><span>6 Marla</span></span></li>
        reg_exp = "<span class=\"property-key\">Living</span><span class=\"property-value\">([0-9.]*) ([^\<]*)</span>";
        reg_exp2 = "<span class=\"property-key\">Internal</span><span class=\"property-value\">([0-9.]*) ([^\<]*)</span>";
        reg_exp3 = "<span class=\"property-key\">Land</span><span class=\"property-value\">([0-9.]*) ([^\<]*)</span>";

        var lot_size = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
        var lot_size2 = get_unique_regex_match(html, reg_exp2, 1, KDONOTNOTIFYERROR);
        var lot_size3 = get_unique_regex_match(html, reg_exp3, 1, KDONOTNOTIFYERROR);
        if (isDefined(lot_size)) {
			mls.LotSize.value = lot_size;
			mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
        }
        else if(isDefined(lot_size2)){
            mls.LotSize.value = lot_size2;
			mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp2, 2, KDONOTNOTIFYERROR);
        }
        else if(isDefined(lot_size3)){
            mls.LotSize.value = lot_size3;
			mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp3, 2, KDONOTNOTIFYERROR);
        }
		
		// BEDROOMS -------------------------------------------------------
		//<li aria-label="Property detail beds"><span class="_3af7fa95">Bedroom(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<span class=\"property-key\">Bedrooms</span><span class=\"property-value\">([0-9.]*)</span>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		
		// BATHROOMS -------------------------------------------------------
		//<li aria-label="Property detail baths"><span class="_3af7fa95">Bath(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<span class=\"property-key\">Bathrooms</span><span class=\"property-value\">([0-9.]*)</span>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<span class=\"property-key\">Type</span><span class=\"property-value\">([^\<]*)</span>";
        reg_exp2 = "<title data-react-helmet=\"true\">([^\<]*)</title>";
        var property = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
        var check_property = get_unique_regex_match(html, reg_exp2, 1, KDONOTNOTIFYERROR); 
 		if (isDefined(property)) {
			mls.PropertyType.value = property;
        }
        forRent = forRent.toLowerCase();
        if (isDefined(check_property) && forRent.includes("sale")) {
            check_property = check_property.toLowerCase();
            if (check_property.includes("house")) {
                mls.PropertyType.value = "House";
                mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
            }
            if (check_property.includes("apartment")) {
                mls.PropertyType.value = "Apartment";
                mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
            }
            if (check_property.includes("land")) {
                mls.PropertyType.value = "Land";
                mls.NonMLSListingData.category = getCategory(KLANDFORSALE);
            }
            if (check_property.includes("building")) {
                mls.PropertyType.value = "Building";
                mls.NonMLSListingData.category = getCategory(KBUILDINGFORSALE);
            }
        }
        
        
		if (isDefined(mls.PropertyType.value)) {
            check_property = check_property.toLowerCase();
			if (check_property.includes("house")) {
				if (isDefined(forRent)) {
					if (forRent.includes("rent")) {
						mls.NonMLSListingData.category = getCategory(KHOUSEFORLEASE);
					}
				}
			}
			if (check_property.includes("apartment")) {
				if (isDefined(forRent)) {
					if (forRent.includes("rent")) {
						mls.NonMLSListingData.category = getCategory(KFLATFORLEASE);
					}
				}
            }
            if (check_property.includes("land")) {
				if (isDefined(forRent)) {
					if (forRent.includes("rent")) {
						mls.NonMLSListingData.category = getCategory(KLANDFORLEASE);
					}
				}
            }
            if (check_property.includes("building")) {
				if (isDefined(forRent)) {
					if (forRent.includes("rent")) {
						mls.NonMLSListingData.category = getCategory(KBUILDINGFORLEASE);
					}
				}
			}
		}
		
		// BROKERAGE NAME -------------------------------------------------------
		//<span class="_725b3e64">Arif Khan</span></div><div class="_6f510616"><a href="/Profile/Abbottabad-Fahad_Real_Estate-179977-1.html" class="_23d69e6d" title="Fahad Real Estate">Agency profile</a></div>
		reg_exp = "<h6 class=\"heading account-name\" dir=\"auto\">([^\<]*)</h6><div class=\"agency-contact-info\">";
		if (isDefined(mls.Brokerage.Name)) {
			mls.Brokerage.Name = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		
		// BROKERAGE Phone No. -------------------------------------------------------
		//<div class="_9ff5faa9 _02e9ff75 _27fee0c9">0800-ZAMEEN (92633)</div><div class="_9ff5faa9 _02e9ff75 _27fee0c9">(+92) 42 3256 0445</div>
		reg_exp = "<span><a href=\"tel:([^\"]*)\" id=\"call-agency-button\" class=\"link btn btn-link\"><span>Call the agency</span></a></span>";
		if (isDefined(mls.Brokerage.Phone)) {
			mls.Brokerage.Phone = "+1" + get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);

		}
		
		// BROKERAGE ADDRESS -------------------------------------------------------
		//<li aria-label="Property detail location"><span class="_3af7fa95">Location</span><span class="_812aa185" aria-label="Value">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</span></li>
		//"loc_city_id":"385","loc_city_name":"Abbottabad","loc_id":"15946","loc_name":"Phul Ghulab Road","city_id":"385","city_name":"Abbottabad","listing_type":9,
		reg_exp = "\"city\":\"([^\"]*)\"";
		if (isDefined(mls.Address.City.value)) {
			var city_name = get_next_regex_match(html, 0, reg_exp, 1, KDONOTNOTIFYERROR);


			mls.Address.City.value = city_name;

		}
		
		mls.Address.Country.value = countryContext;
		reg_exp = "\"latitude\":([^\,]*),\"longitude\":([^\,/}]*)";
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_next_regex_match(html, 0, reg_exp, 1, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_next_regex_match(html, 0, reg_exp, 2, KDONOTNOTIFYERROR);
				
		//<picture class="_219b7e0a"><source type="image/webp" srcSet="https://media.zameen.com/thumbnails/85464870-120x90.webp"/><img role="presentation" alt="2 " title="2 " src="https://media.zameen.com/thumbnails/85464870-120x90.jpeg"/></picture>
        reg_exp = "\"resources\":\\{\"pictures\":\\{\"number\":([0-9.]*),\"items\":\\[([^\]]*)";
        image_array = get_next_regex_match(html, 0, reg_exp, 2, KDONOTNOTIFYERROR);
        reg_exp = "\"url\":\"([^\"]*?)\",\"displayOrder\":([0-9]*?)";
		images = get_all_regex_matched(image_array, reg_exp, 1);//(?!.*\1)
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			//if (getFileSizeWithProxyConsiderations (obj.MediaURL) > 0) {
				mls.Photos.photo.push(obj);
							imageCount++;
			//	}
		});
		
		// VIDEOS --------------------------------------------------------
		//"title":null,"host":"youtube","url":"https:\u002F\u002Fyoutu.be\u002FwgGhdi9Ed7U","orderIndex":0},"createdAt":1591812523,"
		reg_exp = "\"id\":\"Video\",\"name\":\"Video\"},\"url\":\"([^\"]*)\",\"displayOrder\":([0-9.]*)";
		videos = get_next_regex_match(html, 0, reg_exp, 1, KDONOTNOTIFYERROR);
        videos = videos.replace('\u002F', '');
        mls.Videos.video = videos;
	
		
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
