qa_override("[E1554112646]", "Some properties have images and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'itemprop="url" class="ann-box-title" href="([^"]*)">', category, "https://www.encuentra24.com", 1, KDONOTNOTIFYERROR, true)) > 0) {
			tracer++;
			var dMatch = get_next_regex_match(html, tracer, '<li class="hide-on-hover ann-box-hilight-time">[ \\t\\r\\n]*<span class="icon icon-time"></span>[ \\t\\r\\n]*<span class="value">([^<]*)</span>[ \\t\\r\\n]*</li>', 1, KNOTIFYERROR);
			actualCount++;
			cumulatedCount++;
			if (isDefined(dMatch) && dMatch.match(stopword)) {
				return cumulatedCount;
			}
			if (passedMaxPublications()) {
				break;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a rel="next" href="([^\"]*)" aria-label="Next" title="Next"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.encuentra24.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(false);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		var list_not_available = get_unique_regex_match(html, '<h2 class="detail">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "Anuncio borrado!") {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListPrice.value = get_unique_regex_match(html, 'Alquiler:</span>[\s\t\n ]*?<span class="info-value">([a-zA-Z\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Alquiler:</span>[\s\t\n ]*?<span class="info-value">([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		} else {
			mls.ListPrice.value = get_unique_regex_match(html, 'Precio:</span>[\s\t\n ]*?<span class="info-value">([a-zA-Z\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Precio:</span>[\s\t\n ]*?<span class="info-value">([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
			if (isUndefined(mls.ListPrice.value)) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls.ListingTitle = get_unique_regex_match(html, '<h1 class="product-title">([^<]*)', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'Tama[^\<]*o del lote:</span>[\s\t\n ]*?<span class="info-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, 'M2:</span>[\s\t\n ]*?<span class="info-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, '<span class="info-name">M[^\<] de construcci[^\:]*n:</span>[\s\t\n ]*?<span class="info-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, '<span class="info-name">Tama[^\<]o del Lote m[^\:]*:</span>[\s\t\n ]*?<span class="info-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.PropertyType.value = get_unique_regex_match(html, 'Category:</span>[\\s\\t\\n ]*?<span class="info-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<span class="info-name">Recamaras:</span>[ \\t\\r\\n]*<span class="info-value">([0-9]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<span class="info-name">Ba?os:</span>[ \\t\\r\\n]*<span class="info-value">([0-9]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, '<span class="info-name">Parking:</span>[ \\t\\r\\n]*<span class="info-value">([0-9]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<span class="ad-id">ID.:([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<span class="user-name">([^<]*)</', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, '<span class="phone icon icon-call">([^<]*)<div class="see-phone"', 1, KDONOTNOTIFYERROR);
		var code = get_next_regex_match(html, 0, '<span class="ad-id">ID.: ([0-9]*)</span>', 1, KNOTIFYERROR);
		if (mls.NonMLSListingData.brokerCodes == undefined) {
			mls.NonMLSListingData.brokerCodes = [];
		}
		mls.NonMLSListingData.brokerCodes.push(code);
		mls.ListingDescription = get_next_regex_match(html, 0, '<span class="box-name">Detalles</span>(.+?)</div>', 1, KNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '"addressLocality": "([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, '"addressCountry": "([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		if (isUndefined(mls.Address.City.value)) {
			mls.Address.City.value = get_unique_regex_match(html, "listing_region: '([^']*)'", 1, KDONOTNOTIFYERROR);
		}
		var images = get_all_regex_matched(html, '<a href="([^"]*)" data-lightbox="product-gallery">', 1);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
