

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'href="([^"]*)" rel="bookmark"', category, "https://www.sothebysrealty.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a href="([^"]*)" onclick="[^"]*" class="paging__item  paging__item--next  js-at-icon-paging  js-paging-next"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.sothebysrealty.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="main-address">([^<]*)', 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, 'class="listing-info__value">([0-9\,\.]*)([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Interior', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'class="listing-info__value">([0-9\,\.]*)([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Interior', 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'class="listing-info__value">([0-9\.\,]*)([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Exterior', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'class="listing-info__value">([0-9\.\,]*)([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Exterior', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?Full Bathrooms', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<span class="prop-description__amenities-list-item-text" itemprop="value">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		description = get_next_regex_match(html, 0, '<div class="p">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(description)) {
			description = description.replace(/(<([^>]+)>)/ig, "");
			mls.ListingDescription = description.replace(/\s+/g, ' ').trim();
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<a class="org  m-contact-info__broker-name" itemprop="url" onclick="[^"]*" href="[^"]*"><span>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'show-on-touch-device" href="tel:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, 'class="listing-info__property-item">MLS ID:</span><span class="listing-info__property-item">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="c-price"><span class="price__value  u-ignore-phonenumber">[\$]([0-9\,\.]*)<!----></span><span class="price__currency">[^\<]*</span>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<div class="c-price"><span class="price__value  u-ignore-phonenumber">[\$][0-9\,\.]*<!----></span><span class="price__currency">([^\<]*)</span>', 1, KDONOTNOTIFYERROR)
				+ get_next_regex_match(html, 0, '<div class="c-price"><span class="price__value  u-ignore-phonenumber">([\$])[0-9\,\.]*<!----></span><span class="price__currency">[^\<]*</span>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			list_price = get_next_regex_match(html, 0, '<span class="price__value  price__upon-request">([^<]*)', 1, KDONOTNOTIFYERROR);
			if (list_price == "Price Upon Request") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		currency_period = get_next_regex_match(html, 0, "<span class=\"price__rental-frequency\">([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(currency_period)) {
			if (currency_period == "Monthly") {
				currency_period = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(currency_period);
			} else if (currency_period == "Daily") {
				currency_period = "day";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(currency_period);
			} else if (currency_period == "Season") {
				currency_period = "year";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(currency_period);
			} else {
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<td>Property Type</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '"#detailMap",Latitude:"([^"]*)",Longitude:"([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"#detailMap",Latitude:"([^"]*)",Longitude:"([^"]*)"', 2, KDONOTNOTIFYERROR);
		mls.Address.PostalCode.value = get_next_regex_match(html, 0, '<span itemprop="postalCode" class="postal-code">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, '<span itemprop="addressLocality" class="locality">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, '<span itemprop="addressRegion" class="region">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_next_regex_match(html, 0, '<span itemprop="addressCountry" class="country-name">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryContext;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(
				html,
				'js-delay-smartimage"><a class="detail-media__fullscreen  js-go-fullscreen" aria-label="Go fullscreen"><i class="icon-custom icon-custom-full-screen" aria-hidden="true"><!----></i></a><a class="carousel__expand-src  js-expand-src  js-image-src  u-out-from-viewport" tabindex="-1" href="([^"]*)"',
				1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.sothebysrealty.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}