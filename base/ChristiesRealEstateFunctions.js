

// crawlForPublications crawl-mode: Virtual Browser Crawling

function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href, '/sales/detail/') and @class='listing-item__text--spaced  listing-item__text'])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					// el1 = "https://clasificados.com.do/"+el1;
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for (var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			index1 = 2;
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//a[@class='paging__item  paging__item--next  js-at-icon-paging  js-paging-next']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KNOTIFYERROR)) {
						loadedMore = false;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
				index1++;
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(true);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_unique_regex_match(html, '<div class="main-address">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="label">Sale Price:</span>([ \$ ]*)[0-9\,\. ]*', 1, KDONOTNOTIFYERROR) + get_unique_regex_match(html, '<span class="price__currency">([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="price__value  u-ignore-phonenumber">[ \$ ]*([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, 'info__value">([0-9\.\, ]*) ([a-zA-Z\.\, ]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Exterior', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'info__value">([0-9\.\, ]*) ([a-zA-Z\.\, ]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Exterior', 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<span class="label">Floor Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<span class="label">Floor Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'class="listing-info__value">([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'class="listing-info__value">([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Full Baths', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<span class="icon icon-checked  prop-description__amenities-list-item-icon"><!----></span><span class="prop-description__amenities-list-item-text" itemprop="value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div itemprop="description" class="prop-description__comments-long  u-text-align-justify  u-margin-bottom"><div class="p">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Christies's International Real Estate";

		mls.office.PhoneNumber = "+1 646 960 3305  / +1 877 745 5574";
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.NonMLSListingData.office = {};
		mls.NonMLSListingData.office.Phone = "+ 44 20 3824 1951 / +1 970 355 3153  / +852 5808 7680";
		mls.YearBuilt = get_unique_regex_match(html, "class=\"listing-info__value\">([^\<]*)</dd><dt itemprop=\"name\" class=\"listing-info__title\">[\\s\\t\\n ]*?Year Built", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "class=\"listing-info__value\">([^\<]*)</dd><dt itemprop=\"name\" class=\"listing-info__title\">Property Type", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, 'Latitude:"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, 'Longitude:"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "<span itemprop=\"addressCountry\" class=\"country-name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryText;
		}
		mls.Address.City.value = get_unique_regex_match(html, "<span class=\"locality\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<span style="" data-random="[^\"]*" class="o-smartimage  js-smartimage-parent  "><span class="js-smartimage-wrap smartimage-wrap o-loader u-align-vh"><noscript><img src="([^\"]*)"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (oneImageTag !== "/siteresources/my folder/responsive/images/logos/cire-logo.png") {
					if (/(http(s?)):\/\//gi.test(oneImageTag)) {
						obj.MediaURL = oneImageTag;
					} else {
						obj.MediaURL = oneImageTag;
					}
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
				}
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}

}