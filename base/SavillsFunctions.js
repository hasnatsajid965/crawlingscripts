qa_override("[E2431535312]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="sv-details__link" id="[^\"]*" href="([^\"]*)">', category, "https://search.savills.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="sv-pagination__list-item-button" href="([^\"]*)">Next &gt;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://search.savills.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	url = url.replace(/ /g, "");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="sv-property-intro__address-line-1">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<li><b>Land</b>([ 0-9\,\. ]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<li><b>Land</b>([ 0-9\,\. ]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="sv-description-point__body">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<p class="card-body card-text card-remarks">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<li class="sv-agent__point sv--name">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, '<span class="sv-view-gt-lrg">([^\<]*)', 1, KDONOTNOTIFYERROR);
		forRent = get_unique_regex_match(html, '<span class="sv-property-price__rent-basis">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("year")) {
				freq = "year";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (forRent.includes("Weekly")) {
				freq = "weekly";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (forRent.includes("Monthly")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="sv-property-price__original">[\\(]<span>([\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="sv-property-price__original">[\\(]<span>([\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var features = get_all_regex_matched(html, "<li><span><span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "A/C Unit") {
						mls.DetailedCharacteristics.hasAC = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<div class=\"sv-property-attribute sv--residential sv--yellow sv--large sv--dark\"><span class=\"sv-property-attribute__value\" title=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<div class=\"sv-property-attribute sv--bedrooms sv--yellow sv--large sv--dark\"><span class=\"sv-property-attribute__value\" title=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "title=\"[^\"]*\">([^\<]*)</span><span class=\"sv-property-attribute__label\">Bathrooms", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<div class=\"sv-property-attribute sv--size sv--yellow sv--large sv--dark\"><span class=\"sv-property-attribute__value\" title=\"[^\"]*\">([ 0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<div class=\"sv-property-attribute sv--size sv--yellow sv--large sv--dark\"><span class=\"sv-property-attribute__value\" title=\"[^\"]*\">([ 0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '"Latitude":([^\,]*),', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"Longitude":([^\,]*),', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '"CityTown":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = countryText;
		var imageCount = 0;
		var images;
		// images = get_all_regex_matched(html, '"ImageUrl_L":"([^\"]*)"', 1);
		images = get_all_regex_matched(html, '"ImageUrl_L":"([^\"]*)","Caption":"Picture No', 1);
		if (isDefined(images) && images !== null && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					if (oneImageTag.includes("_gal") && oneImageTag !== null && oneImageTag !== "") {
						obj.MediaURL = oneImageTag;
						obj.MediaOrderNumber = imageCount;
						if (mls.Photos.photo == undefined) {
							mls.Photos.photo = [];
						}
						mls.Photos.photo.push(obj);
						imageCount++;
					}
				} else {
					if (oneImageTag !== null && oneImageTag !== "") {
						obj.MediaURL = "https://assets.savills.com" + oneImageTag;
					}
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
				}

			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}