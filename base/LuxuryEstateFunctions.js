qa_override("[E1473228885]", "Some properties have lot size and some not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="js_clickable" data-role="self-link-if-is-mobile" href="([^"]*)"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="next" href="([^"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	setCountryPhoneCode("599");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	if (html == null) {
		return analyzeOnePublication_return_innactive;
	}
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		notAvaiable = get_unique_regex_match(html, '<h1 class="static-title serif-light">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvaiable)) {
			if (notAvaiable.includes("Page not")) {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingTitle = get_unique_regex_match(html, '<h1 class="serif-light title-property">([^<]*)</h1>', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<div class="item-inner short-item feat-item"><span class="feat-label">Size</span><div class="single-value">[\s\t\n ]*?([0-9\.\, ]+)([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<div class="item-inner short-item feat-item"><span class="feat-label">Size</span><div class="single-value">[\s\t\n ]*?([0-9\.\, ]+)([a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		total_Rooms = get_unique_regex_match(html, '<div class="item-inner short-item feat-item"><span class="feat-label">Rooms</span><div class="single-value">([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		total_bedrooms = get_unique_regex_match(html, '<div class="item-inner short-item feat-item"><span class="feat-label">Bedrooms</span><div class="single-value">([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		total_bathrooms = get_unique_regex_match(html, '<div class="item-inner short-item feat-item"><span class="feat-label">Bathrooms</span><div class="single-value">([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		total_MlsId = get_unique_regex_match(html, '<span class="feat-label">Reference</span><div class="single-value">([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		final_view = get_unique_regex_match(html, '<span class="feat-label">View</span><div class="multiple-values">(.*?)</div>', 1, KDONOTNOTIFYERROR);
		cooling_system = get_unique_regex_match(html, '<span class="feat-label">Cooling Systems</span><div class="multiple-values">(.*?)</div>', 1, KDONOTNOTIFYERROR);
		exterior_type = get_unique_regex_match(html, '<span class="feat-label">Exterior Amenities</span><div class="multiple-values">(.*?)</div>', 1, KDONOTNOTIFYERROR);
		full_video = get_unique_regex_match(html, 'videoUrl":"([^"]*)', 1, KDONOTNOTIFYERROR);
		tour_video = get_unique_regex_match(html, 'virtualTourUrl":"([^"]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, '<span class="breadcrumb-name" itemprop="name">([^<]*)</span></a><meta itemprop="position" content="2"[ ]?', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = countryText;
		}
		mls.Address.City.value = get_unique_regex_match(html, 'itemprop="name">([^\<]*)</span><svg class="le-icon dropdown-icon visible-xs-inline-block" ><use xlink:href="[^\"]*"></use></svg></a><meta itemprop="position" content="[^\"]*" />', 1,
				KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(
				html,
				'<div class="agency__name-container">[\r\n\t ]*?<a[ ]?href="([^"]*)"[ ]?>([^<]*)</a>[\r\n\t ]*?</div>[\r\n\t ]*?<div[ ]?class="agency__location-container small text-muted address">([^<]*)</div>[\r\n\t ]*?</div>[\r\n\t ]*?<div data-role="limit-button"></div>',
				2, KDONOTNOTIFYERROR);
		mls.Videos.video = {};
		if (isUndefined(full_video)) {
			mls.Videos.video = [];
		} else {
			mls.Videos.video.push(full_video);
		}
		if (isUndefined(tour_video)) {
			mls.Videos.video = [];
		} else {
			mls.Videos.video.push(tour_video);
		}
		if (total_Rooms == "undefined") {
			mls.Rooms = "";
		} else {
			mls.Rooms = total_Rooms;
		}
		if (total_bedrooms == "undefined") {
			mls.Bedrooms = "";
		} else {
			mls.Bedrooms = total_bedrooms;
		}
		if (total_bathrooms == "undefined") {
			mls.Bathrooms = "";
		} else {
			mls.Bathrooms = total_bathrooms;
		}
		if (total_MlsId == "undefined") {
			mls.MlsId = "";
		} else {
			mls.MlsId = total_MlsId;
		}
		if (final_view == "undefined") {
			mls.ViewTypes = mls.ExteriorTypes = get_unique_regex_match(html, '<span class="feat-label">View</span><div class="single-value">[\r\n\t ]*?([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		} else {
			mls.ViewTypes = final_view;
		}
		if (cooling_system == "undefined") {
			mls.ExteriorTypes = get_unique_regex_match(html, '<span class="feat-label">Cooling Systems</span><div class="single-value">[\r\n\t ]*?([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		} else {
			mls.CoolingSystems = cooling_system;
		}
		if (exterior_type == "undefined") {
			mls.ExteriorTypes = get_unique_regex_match(html, '<span class="feat-label">Exterior Amenities</span><div class="single-value">[\r\n\t ]*?([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		} else {
			mls.ExteriorTypes = exterior_type;
		}
		for_rent = get_next_regex_match(html, 0, '<span class="price-transaction">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "week") {
				freq = "week";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (url.includes("rent") || url.includes("month")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		var element = virtual_browser_find_one(browser, "//span[starts-with(@data-role, 'description-text-content')]", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.ListingDescription = virtual_browser_element_text(element);
		}
		if (isUndefined(mls.ListingDescription) || mls.ListingDescription == "") {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="text-right price" data-role="property-price">([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="text-right price" data-role="property-price">([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			list_price = get_next_regex_match(html, 0, '<div class="pull-left text-right price upon-request">([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(list_price)) {
				if (list_price.includes("Price")) {
					mls.ListPrice.value = KPRICEONDEMAND;
				}
			}
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<span class="selected value--text" data-role="property-currency-selected">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '\\Q{"original":"","src":"\\E([^"]*)\\Q"\\E', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("\\", "").replace("//", "https://");
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_innactive;
	}
}