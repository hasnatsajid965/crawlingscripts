qa_override("[E2592408747]", "There is no city data on this site");
qa_override("[E1473228885]", "Some properties have lot size and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" class=\"readmore\">", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<div class=\"alignright\"><a href=\"([^\"]*)\" >", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (html == null) {
			return analyzeOnePublication_return_innactive;
		}
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		notFound = get_next_regex_match(html, 0, "<div class=\"entry\">[\\s\\t\\n ]*?<!--[^\>]*-->[\\s\\t\\n ]*?<h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(notFound)) {
			if (notFound == "No Results Found") {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_next_regex_match(html, 0, "new google\\.maps\\.LatLng\\(([0-9\\.-]*),[ ]?([0-9\\.-]*)\\)", 1, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_next_regex_match(html, 0, "new google\\.maps\\.LatLng\\(([0-9\\.-]*),[ ]?([0-9\\.-]*)\\)", 2, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, "<h1 class=\"title\">([^\<]*)</h1>", 1, KDONOTNOTIFYERROR);
		mls.Videos = get_unique_regex_match(html, "<iframe src=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, "<div id=\"body-content\">(.+?)</div>", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListingDescription)) {
			if (mls.ListingDescription.includes("For further information")) {
				var index = mls.ListingDescription.indexOf("For further information");
				mls.ListingDescription = mls.ListingDescription.substr(0, index);
			}
		}
		list_price = get_unique_regex_match(html, "<span class=\"price2\"><span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			if (list_price == "UNDER OFFER" || list_price == "Under Offer" || list_price == "Rented" || list_price == "SOLD" || list_price.includes("Price")) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
			if (list_price.includes("/")) {
				var period = list_price.split('/')[1];
				if (isDefined(period)) {
					if (period == "weekly") {
						freq = "week";
						mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
					}
					if (period == "month") {
						freq = "month";
						mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
					}
				}
			}
		}
		mls.ListPrice.value = get_unique_regex_match(html, "<span class=\"price2\"><span>([a-zA-Z]* [\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<span class=\"price2\"><span>([a-zA-Z]* [\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, "<span class=\"ruler-icon\">([0-9\.]*)[\t\s ]*?([^\<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<span class=\"ruler-icon\">([0-9\.]*)[\t\s ]*?([^\<]*)</span>", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<span class=\"bath-icon\">([0-9\.\s]*)[\s\t]*?[a-zA-Z]*</span>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<span class=\"bed-icon\">([0-9\.\s]*)[\s\t]*?[a-zA-Z]*</span>", 1, KDONOTNOTIFYERROR);
		mls.ExteriorTypes = get_unique_regex_match(html, "<span class=\"garage-icon\">([^\<]*)</span>", 1, KDONOTNOTIFYERROR);
		if (mls.ExteriorTypes == "undefined") {
			mls.ExteriorTypes = [];
		}
		mls.CoolingSystems = get_unique_regex_match(html, "<span class=\"ac-icon\">([^\<]*)</span>", 1, KDONOTNOTIFYERROR);
		if (mls.Address.Country == undefined)
			mls.Address.Country = {};
		mls.Address.Country.value = "Antigua";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a href=\"([^\"]*)\" rel=\"gallery\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=5", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=9", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=8", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_search_cat%5B%5D=19", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_search_cat%5B%5D=18", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=7", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Others/sale
		{
			try {
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=25&absc_search_cat%5B%5D=23&absc_search_cat%5B%5D=29&absc_search_cat%5B%5D=10&absc_search_cat%5B%5D=61",
						KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=4", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				cumulatedCount += crawlCategory("https://stanleysestates.com/?s=&absc_mode=and&absc_search_cat%5B%5D=3&absc_search_cat%5B%5D=8", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
