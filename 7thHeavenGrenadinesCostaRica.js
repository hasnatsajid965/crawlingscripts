var countryContext = "Costa Rica";

include("base/7thHeavenGrenadinesFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/condos-for-sale/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/beachfront-condos-for-sale/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/homes-for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/luxury/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/beachfront-homes-for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/land-for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Building
		{
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/hotels-for-sale/", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/costa-rica/farms-for-sale/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
