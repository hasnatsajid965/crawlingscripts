qa_override("[E4063587116]", "Some of the properties contains description and some does not");
qa_override("[E1473228885]", "Some properties have lotsize and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[@class='CoveoResultLink teaser-card-container'])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					// el1 = "https://clasificados.com.do/"+el1;
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			index1 = 2;
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//a[@title='Next']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement) && verMasButtonElement !== null) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KDONOTNOTIFYERROR)) {
						loadedMore = false;
						break;
					} else {
						return actualCount;
					}
				} else {
					return actualCount;
				}
				if (count != 2) {
					wait(1000);
				}
				index1++;
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="js-property-details__info-description property-details__info-description">(.+?)</div>', 1, KDONOTNOTIFYERROR);

		mls.Location.Latitude = get_unique_regex_match(html, 'initGoogleMap[\(]([^\,]*), ([^\,]*), \'/content/site/img/map/mappin.png\', [^\,]*, [^\;]*[\)];', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'initGoogleMap[\(]([^\,]*), ([^\,]*), \'/content/site/img/map/mappin.png\', [^\,]*, [^\;]*[\)];', 2, KDONOTNOTIFYERROR);

		for_rent = get_unique_regex_match(html, "<li class=\"enhanced\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		// if (isDefined(for_rent)) {
		// if (for_rent.includes('Lease')) {
		// mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
		// }
		// }
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"property-details__info-amount\">([\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		checkUnits = get_next_regex_match(html, 0, "<span class=\"property-details__info-amount\">([\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR)
				+ get_next_regex_match(html, 0, "<span class=\"property-details__info-currency\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (checkUnits !== "undefinedundefined") {
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"property-details__info-amount\">([\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR)
					+ get_next_regex_match(html, 0, "<span class=\"property-details__info-currency\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img data-object-fit="contain" data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www2.colliers.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"property-heading\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Land Area:([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Land Area:([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, 'var listData = [^\"]* "json": [\\[](.+?)[\\]] [^\;]*;', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/['"]+/g, '');
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "AC") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.Address.Country.value = "USA";
		mls.Location.Directions = get_unique_regex_match(html, "<h2 class=\"property-address\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			addressValue = mls.Location.Directions.split(",");
			if (isDefined(addressValue[0])) {
				mls.Address.City.value = addressValue[0];
			}
			if (isDefined(addressValue[1])) {
				mls.Address.StateOrProvince.value = addressValue[1];
			}
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<h3 class=\"expert-card__name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<a href=\"tel:([^\"]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType = get_unique_regex_match(html, "\"Property Types\": \"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Land]&f:recenttransactions=[0]", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Office]&f:recenttransactions=[0]", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Industrial]&f:recenttransactions=[0]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Commercial-Specialty]&f:recenttransactions=[0]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Multifamily]&f:recenttransactions=[0]", KMULTIFAMILIAVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Healthcare-Medical]&f:recenttransactions=[0]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Residential]&f:recenttransactions=[0]", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Hotel]&f:recenttransactions=[0]", KEDIFICIOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Institutional]&f:recenttransactions=[0]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Retail]&f:recenttransactions=[0]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#sort=relevancy&f:listingtype=[For%20Sale]&f:propertytype=[Technical]&f:recenttransactions=[0]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Technical]&f:recenttransactions=[0]", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Retail]&f:recenttransactions=[0]", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Industrial]&f:recenttransactions=[0]", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Commercial-Specialty]&f:recenttransactions=[0]", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Healthcare-Medical]&f:recenttransactions=[0]", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Institutional]&f:recenttransactions=[0]", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Land]&f:recenttransactions=[0]", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Multifamily]&f:recenttransactions=[0]", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Residential]&f:recenttransactions=[0]", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Office]&f:recenttransactions=[0]", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www2.colliers.com/en/Properties#f:listingtype=[For%20Lease]&f:propertytype=[Hotel]&f:recenttransactions=[0]", KEDIFICIOSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
