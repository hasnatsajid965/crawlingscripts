qa_override("[E2431535312]", "Some properties does not contain description.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a title="Details" class="small button" href="([^\"]*)">Details', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="arrow"><a href="([^\"]*)">&gt;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_unique_regex_match(html, '<div class="small-12 medium-7 columns">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, 'Cod.([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="prezzo">([0-9\,\. ]*)([a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="prezzo">([0-9\,\. ]*)([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		getCity = mls.ListingTitle.split(',');
		if (isDefined(getCity[2])) {
			mls.Address.City.value = getCity[2];
		}
		mls.LotSize.value = get_unique_regex_match(html, '<strong>Area:</strong>([0-9\.\, ]*)&nbsp;', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<strong>Area:</strong>[0-9\.\, ]*&nbsp;([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, '<strong>Floor:</strong>([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '</strong>([^\<]*)Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'class="listing-info__value">([^\<]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Full Baths', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<p class="translate">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Realigro Real Estate Limited";

		mls.office.PhoneNumber = "+44 0208 036 5029";
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		propertyCheck = get_unique_regex_match(html, "<div class=\"specifiche\">[\s\t\n ]*?<h4>([^\-]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(propertyCheck)) {
			if (propertyCheck.includes("rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.Address.Country.value = "Argentina";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a class="rsImg" data-rsh="[^\"]*" data-rsbigimg="([^\"]*)"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}

}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-sale/apartment/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-rent/apartment/", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Hotels en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-sale/hotel/", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Hotels en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-rent/hotel/", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// shop en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-sale/office/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// shop en rent
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-rent/office/", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-sale/traditionalfolk-home/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-rent/traditionalfolk-home/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}

		}
		// Terrence
		{
			try {
				// Terrence en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-sale/farmland/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrence en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-rent/farmland/", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrence en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-sale/development-property/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrence en venta
				cumulatedCount += crawlCategory("https://argentina.realigro.com/for-rent/development-property/", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}

		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
