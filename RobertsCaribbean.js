qa_override("[E4063587116]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'href="([^"]*)" class="view-profile"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="next page-numbers" href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var Main_Title = "";
		Main_Title = get_unique_regex_match(html, '<h2 class="ptitle">([^<]*)', 1, KNOTIFYERROR);
		if (isDefined(Main_Title)) {
			Main_Title = Main_Title.split("|");
			if (Main_Title[0] !== "") {
				mls.ListingTitle = Main_Title[0];
			}
		}
		mls.LivingArea.value = get_unique_regex_match(html, "Floor Area:</strong>[\s\t\n ]*?([0-9\,\. ]+)([^\&]*)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "Floor Area:</strong>[\s\t\n ]*?([0-9\,\. ]+)([^\&]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Area:</strong>[\s\t\n ]*?([0-9\,\. ]+)([^\&]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Area:</strong>[\s\t\n ]*?([0-9\,\. ]+)([^\&]*)", 2, KDONOTNOTIFYERROR);
		mls.FloorCoverings = get_unique_regex_match(html, "Stories:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "Garage:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="property-info-agent clear">(.+?)</div>[\\s\\t\\n ]*?<p>(.+?)</section>', 2, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, 'Amenities[\\s\\t\\n ]*?</h4>[\\s\\t\\n ]*?<a href="[^"]*" rel="tag">(.+?)<h4>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "AC") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="agent-desc">[\\s\\t\\n ]*?<h4>[\\s\\t\\n ]*?<a href="[^"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "Mobile:[\\s\\t\\n ]*?</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '</i><a href="mailto:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, "Tel no:[\\s\\t\\n ]*?</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<div class="property-page-id">[\\s\\t\\n ]*?Property ID :[\\s\\t\\n ]*?<span>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<div class="inside"><h2 class="ptitle">[^\<]* ([\$])([0-9\.\,]*) [a-zA-Z]*', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<div class="inside"><h2 class="ptitle">[^\<]* ([\$])[0-9\.\,]* [a-zA-Z]*', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, '<div class="inside"><h2 class="ptitle">[^\<]* [\$][0-9\.\,]* ([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		for_rent = get_unique_regex_match(html, '<div class="property-page-status">[\s\t\n ]*?<span>[\s\t\n ]*?<a href="[^\"]*" rel="tag">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "For Rent") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			} else if (mls.ListPrice.value < 99000) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			} else if (for_rent == "Rented") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		Property_Type = get_unique_regex_match(html, '<div class="property-page-type">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(Property_Type)) {
			mls.PropertyType.value = Property_Type.replace(/(<([^>]+)>)/gi, "");
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "lat:([^,]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "lng:([^,]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, '<h4 class="subtitle"><label>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			mls.Address.Country.value = "Bahamas";
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 1].trim();
				}
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a href="([^"]*)" rel="gallery" title="View Fullscreen">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.robertscaribbean.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// multifamily en rental
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/long-term-rentals-grenada/", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// multifamily en rental
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/sgu-rentals/", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/single-family-homes/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/foreclosure/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/beachfronts-waterfront-homes/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// temporal en venta
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/vacation-rentals-grenada/", KCASASTEMPORAL);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.robertscaribbean.com/property-type/single-family-homes-find-properties-for-rent-in-grenada/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
