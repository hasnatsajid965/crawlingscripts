qa_override("[E1473228885]", "Some properties does not contain lotsize.");
qa_override("[E219240453]", "I have implemented frequency condition on the basis of the description.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//div[@class='span-title']/a)[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//a[text()='Siguiente']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KDONOTNOTIFYERROR)) {
						loadedMore = false;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.LotSize.value = get_unique_regex_match(html, '<img src="/App_Theme/css/img/ico_area.png" width="36" height="36" class="imgvertical" />[\s\t\n ]*?([0-9\, ]+)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<img src="/App_Theme/css/img/ico_area.png" width="36" height="36" class="imgvertical" />[\s\t\n ]*?[0-9\, ]+([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<img src="/App_Theme/css/img/ico_bed.png" class="imgvertical" />([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = mls.Bedrooms.replace(/[^0-9]/g, "");
		mls.Bathrooms = get_unique_regex_match(html, '<img src="/App_Theme/css/img/ico_bath.png" width="36" height="36" class="imgvertical">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = mls.Bathrooms.replace(/[^0-9]/g, "");
		mls.NumParkingSpaces = get_unique_regex_match(html, '<img src="/App_Theme/css/img/ico_garaje.png" width="36" class="imgvertical" height="36">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = mls.NumParkingSpaces.replace(/[^0-9]/g, "");
		// forRent = get_next_regex_match(html, 0, '<div
		// class="price-operation">([^\<]*)', 1, KDONOTNOTIFYERROR);
		// if(isDefined(forRent)){
		// if(forRent.includes("Renta")){
		// mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
		// }
		// }
		var features = get_all_regex_matched(html, '<li>([^\<]*)</li><li>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<h2 class="description">Descripci[^\<]*n <span>[^\<]*<b>[^\<]*</b></span></h2>(.+?)<div', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, "<a href=\"[^\"]*\">[\s\t\n ]*?<img src=\"[^\"]*\" /></a>[\s\t\n ]*?</p>[\s\t\n ]*?<p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, 'C[^\:]*digo Fincaraiz.com.co: <b>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 style="margin-bottom: 0px; line-height: 95%;">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.ListingTitle.includes("Arriendo")) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="price">[\s\t\n ]*?<h2>([ \$ ]*)([0-9\.\, ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<div class="price">[\s\t\n ]*?<h2>([ \$ ]*)([0-9\.\, ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="price">[\s\t\n ]*?<h2><span>[^\<]*</span><label>([ \$ ]*)([0-9\.\,]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<div class="price">[\s\t\n ]*?<h2><span>[^\<]*</span><label>([ \$ ]*)([0-9\.\,]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var dVideo = get_unique_regex_match(html, "onclick=\"PopupVideo[^\;]*;([^\;]*)&#39;[^\"]*\"><i class=\"fa fa-play-circle-o\">", 1, KDONOTNOTIFYERROR);
		if ((dVideo != undefined) && (dVideo != "undefined")) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = "https://youtu.be/" + dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "\"Latitude\" : \"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "\"Longitude\" : \"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Colombia";
		cityValue = get_next_regex_match(html, 0, "<span class=\"Breadcrumbstext\">Est[^\:]* en:</span>(.+?)<span class=\"advertBreadcrumb\">", 1, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			cityValue = cityValue.replace(/(<([^>]+)>)/ig, "");
			cityValue = cityValue.replace(/\s+/g, ' ').trim();
			cityValue = cityValue.split("/");
			if (isDefined(cityValue[2])) {
				mls.Address.City.value = cityValue[2].trim();
			}
		}
		var imageCount = 0;
		var images;
		images = get_next_regex_match(html, 0, "var lsPhotos = [^\']*(.+?),\'#ClickToCall#\'", 1, KDONOTNOTIFYERROR);
		if (isDefined(images)) {
			images = images.replace("'", "").trim();
			images = images.split(",");
		}
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag.replace("\u0027", "").trim();
				} else {
					obj.MediaURL = "https://www.fincaraiz.com.co" + oneImageTag.replace("\u0027", "").trim();
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/apartamentos/venta/?ad=30|1||||1||8||||||||||||||||||||1|||1||griddate%20desc||||-1||", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/casas/venta/?ad=30|1||||1||9||||||||||||||||||||1|||1||griddate%20desc||||-1||", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/lotes/venta/?ad=30|1||||1||2||||||||||||||||||||1|||1||griddate%20desc||||-1||", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/fincas/venta/?ad=30|1||||1||7||||||||||||||||||||1|||1||griddate%20desc||||-1||", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/casas-campestres/venta/?ad=30|1||||1||21||||||||||||||||||||1|||1||griddate%20desc||||-1||", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/oficinas/venta/?ad=30|1||||1||4||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/locales/venta/?ad=30|1||||1||3||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/bodegas/venta/?ad=30|1||||1||5||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/apartaestudios/venta/?ad=30|1||||1||22||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/edificios/venta/?ad=30|1||||1||19||||||||||||||||||||1|||1||griddate%20desc||||-1||", KEDIFICIOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/casa-lotes/venta/?ad=30|1||||1||23||||||||||||||||||||1|||1||griddate%20desc||||-1||", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/consultorios/venta/?ad=30|1||||1||18||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/caba%C3%B1as/venta/?ad=30|1||||1||20||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/habitaciones/venta/?ad=30|1||||1||10||||||||||||||||||||1|||1||griddate%20desc||||-1||", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/apartamentos/arriendo/?ad=30|1||||2||8||||||||||||||||||||1|||1||griddate%20desc||||-1||", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/locales/arriendo/?ad=30|1||||2||3||||||||||||||||||||1|||1||griddate%20desc||||-1||", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/oficinas/arriendo/?ad=30|1||||2||4||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/casas/arriendo/?ad=30|1||||2||9||||||||||||||||||||1|||1||griddate%20desc||||-1||", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/bodegas/arriendo/?ad=30|1||||2||5||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/apartaestudios/arriendo/?ad=30|1||||2||22||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/habitaciones/arriendo/?ad=30|1||||2||10||||||||||||||||||||1|||1||griddate%20desc||||-1||", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/consultorios/arriendo/?ad=30|1||||2||18||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/edificios/arriendo/?ad=30|1||||2||19||||||||||||||||||||1|||1||griddate%20desc||||-1||", KEDIFICIOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/lotes/arriendo/?ad=30|1||||2||2||||||||||||||||||||1|||1||griddate%20desc||||-1||", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/casas-campestres/arriendo/?ad=30|1||||2||21||||||||||||||||||||1|||1||griddate%20desc||||-1||", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/fincas/arriendo/?ad=30|1||||2||7||||||||||||||||||||1|||1||griddate%20desc||||-1||", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/casa-lotes/arriendo/?ad=30|1||||2||23||||||||||||||||||||1|||1||griddate%20desc||||-1||", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/parqueaderos/arriendo/?ad=30|1||||2||24||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.fincaraiz.com.co/caba%C3%B1as/arriendo/?ad=30|1||||2||20||||||||||||||||||||1|||1||griddate%20desc||||-1||", KOTROSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
