

// crawlForPublications crawl-mode: Regular-Expression Crawling

function crawlCategory(url, category) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	if (isDefined(html)) {
		var expectedCount = get_unique_regex_match(html, "<div class=\"quantity-results\">[ \\t\\r\\n]*(.*) resultado[s]?[ \\t\\r\\n]*</div>", 1, KNOTIFYERROR);
		var actualCount;
		var page = 1;
		while ((isDefined(html)) && (actualCount = addAllToPropertiesList(html, "<a[ \\t\\r\\n]*href=\"([^\"]*)\"[ \\t\\r\\n]*class=\"item__info-link *\">", category, 1, 1)) > 0) {
			cumulatedCount += actualCount;
			print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + "/" + expectedCount + ") ----");
			if (passedMaxPublications()) {
				break;
			}
			expectedCount = get_unique_regex_match(html, "<div class=\"quantity-results\">[ \\t\\r\\n]*(.*) resultado[s]?[ \\t\\r\\n]*</div>", 1, KNOTIFYERROR).replace(",", "");
			if (cumulatedCount >= expectedCount) {
				break;
			}
			try {
				html = gatherContent_url(get_unique_regex_match(html,
						"<li[ \\t\\r\\n]*class=\"andes-pagination__button andes-pagination__button--next *\">[ \\t\\r\\n]*<a[ \\t\\r\\n]*class=\"andes-pagination__link prefetch\"[ \\t\\r\\n]*href=\"([^\"]*)\">", 1, cumulatedCount != expectedCount), KDONOTNOTIFYERROR);
				page++;
			} catch (e) {
				if (cumulatedCount >= expectedCount) {
					break;
				} else {
					throw (e);
				}
			}
		}
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html == "null" || html == null) {
		return analyzeOnePublication_return_unreachable;
	}
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		var dVideo = get_unique_regex_match(html, "'videoId': '([^']*)'", 1, KDONOTNOTIFYERROR);
		if (isDefined(dVideo)) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = "https://youtu.be/" + dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		if ((isDefined(get_next_regex_match(html, 0, "<dt>Ordenar publicaciones</dt>", 0, KDONOTNOTIFYERROR))) || (isDefined(get_next_regex_match(html, 0, "Parece que la p?gina no existe", 0, KDONOTNOTIFYERROR)))) {
			return analyzeOnePublication_return_innactive;
		}
		listFrequency = get_unique_regex_match(html, "<p class=\"card-title\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(listFrequency)) {
			if (listFrequency.includes("Alquila")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.ListPrice.value = get_unique_regex_match(html, "<span class=\"price-tag-fraction\">([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		if (mls.ListPrice.value < 100) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<span class=\"price-tag-symbol\" content=\"([^\"]*)\">([^<]*)</span>", 2, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_innactive;
		}
		if (isDefined(mls.ListingTitle)) {
			if (mls.ListingTitle.endsWith(" en Mercado Libre")) {
				mls.ListingTitle = mls.ListingTitle.substring(0, mls.ListingTitle.length - " en Mercado Libre".length);
			}
		}
		images = get_all_regex_matched(html, '<a href="([^\"]*)" class="gallery-trigger gallery-item--[^\"]*"', 1);
		var imageCount = 0;
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag.replace(".webp", ".jpg")
				if (obj.MediaOrderNumber == undefined) {
					obj.MediaOrderNumber = imageCount;
				}
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		mls.ListingDescription = get_unique_regex_match(html, "<h2 class=\"main-section__title item-description__title\">[^\<]*</h2>(.+?)</div>", 1, KDONOTNOTIFYERROR);
		if (images == "" || isUndefined(images)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, "<strong>Superficie total</strong>[^<]*<span>[^0-9]*([0-9]*)([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>Superficie total</strong>[^<]*<span>[^0-9]*([0-9]*)([^<]*)</span>", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Superficie construida</strong>[^<]*<span>[^0-9]*([0-9]*)([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Superficie construida</strong>[^<]*<span>[^0-9]*([0-9]*)([^<]*)</span>", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Dormitorios</strong>[^<]*<span>([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Ba?os</strong>[^<]*<span>([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<strong>Parqueo</strong>[^<]*<span>([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, "<strong>Niveles</strong>[^<]*<span>([^<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<p class=\"card-description card-description--bold\">[^<]*<span>([^<]*)</span>", 1, KNOTIFYERROR);
		var phoneNum = get_all_regex_matched(html, "<span class=\"profile-info-phone-value\">([^<]*)</span>", 1);
		if (isDefined(phoneNum)) {
			phoneCount = 1;
			mls.NonMLSListingData.otherListingBrokerInfo = {};
			phoneNum.forEach(function(phones) {
				// print("counter...."+phoneCount);
				if (phoneCount == 1) {
					mls.Brokerage.Phone = phones;
				}
				if (phoneCount == 2) {
					mls.NonMLSListingData.otherListingBrokerInfo.phone = phones;
				}
				phoneCount++;
			});
		}
		mls.Location.Directions = arrayElement(extract_through_DOM_class_selector(html, "map-address", KDOMSELECTOR_INNERTEXT, KDONOTNOTIFYERROR), 0);
		var selectors = extract_through_DOM_class_selector(html, "ui-dropdown", KDOMSELECTOR_INNERHTML, KNOTIFYERROR);
		selectors.forEach(function(element) {
			if (element.includes("<span>Ambientes</span>")) {
				var elements = extract_through_DOM_tagname_selector(element, "li", KDOMSELECTOR_INNERTEXT, KDONOTNOTIFYERROR);
				elements.forEach(function(onetag) {
					var obj = JSON.parse(get_list_empty_variable("room"));
					obj.value = onetag;
					if (mls.DetailedCharacteristics.Rooms.room == undefined) {
						mls.DetailedCharacteristics.Rooms.room = [];
					}
					mls.DetailedCharacteristics.Rooms.room.push(obj);
				});
			} else if ((element.includes("<span>Comodidades y amenities</span>")) || (element.match(".*<span>Caracter.*sticas adicionales</span>.*"))) {
				var elements = extract_through_DOM_tagname_selector(element, "li", KDOMSELECTOR_INNERTEXT, KDONOTNOTIFYERROR);
				elements.forEach(function(onetag) {
					var obj = JSON.parse(get_list_empty_variable("appliance"));
					obj.value = onetag;
					if (mls.DetailedCharacteristics.Appliances.appliance == undefined) {
						mls.DetailedCharacteristics.Appliances.appliance = [];
					}
					mls.DetailedCharacteristics.Appliances.appliance.push(obj);
				});
			}
		});
		mls.Address.Country.value = "Dominican Republic";
		var locationData = get_unique_regex_match(html, "<h3 class=\"map-location\">([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(locationData)) {
			locationData = locationData.split(",");
			if (isDefined(locationData[0])) {
				mls.Address.StateOrProvince.value = locationData[0];
			}
			if (isDefined(locationData[1])) {
				mls.Address.City.value = locationData[1];
			}
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Fincas
		{
			try {
				// Fincas en venta
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/fincas/venta/_PublishedToday_YES", KFINCASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Fincas en alquiler
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/fincas/alquiler/", KFINCASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Fincas vacacionales
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/fincas/alquiler-tempora/", KFINCASTEMPORAL);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/apartamentos/venta/_PublishedToday_YES", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/apartamentos/alquiler/_PublishedToday_YES", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos vacacionales
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/apartamentos/alquiler-tempora/_PublishedToday_YES", KAPTOTEMPORAL);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/casas/venta/_PublishedToday_YES", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/casas/alquiler/_PublishedToday_YES", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas vacacionales
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/casas/alquiler-tempora/_PublishedToday_YES", KCASASTEMPORAL);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/locales-tiendas/venta/_PublishedToday_YES", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Locales - Tiendas en alquiler
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/locales-tiendas/alquiler/_PublishedToday_YES", KTIENDASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/oficinas/venta/_PublishedToday_YES", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/oficinas/alquiler/_PublishedToday_YES", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/terrenos/venta/_PublishedToday_YES", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://inmuebles.mercadolibre.com.do/terrenos/alquiler/", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
