var countryContext = "Croatia";

include("base/Century21GlobalFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Croatia?subtype=apartment", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Croatia?subtype=House", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Croatia?subtype=Townhouse", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Croatia?subtype=75", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// other
		{
			try {
				// other en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Croatia?subtype=9", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
