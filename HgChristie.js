qa_override("[E1473228885]", "Some of the properties have lot size and some does not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<link itemprop="mainEntityOfPage" href="([^"]*)"', category, "https://www.hgchristie.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a href="([^"]*)" onclick="[^"]*" class="paging__item  paging__item--next  js-at-icon-paging  js-paging-next" aria-label="Next Page">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.hgchristie.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		if (isDefined(get_unique_regex_match(html, ">The page you are looking for could not be found</h2>", 0, KDONOTNOTIFYERROR))) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="main-address">([^<]*)</div>', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'class="listing-info__value">([0-9 ]*)([a-zA-Z ]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n]*?Exterior', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'class="listing-info__value">([0-9 ]*)([a-zA-Z ]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n]*?Exterior', 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(
				html,
				'<li class="grid__item  default--1-5  lap-wide--1-4 lap--1-3  palm-wide--1-2  palm--1-1"><span class="icon icon-checked  prop-description__amenities-list-item-icon"><!----></span><span class="prop-description__amenities-list-item-text" itemprop="value">([^<]*)',
				1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="p">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '"url  contact-info__agent-name" onclick="DoReport[(]rd, \'RE.Listing.ClickThroughAgentDetail\'[)]" href="[^"]*" itemprop="url"><span itemprop="name">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'Phone: </span><a class="o-phone-number  tel  phone_block  phone-num-1  icon-text  js-phone-link" href="tel:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[s\t\n ]*?MLS ID', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, 'class="price__value  u-ignore-phonenumber">([\$]*)([0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, 'class="price__value  u-ignore-phonenumber">([\$]*)([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.PropertyType.value = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">Property Type', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[s\t\n ]*?Full Baths', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<dd itemprop="value" class="listing-info__value">([0-9\,\. ]*)[\s\t\n ]*?([a-zA-Z\.\, ]*)[\s\t\n ]*?</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Interior', 1,
				KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<dd itemprop="value" class="listing-info__value">([0-9\,\. ]*)[\s\t\n ]*?([a-zA-Z\.\, ]*)[\s\t\n ]*?</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Interior', 2,
				KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'detailMap",Latitude:"([^"]*)",Longitude:"([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'detailMap",Latitude:"([^"]*)",Longitude:"([^"]*)"', 2, KDONOTNOTIFYERROR);
		full_address = get_next_regex_match(html, 0, '<div class="c-address">(.+?)</span></div>', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = full_address.replace(/(<([^>]+)>)/gi, "");
		if (isDefined(mls.Location.Directions)) {
			var tokens = mls.Location.Directions.split(",");
			if (tokens.length > 1) {
				mls.Address.City.value = tokens[tokens.length - 2].trim();
			}
			if (tokens.length > 0) {
				mls.Address.Country.value = tokens[tokens.length - 1].trim();
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<noscript><img src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (oneImageTag !== "/siteresources/my folder/hgc_cirecombined-white.png") {
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			}
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// multifamily/Buildings en venta
				cumulatedCount += crawlCategory("https://www.hgchristie.com/eng/sales/multi-family-type", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.hgchristie.com/eng/sales/co-op-condo-type", KNUEVASCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.hgchristie.com/eng/sales/single-family-home-type", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.hgchristie.com/eng/sales/land-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.hgchristie.com/eng/sales/commercial-type", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
