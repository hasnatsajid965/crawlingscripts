qa_override("[E1473228885]", "Some properties have lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="columnmore">[s\t\n ]*?<a href="([^"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a class="next page-numbers" href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setCountryPhoneCode("599");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var list_not_available = get_unique_regex_match(html, '<h1 class=\"entry-title\">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available.includes("Not")) {
				return analyzeOnePublication_return_unreachable;
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="left-inner">[\s\t\n ]*?<h1>([^<]*)</h1>', 1, KNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li>([^\<]*)</li>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air Condition") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
			});
		}
		mls.ViewTypes = get_unique_regex_match(html, '<b>Prop.View</b>:([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<p itemprop="description" id="corepropertydescription">(.*)(?=</p></div>)', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, "</div>[\ts\n ]*?<h3>([^<]*)</h3>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<span>Mobile</span>[\t\s\n ]*:([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '<li class="email"><a href="">([^<]*)</a>', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, "<span>Office</span>[\ts\n ]*:([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "<div>MLS#[ ]*?<span>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "itemprop=\"price\"/>([a-zA-Z\$ ]*)<span>([0-9\,]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "itemprop=\"price\"/>([a-zA-Z\$ ]*)<span>([0-9\,]*)", 1, KDONOTNOTIFYERROR);
		currency_period = get_unique_regex_match(html, "<span class='per'>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(currency_period)) {
			if (currency_period.includes("/")) {
				if (currency_period == "/Mth") {
					currency_period = "month";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(currency_period);
				}
				if (currency_period == "/Wk") {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("week");
				}
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<li><b>Prop.Type</b>:([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<b>Land Area</b>:([ 0-9\, ]*)([a-zA-Z\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<b>Land Area</b>:([ 0-9\, ]*)([a-zA-Z\.]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<b>Living Space</b>:([ 0-9\, ]*)([a-zA-Z\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<b>Living Space</b>:([ 0-9\, ]*)([a-zA-Z\.]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<b>Beds</b>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<b>Baths</b>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "<b>Year Built</b>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, 'itemprop="addressRegion">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, 'itemprop="addressCountry">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-large-src="([^"]*)"[s\t ]*?/>', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "https://c21grenada.com" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/single-family-homes-for-sale-grenada/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/exclusive-estates-1-million-properties-grenada/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/luxury-properties-grenada/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/exclusive-estates-2-million-properties-grenada/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/exclusive-estates-3-million-properties-grenada/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/water-front-properties-grenada/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/single-family-homes-for-rent-grenada/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/condos-apartments-for-rent-grenada/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/university-rentals-SGU/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/vacation-rentals/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Stores
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/commercial-properties-for-sale/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/condos-apartments-for-sale-grenada/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/residential-land-for-sale-grenada/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/beach-water-front-land-grenada/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://c21grenada.com/en/grenada-real-estate/agricultural-farm-land-grenada/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
