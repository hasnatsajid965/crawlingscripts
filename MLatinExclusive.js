// qa_override("[E1473228885]", "Some of the properties contain lot size and some does not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in MLatinExclusive.js ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	var check = [];
	i = 1;
	wait(20000);
	while (true) {
		var loadedMore = false;
		var el1;
		var index = 1;
		var resArray = [];
		do {
			el1 = virtual_browser_find_one(browser, "(//h1[@class='title']/a[1])[" + index + "]", KDONOTNOTIFYERROR);
			if (index > 238) {
				return actualCount;
			}
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			check[index] = el1;
			if (check[index] == check[index - 1]) {
				return actualCount;
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			// print("check array.."+resArray[element]);
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		for (var count = 0; count <= 2; count++) {
			virtual_browser_sendKeys(browser, "\\\uE010");
			element = virtual_browser_find_one(browser, "//a[@class='btn property__loading']", KDONOTNOTIFYERROR);
			if (isDefined(element) || element !== null) {
				loadedMore = true;
				break;
			} else {
				loadedMore = false;
			}

		}
		if (!loadedMore) {
			return actualCount;
		}

		i++;
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Land size</td>[\s\t\n ]*?<td class="bc">([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Land size</td>[\s\t\n ]*?<td class="bc">[0-9\,\. ]*([a-zA-Z]*)<sup>([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, 'Land size</td>[\s\t\n ]*?<td class="bc">[0-9\,\. ]*([a-zA-Z]*)<sup>([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, 'Property floor</td>[\s\t\n ]*?<td class="bc">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Parking spaces</td>[\s\t\n ]*?<td class="bc">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms</td>[\s\t\n ]*?<td class="bc">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms</td>[\s\t\n ]*?<td class="bc">([^<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li class="available[^\"]*?">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}

		mls.ListingDescription = get_next_regex_match(html, 0, '<article class="property__content js-wp" id="description">(.+?)<table class="property__rates property__rates__sale">', 1, KDONOTNOTIFYERROR);
		if (mls.ListingDescription.includes("Rates")) {
			var index = mls.ListingDescription.indexOf("Rates"); // Gets the
																	// first
																	// index
																	// where a
																	// space
																	// occours
			mls.ListingDescription = mls.ListingDescription.substr(0, index);
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Phone = "+1 917 652 0027";
		mls.Brokerage.Name = "m.latinexclusive";
		mls.MlsId = get_unique_regex_match(html, 'Property ID:</span>[\s\t\n ]*?<span class="property-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 id="pageTitle">([^\<]*)', 1, KNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<p class="price">Price <strong>([a-zA-Z\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p class="price">Price <strong>([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_unique_regex_match(html, '<p class="price">Monthly rental <strong>([a-zA-Z\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p class="price">Monthly rental <strong>([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		list_price = get_unique_regex_match(html, '<p class="price">Monthly rental <strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, '"latLng":[^\"]*"([^\"]*)","([^\"]*)"[^\,]*,', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"latLng":[^\"]*"([^\"]*)","([^\"]*)"[^\,]*,', 2, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Colombia";
		if (isDefined(mls.ListingTitle)) {
			if (mls.ListingTitle.includes("in"))
				getCity = mls.ListingTitle.split("in");
		}
		if (mls.ListingTitle.includes("en")) {
			getCity = mls.ListingTitle.split("en");
		}
		if (isDefined(getCity[1])) {
			mls.Address.City.value = getCity[1].trim();

		}

		var imageCount = 0;
		var images;

		images = get_all_regex_matched(html, 'data-large="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "http://m.latinexclusive.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in MLatinExclusive.js for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/properties-for-sale-in-colombia?area=&rooms=&price=&building_type=apartment", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/properties-for-sale-in-colombia?area=&rooms=&price=&building_type=commercial", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/properties-for-sale-in-colombia?area=&rooms=&price=&building_type=house", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/properties-for-sale-in-colombia?area=&rooms=&price=&building_type=island", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/properties-for-sale-in-colombia?area=&rooms=&price=&building_type=land", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/properties-for-sale-in-colombia?area=&rooms=&price=&building_type=penthouse", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/long-term-rentals/colombia?area=&rooms=&price=&building_type=apartment", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/long-term-rentals/colombia?area=&rooms=&price=&building_type=commercial", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/long-term-rentals/colombia?area=&rooms=&price=&building_type=house", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "http://m.latinexclusive.com/en/long-term-rentals/colombia?area=&rooms=&price=&building_type=penthouse", KCASASALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in MLatinExclusive.js required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
