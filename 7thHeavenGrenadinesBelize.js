var countryContext = "Belize";

include("base/7thHeavenGrenadinesFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION)
			.toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{

			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/condos-for-sale/",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/beachfront-condos-for-sale/",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}

		}
		// Casas
		{

			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/luxury/",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/homes-for-sale/",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/beachfront-homes-for-sale/",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{

			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/land-for-sale/",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/islands-for-sale/",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Building
		{
			try {
				// Building en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/hotels-for-sale/",
						KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory(
						"https://www.7thheavenproperties.com/real-estate/belize/farms-for-sale/",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required "
				+ formattedTime(new Date().getTime() - startTime)
				+ " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
