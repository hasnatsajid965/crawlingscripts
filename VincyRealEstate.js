

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" class=\"property_mark_a\">", category, "http://www.vincyrealestate.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "title=\"Next\" href=\"([^\"]*)\">Next</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://www.vincyrealestate.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		// lot_size = get_unique_regex_match(html, "Land Size:([^\\Sq]*)", 1,
		// KDONOTNOTIFYERROR);
		// if (isDefined(lot_size)) {
		// mls.LotSize.value = lot_size;
		// mls.LotSize.areaUnits = "sq.ft.";
		// }
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div id=\"currency_div\" class=\"padding0\">[\s\t\n ]*?([^\;]+);([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div id=\"currency_div\" class=\"padding0\">[\s\t\n ]*?([^\;]+);([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<span style=\"color:orange\">([^<]*)", 1, KNOTIFYERROR);
		mls.ListingTitle = mls.ListingTitle.replace(/\s+/g, " ").trim();
		mls.Bedrooms = get_unique_regex_match(html, "Bed:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bath:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Fountain, St. Vincent";
		mls.Brokerage.Phone = "(784) 431-2402";
		mls.Brokerage.Email = "admin@wylliedesign.com";
		mls.ListingDescription = get_unique_regex_match(html, '<legend><h2 class="shell">Property Details</h2></legend>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = mls.Location.Directions = get_next_regex_match(html, 0, ">([^\<]*)[\\s\\t\\n ]*?<img src=\"http://www.vincyrealestate.com/components/com_osproperty/templates/theme3/img/hit.png\">", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			mls.Address.Country.value = "Saint Vincent and the Grenadines";
			if (total_commas == "1") {
				if (tokens.length > 0) {
					mls.Address.City.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 1) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 2].trim();
				}
			}
			if (total_commas == "3") {
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 2].trim();
				}
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a class=\"propertyphotogroup propertyinfolilink\" href=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Uncategorized
		{
			try {
				// Uncategorized en venta
				cumulatedCount += crawlCategory("http://www.vincyrealestate.com/listing/properties-for-sale", KUNCATEGORIZED);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
