qa_override("[E2592408747]", "Some properties have city and some not.");
qa_override("[E1473228885]", "Some properties have lot size and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 2;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)" class="job_listing-clickbox">', category, "https://www.aurorabeachfront.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<span class="page active">[^\<]*</span>[\s\t\n ]*?<a href="([^\"]*)" class="page">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.aurorabeachfront.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="widget-title widget-title-job_listing "><span style="font-size: 21px; color:#1a7db8 !important; margin-top: -20px !important; padding-top: 0px !important;">([^\<]*)', 1,
				KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '</div></h1>(.+?)</aside>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, '<strong>Lot Size:</strong>([ 0-9\.\, ]*)([^\&]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<strong>Lot Size:</strong>([ 0-9\.\, ]*)([^\&]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, '<strong>Lot size: </strong>([0-9\.\, ]*)([0-9a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, '<strong>Lot size: </strong>([0-9\.\, ]*)([0-9a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, '<strong>Bedrooms:</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<strong>Bathrooms: </strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, 'Stories/Floors</div>[\s\t\n ]*?<div class="details-parameters-val">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'Year built</div>[\s\t\n ]*?<div class="details-parameters-val">([^<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<td style="width:33%;">?([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="job_listing-author-info" style="font-size: 14px !important;">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<img src="/images/WhatsApp.png" width="25">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = mls.Brokerage.Phone.replace(/\s+/g, ' ').trim();
		mls.Brokerage.Name = mls.Brokerage.Name.replace(/\s+/g, ' ').trim();
		mls.MlsId = get_unique_regex_match(html, '<strong>Listing #</strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = mls.MlsId.replace("|", "");
		mls.MlsId = mls.MlsId.replace(/\s+/g, ' ').trim();
		mls.ListPrice.currencyCode = get_unique_regex_match(html,
				'<h1 class="widget-title widget-title-job_listing "><span style="font-size: 21px; color:#1a7db8 !important; margin-top: -20px !important; padding-top: 0px !important;">[^\<]*</span><br>[\s\t\n ]*?([a-zA-Z \s]*[\$])([0-9\,\.]*)', 1,
				KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html,
				'<h1 class="widget-title widget-title-job_listing "><span style="font-size: 21px; color:#1a7db8 !important; margin-top: -20px !important; padding-top: 0px !important;">[^\<]*</span><br>[\s\t\n ]*?([a-zA-Z \s]*[\$])([0-9\,\.]*)', 2,
				KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>Location:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Nicaragua";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img width="800" src="([^\"]*)" data-caption=""  />', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.aurorabeachfront.com/nicaraguarealestate/condos.php", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.aurorabeachfront.com/nicaraguarealestate/houses.php", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.vacationrentalsnicaragua.com/search-result.php?rentaltype=House", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.aurorabeachfront.com/nicaraguarealestate/lots.php", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.aurorabeachfront.com/nicaraguarealestate/beachfront.php", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.aurorabeachfront.com/nicaraguarealestate/commercial.php", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
