

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="more" href="([^\"]*)">', category, "https://www.gatewaytosouthamerica.com/en/", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="pager-next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_unique_regex_match(html, '<div id="property-info">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="label">Asking: </span>([a-zA-Z ]*)([0-9\.\,]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="label">Asking: </span>([a-zA-Z ]*)([0-9\.\,]*)', 2, KDONOTNOTIFYERROR);
		if (mls.ListPrice.currencyCode == "" || isUndefined(mls.ListPrice.currencyCode)) {
			mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Asking: </span>[0-9\,\. ]*([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == ".") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, '<span class="label">Size [^\:]*: </span>([0-9\.\, ]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<span class="label">Size ([^\:]*): </span>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, 'Land Size: </span>([0-9\,\. ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, 'Land Size: </span>([0-9\,\. ]*)([^\<]*)', 2, KDONOTNOTIFYERROR);

		}
		var features = get_all_regex_matched(html, '<span class="label">([^\:]*): </span>Yes', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<p><span class="label"></span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Gateway To South America";
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.NumFloors = get_unique_regex_match(html, "Floor: </span>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "Code: </span>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Subcategory: </span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '!3d-([^\!]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '!2d([^\!]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Argentina";
		getCity = get_unique_regex_match(html, "Location:</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		getCity = getCity.split(',');
		if (isDefined(getCity[0])) {
			mls.Address.City.value = getCity[0];
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<li><img src="([^\"]*)" />', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = "https://www.gatewaytosouthamerica.com" + oneImageTag.replace("..", "");
				} else {
					obj.MediaURL = "https://www.gatewaytosouthamerica.com" + oneImageTag.replace("..", "");
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}

}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.gatewaytosouthamerica.com/en/residential-argentina.php", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}

		}

		// Land
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.gatewaytosouthamerica.com/en/farm-argentina.php", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}

		}

		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.gatewaytosouthamerica.com/en/commercial-argentina.php", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}

		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
