qa_override("[E1473228885]", "Some properties have lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling

function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<h3 style="margin:1 1 1 1"><a href=\"([^\"]*)\">', category, "https://www.viviun.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="next page-numbers" href="([^\"]*)">Next &rarr;</a>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="property-main-h1">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<b>Asking Price:</b> ([\$]*)([0-9\,\. ]*)([ a-zA-Z]*)', 3, KDONOTNOTIFYERROR)
				+ get_next_regex_match(html, 0, '<b>Asking Price:</b> ([\$]*)([0-9\,\. ]*)([ a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<b>Asking Price:</b> ([\$]*)([0-9\,\. ]*)([ a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, '<b>Asking Price:</b>([0-9\,\. ]*)([a-zA-Z\&]*)', 1, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<b>Asking Price:</b>([0-9\,\. ]*)([a-zA-Z\&]*)', 2, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || isUndefined(mls.ListPrice.currencyCode) || mls.ListPrice.currencyCode == "" || mls.ListPrice.value == 0 || mls.ListPrice.value == ".") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Area:</div>[\s\t\n ]*?<div class="property-facts-cell">([0-9\,\. ]*)([0-9a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Area:</div>[\s\t\n ]*?<div class="property-facts-cell">([0-9\,\. ]*)([0-9a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms:</div>[\s\t\n ]*?<div class="property-facts-cell">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms:</div>[\s\t\n ]*?<div class="property-facts-cell">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<p style="line-height:138%">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, 'class="property-features-div-ul-li">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<td colspan="3">[\s\t\n ]*?<b>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_next_regex_match(html, 0, 'Phones:</td>[\s\t\n ]*?<td class="ui-tablet">&nbsp;</td>[ ]*?<td nowrap>([^\&]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Category:</div>[\s\t\n ]*?<div class=\"property-facts-cell\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "Parking Spaces:</div>[\s\t\n ]*?<div class=\"property-facts-cell\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, "Stories:</div>[\s\t\n ]*?<div class=\"property-facts-cell\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "Construction Year:</div>[\s\t\n ]*?<div class=\"property-facts-cell\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Venezuela";
		mls.Address.City.value = get_next_regex_match(html, 0, "<div class=\"property-facts-cell\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<div class="property-images-div-div"><a href="([^\"]*)"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = "https://www.viviun.com" + oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();

		// appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Venezuela/Apartments/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Venezuela/Condos/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Venezuela/Villas/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Venezuela/Lots-Land/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Venezuela/Farms-Ranches/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Commercial/Venezuela/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.viviun.com/Real_Estate/Vacation/Venezuela/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
