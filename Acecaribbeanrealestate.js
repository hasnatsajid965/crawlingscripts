qa_override("[W4063587116]", "Some properties have description and some not");
qa_override("[E1473228885]", "Some properties contain lot size and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<p align="right" class="green" style="font-size: 11px; padding-top: 10px;"><a href="([^"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href='([^\"]*)' class='ditto_next_link'>Next &gt;</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<b>Land Size: </b>([0-9\,]*)([a-zA-Z\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<b>Land Size: </b>([0-9\,]*)([a-zA-Z\.]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<BR>([0-9.]*)([.|\\s]*?)<b>bedrooms", 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, "<BR>([0-9\s ]*)<b>stories", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "</b>([.|\\s]*)([0-9.]*)([.|\\s]*)<b>bathrooms", 2, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_unique_regex_match(html, '<ins>([^"]*)</td>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, '<b>Price [(]USD[)]:</b>[^\<]*<BR><BR>(.+?)</td>', 1, KDONOTNOTIFYERROR);
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<b>Price[ ]?([^<]*):</b>[ ]?[$]?([^<(]*)?([^<]*)<BR>", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<b>Price[ ]?([(]?)([^)]*)([)]*?):", 2, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "<b>Reference[ ]?#[ ]?</b>([a-zA-Z0-9]*?)<BR>", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominica";
		mls.Address.City.value = get_unique_regex_match(html, "<b>Location: </b>([^<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a onclick="return[ ]?showPic[(a-z)]*"[ ]?href="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.acecaribbeanrealestate.com/dominica-properties/caribbean-homes", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.acecaribbeanrealestate.com/dominica-properties/caribbean-land", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
