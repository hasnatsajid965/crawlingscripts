qa_override("[E1554112646]", "I am overriding this error because images are crawling but showing as an error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<td class="list-field field_thumbnail">[\\s\\t\\n ]*?<a href="([^"]*)">', category, "http://www.arubarealestate.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, 'class="next[^"]*"><a href="([^"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		notAvailable = get_unique_regex_match(html, '<div class="wrapper">[\\s\\t\\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			if (notAvailable.includes("Oops")) {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingTitle = "Aruba " + get_next_regex_match(html, 0, 'Reference Number</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class=\"label\">Price [(]US [$][)]</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<span class=\"label\">Price [(]([a-zA-Z\$ ]*)[)]</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<span class=\"label\">Description</span>(.+?)<div class="group group_facts">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingDescription) || mls.ListingDescription == "" || mls.ListPrice.value == "") {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, '<span class=\"label\">Land size [(]sq.m.[)]</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, '<span class=\"label\">Land size [(]([a-zA-Z\.]*)[)]</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, '<span class="label">Area [(]sq.m.[)]</span>[\\s\\t\\n ]*?<div class="value">([^\<]*)', 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_next_regex_match(html, 0, '<span class="label">Area [(]([a-zA-Z\.]*)[)]</span>[\\s\\t\\n ]*?<div class="value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, '<span class=\"label\">Bedrooms</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<span class=\"label\">Bathrooms</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<span class="label">([^\<]*)</span>[\\s\\t\\n ]*?<div class="value">[\\s\\t\\n ]*?Yes[\\s\\t\\n ]*?</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Phone = "+297 5834144";
		mls.MlsId = get_unique_regex_match(html, 'Reference Number</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			forRent = get_unique_regex_match(html, 'Period</span>[\\s\\t\\n ]*?<div class="value">([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Night")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
				}
			}
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.CoolingSystems = get_unique_regex_match(html, '<span class=\"label\">Airconditioned</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, "!2d([^!]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "!3d([^!]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '<span class=\"label\">Location</span>[\\s\\t\\n ]*?<div class=\"value\">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Aruba";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'class="gallery-image" rel="lightbox-gallery" href=\"([^"]*)\"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "http://www.arubarealestate.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// multifamily en venta
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-condominiums-for-sale/", KNUEVASMULTIFAMILIAVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// multifamily en rental
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-condominiums-for-rent/", KNUEVASCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-houses-for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/residences/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-houses-for-rent/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-vacation-rentals/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// STORES en venta
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-commercial-for-sale/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-commercial-for-rent/", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.arubarealestate.com/aruba-land-for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
