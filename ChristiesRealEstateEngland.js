var countryText = "England";

include("base/ChristiesRealEstateFunctions.js")

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/eng/apartments-flats-type", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/eng/single-family-home-type", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Land for sale
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/eng/estate-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/eng/farm-ranch-plantation-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/eng/land-lots-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

