

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			var array = extract_through_DOM_selector(html, "div.productListTxt > h2 > a", "selftag", KDONOTNOTIFYERROR);
			for ( var element in array) {
				var link = "";
				if (array[element].indexOf("href=\"") != -1) {
					link = array[element].substring(array[element].indexOf("href=\"") + "href=\"".length);
					if (link.indexOf("\"") != -1) {
						link = link.substring(0, link.indexOf("\""));
					}
				}
				if (addUrl("http://laclave.com.do" + link, category)) {
					actualCount++;
					cumulatedCount++;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		if (actualCount == 0) {
			return cumulatedCount;
		}
		var url = get_next_regex_match(html, 0, " href=\"([^\"]*)\" class=\"single\">" + (page + 1) + "</a>", 1, KDONOTNOTIFYERROR);
		if (url != undefined) {
			html = gatherContent_url(url, KDONOTNOTIFYERROR);
			tracer = 0;
			actualCount = 0;
			page++;
		} else {
			break;
		}
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(true);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		if (isDefined(get_next_regex_match(html, 0, "El anuncio (.*) est? desactualizado", 1, KDONOTNOTIFYERROR))) {
			return analyzeOnePublication_return_innactive;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a href=\"([^\"]*)\" class=\"lightbox\">", 1);
		images.forEach(function(oneImageTag) {
			if (!oneImageTag.endsWith("/112233-600x600.jpg")) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				oneImageTag = oneImageTag.replace("-600x600", "");
				obj.MediaURL = "http://laclave.com.do" + oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			}
		});
		mls.ListPrice.value = get_next_regex_match(html, 0, "<td>Precio:</td>[ \\t\\r\\n]*<td style=\"[^\"]*\">([A-Z$]*)[ ]?([^<>A-Za-z]*)</td>", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<td>Precio:</td>[ \\t\\r\\n]*<td style=\"[^\"]*\">([A-Z$]*)[ ]?([^<>A-Za-z]*)</td>", 1, KDONOTNOTIFYERROR);
		// print("Images"+images);
		var justForCheck;
		if (isDefined(images) || images == "") {
			images.forEach(function(oneImageTag) {
				// print("enter"+oneImageTag);
				// if(oneImageTag.includes("images/anuncios/112233-600x600.jpg")){
				justForCheck = oneImageTag;
				// return analyzeOnePublication_return_innactive;
				// }
			});
			if (isDefined(justForCheck)) {
				if (justForCheck.includes("images/anuncios/112233-600x600.jpg")) {
					return analyzeOnePublication_return_innactive;
				}
			}
			// print("outside"+justForCheck);
		}
		mls.ListingTitle = get_unique_regex_match(html, "(?s)<title>([^<]*)</title>", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = arrayElement(extract_through_DOM_attribute_selector(html, "itemprop=description", KDOMSELECTOR_INNERHTML, KDONOTNOTIFYERROR), 0);
		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = removeTags(mls.ListingDescription, "table");
			mls.ListingDescription = removeTags(mls.ListingDescription, "a");
			mls.ListingDescription = removeTags(mls.ListingDescription, "input");
			mls.ListingDescription = removeTags(mls.ListingDescription, "style");
			mls.ListingDescription = removeComments(mls.ListingDescription);
		} else {
			return analyzeOnePublication_return_innactive;
		}
		var code = get_next_regex_match(html, 0, "<input type=\"hidden\" id=\"itemid\" value=\"([0-9]*)\" />", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		mls.LivingArea.value = get_unique_regex_match(html, "<td width=\"[^\"]*\">Area:</td>[ \\t\\r\\n]*<td width=\"[^\"]*\" style=\"[^\"]*\">([0-9]*) ([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<td width=\"[^\"]*\">Area:</td>[ \\t\\r\\n]*<td width=\"[^\"]*\" style=\"[^\"]*\">([0-9]*) ([^<]*)</td>", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<td>Habitaciones:</td>[ \\t\\r\\n]*<td style=\"[^\"]*\">([0-9]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<td>Ba?os:</td>[ \\t\\r\\n]*<td style=\"[^\"]*\">([0-9]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<td>Parqueos:</td>[ \\t\\r\\n]*<td style=\"[^\"]*\">([0-9]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<label>Contacta <a target=\"_blank\" href=\"[^\"]*\">([^<]*)</a></label>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<li class=\"nd2_icon_mobile\">([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		var second = get_next_regex_match(html, 0, "<li class=\"nd2_icon_telephone\">([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		if ((isDefined(second)) && (second.length > 0)) {
			if (mls.Brokerage.Phone != undefined) {
				mls.Brokerage.Phone = "/" + second;
			} else {
				mls.Brokerage.Phone = second;
			}
		}
		mls.Address.Country.value = "Dominican Republic";
		mls.Address.City.value = get_unique_regex_match(html, "<td>Ubicaci[^\:]*n:</td>[ \\t\\r\\n]*<td style=\"[^\"]*\"><a href=\"[^\"]*\">([^<]*)</a></td>", 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/apartamentos_4_118.html#http://laclave.com.do/list.php?cat=4&subcat=118&order=3&perpage=88", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/penthouses_4_324.html#http://laclave.com.do/list.php?cat=4&subcat=324&order=3&perpage=88", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Fincas
		{
			try {
				// Fincas en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/fincas_4_122.html#http://laclave.com.do/list.php?cat=4&subcat=122&order=3&perpage=88", KFINCASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/casas_4_119.html?order=4", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/cabanas_4_125.html", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/villas_4_124.html#http://laclave.com.do/list.php?cat=4&subcat=124&order=3&perpage=88", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/locales_4_120.html#http://laclave.com.do/list.php?cat=4&subcat=120&order=3&perpage=88", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Edificios
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/edificios_4_287.html#http://laclave.com.do/list.php?cat=4&subcat=287&order=4", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/oficinas_4_123.html#http://laclave.com.do/list.php?cat=4&subcat=123&order=3&perpage=88", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/terrenos_4_126.html#http://laclave.com.do/list.php?cat=4&subcat=126&order=3&perpage=88", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos (solares) en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/solares_4_121.html#http://laclave.com.do/list.php?cat=4&subcat=121&order=3&perpage=88", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Naves industriales
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("http://laclave.com.do/bienes-raices/naves_4_127.html#http://laclave.com.do/list.php?cat=4&subcat=127&order=3&perpage=88", KNAVESVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (0 passed, 20 warning, 0 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * http://laclave.com.do/ALQUILO-APARTAMENTO-EN-ALMA-ROSA-II-1115426.html 2. No
 * JSON was returned from javascript with page
 * http://laclave.com.do/ELEGANTE-PENTHOUSE-EN-EL-MILLON-1108615.html 3. No JSON
 * was returned from javascript with page
 * http://laclave.com.do/Apartamento-Guavaberry-Juan-Dolio-Republica-Dominica-1046230.html
 * 4. No JSON was returned from javascript with page
 * http://laclave.com.do/Penthouse-en-Torre-Real-I-1108760.html 5. No JSON was
 * returned from javascript with page
 * http://laclave.com.do/PENTHOUSE-EN-ARROYO-HONDO-1046168.html 6. No JSON was
 * returned from javascript with page
 * http://laclave.com.do/PENTHOUSE-EN-URB-REAL-1075804.html 7. No JSON was
 * returned from javascript with page
 * http://laclave.com.do/Penthouse-en-venta-en-el-esanche-ozama-1031072.html 8.
 * No JSON was returned from javascript with page
 * http://laclave.com.do/VENDO-PENTHOUSE-ENSANCHE-OZAMA-1111129.html 9. No JSON
 * was returned from javascript with page
 * http://laclave.com.do/APARTAMENTO-TIPO--PENT--HOUSE-PIANTINI-1111931.html 10.
 * No JSON was returned from javascript with page
 * http://laclave.com.do/Pent-House-de-3-habitaciones-en-Mirador-Norte-1104707.html
 * 11. No JSON was returned from javascript with page
 * http://laclave.com.do/Pent-house-en-Venta-Urb-Real-1077656.html 12. No JSON
 * was returned from javascript with page
 * http://laclave.com.do/Pent-House-de-3-habitaciones-en-Av-Republica-de-Colombia-1104705.html
 * 13. No JSON was returned from javascript with page
 * http://laclave.com.do/Pent-House-de-4-habitaciones-en-Renacimiento-1104893.html
 * 14. No JSON was returned from javascript with page
 * http://laclave.com.do/Pent-House-de-2-habitaciones-en-El-Cacique-1104708.html
 * 15. No JSON was returned from javascript with page
 * http://laclave.com.do/Pent-house-de-620mts-en-Piantini-1104703.html 16. No
 * JSON was returned from javascript with page
 * http://laclave.com.do/Pent-House-de-4-habitaciones-en-Mirador-Norte-1104706.html
 * 17. No JSON was returned from javascript with page
 * http://laclave.com.do/Pent-House-de-43224mts-en-Malec�n-Center-1104891.html
 * 18. No JSON was returned from javascript with page
 * http://laclave.com.do/Penthouses-de-4-Habitaciones-en-Av-Independencia-1078567.html
 * 19. No JSON was returned from javascript with page
 * http://laclave.com.do/Penthouses-en-Alma-Rosa-1113314.html 20. No JSON was
 * returned from javascript with page
 * http://laclave.com.do/Penthouse-en-VentPenthouse-en-Venta-Mirador-Nortea-Mirador-Norte-1113718.html
 * 
 * HTTP response codes: [E45371257] HTTP200 (0/2) (Error)
 * 
 * 
 * 1. http://laclave.com.do/ALQUILO-APARTAMENTO-EN-ALMA-ROSA-II-1115426.html 2.
 * http://laclave.com.do/ELEGANTE-PENTHOUSE-EN-EL-MILLON-1108615.html 3.
 * http://laclave.com.do/Apartamento-Guavaberry-Juan-Dolio-Republica-Dominica-1046230.html
 * 4. http://laclave.com.do/Penthouse-en-Torre-Real-I-1108760.html 5.
 * http://laclave.com.do/PENTHOUSE-EN-ARROYO-HONDO-1046168.html 6.
 * http://laclave.com.do/PENTHOUSE-EN-URB-REAL-1075804.html 7.
 * http://laclave.com.do/Penthouse-en-venta-en-el-esanche-ozama-1031072.html 8.
 * http://laclave.com.do/VENDO-PENTHOUSE-ENSANCHE-OZAMA-1111129.html 9.
 * http://laclave.com.do/APARTAMENTO-TIPO--PENT--HOUSE-PIANTINI-1111931.html 10.
 * http://laclave.com.do/Pent-House-de-3-habitaciones-en-Mirador-Norte-1104707.html
 * 11. http://laclave.com.do/Pent-house-en-Venta-Urb-Real-1077656.html 12.
 * http://laclave.com.do/Pent-House-de-3-habitaciones-en-Av-Republica-de-Colombia-1104705.html
 * 13.
 * http://laclave.com.do/Pent-House-de-4-habitaciones-en-Renacimiento-1104893.html
 * 14.
 * http://laclave.com.do/Pent-House-de-2-habitaciones-en-El-Cacique-1104708.html
 * 15. http://laclave.com.do/Pent-house-de-620mts-en-Piantini-1104703.html 16.
 * http://laclave.com.do/Pent-House-de-4-habitaciones-en-Mirador-Norte-1104706.html
 * 17.
 * http://laclave.com.do/Pent-House-de-43224mts-en-Malec�n-Center-1104891.html
 * 18.
 * http://laclave.com.do/Penthouses-de-4-Habitaciones-en-Av-Independencia-1078567.html
 * 19. http://laclave.com.do/Penthouses-en-Alma-Rosa-1113314.html 20.
 * http://laclave.com.do/Penthouse-en-VentPenthouse-en-Venta-Mirador-Nortea-Mirador-Norte-1113718.html
 * 
 * QA executed on 19-Mar-2020 at 11:26:35 PM
 *  <<< Extraction QA Results <<< END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */
