var countryText = "Thailand";
include("base/KNightFrankFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.co.th/properties/residential/for-sale/thailand-bangkok/country%20house%2Ccountry%20house%20hotel%2Cfarmhouse%2Cguesthouse%2Chouse%2Chouseboat%2Chouses%20of%20multiple%20occupation%2Cportfolio%3A%20houses%2Cvillage%20house%2Cwarehouse%2Cwarehouse%20conversion/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.co.th/properties/residential/for-sale/thailand-bangkok/townhouse%2Ctownhousevilla/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.co.th/properties/residential/for-sale/thailand-bangkok/villa%2Cvillage%20house/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.co.th/properties/residential/for-sale/thailand-bangkok/condominium/all-beds", KAPTOVENTAS);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
