var countryText = "Cape Verde";
include("base/GlobalPropertyGuideFunctions.js");
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://search.globalpropertyguide.com/property/apartment/cape_verde/", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://search.globalpropertyguide.com/property/house/cape_verde/", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://search.globalpropertyguide.com/property/land/cape_verde/", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://search.globalpropertyguide.com/property/commercial/cape_verde/", KOTROSVENTAS);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
