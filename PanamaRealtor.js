

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html))
					&& (tracer = addNextToPropertiesList(html, tracer, "<figure class=\"ribbon display-none\"></figure>[\\s\\t\\n ]*?<a href=\"([^\"]*)\">", category, "https://www.panamarealtor.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\">&gt;</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.panamarealtor.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.PropertyType.value = get_unique_regex_match(html, "Property Type</option>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "Rent Price</dt>[\s\t\n ]*?<dd><span class=\"tag price\">([^\;]*);([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "Rent Price</dt>[\s\t\n ]*?<dd><span class=\"tag price\">([^\;]*);([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<header class=\"property-title\">[\s\t\n ]*?<h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, 'Property Description</h2></header>[\\s\\t\\n ]*?<div[ ]*?>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, 'Property Description</h2></header>[\\s\\t\\n ]*?<div class="overflow">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, 'Property Description</h2></header>[\s\t\n ]*?<div class="overflow">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</dt>[\s\t\n ]*?<dd><span class=\"tag price\">([^\;]*);([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</dt>[\s\t\n ]*?<dd><span class=\"tag price\">([^\;]*);([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
			if (isUndefined(mls.ListPrice.value)) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls.LotSize.value = get_unique_regex_match(html, "Total Area</dt>[\s\t\n ]*?<dd>([0-9\.\,\\- ]*)([a-zA-Z0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Total Area</dt>[\s\t\n ]*?<dd>([0-9\.\,\\- ]*)([a-zA-Z0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Lot Size</dt>[\s\t\n ]*?<dd>([0-9\.\, ]*)([a-zA-Z0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size</dt>[\s\t\n ]*?<dd>([0-9\.\, ]*)([a-zA-Z0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		}
		mls.YearBuilt = get_unique_regex_match(html, "Year Built</dt>[\s\t\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<ul class="list-unstyled property-features-list">(.+?)</ul>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.Bedrooms = get_next_regex_match(html, 0, "Bedrooms</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "Bathrooms</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "Property ID</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<div class=\"agent-contact-info\">[\\s\\t\\n ]*?<h3>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "Mobile:</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, "Email:</dt>[\\s\\t\\n ]*?<dd><a href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "Location</dt>[\\s\\t\\n ]*?<dd><a href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Panama";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a href=\"([^\"]*)\" class=\"image-popup\">", 1);
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = "https://www.panamarealtor.com/" + oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=sale&type=1&location=&lid=&minp=&maxp=", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=rent&type=1&location=&lid=&minp=&maxp=", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// // Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=&type=2&location=&lid=&minp=&maxp=", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=rent&type=2&location=&lid=&minp=&maxp=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=&type=3&location=&lid=&minp=&maxp=", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en rent
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=rent&type=3&location=&lid=&minp=&maxp=", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Others
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=&type=4&location=&lid=&minp=&maxp=", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.panamarealtor.com/search/real-estate/sale/?tran=rent&type=4&location=&lid=&minp=&maxp=", KOTROSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
