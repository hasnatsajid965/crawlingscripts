var countryContext = "Ecuador";
include("base/RealTorFunctions.js");
function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Appartment
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/apartment/", KAPTOVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Appartment en rent
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/rent/apartment/", KAPTOALQUILER);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/house/", KCASASVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/townhouse/", KCASASVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/rent/house/", KCASASALQUILER);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/rent/townhouse/", KCASASALQUILER);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/land/", KTERRENOSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.realtor.com/international/ec/rent/land/", KTERRENOSALQUILER);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
