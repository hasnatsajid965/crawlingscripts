qa_override("[E1473228885]", "Some of the properties contain lot size and some does not");
qa_override("[E2431535312]", "Some of the properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" class=\"link-block-7 w-inline-block\">", category, "https://www.stmaartenrealestate.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\"><i class=\"fa fa-forward\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// setProxyConditions(true, null);
	// rotateUserAgents(true);
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		list_not_available = get_unique_regex_match(html, '<h1 class="text-really-big p-lg pos-relative">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "404") {
				return analyzeOnePublication_return_unreachable;
			}
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.Bedrooms = get_unique_regex_match(html, ">([^\<]*)</p><p class=\"paragraph-14\">beds", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, ">([^\<]*)</p><p class=\"paragraph-10\">baths", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_unique_regex_match(html, '<div class=\"rich-text-block-2 w-richtext\">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<p class=\"paragraph-27\">([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value.includes("request") || mls.ListPrice.value.includes("Request")) {
				mls.ListPrice.value = KPRICEONDEMAND;
			} else {
				mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p class=\"paragraph-28\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			}
		} else {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.MlsId = get_unique_regex_match(html, "Listing Number</li><li class=\"property-detail-value\"><p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"heading-23 js-name-filter w-hidden-medium w-hidden-small w-hidden-tiny\">([^\<]*)", 1, KNOTIFYERROR);
		mls.PropertyType.value = get_next_regex_match(html, 0, "Property Type</li><li class=\"property-detail-value\"><p class=\"paragraph-40\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (mls.PropertyType.value == "Long-Term-Rentals" || mls.PropertyType.value == "long-term-rentals") {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
		}
		if (mls.PropertyType.value == "Vacation-Rentals" || mls.PropertyType.value == "vacation-rentals") {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
		}
		mls.Address.Country.value = "St. Maarten";
		mls.Address.City.value = get_unique_regex_match(html, 'Neighborhood</h4><a id="communityName" href="[^\"]*" class="link-9">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Sunshine Properties";
		mls.Brokerage.Phone = "1721 544-4498";
		mls.Brokerage.Email = "Info@sunshine-properties.com";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div style=\"background-image:url[(]&quot;([^\"]*)&quot;[)]\" class=\"slide-2 w-slide\">", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		// setProxyConditions(true, null);
		// rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Villa
		{
			try {
				// Villa en venta
				cumulatedCount += crawlCategory("https://www.stmaartenrealestate.com/listings/villas-homes", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villa en rent
				cumulatedCount += crawlCategory("https://www.stmaartenrealestate.com/listings/long-term-rentals", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villa en rent
				cumulatedCount += crawlCategory("https://www.stmaartenrealestate.com/listings/vacation-rentals", KAPTOTEMPORAL);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.stmaartenrealestate.com/listings/apartments-condos", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.stmaartenrealestate.com/listings/commercial-land", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.stmaartenrealestate.com/listings/land", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
