

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="hvr-sweep-to-bottom" href="([^"]*)"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a rel="next" href="([^"]*)">&gt;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		notAvailable = get_next_regex_match(html, 0, '<div class="inner-page-heading ba-animated" id="h1-title" data-animate="fadeInLeft">[\s\t\n ]*?<h1 >([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			if (notAvailable.includes("404")) {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<section class="page-heading">[\\s\\t\\n ]*?<div class="container">[\\s\\t\\n ]*?<h1>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<h2>PROPERTY DESCRIPTION</h2>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.Bedrooms = get_unique_regex_match(html, "<p>([^<]*)Bedroom</p>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<p>([^<]*)bathrooms</p>", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, "<li>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature !== "") {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					} else {
					}
				}
				if (feature.startsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<div class=\"title-pane\">[\s\t\n ]*?<h2>([^<]*)", 1, KNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "luxurylocations";
		mls.Brokerage.Phone = "+1 268 7645874 ";
		mls.Brokerage.Email = "info@luxurylocations.com";
		mls.office.PhoneNumber = "+1 268 5628174";
		mls.MlsId = get_unique_regex_match(html, "<td>Listing #</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<div class="title-pane">[\s\t\n ]*?<h2>[^<]*</h2>[\s\t\n ]*?<p>[^<]*</p>[\s\t\n ]*?<h3>([a-zA-Z\&\; ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<div class="title-pane">[\s\t\n ]*?<h2>[^<]*</h2>[\s\t\n ]*?<p>[^<]*</p>[\s\t\n ]*?<h3>([a-zA-Z\&\; ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, '<abbr title="per calendar month" class="pcm">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			for_rent = "/" + for_rent;
		} else {
			for_rent = get_unique_regex_match(html, '<abbr title="per week" class="pcm">([^<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(for_rent)) {
				for_rent = "/" + for_rent;
			} else {
				for_rent = "";
			}
		}
		if (mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;

		}
		if (isDefined(for_rent)) {
			if (for_rent == "/mth.") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (for_rent == "/wk.") {
				freq = "week";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.Videos = get_unique_regex_match(html, '<iframe class="youtube-video" style="[^"]*" src="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, 'alt="Home Icon">[\\s\\t\\n ]*?<p>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'data-lat="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'data-lng="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, '<div class="title-pane">[\\s\\t\\n ]*?<h2>[^<]*</h2>[\\s\\t\\n ]*?<p>([^<]*)</p>', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 2].trim();
				}
				if (tokens.length > 0) {
					mls.Address.Country.value = tokens[tokens.length - 1].trim();
				}
			}
			if (mls.Address == undefined)
				mls.Address = {};
			if (total_commas == "2") {
				if (tokens.length > 0) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 3].trim();
				}
			} else {
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a data-fancybox="gallery-1" href="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.luxurylocations.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.luxurylocations.com/properties/for/sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// temporal en venta
				cumulatedCount += crawlCategory("https://www.luxurylocations.com/properties/for/shortterm/", KCASASTEMPORAL);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.luxurylocations.com/properties/for/longterm/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
