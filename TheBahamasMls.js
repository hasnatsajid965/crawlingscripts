qa_override("[E1473228885]", "properties does not have areaUnits");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			// wait(4000);
			el1 = virtual_browser_find_one(browser, "(//a[@title='View Details/Photos..'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				onclicks = virtual_browser_element_attribute(el1, "onclick");
				onclicks = onclicks.replace(/[^0-9,]/g, "");
				onclicks = onclicks.split(',');
				if (isDefined(onclicks[1])) {
					finalUrl = "http://www.thebahamasmls.com/ViewDetail?id=" + onclicks[1];
					resArray.push(finalUrl);
				}
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "(//a[@class='paging_arrow paging_arrow_forward'])[1]", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		notAvailable = get_unique_regex_match(html, '<p style="text-align:center;">[\\s\\t\\n ]*?<strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			if (notAvailable.includes("Your request could not be completed")) {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div id="property_detail">(.+?)<div', 1, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, "<div class=\"rd\"style=\"display:inline;color:#009;align:right;margin-right:10px;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<h1 class=\"price\">[\s\t\n ]*?([a-zA-Z ]+)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<h1 class=\"price\">[\s\t\n ]*?([a-zA-Z ]+)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<h1 class=\"price\">[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<h1 class=\"price\">[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<div id=\"property_title\">[\\s\\t\\n ]*?<h1 class=\"price\">[^\<]*</h1>[\\s\\t\\n ]*?<h1>([^\<]*)", 1, KNOTIFYERROR);
		mls.ListingTitle = mls.ListingTitle.replace(/\s+/g, " ").trim();
		mls.LotSize.value = get_unique_regex_match(html, "Lot Acres</td>[\s\t\n ]*?<td class=\"detailData\">([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<td>Bedrooms</td>[\\s\\t\\n ]*?<td class=\"detailData\">([^\<]*)&nbsp;", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<td>Bathrooms</td>[\s\t\n ]*?<td class=\"detailData\">([^\<]*)&nbsp;", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "Listed By:<br />[\\s\\t\\n ]*?<strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "Listed By:<br />[\\s\\t\\n ]*?<strong>[^\<]*</strong><br />([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "MLS #([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Bahamas";
		mls.Location.Directions = get_unique_regex_match(html, "<div id=\"property_title\">[\\s\\t\\n ]*?<h1 class=\"price\">[^\<]*</h1>[\\s\\t\\n ]*?<h1>[^\<]*</h1>[\\s\\t\\n ]*?<h2>([^\<]*)", 1, KNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var city = mls.Location.Directions.split(",");
			if (isDefined(city[1])) {
				mls.Address.City.value = city[1];
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<br />[\\s\\t\\n ]*?([^\<]*)<br />[\\s\\t\\n ]*?MLS #", 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, 'Amenities</td>[\\s\\t\\n ]*?<td colspan="3" class="detailData">([^\<]*)&nbsp;', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Swimming Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "AC") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "onclick=\"enlargeImage[(]'([^\']*)',''[)]; return false;\" id=\"link[^\"]*\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "http://images.realtyserver.com/photo_server.php?btnSubmit=GetPhoto&board=bahamas&name=" + oneImageTag + "&failover=portal_Blank.gif";
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function housesForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function halfDuplex(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[3]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function condo(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[4]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function fullDuplex(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[5]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function triplex(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[6]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function fourplex(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[7]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function farm(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[8]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function lots(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[9]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function buildings(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[10]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function privateIsland(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[11]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function sixplex(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[12]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function appartment(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[13]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

// For Rent
function housesForRent(browser, category) {
	var cumulatedCount = 0;
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function halfDuplexForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[3]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function condoForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[4]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function fullDuplexForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[5]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function triplexForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[6]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function fourplexForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[7]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function farmForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[8]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function lotsForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[9]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function buildingsForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[10]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function privateIslandForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[11]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function sixplexForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[12]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function appartmentForRent(browser, category) {
	var cumulatedCount = 0;
	// virtual_browser_sendKeys(browser, "\\uE035");
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/");
	wait(100);
	virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
	wait(20000);
	var element = virtual_browser_find_one(browser, "(//input[@type='radio'][@name='price_type'])[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(100);
	}
	element = virtual_browser_find_one(browser, "//select[@name='district']/option[2]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "//select[@name='property_type']/option[13]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(20000);
	}
	element = virtual_browser_find_one(browser, "(//*[@title='Search'])[1]", KDONOTNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR);
		wait(1000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		virtual_browser_navigate(browser, "http://www.thebahamasmls.com/MainListingSearch#");
		cumulatedCount += housesForSale(browser, KCASASVENTAS);
		cumulatedCount += halfDuplex(browser, KCASASVENTAS);
		cumulatedCount += condo(browser, KCASASVENTAS);
		cumulatedCount += fullDuplex(browser, KAPTOVENTAS);
		cumulatedCount += triplex(browser, KAPTOVENTAS);
		cumulatedCount += fourplex(browser, KAPTOVENTAS);
		cumulatedCount += sixplex(browser, KAPTOVENTAS);
		cumulatedCount += farm(browser, KAPTOVENTAS);
		cumulatedCount += lots(browser, KTERRENOSVENTAS);
		cumulatedCount += buildings(browser, KEDIFICIOSVENTAS);
		cumulatedCount += privateIsland(browser, KTERRENOSVENTAS);
		cumulatedCount += appartment(browser, KAPTOVENTAS);
		cumulatedCount += housesForRent(browser, KCASASVENTAS);
		cumulatedCount += halfDuplexForRent(browser, KCASASVENTAS);
		cumulatedCount += condoForRent(browser, KCASASVENTAS);
		cumulatedCount += fullDuplexForRent(browser, KAPTOVENTAS);
		cumulatedCount += triplexForRent(browser, KAPTOVENTAS);
		cumulatedCount += fourplexForRent(browser, KAPTOVENTAS);
		cumulatedCount += sixplexForRent(browser, KAPTOVENTAS);
		cumulatedCount += farmForRent(browser, KAPTOVENTAS);
		cumulatedCount += lotsForRent(browser, KTERRENOSVENTAS);
		cumulatedCount += buildingsForRent(browser, KEDIFICIOSVENTAS);
		cumulatedCount += privateIslandForRent(browser, KTERRENOSVENTAS);
		cumulatedCount += appartmentForRent(browser, KAPTOVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
