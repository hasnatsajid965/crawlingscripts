qa_override("[E2431535312]", "This website does not have any description");
qa_override("[E1473228885]", "Some properties have lotsize and some not.");
qa_override("[E1554112646]", "I am overriding this error because it is crawling images but showing error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="tarpInfo">[\s\t\n ]*?<a href="([^\"]*)"', category, "http://bienesraicesvip.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a class="nextlink" href=\"([^\"]*)\"><span>Next', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://bienesraicesvip.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Land area:&nbsp;</span><span class="value">([0-9\,\. ]*)([a-zA-Z]*)&sup[0-9]*', 1, KDONOTNOTIFYERROR);
		areaUnitsValue = get_unique_regex_match(html, 'Land area:&nbsp;</span><span class="value">([0-9\,\. ]*)([a-zA-Z]*)&sup[0-9]*', 2, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, 'Land area:&nbsp;</span><span class="value">[0-9\,\. ]*[a-zA-Z]*&sup([0-9]*)', 1, KDONOTNOTIFYERROR);
		if (areaUnitsValue !== "undefinedundefined") {
			mls.LotSize.areaUnits = areaUnitsValue;
		}
		mls.LivingArea.value = get_unique_regex_match(html, 'Internal Area:&nbsp;</span><span class="value">([0-9\,\. ]*)([a-zA-Z]*)&sup[0-9]*', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'Internal Area:&nbsp;</span><span class="value">([0-9\,\. ]*)([a-zA-Z]*)&sup[0-9]*', 2, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, 'Internal Area:&nbsp;</span><span class="value">[0-9\,\. ]*[a-zA-Z]*&sup([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="section" title="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '>For sale</span>&nbsp;<span class="value">([a-zA-Z\.]*)&nbsp;([a-zA-Z\.\,]*)&nbsp;([0-9\,\. ]*)</span>', 3, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '>For sale</span>&nbsp;<span class="value">([a-zA-Z\.]*)&nbsp;([a-zA-Z\.\,]*)&nbsp;([0-9\,\. ]*)</span>', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, '>For sale</span>&nbsp;<span class="value">([a-zA-Z\.]*)&nbsp;([a-zA-Z\.\,]*)&nbsp;([0-9\,\. ]*)</span>', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms:&nbsp;</span><span class="value">([0-9]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li class="SubFeat value[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("view")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		// mls.ListingDescription = get_next_regex_match(html, 0, '<div
		// class="wpestate_property_description">(.+?)</div>', 1,
		// KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<a href="[^\"]*" class="link">[\s\t\n ]*?<div class="title">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<div class="text icoPhone"><span class="label">Telephone: </span>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '<div class="text icoEmail"><span class="label">Email: </span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, 'Reference:&nbsp;</span><span class="value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, 'Property Type:&nbsp;</span><span class="value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, "Capital City:&nbsp;</span><span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Venezuela";
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, "State:&nbsp;</span><span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'previewsrc="([^\"]*)" >', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "//images.egorealestate.com/Z1024x768" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=2&pss=BasicPropertySearchFormLimit11U12", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=19&pss=BasicPropertySearchFormLimit11U12", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=189&pss=BasicPropertySearchFormLimit11U12", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=188&pss=BasicPropertySearchFormLimit11U12", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Office
		{

			try {
				// Office en rent
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=9&pss=BasicPropertySearchFormLimit11U12", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=34&pss=BasicPropertySearchFormLimit11U12", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=176&pss=BasicPropertySearchFormLimit11U12", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=8&pss=BasicPropertySearchFormLimit11U12", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("http://bienesraicesvip.com/engb/properties/?orc=6&nat=139&pss=BasicPropertySearchFormLimit11U12", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
