qa_override("[E1473228885]", "Some properties does not contain lot size areaUnits.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" class=\"address\">", category, "https://www.jennadavishomes.com", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" rel=\"next\" title=\"Next Page\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.jennadavishomes.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<h3>Property Description</h3>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "List Price</dt>[\\s\\t\\n ]*?<dd>([\$]*)([0-9\,\. ]*)", 2, KNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "List Price</dt>[\\s\\t\\n ]*?<dd>([\$]*)([0-9\,\. ]*)", 1, KNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "MLS#</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<div class=\"small-12 columns prop-address\">[\\s\\t\\n ]*?<h1>([^\<]*)", 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<dt>Living([a-zA-Z\.\, ]*)</dt>[\s\t\n ]*?<dd>([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<dt>Living([a-zA-Z\.\, ]*)</dt>[\s\t\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "([a-zA-Z\,\. ]*) Above</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "([a-zA-Z\,\. ]*) Above</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Lot Size</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.YearBuilt = get_unique_regex_match(html, "Year Built</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Listing Type</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "Municipality</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "County</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Full baths</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, 'div class="prop-descrip">[\\s\\t\\n ]*?<dt>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Location.Latitude = get_unique_regex_match(html, 'property="og:latitude" content="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'property="og:longitude" content="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, '<div id="company-name" style="display:none">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'class="phone-number">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = "jennakdavis@gmail.com";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img src=\"([^\"]*)\" />", 1);
		if (images == "") {
			images = get_all_regex_matched(html, "<img src=\"([^\"]*)\" alt=\"\" />", 1);
		}
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				if (oneImageTag != "https://t2.realgeeks.media/thumbnail/n-BVoYUvGIDg0_5-nLx7ij85Hys=/fit-in/470x570/filters:format(png)/https://u.realgeeks.media/jennadavishomes/KW_Clear_Back_multi-color.png") {
					var obj = JSON.parse(get_list_empty_variable("photo"));
					oneImageTag = oneImageTag.split("50/");
					if (isDefined(oneImageTag[1])) {
						obj.MediaURL = oneImageTag[1];
						obj.MediaOrderNumber = imageCount;
						if (mls.Photos.photo == undefined) {
							mls.Photos.photo = [];
						}
						mls.Photos.photo.push(obj);
						imageCount++;
					}
				}
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.jennadavishomes.com/search/results/?city=all&school_district=all&list_price_min=all&list_price_max=all&beds_min=all&baths_min=all&type=con&sort_latest=true", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.jennadavishomes.com/search/results/?city=all&school_district=all&list_price_min=all&list_price_max=all&beds_min=all&baths_min=all&type=mul&sort_latest=true", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.jennadavishomes.com/search/results/?city=all&school_district=all&list_price_min=all&list_price_max=all&beds_min=all&baths_min=all&type=res&sort_latest=true", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.jennadavishomes.com/search/results/?city=all&school_district=all&list_price_min=all&list_price_max=all&beds_min=all&baths_min=all&type=lnd&sort_latest=true", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
