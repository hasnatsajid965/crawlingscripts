var countryText = "British Virgin Islands";
// crawlForPublications crawl-mode: Regular-Expression Crawling
include("base/PrimeLocationFunctions.js");

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Houses
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/houses/british-virgin-islands/?results_sort=newest_listings", KHOUSEFORSALE);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Apartments
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/flats/british-virgin-islands/?results_sort=newest_listings", KFLATFORSALE);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Commercials
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/commercial/british-virgin-islands/?results_sort=newest_listings", KLOCALFORSALE );
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Land
	{
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/property/british-virgin-islands/?results_sort=newest_listings", KLANDFORSALE);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
