qa_override("[E1473228885]", "Some of the properties contain lot size and some does not");
qa_override("[E2431535312]", "Some properties does not have description.");
function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	var check = [];
	i = 1;
	wait(20000);
	while (true) {
		var loadedMore = false;
		var el1;
		var index = 1;
		var resArray = [];
		do {
			el1 = virtual_browser_find_one(browser, "(//a[@class='card card--property '])[" + index + "]", KDONOTNOTIFYERROR);
			if (index > 238) {
				return actualCount;
			}
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			check[index] = el1;
			if (check[index] == check[index - 1]) {
				return actualCount;
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			// print("check array.."+resArray[element]);
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		for (var count = 0; count <= 2; count++) {
			virtual_browser_sendKeys(browser, "\\\uE010");
			loadedMore = true;
			break;
		}
		if (!loadedMore) {
			return actualCount;
		}
		i++;
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome", "!--incognito");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		var element;
		var imageCount = 0;
		var index = 1;
		wait(5000);
		element = virtual_browser_find_one(browser, "//div[@class='realestate-album--content--image--text']", KDONOTNOTIFYERROR);
		if ((isDefined(element)) && (virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR))) {
			wait(20000);
			do {
				element = virtual_browser_find_one(browser, "(//div[contains(@class, 'thumbnail_199fmdb')])[" + index + "]", KDONOTNOTIFYERROR);
				// element = virtual_browser_find_one(browser,
				// "(//div[contains(concat(' ', @class, ' '),
				// 'thumbnail_199fmdb')])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(element))) {
					var obj = JSON.parse(get_list_empty_variable("photo"));
					imageUrl = virtual_browser_element_attribute(element, "style");
					// var rawText = imageUrl;
					// var urlRegex
					// =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
					// imageUrl = rawText.replace(urlRegex, function(url) {
					// if (( url.indexOf(".jpg") > 0 ) || ( url.indexOf(".png")
					// > 0 ) || ( url.indexOf(".jpeg") > 0 )) {
					// return url;
					// }
					// });
					imageUrl = imageUrl.replace("background-image: url(\"", "");
					imageUrl = imageUrl.replace("\");", "");
					obj.MediaURL = imageUrl.replace("small", "big");
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
					// }
				}
				index++;
			} while (element != undefined);
		}
		var index = 1;
		do {
			feature = virtual_browser_find_one(browser, "(//div[@class='realestate-improvement-value realestate-badge']/span)[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(feature)) && (virtual_browser_element_interactable(browser, feature))) {
				feature = virtual_browser_element_text(feature);
				if (isDefined(feature)) {
					if (isDefined(feature)) {
						if (mls.DetailedCharacteristics == undefined) {
							mls.DetailedCharacteristics = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						if (feature === "Gated Community") {
							mls.DetailedCharacteristics.HasGatedEntry = true;
						} else if (feature === "Pool") {
							mls.DetailedCharacteristics.HasPool = true;
						} else {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
					if (feature.endsWith("View")) {
						if (mls.DetailedCharacteristics.ViewTypes == undefined) {
							mls.DetailedCharacteristics.ViewTypes = {};
						}
						if (mls.DetailedCharacteristics.ViewTypes.viewType == undefined) {
							mls.DetailedCharacteristics.ViewTypes.viewType = [];
						}
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.viewType.push(obj);
					}
				}
			}
			index++;
			// print("index.."+index);
		} while (feature);
		element = virtual_browser_find_one(browser, "//h3[@class='price-value']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			var price = virtual_browser_element_text(element);
			if (isDefined(price)) {
				mls.ListPrice.value = get_next_regex_match(price, 0, "[a-zA-Z$ ]*([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
				if (mls.ListPrice.value == 0) {
					mls.ListPrice.value = KPRICEONDEMAND;
				} else {
					mls.ListPrice.currencyCode = get_next_regex_match(price, 0, "([a-zA-Z$ ]*)[0-9\,\.]*", 1, KDONOTNOTIFYERROR);
				}
				if (url.includes("alquiler")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
			element = virtual_browser_find_one(browser, "//span[@class='realestate-subtitle']", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				var location = virtual_browser_element_text(element);
				location = location.split(',');
				if (isDefined(location[0])) {
					if (isDefined(location[0])) {
						mls.Address.City.value = location[0];
					}
					if (isDefined(location[1])) {
						mls.Address.Country.value = location[1];
					}
				} else {
					mls.Address.City.value = location;
				}
			}
			element = virtual_browser_find_one(browser, "//h1[@class='realestate-title']", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.ListingTitle = virtual_browser_element_text(element);
			}
			element = virtual_browser_find_one(browser, "(//h5[@class='agent-sidebar-name'])[1]", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Brokerage.Name = virtual_browser_element_text(element);
			}
			element = virtual_browser_find_one(browser, "//li[@class='agent-sidebar-contact-entry col-sm-6']/a/span", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Brokerage.Phone = virtual_browser_element_text(element);
			}
			element = virtual_browser_find_one(browser, "(//li[@class='agent-sidebar-contact-entry col-sm-12']/a)[2]", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Brokerage.Email = virtual_browser_element_text(element);
			}
			element = virtual_browser_find_one(browser, "//div[@class='realestate-code realestate-badge']", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.MlsId = virtual_browser_element_text(element);
				mls.MlsId = mls.MlsId.replace(/[^0-9]/g, "");
			}
			element = virtual_browser_find_one(browser, "//p[@class='realestate-description-content']", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.ListingDescription = virtual_browser_element_text(element);
			}
			element = virtual_browser_find_one(browser, "//div[@class='realestate-badge realestate-feature bathrooms']/font", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Bedrooms = virtual_browser_element_text(element);
			}
			element = virtual_browser_find_one(browser, "(//div[@class='realestate-badge realestate-feature meters'])[1]", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				element = virtual_browser_element_text(element);
				mls.LotSize.value = get_next_regex_match(element, 0, "([0-9\,\. ]*)[a-zA-Z0-9]*", 1, KDONOTNOTIFYERROR);
				mls.LotSize.areaUnits = get_next_regex_match(element, 0, "[0-9\,\. ]*([a-zA-Z0-9]*)", 1, KDONOTNOTIFYERROR);
			}
			resulting_json(JSON.stringify(mls));
			return analyzeOnePublication_return_success;
		} else {
			return analyzeOnePublication_return_unreachable;
		}
	}
	return analyzeOnePublication_return_unreachable;
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome", "!--incognito");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:house+business:sale+perPage:24/?", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:villa/?", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:house+business:lease+perPage:24/?", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+business:lease+perPage:24/?", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:land+business:sale+perPage:24/?", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:land+business:lease+perPage:24/?", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:estate+business:sale+perPage:24/?", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:estate+business:lease+perPage:24/?", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:industrial+site/?", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:industrial+site+business:lease+perPage:24/?", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:comercial+site+business:sale+perPage:24/?", KTIENDASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:comercial+site+business:lease+perPage:24/?", KTIENDASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:building+business:sale+perPage:24/?", KEDIFICIOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:building+business:lease+perPage:24/?", KEDIFICIOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:apartment+business:sale+perPage:24/?", KNUEVOSAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:apartment+business:lease+perPage:24/?", KNUEVOSAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:penthouse+business:sale+perPage:24/?", KCASASTEMPORAL);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:penthouse+business:lease+perPage:24/?", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:office+site+business:sale+perPage:24/?", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.remaxrd.com/propiedades/q:%22%22+type:office+site+business:lease+perPage:24/?", KOFICINASALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (77 passed, 16 warning, 1 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/127916-venta-casa-altos-de-arroyo-hondo-iii?
 * 2. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/28162-venta-o-alquiler-terreno-bella-vista?
 * 3. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/130025-venta-casa-urbanizacion-fernndez? 4.
 * No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129548-venta-casa-cap-cana? 5. No JSON was
 * returned from javascript with page
 * https://www.remaxrd.com/propiedad/129518-venta-casa-las-palmas-de-alma-rosa?
 * 6. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129451-venta-casa-vista-hermosa? 7. No JSON
 * was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129415-venta-casa-catalina-arriba? 8. No
 * JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/130187-venta-casa-altos-de-arroyo-hondo-iii?
 * 9. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129763-venta-casa-reparto-los-tres-ojos?
 * 10. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129569-venta-casa-ensanche-ozama? 11. No
 * JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129535-venta-casa-guavaberry? 12. No JSON
 * was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129517-venta-casa-alba-rosa? 13. No JSON
 * was returned from javascript with page
 * https://www.remaxrd.com/propiedad/129463-venta-casa-cocotal-golf-country-club?
 * 14. No JSON was returned from javascript with page
 * https://www.remaxrd.com/propiedad/130239-venta-casa-las-ballenas? 15. No JSON
 * was returned from javascript with page
 * https://www.remaxrd.com/propiedad/130130-venta-ambueblado-casa-las-ballenas?
 * 
 * Regular-expressions/xpath: virtual_browser_find_one -
 * //div[@class='realestate-code realestate-badge'] (1) (OK) [W1306029524]
 * virtual_browser_find_one - //h3[@class='price-value'] (0.8333333) (Warning)
 * [E3303188613] virtual_browser_find_one - //div[@class='realestate-badge
 * realestate-feature bathrooms']/font (0) (Error) [W2941634693]
 * virtual_browser_find_one - //li[@class='agent-sidebar-contact-entry
 * col-sm-6']/a/span (0.6) (Warning) virtual_browser_find_one -
 * (//div[@class='realestate-badge realestate-feature meters'])[*] (1) (OK)
 * virtual_browser_find_one - //p[@class='realestate-description-content'] (1)
 * (OK) get_next_regex_match - ([0-9,. ]*)[a-zA-Z0-9]* (1) (OK)
 * get_next_regex_match - [a-zA-Z$ ]*([0-9,.]*) (1) (OK) [W3489022229]
 * virtual_browser_find_one -
 * //div[@class='realestate-album--content--image--text'] (0.8333333) (Warning)
 * virtual_browser_find_one - //span[@class='realestate-subtitle'] (1) (OK)
 * virtual_browser_find_one - (//h5[@class='agent-sidebar-name'])[*] (1) (OK)
 * [W4030210017] virtual_browser_find_one -
 * (//div[@class='realestate-improvement-value realestate-badge']/span)[*]
 * (0.93877554) (Warning) [W2821236243] virtual_browser_find_one -
 * (//li[@class='agent-sidebar-contact-entry col-sm-12']/a)[*] (0.6) (Warning)
 * virtual_browser_find_one - //h1[@class='realestate-title'] (1) (OK)
 * get_next_regex_match - ([a-zA-Z$ ]*)[0-9,.]* (1) (OK) get_next_regex_match -
 * [0-9,. ]*([a-zA-Z0-9]*) (1) (OK) Json is set:
 * Brokerage.Address.preference-order (OK) MlsId (OK)
 * Brokerage.Address.address-preference-order (OK)
 * Franchise.Address.address-preference-order (OK) ListPrice.currencyPeriod (OK)
 * LotSize.value (OK) NonMLSListingData.category (OK)
 * DetailedCharacteristics.AdditionalInformation.additionalInformation.Description
 * (OK) Franchise.Address.preference-order (OK) Address.City.value (OK)
 * ListPrice.value (OK) [W4063587116] ListingDescription (Warning)
 * ListPrice.currencyCode (OK) Address.Country.value (OK) LotSize.areaUnits (OK)
 * NonMLSListingData.lang (OK) Brokerage.Phone (OK)
 * Address.address-preference-order (OK) ListingTitle (OK)
 * Address.preference-order (OK) Brokerage.Name (OK) Brokerage.Email (OK)
 * NonMLSListingData.countryPhoneCode (OK) Intermediate JSON value set:
 * Address.Country.value (5/5) (OK) Address.City.value (5/5) (OK)
 * ListPrice.value (5/5) (OK) LotSize.value (5/5) (OK) JSON value in empty JSON:
 * Brokerage.Name (5/5) (OK) Address.Country.value (5/5) (OK) MlsId (5/5) (OK)
 * Brokerage (5/5) (OK) LotSize.areaUnits (5/5) (OK)
 * Address.address-preference-order (5/5) (OK) LotSize.value (5/5) (OK) Address
 * (5/5) (OK) Franchise (5/5) (OK) Address.preference-order (5/5) (OK)
 * Brokerage.Address (5/5) (OK) ListPrice.currencyCode (5/5) (OK)
 * NonMLSListingData.countryPhoneCode (5/5) (OK) Brokerage.Phone (3/3) (OK)
 * Franchise.Address.address-preference-order (5/5) (OK)
 * Brokerage.Address.address-preference-order (5/5) (OK) ListPrice (5/5) (OK)
 * Brokerage.Email (3/3) (OK) Franchise.Address (5/5) (OK) Address.City (5/5)
 * (OK) NonMLSListingData.lang (5/5) (OK) ListPrice.value (5/5) (OK)
 * Franchise.Address.preference-order (5/5) (OK)
 * Brokerage.Address.preference-order (5/5) (OK) Address.Country (5/5) (OK)
 * DetailedCharacteristics.AdditionalInformation (4/4) (OK)
 * ListPrice.currencyPeriod (1/1) (OK) ListingDescription (4/4) (OK) LotSize
 * (5/5) (OK) Address.City.value (5/5) (OK) ListingTitle (5/5) (OK)
 * DetailedCharacteristics (4/4) (OK) NonMLSListingData (5/5) (OK) Images:
 * [E4230575792] Images average count (0) (Error) Front-end ready QA: List price
 * frequency is set (1/1) (OK) Lot size area is set (1/1) (OK) Title is set
 * (5/5) (OK) [E2431535312] - overwritten from error - Description is set (4/5)
 * (OK) List price is set (5/5) (OK) Coding style guidelines: Virtual-Browser
 * invokations must be headless. (1) (OK) LotSize.value, LotSize.areaUnits,
 * LivingArea.value, LivingArea.areaUnits, ListPrice.value and
 * ListPrice.currencyCode must not use the replace function to evaluate. (1)
 * (OK) Only Address.Country.value may hard-code location, other sublocations
 * must crawl. (1) (OK) LotSize.areaUnits and LivingArea.areaUnits must not be
 * hard-coded. (1) (OK)
 * 
 * 
 * 1.
 * https://www.remaxrd.com/propiedad/127916-venta-casa-altos-de-arroyo-hondo-iii?
 * 2.
 * https://www.remaxrd.com/propiedad/133038-alquiler-amueblado-apartamento-bella-vista?
 * 3. https://www.remaxrd.com/propiedad/128964-venta-terreno-san-isidro-afuera?
 * 4.
 * https://www.remaxrd.com/propiedad/28162-venta-o-alquiler-terreno-bella-vista?
 * 5.
 * https://www.remaxrd.com/propiedad/130301-venta-casa-altos-de-arroyo-hondo-iii?
 * 6.
 * https://www.remaxrd.com/propiedad/130188-venta-casa-altos-de-arroyo-hondo-iii?
 * 7. https://www.remaxrd.com/propiedad/130025-venta-casa-urbanizacion-fernndez?
 * 8. https://www.remaxrd.com/propiedad/129586-venta-casa-viejo-arroyo-hondo? 9.
 * https://www.remaxrd.com/propiedad/129548-venta-casa-cap-cana? 10.
 * https://www.remaxrd.com/propiedad/129518-venta-casa-las-palmas-de-alma-rosa?
 * 11. https://www.remaxrd.com/propiedad/129451-venta-casa-vista-hermosa? 12.
 * https://www.remaxrd.com/propiedad/129415-venta-casa-catalina-arriba? 13.
 * https://www.remaxrd.com/propiedad/130187-venta-casa-altos-de-arroyo-hondo-iii?
 * 14.
 * https://www.remaxrd.com/propiedad/129763-venta-casa-reparto-los-tres-ojos?
 * 15. https://www.remaxrd.com/propiedad/129569-venta-casa-ensanche-ozama? 16.
 * https://www.remaxrd.com/propiedad/129535-venta-casa-guavaberry? 17.
 * https://www.remaxrd.com/propiedad/129517-venta-casa-alba-rosa? 18.
 * https://www.remaxrd.com/propiedad/129463-venta-casa-cocotal-golf-country-club?
 * 19. https://www.remaxrd.com/propiedad/130239-venta-casa-las-ballenas? 20.
 * https://www.remaxrd.com/propiedad/130130-venta-ambueblado-casa-las-ballenas?
 * 
 * Errors and warnings overwritten: 1. The error [E2431535312] was overwitten by
 * code with the comment "Some properties does not have description."
 * 
 * QA executed on 27-Mar-2020 at 03:03:52 AM <<< Extraction QA Results <<<
 * END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */
