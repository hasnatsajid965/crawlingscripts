

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in FincaraizElpais.js ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)" title="[^\"]*" class="link-info">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in FincaraizElpais.js on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<li class="next">[\s\t\n ]*?<a href=\"([^\"]*)\" aria-label="Next">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink) || relativeLink.includes("#")) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="col-md-9 nombre-proyecto">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<p class="precio fondo-precio">([\$]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p class="precio fondo-precio">([\$]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, '[^\:]*rea:[\s\t\n ]*?</strong>[\s\t\n ]*([0-9\.\,]+)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'No. de Alcobas:[\s\t\n ]*?</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'No. de Ba[^\:]*os:[\s\t\n ]*?</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, 'Departamento:[\s\t\n ]*?</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Garage<span class="colon">:</span></span><span class="pri-props-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.NumParkingSpaces)) {
			mls.NumParkingSpaces = get_unique_regex_match(html, 'Garage: <span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.ListingDescription = get_next_regex_match(html, 0, 'div class="box-detail descripcion">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, 'corporativos -->[\s\t\n ]*?<h3>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '<p><i class="icon-email"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<p class="seller_phone"><i class="icon-phone"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = mls.Brokerage.Phone.replace("-", "/").trim();
		mls.Brokerage.Phone = mls.Brokerage.Phone.replace(",", "/").trim();
		mls.MlsId = get_unique_regex_match(html, '<p class="id">ID WEB:([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListingTitle)) {
			if (mls.ListingTitle.includes("Renta") || mls.ListingTitle.includes("Alquiler")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html,
				'<span class="ttlo">Otras Caracter?sticas</span>[\s\t\n ]*?</a>[\s\t\n ]*?</h4>[\s\t\n ]*?</div>[\s\t\n ]*?<div id="[^\"]*" class="collapse" role="tabpanel" aria-labelledby="[^\"]*">[\s\t\n ]*?<div class="panel-body">(.+?)</ul>', 1,
				KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.Country.value = get_next_regex_match(html, 0, 'Pa[^\:]*s:[\s\t\n ]*?</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, 'Ciudad:[\s\t\n ]*?</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '"latitude":([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"longitude":([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = mls.Location.Longitude.replace("}}", "").trim();
		var imageCount = 0;
		var images;

		images = get_all_regex_matched(html, '<a class="img-modal" rel="galeria" href="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			if (oneImageTag.includes("youtube")) {
				var objj = JSON.parse(get_list_empty_variable("video"));
				objj.MediaURL = oneImageTag;
				objj.MediaOrderNumber = 0;
				if (mls.Videos.video == undefined) {
					mls.Videos.video = [];
				}
				mls.Videos.video.push(objj);
			}
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (oneImageTag.includes("youtube")) {
				var obj = JSON.parse(get_list_empty_variable("video"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = 0;
				if (mls.Videos.video == undefined) {
					mls.Videos.video = [];
				}
				mls.Videos.video.push(obj);
			} else {
				obj.MediaURL = "https://fincaraiz.elpais.com.co" + oneImageTag;
			}

			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/apartaestudios/cali?q=OTksMSxWYWxsZSBkZWwgQ2F1Y2F8OTgsMSxDb2xvbWJpYQ&filter-name=Ciudad", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/apartaestudios/cali?q=OTksMSxWYWxsZSBkZWwgQ2F1Y2F8OTgsMSxDb2xvbWJpYQ&filter-name=Ciudad", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/apartamentos/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/apartamentos/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Building
		{
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/edificios/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Building en rent
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/edificios/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/casas/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/casas/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/fincas-y-casas-campestres/valle-del-cauca/cali?q=OTgsMSxDb2xvbWJpYQ", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/fincas-y-casas-campestres/valle-del-cauca/cali?q=OTgsMSxDb2xvbWJpYQ", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}

		}

		// Office
		{
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/oficinas-y-consultorios?q=OTgsMSxDb2xvbWJpYQ", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/oficinas-y-consultorios?q=OTgsMSxDb2xvbWJpYQ", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Terrence
		{
			try {
				// Terrence en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/lotes-funerarios?q=OTgsMSxDb2xvbWJpYQ", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrence en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/lotes-funerarios?q=OTgsMSxDb2xvbWJpYQ", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/garajes-y-parqueaderos/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/garajes-y-parqueaderos/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/locales-y-bodegas/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/locales-y-bodegas/cali?q=OTgsMSxDb2xvbWJpYXw5OSwxLFZhbGxlIGRlbCBDYXVjYQ", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/venta/lotes/cali?q=OTgsMSxDb2xvbWJpYQ", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://fincaraiz.elpais.com.co/avisos/alquiler/lotes/cali?q=OTgsMSxDb2xvbWJpYQ", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in FincaraizElpais.js required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (16 passed, 13 warning, 13 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S07053/ 2. No JSON
 * was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08020/ 3.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08037/ 4.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08031/ 5.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08040/ 6.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S01727/ 7. No JSON
 * was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08009/ 8.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08014/ 9.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08003/ 10.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08008/ 11.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08018/ 12.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08060/ 13.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08055/
 * 
 * Regular-expressions/xpath: get_next_regex_match - Reference
 * Number&lt;/span&gt;[\s\t\n ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*)
 * (1) (OK) get_next_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Description&lt;/span&gt;(.+?)&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - &lt;span class=&quot;label&quot;&gt;Area
 * [(]sq.m.[)]&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Bedrooms&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * Reference Number&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_next_regex_match -
 * !3d([^!]*) (1) (OK) get_next_regex_match - !2d([^!]*) (1) (OK)
 * get_unique_regex_match - Period&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Land size [(]sq.m.[)]&lt;/span&gt;[\s\t\n
 * ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK)
 * get_unique_regex_match - &lt;span class=&quot;label&quot;&gt;Price [(]US
 * [$][)]&lt;/span&gt;[\s\t\n ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*)
 * (1) (OK) get_unique_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Location&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Airconditioned&lt;/span&gt;[\s\t\n
 * ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK)
 * get_unique_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Bathrooms&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_all_regex_matched -
 * &lt;span class=&quot;label&quot;&gt;([^&lt;]*)&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;[\s\t\n ]*?Yes[\s\t\n ]*?&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - &lt;div class=&quot;wrapper&quot;&gt;[\s\t\n
 * ]*?&lt;h1&gt;([^&lt;]*) (1) (OK) HTTP response codes: HTTP200 (13/13) (OK)
 * Javascript Exceptions: ReferenceError: s is not defined (13) (Error)
 * 
 * 
 * 1. http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S07053/ 2.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08020/ 3.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08037/ 4.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08031/ 5.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08040/ 6.
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S01727/ 7.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08009/ 8.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08014/ 9.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08003/ 10.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08008/ 11.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08018/ 12.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08060/ 13.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08055/
 * 
 * QA executed on 10-Feb-2020 at 11:01:34 PM
 *  <<< Extraction QA Results <<< END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */

