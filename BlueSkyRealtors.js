qa_override("[E1473228885]", "Some properties does have lot size and some does not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<strong><a href="([^"]*)"', category, "https://www.blueskyrealtors.com/", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, 'href="([^"]*)" class="bt_pages">&nbsp;&nbsp;&gt;&gt;&nbsp;&nbsp;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.blueskyrealtors.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<font face="Georgia, Times New Roman, Times, serif" size="[^"]*">([^<]*)', 1, KNOTIFYERROR);
		if (!isDefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, "<strong>LAND SIZE</strong>:([ 0-9\,\.]*)([a-zA-Z\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>LAND SIZE</strong>:([ 0-9\,\.]*)([a-zA-Z\.]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>BEDS</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>BATHS</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.FloorCoverings = get_unique_regex_match(html, "<strong>FLOORS</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "<strong>Year Built</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, "<br /><h4>(.+?)</h4>", 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Blue Sky Realtors Ltd.";
		mls.Brokerage.Phone = "1-767-440-4733";
		mls.Brokerage.Email = get_unique_regex_match(html, 'href="mailto:([^?]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = "1-767-440-4733";
		mls.MlsId = get_unique_regex_match(html, "<strong>LISTING ID</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "<strong>PRICE ([a-zA-Z\$]*)</strong>: ([0-9\,\.]*)<br />", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<strong>PRICE ([a-zA-Z\$]*)</strong>: ([0-9\,\.]*)<br />", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		for_rent = get_unique_regex_match(html, "Description</strong>:[\\s\\t\\n ]*?<font color=\"#ae1819\"><strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent.includes("FOR RENT")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.Address.City.value = get_unique_regex_match(html, "<strong>VILLAGE</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_unique_regex_match(html, "<strong>PARISH</strong>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominica";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'title="Click/Tap on right/left of image to navigate" href="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.blueskyrealtors.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.blueskyrealtors.com/homes", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.blueskyrealtors.com/rent", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Stores
		{
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("https://www.blueskyrealtors.com/commercial-properties", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.blueskyrealtors.com/lands", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=692", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
