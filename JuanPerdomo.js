

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"links_more\" href=\"([^\"]*)\"> \\[ More info about this listing \\]</a>", category, "http://www.juanperdomo.com/", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\">Next >></a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://www.juanperdomo.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		var code = get_unique_regex_match(html, ">Listing #: ([^<]*)</h3>", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, ">Price: ([A-Z$]*)[ ]?([^<>]*)</h3>", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, ">Price: ([A-Z$]*)[ ]?([^<>]*)</h3>", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
			mls.ListingTitle = get_next_regex_match(html, 0, "<p style=\"font-weight:none; padding-bottom: 3px;\">[^\<]*</p>[\s\t\n ]*?<h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.Address.Country.value = "Dominican Republic";
		mls.Address.City.value = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">([a-zA-Z]*)",
				1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[\s\t\n ]*?([0-9\.\, ]+)([a-zA-Z ]*)</div></td>",
				1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[\s\t\n ]*?([0-9\.\, ]+)([a-zA-Z ]*)</div></td>",
				2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[\s\t\n ]*?[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*<br>([0-9\,\. ]*)([a-zA-Z\.\, ]*)&sup2[\s\t\n ]*?</div></td>",
				1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[\s\t\n ]*?[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*<br>([0-9\,\. ]*)([a-zA-Z\.\, ]*)&sup2[\s\t\n ]*?</div></td>",
				2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[\s\t\n ]*?[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">([^\<]*)</div></td>",
				1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(
				html,
				0,
				"<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Location</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Lot size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">House size</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bedrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Bathrm.</div></td>[\s\t\n ]*?<td height=\"20\" bgcolor=\"fed626\" class=\"bold\"><div align=\"center\">Distance to Beach</div></td>[\s\t\n ]*?</tr>[\s\t\n ]*?<tr>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[\s\t\n ]*?[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">[^\<]*<br>[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"e8e1c5\" class=\"table_black\"><div align=\"center\">[^\<]*</div></td>[\s\t\n ]*?<td height=\"40\" bgcolor=\"fff7d6\" class=\"table_black\"><div align=\"center\">([^\<]*)",
				1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Juan Perdomo";
		mls.Brokerage.Phone = "(809) 571 2100";
		mls.ListingDescription = get_next_regex_match(html, 0, "<div class=\"listing-description\">(.+?)</div>", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img src=\"([^\"]*)\" name=\"([^\"]*)\" alt=\"([^\"]*)\" title=\"Hover image to pause\">", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "http://www.juanperdomo.com/" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://www.juanperdomo.com/apartments-condos-page-1.htm", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.juanperdomo.com/villas-page-1.htm", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("http://www.juanperdomo.com/commercials-page-1.htm", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.juanperdomo.com/lots-farms-page-1.htm", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
