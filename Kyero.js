qa_override("[E2592408747]", "There is no city on this webiste.");

// crawlForPublications crawl-mode: Regular-Expression Crawling

function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'href=\"([^\"]*)\"><span>See details</span', category, "https://www.kyero.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="k-btn flex-1 h-12 items-center justify-center sm:mb-0 sm:mr-2 sm:w-32 sm:mx-0 sm:ml-2 sm:flex-none" href="([^\"]*)" title="Next page">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.kyero.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	url = url.replace(/ /g, "");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="property-page__title text-0.5xl md:text-2xl font-bold text-center mb-4 md:mb-2">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, ' data-scroll-target="#property-description"><script data-target="expandable.source" type="text/template">(.+?)</script>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<p class="card-body card-text card-remarks">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		}
		mls.Brokerage.Name = "Kyero";
		mls.ListPrice.value = get_unique_regex_match(html, '<h1 class="property-page__title text-0.5xl md:text-2xl font-bold text-center mb-4 md:mb-2">[^\<]*<span>([\?]*)([0-9\,\. ]*)</span>', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<h1 class="property-page__title text-0.5xl md:text-2xl font-bold text-center mb-4 md:mb-2">[^\<]*<span>([\?]*)([0-9\,\. ]*)</span>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Bedrooms = get_unique_regex_match(html, 'text-xl">([^\<]*)</strong><i class="kicon kicon-bed text-xl ml-1 md:hidden"></i></div><span class="text-center md:ml-1">Bedroom', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, ' text-xl">([^\<]*)</strong><i class="kicon kicon-bath text-xl ml-1 md:hidden"></i></div><span class="text-center md:ml-1">Bathroom', 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'Plot size</span><strong class="order-0 text-xl md:order-1 md:text-base md:ml-1">([0-9\,\. ]*)([^\<]*)</strong>', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Plot size</span><strong class="order-0 text-xl md:order-1 md:text-base md:ml-1">([0-9\,\. ]*)([^\<]*)</strong>', 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, 'Build size</span><strong class="order-0 text-xl md:order-1 md:text-base md:ml-1">([0-9\,\. ]*)([^\<]*)</strong>', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'Build size</span><strong class="order-0 text-xl md:order-1 md:text-base md:ml-1">([0-9\,\. ]*)([^\<]*)</strong>', 2, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'latitude&quot;:([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '&quot;longitude&quot;:([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '"nation_name":"[^\"]*","city":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Spain";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'class="gallery-slide__image" data-gallery="property" data-id="[^\"]*" data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://d2hhh2ewuz3i8z.cloudfront.net/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-apartments-for-sale-0l55529g1", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-villas-for-sale-0l55529g2", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-town-houses-for-sale-0l55529g3", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-country-houses-for-sale-0l55529g4", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-cave-houses-for-sale-0l55529g6", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-land-for-sale-0l55529g5", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-garages-for-sale-0l55529g7", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-commercial-properties-for-sale-0l55529g8", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.kyero.com/en/spain-wooden-homes-for-sale-0l55529g9", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
