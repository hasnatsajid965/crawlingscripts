

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="importantlink viewadvertlink" href="([^\"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="pager-next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		//
		mls.ListingTitle = get_unique_regex_match(html, '<div class="container theme-ebas clearfix"><h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<dt>Precio</dt><dd>([ \$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<dt>Precio</dt><dd>([ \$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		freq = get_unique_regex_match(html, '<dt>Precio</dt><dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(freq)) {
			if (freq.includes("Mes")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.LivingArea.value = get_unique_regex_match(html, '<dt>Antiguedad</dt><dd>([ 0-9\,\. ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<dt>Antiguedad</dt><dd>([ 0-9\,\. ]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<dt>Dormitorios</dt><dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<dt>Ba[^\<]*os</dt><dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<i class="glyphicon glyphicon-ok"></i>&nbsp;<p>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<h3>Descripci[^\<]*n</h3><p itemprop="description"><p>(.+?)</p>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<div class="realtor_box side-block"><h3>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<dt>Clase</dt><dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<dt>Cocheras</dt><dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, "<dt>Dptos. Por Piso</dt><dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, 'var polygonLatLngs = \\[\\[([^\,]*),([^\,]*)\\]', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, 'var polygonLatLngs = \\[\\[([^\,]*),([^\,]*)\\]', 2, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, 'Direcci[^\<]*n</dt><dd><span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			address = mls.Location.Directions.split(",");
			if (isDefined(address[3])) {
				mls.Address.City.value = address[3];
			}
		}
		mls.Address.Country.value = "Argentina";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-lazy="([^\"]*)" alt="[^\"]*" class="img-responsive"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}

}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Uncategorized
		{
			try {
				// Uncategorized
				cumulatedCount += crawlCategory("https://www.enbuenosaires.com/capital-federal-ventas.html", KUNCATEGORIZED);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Uncategorized
				cumulatedCount += crawlCategory("https://www.enbuenosaires.com/capital-federal-alquiler.html", KUNCATEGORIZED);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
