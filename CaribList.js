qa_override("[E1473228885]", "Some properties have lot size and some not");
qa_override("[E2431535312]", "Some properties have description and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "title=\"[^\"]*\" href=\"([^\"]*)\">Property[\s ]*?#", category, "https://www.cariblist.com/", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "title=\"Next Page\" href=\"([^\"]*)\">Next</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.cariblist.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(encodeURI(url), KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		var not_found = get_unique_regex_match(html, "<h3 class=\"CurrencyColor text-center\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(not_found)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<div class=\"col-lg-10 col-md-10 col-sm-12 col-xs-12\">[\s\t\n ]*?<h4 style=\"color:#EC1D25;\">([^\<]*)", 1, KNOTIFYERROR);
		var features = get_all_regex_matched(html, '<h5><i class=\"fa fa-angle-double-right faicon\"></i>([^\<]*)</h5>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "A/C Unit") {
						mls.DetailedCharacteristics.hasAC = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, "<b class=\"CurrencyColor\">[^\<]*</b><br>[\s\t\n ]*?<h5>(.+?)</h5>", 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, "<h4><b>Contact name:</b></h4>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-8\">[\\s\\t\\n ]*?<h4>([^\<]*)</h4>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<h4><b>Work Phone:</b></h4>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-8\">[\\s\\t\\n ]*?<h4>([^\<]*)</h4>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, " <h4><b>Email:</b></h4>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-8\">[\\s\\t\\n ]*<h5><b>([^\<]*)</b></h5>", 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, "<span>Office</span>[\t\s\n ]*?:([^\<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "<h4 style=\"color:#EC1D25;\">[(]Property ID:([^\"]*)[)]</h4>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<h4 style=\"color:#EC1D25;margin-top: 12%;\">([a-zA-Z\$ ]*)([0-9\,]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "<h4 style=\"color:#EC1D25;margin-top: 12%;\">([a-zA-Z\$ ]*)([0-9\,]*)", 2, KDONOTNOTIFYERROR);
		if (mls.ListPrice.value == 0) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var for_rent = get_unique_regex_match(html, "<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-4\">[\\s\\t\\n ]*?<b><br>Monthly([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "Rental") {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<b>Property Type</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-7 col-md-7 col-sm-7 col-xs-7\">[\\s\\t\\n ]*?<span>([^\<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<b>Property Area</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-7 col-md-7 col-sm-7 col-xs-7\">[\\s\\t\\n ]*?<span>([0-9\, ]*)([a-zA-Z\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<b>Property Area</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-7 col-md-7 col-sm-7 col-xs-7\">[\\s\\t\\n ]*?<span>([0-9\, ]*)([a-zA-Z\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "LoadMap[(]'([^\']*)', '([^\']*)',", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "LoadMap[(]'([^\']*)', '([^\']*)',", 2, KDONOTNOTIFYERROR);
		mls.Address.FullStreetAddress = get_unique_regex_match(html, "<h4><b>Listing Address:</b></h4>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-lg-8\">[\\s\\t\\n ]*?<h4>([^\<]*)</h4>", 1, KDONOTNOTIFYERROR);
		if (mls.Address.Country == undefined)
			mls.Address.Country = {};
		mls.Address.Country.value = "Barbados";
		mls.Location.Directions = get_next_regex_match(html, 0, "'Property [^\:]* : ([^\']*)'", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			if (mls.Location.Directions.endsWith(" Map")) {
				mls.Location.Directions = mls.Location.Directions.substring(0, mls.Location.Directions.length - " Map".length);
			}
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "1") {
				if (tokens.length > 1) {
					if (mls.Address.City == undefined)
						mls.Address.City = {};
					mls.Address.City.value = tokens[tokens.length - 2].trim();
				}
				if (tokens.length > 0) {
					if (mls.Address.StateOrProvince == undefined)
						mls.Address.StateOrProvince = {};
					mls.Address.StateOrProvince.value = tokens[tokens.length - 1].trim();
					if (mls.Address.StateOrProvince.value.endsWith(" Map")) {
						mls.Address.StateOrProvince.value = mls.Address.StateOrProvince.value.substring(0, mls.Address.StateOrProvince.value.length - " Map".length);
					}
				}
			}
			if (mls.Address == undefined)
				mls.Address = {};
			if (total_commas == "2") {
				if (tokens.length > 0) {
					mls.Address.StateOrProvince.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 3].trim();
				}
			}
			mls.Location.Directions += ", " + mls.Address.Country.value;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "class=\"img-responsive SlidingThumbImage\" src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=1&CCode=BB", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=2&CCode=BB", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=rent&PropertyID=1&CCode=BB", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=4&CCode=BB", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=rent&PropertyID=4&CCode=BB", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Oficinas en venta
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=7&CCode=BB", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Oficinas en Rental
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=rent&PropertyID=7&CCode=BB", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=8&CCode=BB", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en Rent
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=rent&PropertyID=8&CCode=BB", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=3&CCode=BB", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=13&CCode=BB", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=rent&PropertyID=13&CCode=BB", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Edificios/Ventas
		{
			try {
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=sale&PropertyID=19&CCode=BB", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.cariblist.com/properties?count=1&for=rent&PropertyID=19&CCode=BB", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
