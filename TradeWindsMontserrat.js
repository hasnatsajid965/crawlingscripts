

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'class="property-item primary-tooltips title-below-image"><a href=([^>]*)', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, 'class="next page-numbers" href=([^>]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(false);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="title">[ ]*?<span>([^<]*)', 1, KNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<div class=meta-data data-toggle=tooltip title=Bedrooms>([^<]*)Bedrooms</div></div><div class="col-sm-4 col-md-3">', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<div class=meta-data data-toggle=tooltip title=Bathrooms>([^<]*)Bathrooms</div></div><div class="col-sm-4 col-md-3">', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, "<section id=property-content>(.+?)</section>", 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<h5 class="widget-title">[^<]*</h5><div class=textwidget><img src=[^>]*><p>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, '<div class=contact><i class="fa fa-mobile"></i>&nbsp;<a href=tel:([^>]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '<a href="mailto:([^"]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_next_regex_match(html, 0, '<div class=contact><i class="fa fa-phone"></i><a href=tel:([^>]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<div class=meta-data data-toggle=tooltip title="Property ID">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<div class=clearfix></div><div class=meta>Asking&nbsp;([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "<div class=clearfix></div><div class=meta>Asking&nbsp;([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		list_price = get_unique_regex_match(html, "</h1><div class=clearfix></div><div class=meta>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			if (list_price.includes("week")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("week");
			} else if (list_price.includes("month")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
			} else if (list_price.includes("rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
			} else if (list_price.includes("Rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
			}
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "google.maps.LatLng[(]([^,]*),([^,]*)[)],", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "google.maps.LatLng[(]([^,]*),([^,]*)[)],", 2, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<span>Location</span></h3><p class=text-muted>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Montserrat";
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 2].trim();
					if (isUndefined(mls.Address.City.value)) {
						mls.Address.City.value = mls.ListingTitle;
					}
				}
				if (tokens.length > 0) {
					mls.StreetAdditionalInfo.value = tokens[tokens.length - 1].trim();
				}
			}
			if (mls.Address == undefined)
				mls.Address = {};
			if (total_commas == "2") {
				if (tokens.length > 0) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 3].trim();
					if (isUndefined(mls.Address.City.value)) {
						mls.Address.City.value = mls.ListingTitle;
					}
				}
			} else {
			}
			// StreetAdditionalInfo
		} else {
			// if(isDefined(mls.Address.City.value)){
			// mls.Address.City.value = mls.ListingTitle;
			// }
		}
		if (isDefined(mls.ListingTitle)) {
			mls.Address.City.value = mls.ListingTitle;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<div><a href=#><img src=([^\>]*) alt>', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag.replace("-600x300", "");
			} else {
				obj.MediaURL = "https://www.tradewindsmontserrat.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.tradewindsmontserrat.com/villas-for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.tradewindsmontserrat.com/villas-for-rent/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.tradewindsmontserrat.com/lands-for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
