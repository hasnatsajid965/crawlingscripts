qa_override("[E2431535312]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "href=\"([^\"]*)\">[\\s\\t\\n ]*?<div class=\"picture\">", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a title=\"P?gina siguiente\" class=\"button\" href=\"([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="name" title="Descripci[^\<]*n"><div><span>Descripci[^\<]*n</span></div></div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"price-tag\" id=\"df_field_price\">([a-zA-Z\$ ]*)([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"price-tag\" id=\"df_field_price\">([a-zA-Z\$ ]*)([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value < 10) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls_id = get_unique_regex_match(html, "ID de la Propiedad</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = mls_id.replace('ID', '');
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^\<]*)</title>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListingDescription) || mls.ListingDescription == "") {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, "Construcci[^\<]*n</span></div></div>[\s\t\n ]*?<div class=\"value\">[\s\t\n ]*?([0-9\,\. ]+)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Construcci[^\<]*n</span></div></div>[\s\t\n ]*?<div class=\"value\">[\s\t\n ]*?([0-9\,\. ]+)([^\<]*)", 2, KDONOTNOTIFYERROR);
		var for_rent = get_unique_regex_match(html, "Frecuencia alquiler</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent.includes("mes")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
			if (for_rent.includes("a")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("year");
			}
		}
		mls.YearBuilt = get_unique_regex_match(html, "Construido</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Habitaciones</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Ba?os</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "Parqueos</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "Pa?s</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (!isDefined(mls.Address.Country.value)) {
			mls.Address.Country.value = "Dominican Republic";
		}
		mls.Address.City.value = get_unique_regex_match(html, "Ciudad</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_unique_regex_match(html, "Sector</span></div></div>[\\s\\t\\n ]*?<div class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li title="[^\"]*" class="active">[\\s\\t\\n ]*?<img src="http://www.hogar.com.do/templates/realty_flatty/img/blank.gif" alt="" />([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Brokerage.Name = get_unique_regex_match(html, '<li class="name">([^\<]*)', 1, KDONOTNOTIFYERROR);
		phone_number = get_unique_regex_match(html, '<li class="tel">[\\s\\t\\n ]*?<a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(phone_number)) {
			mls.Brokerage.Phone = phone_number.replace(/[^0-9,]/g, "");
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a rel=\"group\" href=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/apartamento/sale-rent:1/", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/edificio/sale-rent:1/", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en rent
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/apartamento/sale-rent:2/", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/casa/sale-rent:1/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/casa-playa/sale-rent:1/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/nave/sale-rent:1/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/casa/sale-rent:2/", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Stores
		{
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/local-comercial/sale-rent:1/", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.hogar.com.do/properties/finca/sale-rent:1/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
