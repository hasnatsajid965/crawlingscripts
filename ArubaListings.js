
// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
    var cumulatedCount = 0;
    print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
    var actualCount = 0;
    var page = 1;
    var tracer = 0;
    var html = gatherContent_url(url, KDONOTNOTIFYERROR);
    while (isDefined(html)) {
		try {
		    while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"content\">[\\s\\t\\n ]*?<a class=\"name\" href=\"([^\"]*)\"", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
		    }
		} catch (e) {
		    if (e !== "Duplicate detected.") {
		    	throw("error: " + e);
		    } else {
		    	return cumulatedCount;
		    }
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " ("+ cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<li class=\"next\"><a data-page='([^\']*)' >Next", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
		    break;
		}
		relativeLink = url.replace(/1$/,relativeLink);
		// print(relativeLink);
		// wait(1000);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
    }
    return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
    addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
    var html = gatherContent_url(url, KDONOTNOTIFYERROR);
    var mls = JSON.parse(mlsJSONString);
    if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
		    mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
		    mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_unique_regex_match(html,'<div class="fr-view">(.+?)</div>',1,KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"price\" style=\"[^\"]*\">([\$ ]*)([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"price\" style=\"[^\"]*\">([\$ ]*)([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value === 0 || mls.ListPrice.value === "0"){
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		} else {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription)) {
		    return analyzeOnePublication_return_innactive;
		}
		mls.LotSize.value = get_unique_regex_match(html,"<dt>Lot size</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)([a-zA-Z]*)",1,KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html,"<dt>Lot size</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)([a-zA-Z]*)",2,KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match( html, 'data-center="([^\,]*),([^\"]*)"', 1, KDONOTNOTIFYERROR );
		mls.Location.Longitude = get_unique_regex_match( html, 'data-center="([^\,]*),([^\"]*)"', 2, KDONOTNOTIFYERROR );
		mls.YearBuilt = get_unique_regex_match(html,"Year built</dt>[\\s\\t\\n ]*?<dd>([^\<]*)",1,KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Number of bedrooms</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Number of bathrooms</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Aruba";
		mls.Address.City.value = get_unique_regex_match(html, "<div class=\"area\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0,'<div class="name"><a href="[^\"]*" title="[^\"]*">([^\<]*)',1,KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html,'<i class="fas fa-phone"></i><a href="[^\"]*" target="_blank">([^\<]*)',1,KDONOTNOTIFYERROR); 
		var imageCount = 0;
		var images;
		var cars = [];
		temp = get_all_regex_matched(html, "data-src=\"([^\"]*)\"", 1);
		temp.forEach(function (oneImageTag) {
		    cars.push(oneImageTag);
		});
		const distinct = (value, index, self) => {
		    return self.indexOf(value) === index;
		}
		var filteredArray = cars.filter(distinct); 
		filteredArray.forEach(function(oneImageTag) {
		    var obj = JSON.parse(get_list_empty_variable("photo"));
		    obj.MediaURL = oneImageTag;
		    obj.MediaOrderNumber = imageCount;
		    if (mls.Photos.photo == undefined) {
			mls.Photos.photo = [];
		    }
		    mls.Photos.photo.push(obj);
		    imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
    }
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Apartamentos
	{
	    try {
		// Apartamentos en venta
		cumulatedCount += crawlCategory("https://arubalistings.com/sale/all/all/apartment/price-desc/p1", KAPTOVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://arubalistings.com/sale/all/all/new-development/price-desc/p1", KCASASVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://arubalistings.com/sale/all/all/residential/price-desc/p1", KCASASVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://arubalistings.com/sale/all/all/land/price-desc/p1", KTERRENOSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Hotel
	{
	    try {
		// Hotel en venta
		cumulatedCount += crawlCategory("https://arubalistings.com/sale/all/all/commercial/price-desc/p1", KOTROSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Building
	{
	    try {
		// Building en venta
		cumulatedCount += crawlCategory("https://arubalistings.com/sale/all/all/condominium/price-desc/p1", KOTROSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
	next_crawl_needed(null, false);
    }
}
