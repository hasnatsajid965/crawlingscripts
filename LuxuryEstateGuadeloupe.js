var countryText = "Guadeloupe";
include("base/LuxuryEstateFunctions.js");

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/guadeloupe/guadeloupe/guadeloupe?sort=relevance", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en alquiler
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/guadeloupe/guadeloupe/guadeloupe?sort=relevance", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
