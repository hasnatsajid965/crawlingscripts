

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"bp_ad__title_link\" href=\"([^\"]*)\">", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e === "Duplicate detected.") {
				return cumulatedCount;
			}
			throw ("error: " + e);
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a page=\"([^\"]*)\" href=\"([^\"]*)\"><strong>Siguiente ›</strong></a>", 2, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(false);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		var code = get_unique_regex_match(html, "<div class=\"vap_ad_id\">Anuncio n°: ([0-9]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		mls.ListPrice.currencyPeriod = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^\\(]*) \\(por ([^\\)]*)\\)", 3, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.currencyPeriod)) {
			mls.ListPrice.currencyPeriod = undefined;
			mls.ListPrice.value = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^<>]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^<>]*)", 1, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(mls.ListPrice.currencyPeriod);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^\\(]*) \\(por ([^\\)]*)\\)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^\\(]*) \\(por ([^\\)]*)\\)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Superficie habitable</strong><br />[ \\t\\r\\n]*([0-9]*)([^<]*)[ \\t\\r\\n]*</p>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Superficie habitable</strong><br />[ \\t\\r\\n]*([0-9]*)([^<]*)[ \\t\\r\\n]*</p>", 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<span class=\"user-name\">([^<]*)</", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<span class=\"phone icon icon-call\">([^<]*)<div class=\"see-phone\"", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = arrayElement(extract_through_DOM_attribute_selector(html, "itemprop=description", KDOMSELECTOR_INNERHTML, KDONOTNOTIFYERROR), 0);
		if (mls.ListingDescription != undefined) {
			mls.ListingDescription = mls.ListingDescription.replaceAll("•", "\n•");
		}

		mls.Address.City.value = get_next_regex_match(html, 0, "<span itemprop=\"addressLocality\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		var imageCount = 0;
		var images;
		for (var pass = 0; pass <= 1; pass++) {
			if (pass == 0) {
				images = get_all_regex_matched(html, "<img[ \\t\\r\\n]*id=\"big_img\"[ \\t\\r\\n]*src=\"([^\"]*)\"", 1);
			} else if (pass == 1) {
				images = get_all_regex_matched(html, "<a[ \\t\\r\\n]*class=\"tn_img js-tn_img[^\"]*\"[ \\t\\r\\n]*data-big=\"([^\"]*)\"", 1);
			}
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Fincas
		{
			try {
				// Fincas en venta
				cumulatedCount += crawlCategory("https://vacaciones.locanto.com.do/Segunda-residencia/354/?sort=date", KFINCASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Apartamentos-en-venta/355/?sort=date", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Apartamentos-en-alquiler/301/?sort=date", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Casas-en-venta/356/?minPrice=1&sort=date", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Casas-en-alquiler/307/?sort=date", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Locales-comerciales-en-venta/35101/?sort=date", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Locales - Tiendas en alquiler
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Negocios-en-alquiler/30309/?sort=date", KTIENDASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Oficinas-en-venta/35103/?sort=date", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Oficinas-en-alquiler/30303/?sort=date", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Lotes-terrenos-en-venta/352/?sort=date", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Terrenos-lotes-en-alquiler/311/?sort=date", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Naves industriales
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("https://www.locanto.com.do/Naves-industriales-en-venta/35105/?sort=date", KNAVESVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
