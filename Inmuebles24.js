qa_override("[E4230575792]", "Some properties have images and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[@class='go-to-posting'])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//i[@class='icon-g icon-g-chevron-right']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KDONOTNOTIFYERROR)) {
						loadedMore = false;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.LotSize.value = get_unique_regex_match(html, '<i class="icon-f icon-f-stotal"></i>[\s\t\n ]*?<br>[\s\t\n ]*?<b>([0-9]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<i class="icon-f icon-f-stotal"></i>[\s\t\n ]*?<br>[\s\t\n ]*?<b>([0-9]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<i class="icon-f icon-f-dormitorio"></i>[\s\t\n ]*?<br>[\s\t\n ]*?<b>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<i class="icon-f icon-f-bano"></i>[\s\t\n ]*?<br>[\s\t\n ]*?<b>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, '<i class="icon-f icon-f-cochera"></i>[\s\t\n ]*?<br>[\s\t\n ]*?<b>([^\<]*)', 1, KDONOTNOTIFYERROR);
		// forRent = get_next_regex_match(html, 0, '<div
		// class="price-operation">([^\<]*)', 1, KDONOTNOTIFYERROR);
		// if(isDefined(forRent)){
		// if(forRent.includes("Renta")){
		// mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
		// }
		// }
		var features = get_all_regex_matched(html, '<li><h4>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}

		mls.ListingDescription = get_next_regex_match(html, 0, '<div id="verDatosDescripcion">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, "'publisherId': '[^\']*',[\s\t\n ]*?'name': '([^\']*)',", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<b>C[^\:]*d. Inmuebles24:</b>([^<]*)', 1, KDONOTNOTIFYERROR);
		var code = get_unique_regex_match(html, "<b>C[^\:]*d. del anunciante:</b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h2 class="title-type-sup"><b>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_next_regex_match(html, 0, '<div class="col-xs-12 key-address fts-mark">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_next_regex_match(html, 0, '<div class="section-title">[\\s\\t\\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		// list_price = get_next_regex_match(html, 0, '<span
		// class="data-price">[\s\t\n ]*?<b>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="price-items"><span>([a-zA-Z ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<div class="price-items"><span>([a-zA-Z ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, 'Precio desde[\s\t\n ]*?</span>[\s\t\n ]*?<span class="data-price">[\s\t\n ]*?<b>([a-zA-Z ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, 'Precio desde[\s\t\n ]*?</span>[\s\t\n ]*?<span class="data-price">[\s\t\n ]*?<b>([a-zA-Z ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "'mapLat': ([^\,]*),", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "'mapLng': ([^\,]*),", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Mexico";
		cityValue = get_next_regex_match(html, 0, "<div class=\"section-location\">[\s\t\n ]*?<i></i>[ ]*?<b>[^\<]*</b>,([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			cityValue = cityValue.split(',');
			if (isDefined(cityValue[1])) {
				mls.Address.City.value = cityValue[1].trim();
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-flickity-lazyload="([^\"]*)"', 1);
		if (images == "") {
			images = get_all_regex_matched(html, '<img class="mosaic-photo-[^\"]* js-openDevelopmentMultimedia" src="([^\"]*)"/>', 1);
		}
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.inmuebles24.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/departamentos-en-venta.html", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/casas-o-duplex-o-casa-en-condominio-en-venta.html", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/casa-en-condominio-en-venta.html", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/oficinas-en-venta.html", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/locales-comerciales-en-venta.html", KTIENDASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/bodegas-comerciales-en-venta.html", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/terrenos-en-venta.html", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/otros-tipos-de-propiedades-en-venta.html", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/departamentos-en-renta.html", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/casas-o-duplex-o-casa-en-condominio-en-renta.html", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/casa-en-condominio-en-renta.html", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/oficinas-en-renta.html", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/locales-comerciales-en-renta.html", KTIENDASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/bodegas-comerciales-en-renta.html", KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/terrenos-en-renta.html", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.inmuebles24.com/otros-tipos-de-propiedades-en-renta.html", KOTROSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
