qa_override("[E1473228885]", "This website does not contain lotsize areaUnits");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			// wait(4000);
			el1 = virtual_browser_find_one(browser, "(//a[@class='row collapse'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				finalUrl = virtual_browser_element_attribute(el1, "href");
				resArray.push(finalUrl);
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//a[text()='Next ?']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// if (isUndefined(browser)) {
	// browser = create_virtual_browser("HeadlessChrome");
	// if (isUndefined(browser)) {
	// return analyzeOnePublication_return_tech_issue;
	// }
	// }
	// virtual_browser_navigate(browser, url);
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	// var html = virtual_browser_html(browser);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		if (!html.includes("<h1")) {
			return analyzeOnePublication_return_innactive;
		}
		var notAvailable = get_unique_regex_match(html, '<h1 class="blue main_title_2">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			if (notAvailable == "Property Results") {
				return analyzeOnePublication_return_innactive;
			}
		}
		notAvailable = get_unique_regex_match(html, '<a href="/search" class="button_oops2">[\\s\\t\\n ]*?<span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (notAvailable.includes('TRY')) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="property_description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"mainPrice\">[\s\t\n ]*?([a-zA-Z ]+\$)([0-9\.\, ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"mainPrice\">[\s\t\n ]*?([a-zA-Z ]+\$)([0-9\.\, ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"blue\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Plot Size</div>[\\s\\t\\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "value=\"([^\"]*)\" name=\"lattitude\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "value=\"([^\"]*)\" name=\"longitude\"", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms:</div>[\\s\\t\\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms:</div>[\\s\\t\\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, "Listing type:</div>[\\s\\t\\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isDefined(for_rent)) {
			if (for_rent == "For Rent") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.MlsId = get_next_regex_match(html, 0, "ID: </div>[\s\t\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_next_regex_match(html, 0, "Listing type:</div>[\\s\\t\\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Sagicor Property Services Ltd.";
		mls.Brokerage.Phone = "(876) 929-9182-6";
		mls.Brokerage.Email = "realestate@sagicor.com";
		mls.Location.Directions = mls.ListingTitle;
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			mls.Address.Country.value = "Jamaica";
			if (total_commas == "1") {
				if (tokens.length > 0) {
					mls.Address.City.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 1) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 2].trim();
				}
			}
			if (mls.Address == undefined)
				mls.Address = {};
			if (total_commas == "2") {
				if (tokens.length > 0) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 3].trim();
				}
			} else {
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<li data-img-src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Apartment", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/Apartment", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/House", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Residential%20Lot", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Resort%20Apartment--Villa", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Townhouse", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/House", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/Townhouse", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/Resort%20Apartment--Villa", KCASASTEMPORAL);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Commercial%20Bldg--Offices", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Commercial%20Lot", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/Commercial%20Bldg--Offices", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/Commercial%20Lot", KTIENDASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Development%20Land%20(Commercial)", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Development%20Land%20(Residential)", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/rent/currency/all/property_type/Development%20Land%20(Residential)", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Factory", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Farm--Agriculture", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://sagicorproperties.com/search/sale/currency/all/property_type/Hotel", KOTROSVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
