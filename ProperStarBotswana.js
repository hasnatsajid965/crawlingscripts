var countryContext = "Botswana";

include("base/ProperStarFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Houses
		{
			try {
				// Houses for sale
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/buy/house", KHOUSEFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Houses for lease
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/rent/house", KHOUSEFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartments
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/buy/flat", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartments for rent
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/rent/flat", KFLATFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Lands
		{
			try {
				// Plots for sale
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/buy/land-plot", KLANDFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Plots for rent
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/rent/land-plot", KLANDFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Buildings for sale
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/buy/building", KBUILDINGFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Buildings for rent
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/rent/building", KBUILDINGFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
        }
        // Commercials
		{
			try {
				// Commercials for sale
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/buy/commercial", KCOUNTRYHOUSEFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Commercials for rent
				cumulatedCount += crawlCategory("https://www.properstar.co.uk/botswana/rent/commercial", KCOUNTRYHOUSEFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
