qa_override("[E2431535312]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href, '/listings/')])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//*[text()='Ver m?s']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KNOTIFYERROR)) {
						loadedMore = true;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		if (isDefined(virtual_browser_find_one(browser, "//p[text()='404']", KDONOTNOTIFYERROR))) {
			return analyzeOnePublication_return_innactive;
		}
		var element
		var imageCount = 0;
		var index = 1;
		do {
			element = virtual_browser_find_one(browser, "(//img[@src])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(element)) && (virtual_browser_element_interactable(browser, element))) {
				if (virtual_browser_element_attribute(element, "src").endsWith("?rule=progressive")) {
					var obj = JSON.parse(get_list_empty_variable("photo"));
					obj.MediaURL = virtual_browser_element_attribute(element, "src");
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
				}
			}
			index++;
		} while (element != undefined);
		element = virtual_browser_find_one(browser, "//span[@data-name='price']", KDONOTNOTIFYERROR);
		if (element != undefined) {
			var price = virtual_browser_element_text(element);
			if (price.split(' ').length > 1) {
				mls.ListPrice.value = price.split(' ')[1].trim();
				if (mls.ListPrice.value > 100) {
					mls.ListPrice.currencyCode = price.split(' ')[0].trim();
				} else {
					mls.ListPrice.value = KPRICEONDEMAND;
				}
			} else {
				if (mls.ListPrice.value > 100) {
					mls.ListPrice.value = price.trim();
				} else {
					mls.ListPrice.value = KPRICEONDEMAND;
				}
			}
		}
		mls.Address.Country.value = "Dominican Republic";
		// element = virtual_browser_find_one(browser,
		// "//p[@data-name='adview_category']", KDONOTNOTIFYERROR);
		// if (element != undefined) print("Categoria: " +
		// virtual_browser_element_text(element));
		element = virtual_browser_find_one(browser, "//p[@data-name='adview_location']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			var location = virtual_browser_element_text(element);
			if (location.split(',').length > 1) {
				mls.Address.City.value = location.split(',')[1].trim();
				if (mls.Address.StateOrProvince == undefined)
					mls.Address.StateOrProvince = {};
				mls.Address.StateOrProvince.value = location.split(',')[0].trim();
			} else {
				mls.Address.City.value = location;
			}
		}
		index = 1;
		do {
			element = virtual_browser_find_one(browser, "(//div[string-length(text()) > 0])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(element)) && (virtual_browser_element_text(element).length > 0)) {
				mls.Brokerage.Name = virtual_browser_element_text(element);
				break;
			}
			index++;
		} while (isDefined(element));
		element = virtual_browser_find_one(browser, "//h1[@data-name='adview_title']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.ListingTitle = virtual_browser_element_text(element);
		}
		if (mls.ListingTitle == "" || isUndefined(mls.ListingTitle) || mls.ListingTitle == null) {
			return analyzeOnePublication_return_unreachable;
		}
		element = virtual_browser_find_one(browser, "//button[@id='show_phone_number']", KDONOTNOTIFYERROR);
		if ((isDefined(element)) && (virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR))) {
			element = virtual_browser_find_one(browser, "//p[@data-name='contact_phone']", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Brokerage.Phone = virtual_browser_element_text(element);
			}
		}
		var curxpath = KEXCLUDEFROMKPIPREFIX + "//h2[contains(text(),'Descripci')]/following-sibling::node()";
		var description = "";
		do {
			element = virtual_browser_find_one(browser, curxpath, KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				description += "<" + virtual_browser_element_tag(element) + ">" + virtual_browser_element_text(element) + "</" + virtual_browser_element_tag(element) + ">";
				curxpath += "/following-sibling::node()";
			}
		} while (isDefined(element));
		mls.ListingDescription = description;
		element = virtual_browser_find_one(browser, "//p[text()='N?mero de Habitaciones']/parent::node()/parent::node()/following-sibling::node()/div/p", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Bedrooms = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "//p[text()='N?mero de Ba?os']/parent::node()/parent::node()/following-sibling::node()/div/p", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Bathrooms = virtual_browser_element_text(element);
		}
		if (isUndefined(mls.ListingDescription) || mls.ListingDescription == null || mls.ListingDescription == "" && mls.ListingTitle == null || isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
			return analyzeOnePublication_return_unreachable;
		}
		// element = virtual_browser_find_one(browser,
		// "//p[text()='Tipo']/parent::node()/parent::node()/following-sibling::node()/div/p",
		// KDONOTNOTIFYERROR);
		// if (element != undefined) print("Tipo: " +
		// virtual_browser_element_text(element));
		// element = virtual_browser_find_one(browser,
		// "//p[text()='Condici?n']/parent::node()/parent::node()/following-sibling::node()/div/p",
		// KDONOTNOTIFYERROR);
		// if (element != undefined) print("Condici?n: " +
		// virtual_browser_element_text(element));
		element = virtual_browser_find_one(browser, "//p[text()='Construcci?n (m?)']/parent::node()/parent::node()/following-sibling::node()/div/p", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.LivingArea.value = virtual_browser_element_text(element);
			// if (mls.LivingArea.value.length>0) {
			// mls.LivingArea.areaUnits = "m2";
			// }
		}
		element = virtual_browser_find_one(browser, "//p[contains(text(),'Solar (m')]/parent::node()/parent::node()/following-sibling::node()/div/p", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.LotSize.value = virtual_browser_element_text(element);
			// if (mls.LotSize.value.length>0) {
			// mls.LotSize.areaUnits = "m2";
			// }
		}
		element = virtual_browser_find_one(browser, "//p[contains(text(),'Solar (m')]", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			element = virtual_browser_element_text(element);
			mls.LotSize.areaUnits = get_next_regex_match(element, 0, "Solar [(]([^\<]*)[)]", 1, KDONOTNOTIFYERROR);
			// if (mls.LotSize.value.length>0) {
			// mls.LotSize.areaUnits = "m2";
			// }
		}
		element = virtual_browser_find_one(browser, "//p[text()='N?mero de Ba?os Medios']/parent::node()/parent::node()/following-sibling::node()/div/p", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			var half = virtual_browser_element_text(element);
			if ((isDefined(half)) && (mls.Bathrooms != undefined)) {
				mls.Bathrooms = Number(mls.Bathrooms) + (Number(half) * 0.5);
			} else if ((isDefined(half)) && (mls.Bathrooms == undefined)) {
				mls.Bathrooms = (Number(half) * 0.5);
			}
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-venta/casas", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-venta/apartamentos", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-venta/solares-terrenos", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-venta/oficinas-comerciales", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-venta/penthouse", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-venta/otros-inmuebles", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/apartamentos", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/casas", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/casas-vacacionales", KCASASTEMPORAL);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/oficinas-comerciales", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/solares-terrenos", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/penthouses", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.corotos.com.do/sc/inmuebles-en-alquiler/otros-inmuebles", KOTROSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
