qa_override("[E1473228885]", "Some of the properties have  areaUnits and some not.");
qa_override("[E219240453]", "I am temporary overriding this frequency error. because this website does not have any rent identification");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)" class="summary url">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<span class="current">[^\<]*</span>[\\s\\t\\n ]*?<a href="([^\"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="documentFirstHeading">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Price</dt>[\s\t\n ]*?<dd>([\$])([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, 'Price</dt>[\s\t\n ]*?<dd>([\$])([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_next_regex_match(html, 0, 'Total Lot Size</dt>[\s\t\n ]*?<dd>[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, 'Total Lot Size</dt>[\s\t\n ]*?<dd>[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_next_regex_match(html, 0, 'Total Lot Size</dt>[\s\t\n ]*?<dd class="full">[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_next_regex_match(html, 0, 'Total Lot Size</dt>[\s\t\n ]*?<dd class="full">[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 2, KDONOTNOTIFYERROR);
		}
		mls.LivingArea.value = get_next_regex_match(html, 0, 'Total Living Area</dt>[\s\t\n ]*?<dd>[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_next_regex_match(html, 0, 'Total Living Area</dt>[\s\t\n ]*?<dd>[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LivingArea.value)) {
			mls.LivingArea.value = get_next_regex_match(html, 0, 'Total Living Area</dt>[\s\t\n ]*?<dd class="full">[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 1, KDONOTNOTIFYERROR);
			mls.LivingArea.areaUnits = get_next_regex_match(html, 0, 'Total Living Area</dt>[\s\t\n ]*?<dd class="full">[^\<]*[(]([0-9\,\. ]*)([a-zA-Z\.\, ]*)[)]', 2, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, 'Number of Bedrooms</th>[\s\t\n ]*?<td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Number of Full Bathrooms</th>[\s\t\n ]*?<td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'Year Built</th>[\s\t\n ]*?<td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, 'Number of Stories</th>[\s\t\n ]*?<td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Number of Parking Spaces</th>[\s\t\n ]*?<td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="documentDescription">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<th style="width: 200px;">([^\<]*)</th>[\s\t\n ]*?<td>Yes', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="agent_info">[\s\t\n ]*?<h3 >([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, 'Phone</span>:&nbsp;([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || mls.ListPrice.value == 1) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		forRent = get_unique_regex_match(html, 'Price</dt>[\s\t\n ]*?<dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("month")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.MlsId = get_unique_regex_match(html, 'Listing ID</dt>[\s\t\n ]*?<dd>([^\<]*)([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, 'Property Type</dt>[\s\t\n ]*?<dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Ecuador";
		mls.Location.Latitude = get_next_regex_match(html, 0, "google.maps.LatLng[(]([^\,]*), ([^\;]*)[)];", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "google.maps.LatLng[(]([^\,]*), ([^\;]*)[)];", 2, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<h1 class=\"documentFirstHeading\">[^\<]*</h1>[\s\t\n ]*?<p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			address = mls.Location.Directions.split(",");
			if (isDefined(address[1])) {
				mls.Address.StateOrProvince.value = address[1];
			}
			if (isDefined(address[2])) {
				mls.Address.City.value = address[2];
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a href="([^\"]*)" data-caption="', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://mls-ecuador.com/en/residential-sales?b_start:int=0", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://mls-ecuador.com/en/residental-lease", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://mls-ecuador.com/en/land-listings", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://mls-ecuador.com/en/commercial-sales", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory("https://mls-ecuador.com/en/commercial-lease", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}

		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
