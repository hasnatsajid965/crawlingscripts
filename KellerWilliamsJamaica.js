qa_override("[E1473228885]", "Some of the properties contain lot size and some does not.");
qa_override("[E2592408747]", "Some of the properties have city and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html)
					&& (tracer = addNextToPropertiesList(html, tracer, '<div class="thumbnail recent-properties-box result_item" data-listing="[^"]*" data-mls="[^"]*" >[\\s\\t\\n ]*?<a href="([^"]*)" >', category,
							"https://www.kellerwilliamsjamaica.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="paginate_button next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.kellerwilliamsjamaica.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var list_not_available = "";
		list_not_available = get_next_regex_match(html, 0, "<div class=\"container-fluid text-center\">([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "No Listing Found") {
				return analyzeOnePublication_return_innactive;
			}
		}
		for_rent = get_next_regex_match(html, 0, "<span class=\"r-s-element\" >([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "For Rent") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<title>([^<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_next_regex_match(html, 0, "<li>[\s\t\n ]*?Lot Size:[\s\t\n ]*?([0-9\,\.]+)[\s\t\n ]*?</li>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_next_regex_match(html, 0, "Lot Size:([ 0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, "<li>[\\s\\t\\n ]*?([^<]*)Bedroom[\\s\\t\\n ]*?</li>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<li>[\\s\\t\\n ]*?([^<]*)Bathroom[\\s\\t\\n ]*?</li>", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<ul class="amenities">[\\s\\t\\n ]*?<li>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Garage") {
						mls.DetailedCharacteristics.hasGarage = true;
					}
					if (feature === "Swimming Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.startsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<h3 class="heading">[\\s\\t\\n ]*?Description(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, 'Agent Name[ ]*?-([^"]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<i class="fa fa-phone"></i>[\\s\\t\\n ]*?Tel:([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, "mailto:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = mls.Brokerage.Email.replace("\"\u003e", "");
		mls_id = get_next_regex_match(html, 0, '<span class="id-element">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = mls_id.replace("MLS-", "");
		mls.ListPrice.value = get_next_regex_match(html, 0, '<span class="p-currency">[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<span class="p-currency">[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<div class="pull-left">[\s\t\n ]*?<h3>[\s\t\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<div class="pull-left">[\s\t\n ]*?<h3>[\s\t\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'latitude[^"]*" value="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'longitude[^"]*" value="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Jamaica";
		mls.Location.Directions = get_next_regex_match(html, 0, '<div class="p-address">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_unique_regex_match(html, "<img src=\"([^\"]*)\" class=\"thumb-preview\" alt=\"([^\,]*), Black River image - 0\">", 2, KDONOTNOTIFYERROR);
		/*if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 2].trim();
				}
			}
			if (mls.Address == undefined)
				mls.Address = {};
			if (total_commas == "2") {
				// if (tokens.length > 1) {
				// mls.Address.Country.value = tokens[tokens.length-2].trim();
				// }
				if (tokens.length > 0) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 3].trim();
				}
			} else {
			}
			// StreetAdditionalInfo
		}*/
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<li data-target="#carousel-custom" class="" data-slide-to="[^"]*"  >[\\s\\t\\n ]*?<img src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.kellerwilliamsjamaica.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Apartment/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/House/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Residential%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Townhouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Commercial%20Bldg-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Commercial%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Commercial%20Spaces-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/parish/town/town/property-type/property-type/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Development%20Land%20%28Commercial%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Hotel/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.kellerwilliamsjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent-sale/parish/parish/town/town/property-type/Warehouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
