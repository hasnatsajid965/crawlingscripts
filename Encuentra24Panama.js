var countryContext = "Panama";

include("base/Encuentra24Functions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties",
						KAPTOVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/real-estate-for-sale-houses-homes?sort=f_added&dir=desc", KCASASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-buildings,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-island-properties",
						KOFICINASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties",
						KOFICINASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties",
						KTERRENOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Edificios
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-farms,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-preconstruction-new-property,real-estate-for-sale-business,real-estate-for-sale-island-properties",
						KEDIFICIOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-lots-land,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties",
						KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-offices,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties",
						KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory(
						"https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-business,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings",
						KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
