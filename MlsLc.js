qa_override("[E1473228885]", "Some properties does contain lot size and some does not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var pages = 2;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"listingtitle\"><a id=\"[^\"]*\" title=\"[^\"]*\" href=\"([^\"]*)", category, "http://www.mls.lc/RealEstate/", 1, KDONOTNOTIFYERROR, false)) > 0) {
				print("Check Tracer" + url);
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a id=\"ctl01_PageContent_ListingsPager_PageRepeater_ctl0" + pages + "_PageLink\" title=\"Page " + pages + "\" href=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://www.mls.lc/RealEstate/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
		pages++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// setProxyConditions(true, null);
	// rotateUserAgents(true);
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, 'Description</span></div>[\\s\\t\\n ]*?<div id="[^\"]*" class="col-xs-12 col-border">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "Price</span></div>[\s\t\n ]*?<div class=\"[^\"]*\"><span id=\"[^\"]*\">([\$]*)([0-9\.\, ]*)", 1, KNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "Price</span></div>[\s\t\n ]*?<div class=\"[^\"]*\"><span id=\"[^\"]*\">([\$]*)([0-9\.\, ]*)", 2, KNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || mls.ListPrice.value == 0) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Brokerage.Name = "Realty St. Lucia";
		mls.Brokerage.Phone = "758-725-4528";
		mls.Brokerage.Email = "ml@mls.lc";
		mls.ListingTitle = get_unique_regex_match(html, "<span id=\"ctl01_PageContent_Label_Title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size</span></div>[\s\t\n ]*?<div class=\"[^\"]*\"><span id=\"[^\"]*\">([0-9\.\, ]*)([a-zA-Z\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size</span></div>[\s\t\n ]*?<div class=\"[^\"]*\"><span id=\"[^\"]*\">([0-9\.\, ]*)([a-zA-Z\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, 'LatLng[(]([^\,]*),([^\;]*)[)]', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, 'LatLng[(]([^\,]*),([^\;]*)[)]', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms</span></div>[\\s\\t\\n ]*?<div class=\"ListingDisplay_Data\"><span id=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms</span></div>[\\s\\t\\n ]*?<div class=\"ListingDisplay_Data\"><span id=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = mls.Bathrooms.replace(/[^0-9,]/g, "");
		mls.Address.Country.value = "Saint Lucia";
		get_city = get_unique_regex_match(html, "<span id=\"ctl01_PageContent_CityStatePostal\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(get_city)) {
			get_city = get_city.split(",");
			if (isDefined(get_city[0])) {
				mls.Address.City.value = get_city[0];
			}
		}
		mls.MlsId = get_unique_regex_match(html, "Reference:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Category</span></b>&nbsp;[\\s\\t\\n ]*?<span id=\"[^\"]*\"><a href='[^\']*'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Rentals")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		for_rent = get_unique_regex_match(html, "Listing Type</span></div>[\\s\\t\\n ]*?<div class=\"[^\"]*\"><span id=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent.includes("Rentals")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} else {
			for_rent = get_unique_regex_match(html, "<span id=\"ctl01_PageContent_Category\"><a href='[^\']*'>([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(for_rent)) {
				if (for_rent.includes("Rentals")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}
		var features = get_all_regex_matched(html, '<div style="padding-left:10px; padding-right:25px; padding-bottom:5px;">[\\s\\t\\n ]*?<span id="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div class=\"swiper-slide\"><img src='([^\']*)'", 1, KDONOTNOTIFYERROR);
		if (isDefined(images)) {
			images.forEach(function(oneImageTag) {
				if (oneImageTag.includes("/LG/")) {
					var obj = JSON.parse(get_list_empty_variable("photo"));
					obj.MediaURL = oneImageTag
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
				}
			});
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.mls.lc/RealEstate/Search.aspx?CatId=17&ListingTypeId=1&MinBedrooms=&MinPrice=&MaxPrice=&Country=", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.mls.lc/RealEstate/Search.aspx?CatId=57&ListingTypeId=3&MinBedrooms=&MinPrice=&MaxPrice=&Country=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.mls.lc/RealEstate/Search.aspx?CatId=20&ListingTypeId=1&MinBedrooms=&MinPrice=&MaxPrice=&Country=", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Uncategory
		{
			try {
				// Uncategory en venta
				cumulatedCount += crawlCategory("http://www.mls.lc/RealEstate/Search.aspx?CatId=18&ListingTypeId=1&MinBedrooms=&MinPrice=&MaxPrice=&Country=", KUNCATEGORIZED);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
