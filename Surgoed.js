

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href=\"([^\"]*)\" class="sg3objlista">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="next page-numbers" href="([^\"]*)">Next &rarr;</a>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "dut-nl";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="sg13objt"><h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, 'class="tzobjectprijs1 sg13euro">([? ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, 'class="tzobjectprijs1 sg13euro">([? ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, 'class="tzobjectprijs1">[a-zA-Z ]*([? ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, 'class="tzobjectprijs1">[a-zA-Z ]*([? ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		forRent = get_next_regex_match(html, 0, 'class="tzobjectprijs1">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("dag")) {
				freq = "day";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (forRent.includes("week")) {
				freq = "week";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (forRent.includes("maand")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || isUndefined(mls.ListPrice.currencyCode) || mls.ListPrice.currencyCode == "" || mls.ListPrice.value == 0 || mls.ListPrice.value == ".") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Perceelopp.: </span><span class="sg3span2A">([0-9\,\.]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Perceelopp.: </span><span class="sg3span2A">([0-9\,\.]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, 'Woonopp.: </span><span class="sg3span2A">([0-9\,\.]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'Woonopp.: </span><span class="sg3span2A">([0-9\,\.]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Slaapkamers: </span><span class="sg3span3A">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Badkamers: </span><span class="sg3span3A">([^\<]*)', 1, KDONOTNOTIFYERROR);
		description = get_all_regex_matched(html, '<div class="sg3psize18">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = "";
		if (isDefined(description) && description !== "") {
			description.forEach(function(desc) {
				mls.ListingDescription += desc;
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<p class="sgmaknam"><a href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = "info@surgoed.com";
		mls.office.PhoneNumber = get_next_regex_match(html, 0, '<p class="sginfo2">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Type: </span><span class=\"sg3span1A\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "Parkeerplaatsen: </span><span class=\"sg3span3A\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, "Woonlagen: </span><span class=\"sg3span2A\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '"latitude": "([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"longitude": "([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, '<h3 class="listing-address">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Suriname";
		mls.Address.City.value = get_next_regex_match(html, 0, "\"addressLocality\": \"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '</noscript></a></li><li><a href="([^\"]*)"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.surgoed.com/en/real-estate/for-sale/houses-in-suriname/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.surgoed.com/en/real-estate/for-rent/houses-in-suriname/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.surgoed.com/en/real-estate/for-sale/plots-in-suriname/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.surgoed.com/en/real-estate/for-sale/commercial-properties-in-suriname/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.surgoed.com/en/real-estate/for-rent/commercial-properties-in-suriname/", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.surgoed.com/en/real-estate/for-rent/holiday-vacation-homes-in-suriname/", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (106 passed, 3 warning, 0 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * https://www.surgoed.com/vastgoedaanbod/huren/woningen/jaimanilaan-6/ 2. No
 * JSON was returned from javascript with page
 * https://www.surgoed.com/vastgoedaanbod/kopen/woningen/jacobstraat-4/
 * 
 * Regular-expressions/xpath: get_unique_regex_match - Parkeerplaatsen:
 * &lt;/span&gt;&lt;span class=&quot;sg3span3A&quot;&gt;([^&lt;]*) (1) (OK)
 * get_next_regex_match - &quot;longitude&quot;: &quot;([^&quot;]*)&quot; (1)
 * (OK) get_next_regex_match - &quot;latitude&quot;: &quot;([^&quot;]*)&quot;
 * (1) (OK) get_next_regex_match - &quot;addressLocality&quot;:
 * &quot;([^&quot;]*)&quot; (1) (OK) get_next_regex_match - &lt;p
 * class=&quot;sginfo2&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * Perceelopp.: &lt;/span&gt;&lt;span
 * class=&quot;sg3span2A&quot;&gt;([0-9,.]*)([^&lt;]*) (1) (OK)
 * get_unique_regex_match - Slaapkamers: &lt;/span&gt;&lt;span
 * class=&quot;sg3span3A&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * Badkamers: &lt;/span&gt;&lt;span class=&quot;sg3span3A&quot;&gt;([^&lt;]*)
 * (1) (OK) get_unique_regex_match - Type: &lt;/span&gt;&lt;span
 * class=&quot;sg3span1A&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;p class=&quot;sgmaknam&quot;&gt;&lt;a
 * href=&quot;[^&quot;]*&quot;&gt;([^&lt;]*) (1) (OK) get_next_regex_match -
 * class=&quot;tzobjectprijs1&quot;&gt;([^&lt;]*) (1) (OK) get_next_regex_match -
 * class=&quot;tzobjectprijs1 sg13euro&quot;&gt;([? ]*)([0-9,. ]*) (1) (OK)
 * get_next_regex_match - class=&quot;tzobjectprijs1&quot;&gt;[a-zA-Z ]*([?
 * ]*)([0-9,. ]*) (1) (OK) get_all_regex_matched -
 * &lt;/noscript&gt;&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;a
 * href=&quot;([^&quot;]*)&quot; (1) (OK) get_next_regex_match - &lt;div
 * class=&quot;sg13objt&quot;&gt;&lt;h1&gt;([^&lt;]*) (1) (OK)
 * get_all_regex_matched - &lt;div
 * class=&quot;sg3psize18&quot;&gt;(.+?)&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - Woonlagen: &lt;/span&gt;&lt;span
 * class=&quot;sg3span2A&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * Woonopp.: &lt;/span&gt;&lt;span
 * class=&quot;sg3span2A&quot;&gt;([0-9,.]*)([^&lt;]*) (1) (OK)
 * get_next_regex_match - &lt;h3 class=&quot;listing-address&quot;&gt;([^&lt;]*)
 * (1) (OK) HTTP response codes: HTTP200 (2/2) (OK) Json is set:
 * Franchise.Address.address-preference-order (OK) LivingArea.value (OK)
 * NumFloors (OK) Photos.photo.MediaURL (OK)
 * Brokerage.Address.address-preference-order (OK) Brokerage.Email (OK)
 * Location.Latitude (OK) PropertyType.value (OK) Location.Longitude (OK)
 * NumParkingSpaces (OK) Address.address-preference-order (OK)
 * NonMLSListingData.countryPhoneCode (OK) Address.preference-order (OK)
 * Bathrooms (OK) Address.Country.value (OK) LotSize.areaUnits (OK)
 * office.PhoneNumber (OK) Photos.photo.MediaOrderNumber (OK) ListingDescription
 * (OK) NonMLSListingData.lang (OK) Bedrooms (OK)
 * Brokerage.Address.preference-order (OK) NonMLSListingData.category (OK)
 * Address.City.value (OK) ListingTitle (OK) Brokerage.Name (OK)
 * Franchise.Address.preference-order (OK) ListPrice.currencyPeriod (OK)
 * LivingArea.areaUnits (OK) LotSize.value (OK) Intermediate JSON value set:
 * Address.Country.value (18/18) (OK) LivingArea.value (15/15) (OK)
 * LotSize.value (16/16) (OK) Address.City.value (18/18) (OK) ListPrice.value
 * (18/18) (OK) PropertyType.value (16/16) (OK) JSON value in empty JSON:
 * ListPrice.currencyPeriod (5/5) (OK) NonMLSListingData (18/18) (OK) Bathrooms
 * (16/16) (OK) Address.preference-order (18/18) (OK) ListingDescription (18/18)
 * (OK) LivingArea (15/15) (OK) LotSize.value (16/16) (OK) Address.Country
 * (18/18) (OK) Franchise.Address.address-preference-order (18/18) (OK)
 * Address.City.value (18/18) (OK) LivingArea.areaUnits (15/15) (OK) Address
 * (18/18) (OK) Brokerage.Address.preference-order (18/18) (OK) Photos (18/18)
 * (OK) Franchise (18/18) (OK) Location.Longitude (17/17) (OK)
 * Photos.photo.MediaURL (18/18) (OK) Location (17/17) (OK) PropertyType.value
 * (16/16) (OK) LotSize.areaUnits (16/16) (OK) ListingTitle (18/18) (OK)
 * Bedrooms (16/16) (OK) Franchise.Address (18/18) (OK)
 * NonMLSListingData.countryPhoneCode (18/18) (OK) Photos.photo (18/18) (OK)
 * LivingArea.value (15/15) (OK) PropertyType (16/16) (OK)
 * Franchise.Address.preference-order (18/18) (OK) office.PhoneNumber (11/11)
 * (OK) Brokerage.Name (11/11) (OK) Brokerage.Address (18/18) (OK)
 * Location.Latitude (17/17) (OK) Address.Country.value (18/18) (OK) office
 * (11/11) (OK) Address.City (18/18) (OK) ListPrice (18/18) (OK) LotSize (16/16)
 * (OK) Brokerage (18/18) (OK) Brokerage.Address.address-preference-order
 * (18/18) (OK) NonMLSListingData.lang (18/18) (OK)
 * Address.address-preference-order (18/18) (OK) Brokerage.Email (18/18) (OK)
 * Images: Images average count (20.9) (OK) [W1554112646] Proportion of image
 * URLs pointing to good location (0.9444444) (Warning) Title is set (18/18)
 * (OK) Lot size area is set (2/2) (OK) Description is set (18/18) (OK) Coding
 * style guidelines: LotSize.areaUnits and LivingArea.areaUnits must not be
 * hard-coded. (1) (OK) LotSize.value, LotSize.areaUnits, LivingArea.value,
 * LivingArea.areaUnits, ListPrice.value and ListPrice.currencyCode must not use
 * the replace function to evaluate. (1) (OK) Only Address.Country.value may
 * hard-code location, other sublocations must crawl. (1) (OK) Virtual-Browser
 * invokations must be headless. (1) (OK)
 * 
 * 
 * 1. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/prinsessestraat-104/
 * 2. https://www.surgoed.com/vastgoedaanbod/huren/woningen/picoletstraat-9/ 3.
 * https://www.surgoed.com/vastgoedaanbod/kopen/percelen/vredenburg-serie-a-169/
 * 4.
 * https://www.surgoed.com/vastgoedaanbod/kopen/zakenpanden/saramaccastraat-22-28/
 * 5.
 * https://www.surgoed.com/vastgoedaanbod/huren/vakantiewoningen/van-drimmelenlaan/
 * 6.
 * https://www.surgoed.com/vastgoedaanbod/kopen/percelen/eduard-prachtstraat-4b/
 * 7. https://www.surgoed.com/vastgoedaanbod/huren/woningen/jaimanilaan-6/ 8.
 * https://www.surgoed.com/vastgoedaanbod/huren/woningen/gele-lisweg-19/ 9.
 * https://www.surgoed.com/vastgoedaanbod/huren/woningen/brahmstraat-7/ 10.
 * https://www.surgoed.com/vastgoedaanbod/huren/woningen/granmankondrestraat-21/
 * 11. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/damboengtong-24/
 * 12. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/costerstraat-29/
 * 13. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/tonkastraat-11/ 14.
 * https://www.surgoed.com/vastgoedaanbod/kopen/woningen/koewarasan-serie-a-64/
 * 15. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/frans-madamstraat/
 * 16. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/columbiastraat-16/
 * 17. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/palm-village-29/
 * 18. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/mandarijnstraat-33/
 * 19. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/pelikaanstraat-11/
 * 20. https://www.surgoed.com/vastgoedaanbod/kopen/woningen/jacobstraat-4/
 * 
 * Bad image urls detected: 1.
 * https://www.surgoed.com/wp-content/uploads/2018/09/Columbiastraat-Koopwoning-W0560B6-Beni?s-Park-Paramaribo-Suriname-Surgoed-Makelaardij-NV-02-1024x575.jpg
 * while extracting
 * https://www.surgoed.com/vastgoedaanbod/kopen/woningen/columbiastraat-16/
 * 
 * QA executed on 28-Mar-2020 at 01:24:53 AM
 *  <<< Extraction QA Results <<< END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */
