var countryContext = "Ecuador";

include("base/EscaPeartistFunctions.js")

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=apartment&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=condo&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=apartment-complex&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=penthouse&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=villas&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=farm-and-ranch&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=land&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.escapeartist.com/realestate/advanced-search/?filter_search_action%5B%5D=sales&filter_search_type%5B%5D=lots-land&advanced_city=ecuador&advanced_area=&advanced_rooms=&advanced_bath=&price_low=&price_max=&submit=SEARCH+PROPERTIES",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
