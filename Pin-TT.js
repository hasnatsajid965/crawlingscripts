qa_override("[E1473228885]", "There is no lot data on this site");
qa_override("[E1554112646]", "I am overrriding this error becuase it is crawling images but showing proportion image error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html))
					&& (tracer = addNextToPropertiesList(html, tracer,
							"<div class=\"list-announcement__logo\">[\\s\\t\\n ]*?<img src=\"[^\"]*\" alt=\"[^\"]*\">[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<a class=\"mask\" itemprop=\"url\" href=\"([^\"]*)\"",
							category, "https://pin.tt", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "href=\"([^\"]*)\" class=\"number-list-next js-page-filter number-list-line\"", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://pin.tt" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"title-announcement\" itemprop=\"name\" id=\"ad-title\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_unique_regex_match(html, '<div class="announcement-description" itemprop="description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListPrice.value = get_unique_regex_match(html, "itemprop=\"price\" content=\"([^\"]*)\"", 1, KNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "itemprop=\"priceCurrency\" content=\"([^\"]*)\"", 1, KNOTIFYERROR) + "$";
		var for_rent = get_unique_regex_match(html, "<meta itemprop=\"position\" content=\"3\" />[\\s\\t\\n ]*?<a href=\"[^\"]*\" itemprop=\"item\">[\\s\\t\\n ]*?<span itemprop=\"name\">([^\<]*)", 1, KNOTIFYERROR) + "$";
		if (isDefined(for_rent)) {
			if (for_rent.includes("rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.MlsId = get_unique_regex_match(html, "Ad ID: <span itemprop=\"sku\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "<span class=\"key-chars\">Type</span>[\\s\\t\\n ]*?<a href=\"[^\"]*\" class=\"value-chars\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		// if(mls.PropertyType.value == )
		mls.Address.City.value = get_unique_regex_match(html, "<span itemprop=\"address\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Trinidad and Tobago";
		mls.Bedrooms = get_unique_regex_match(html, "<span class=\"key-chars\">Bedrooms</span>[\\s\\t\\n ]*?<a href=\"[^\"]*\" class=\"value-chars\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Videos = get_unique_regex_match(html, "<iframe src=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'class="wrap js-single-item__location" data-default-lat="([^\"]*)" data-default-lng="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'class="wrap js-single-item__location" data-default-lat="([^\"]*)" data-default-lng="([^\"]*)"', 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, '<p class="author-name js-online-user" data-online="online" data-user="[^\"]*" itemprop="name">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'whatsapp://send[?]phone=([^\"]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img itemprop=\"image\" src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://pin.tt/realestate/commercial-sale/", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en rent
				cumulatedCount += crawlCategory("https://pin.tt/realestate/commercial-rent/", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://pin.tt/realestate/residential-sale/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://pin.tt/realestate/residential-rent/", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://pin.tt/realestate/land-for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
