

// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			// wait(4000);
			el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href,'https://www.coldwellbankercostarica.com/property/')])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				finalUrl = virtual_browser_element_attribute(el1, "href");
				resArray.push(finalUrl);
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//*[@class='fa fa-arrow-right loadmoreprop']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// if (isUndefined(browser)) {
	// browser = create_virtual_browser("HeadlessChrome");
	// if (isUndefined(browser)) {
	// return analyzeOnePublication_return_tech_issue;
	// }
	// }
	// virtual_browser_navigate(browser, url);
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	// var html = virtual_browser_html(browser);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		if (!html.includes("<strong")) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div id=\"cbPropertyDescription\">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<strong id=\"cbPropertyPrice\">([\$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<strong id=\"cbPropertyPrice\">([\$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h2 id=\"cbPropertyTitle\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<small>Land</small><br><b id=\"cbPropertyLand\">([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<small>Land</small><br><b id=\"cbPropertyLand\">[0-9\,\.]*</b>[ ]*?<sub>/([a-zA-Z\.\, ]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<span id=\"cbPropertyBeds\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "0" && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = mls.Bedrooms.replace(/[^0-9]/g, "");
		}
		mls.Bathrooms = get_unique_regex_match(html, "<span id=\"cbPropertyBaths\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = mls.Bathrooms.replace(/[^0-9]/g, "");
		}
		for_rent = get_unique_regex_match(html, "<strong id=\"cbPropertyPrice\">[^\<]*<small>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "monthly") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (for_rent == "weekly") {
				freq = "week";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (mls.ListPrice.value < 2000) {
				freq = "week";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		var features = get_all_regex_matched(html, '<i class=\'fa fa-check\' aria-hidden=\'true\'></i><p>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Swimming Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.MlsId = get_next_regex_match(html, 0, "ID: <b id=\"cbPropertyId\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_next_regex_match(html, 0, "<span id=\"cbPropertyType\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i>&nbsp;([^\<]*)", 1, KDONOTNOTIFYERROR);
		checkTag = get_next_regex_match(html, 0, "<span class=\"[^\"]*\" id=\"cbPropertyCategory\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (mls.PropertyType.value == "Apartment" && checkTag == "Sale") {
			mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
		}
		if (mls.PropertyType.value == "Apartment" && checkTag == "Sale") {
			mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
		}
		if (mls.PropertyType.value.includes("Home") && checkTag == "Sale") {
			mls.NonMLSListingData.category = getCategory(KCASASVENTAS);
		}
		if (mls.PropertyType.value.includes("Office") && checkTag == "Sale") {
			mls.NonMLSListingData.category = getCategory(KOFICINASVENTAS);
		}
		if (mls.PropertyType.value.includes("Warehouse") && checkTag == "Sale") {
			mls.NonMLSListingData.category = getCategory(KOTROSVENTAS);
		}

		mls.Brokerage.Name = get_next_regex_match(html, 0, "<strong id=\"cbPropertyAgentName\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		;
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<span id=\"cbPropertyAgentTell\" itemprop=\"telephone\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		;
		mls.Location.Directions = get_next_regex_match(html, 0, "id=\"cbPropertyLocation\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>&nbsp;([^\<]*)", 1, KDONOTNOTIFYERROR);
		;
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "2") {
				if (isDefined(tokens[0])) {
					mls.Address.Country.value = tokens[0].trim();
				}
				if (isDefined(tokens[1])) {
					mls.Address.StateOrProvince.value = tokens[1].trim();
				}
				if (isDefined(tokens[2])) {
					mls.Address.City.value = tokens[2].trim();
				}
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "data-src=\"([^\"]*)\" class=\"item slidimg \"", 1);
		if (isUndefined(images) || images == "") {
			images = get_all_regex_matched(html, '<li data-src=\'([^\']*)\' class=\'item slidimg \'', 1);
		}
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Apartment&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Business&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Condo&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Home&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Hotel&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KEDIFICIOSVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Land&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Office%20%2F%20Retail&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Sale&propertyType%5B%5D=Warehouse&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Apartment&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Business&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KOTROSALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Condo&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Home&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Hotel&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KEDIFICIOSALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Land&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Office%20%2F%20Retail&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.coldwellbankercostarica.com/real-estate/search?function=website/property/generalSearch&limit=1%2C80&propertyCategory=Rent&propertyType%5B%5D=Warehouse&range=null%3Bnull&minPrice=&minPrice1=%2410%2C000.00&maxprice=&maxprice1=%2440%2C000%2C000.00&startfrom=1&limitrecord=80&limit=1%2C80&range=null%3Bnull&minPropertyFloor=200%2B&maxPropertyFloor=0&minBed=&minBath=&generalSearch=&minPropertyLand=200%2B&maxPropertyLand=&SquareMeters98=809200%2B&propertyState%5B%5D=&locationid=Unite..undefined",
				KOTROSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
