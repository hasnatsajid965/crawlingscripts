

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="content-thumb" href="([^"]*)">', category, "https://www.cubapropertysales.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="pager-next"><a title="[^"]*" href="([^"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.cubapropertysales.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="property-title">[\\s\\t\\n ]*?([^<]*)</h1>', 1, KNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, '<span class="col-xs-6 col-md-5 detail-field-label">Code</span>[\\s\\t\\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([^<]*)</span>', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, '<span class="col-xs-6 col-md-5 detail-field-label">Area</span>[\s\t\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([0-9\,\. ]*)([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<span class="col-xs-6 col-md-5 detail-field-label">Area</span>[\s\t\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([0-9\,\. ]*)([a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<div class="no-has">[\\s\\t\\n ]*?<i class="fa fa-times-circle"></i>([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<h4 class="property-detail-title">Property Description</h4>[\\s\\t\\n ]*?<p>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, '<span class="col-xs-6 col-md-5 detail-field-label">Year Built</span>[s\t\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="col-xs-6 col-md-7 detail-field-value">[\s\t\n ]*?<span class="amount">[\s\t\n ]*?([a-zA-Z ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="col-xs-6 col-md-7 detail-field-value">[\s\t\n ]*?<span class="amount">[\s\t\n ]*?([a-zA-Z ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(
				html,
				'Status</span>[\s\t\n ]*?<span class="col-xs-6 col-md-7 detail-field-value"><div class="field field-name-field-real-status field-type-taxonomy-term-reference field-label-hidden clearfix"><ul class="links"><li class="taxonomy-term-reference-0"><a href="/taxonomy/term/7" typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "For rent") {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.PropertyType.value = get_unique_regex_match(
				html,
				'detail-field-label">Type</span>[\\s\\t\\n ]*?<span class="col-xs-6 col-md-7 detail-field-value"><div class="field field-name-field-real-type field-type-taxonomy-term-reference field-label-hidden clearfix"><ul class="links"><li class="taxonomy-term-reference-0"><a href="/taxonomy/term/13" typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">([^<]*)</a>',
				1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<span class="col-xs-6 col-md-5 detail-field-label">Bedrooms</span>[\\s\\t\\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<span class="col-xs-6 col-md-5 detail-field-label">Bathrooms</span>[\\s\\t\\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'data-latitude="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'data-longitude="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(
				html,
				'<span class="col-xs-6 col-md-5 detail-field-label">Location</span>[^<]*<span class="col-xs-6 col-md-7 detail-field-value"><div class="field field-name-field-real-location field-type-taxonomy-term-reference field-label-hidden clearfix"><ul class="links"><li class="taxonomy-term-reference-0"><a href="[^"]*" typeof="skos:Concept" property="rdfs:label skos:prefLabel" datatype="">([^<]*)</a>',
				1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Cuba";
		mls.FullStreetAddress = get_next_regex_match(html, 0, '<span class="col-xs-6 col-md-5 detail-field-label">Address</span>[\\s\\t\\n ]*?<span class="col-xs-6 col-md-7 detail-field-value">([^<]*)</span>', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<li><a href="[^#"]*"><img src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.bahamasrealty.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Apparment en venta
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=9&gtype=11&gprice-min=&gprice-max=&garea-min=&garea-max=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rental
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=7&gtype=11&gprice-min=&gprice-max=&garea-min=&garea-max=", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=9&gtype=13&gprice-min=&gprice-max=&garea-min=&garea-max=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=9&gtype=32&gprice-min=&gprice-max=&garea-min=&garea-max=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=7&gtype=32&gprice-min=&gprice-max=&garea-min=&garea-max=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=7&gtype=13&gprice-min=&gprice-max=&garea-min=&garea-max=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=9&gtype=24&gprice-min=&gprice-max=&garea-min=&garea-max=", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=7&gtype=24&gprice-min=&gprice-max=&garea-min=&garea-max=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=9&gtype=30&gprice-min=&gprice-max=&garea-min=&garea-max=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.cubapropertysales.com/grid-no-sidebar?glocation=All&gsub-location=All&gbath=&gstatus=7&gtype=30&gprice-min=&gprice-max=&garea-min=&garea-max=", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
