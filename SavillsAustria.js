var countryText = "Austria";
include("base/SavillsFunctions.js");
function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_APT&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_ND&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KNUEVASCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_H&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_PENT&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_TH&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_V&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Others
	{
	    try {
		// Others en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_1239+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_STU&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KOTROSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date()
		.getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
	next_crawl_needed(null, false);
    }
}
