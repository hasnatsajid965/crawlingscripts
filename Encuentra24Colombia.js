var countryContext = "Colombia";

include("base/Encuentra24Functions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-apartamentos", KAPTOVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			// Casas en venta
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-casas", KCASASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Buildings
		{
			// Buildings on venta
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-edificios", KEDIFICIOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-en-islas", KTERRENOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-lotes-y-terrenos", KTERRENOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Edificios
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-oficinas", KEDIFICIOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-negocios", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-comercios", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-comercios", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-casas-y-terrenos-de-playas", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-fincas", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

