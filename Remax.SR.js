qa_override("[E1554112646]", "I am overriding this error becuase images are crawling");
function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '</div>[\s\t\n ]*?<div class="swiper-slide">[\s\t\n ]*?<a href="([^\"]*)" target="_blank" class="cursor-pointer">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<li><a href=\"([^\"]*)\">&raquo;</a>', 1, KDONOTNOTIFYERROR);
		checkRepeat.push(relativeLink);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.remax.sr" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "sr-du";
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Lot size:</td>[\s\t\n ]*?<td>([0-9\,\. ]*)([a-zA-Z\.\, ]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Lot size:</td>[\s\t\n ]*?<td>([0-9\,\. ]*)([a-zA-Z\.\, ]*)', 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, 'Living space:</td>[\s\t\n ]*?<td>([0-9\,\. ]*)([a-zA-Z\.\, ]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'Living space:</td>[\s\t\n ]*?<td>([0-9\,\. ]*)([a-zA-Z\.\, ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<i class="fal fa-heart faa-pulse animated-hover"></i>[\s\t\n ]*?</div>[\s\t\n ]*?<h2>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<p class="lead pull-right ">([^0-9]*)([ 0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p class="lead pull-right ">([^0-9]*)([ 0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.currencyCode) || mls.ListPrice.currencyCode == "") {
			mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p class="lead pull-right ">[ 0-9\,\. ]*([&euro;])', 1, KDONOTNOTIFYERROR);
		}
		freq = get_unique_regex_match(html, '<p class="lead pull-right ">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (freq.includes("mo")) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms:</td>[\s\t\n ]*?<td>([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms:</td>[\s\t\n ]*?<td>([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '</table>[\s\t\n ]*?</div>(.+?)</p>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, 'class="pull-left" style="margin-right: 10px;">[\s\t\n ]*?<strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<i class="fa fa-phone"></i><a href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '<tr><td valign="top">E&nbsp;&nbsp;</td><td valign="top"><a href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<tr><td>P&nbsp;&nbsp;</td><td>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00 || mls.ListPrice.value < 10) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, "District:</td>[\s\t\n ]*?<td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Suriname";
		mls.Location.Latitude = get_unique_regex_match(html, 'new google.maps.LatLng[(]([^\,]*),([^\;]*)[)];', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'new google.maps.LatLng[(]([^\,]*),([^\;]*)[)];', 2, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a class="item[^\"]*" href=\'([^\']*)\' rel=\'objectimages\'', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.remax.sr/en/homes/homes-for-sale.html", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.remax.sr/en/homes/homes-for-rent.html", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.remax.sr/en/lots/lots.html", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.remax.sr/en/commercial/commercial-properties-for-sale.html", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory("https://www.remax.sr/en/commercial/commercial-properties-for-rent.html", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
