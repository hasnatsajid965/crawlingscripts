qa_override("[E2431535312]", "Some of the properties have description and some not");
qa_override("[E1473228885]", "Some of the properties have lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"property_listing   \" data-link=\"([^\"]*)\">", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href='([^\']*)'><i class=\"fa fa-angle-right\">", 1, KDONOTNOTIFYERROR);
		checkRepeat.push(relativeLink);
		if (isDefined(checkRepeat)) {
			for (i = 0; i < checkRepeat.length; i++) {
				if (isDefined(checkRepeat[i - 1])) {
					if (checkRepeat[i - 1] == checkRepeat[i]) {
						return cumulatedCount;
					}
				}
			}
		}
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<div class=\"property_categs\">[\\s\\t\\n ]*?<a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"price_label[ ]*?\">([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"price_label[ ]*?\">([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		list_price = get_next_regex_match(html, 0, "<span class=\"price_label[ ]*?\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		forRent = get_next_regex_match(html, 0, "<a class=\"menu-item-link\"  href=\"#\">For ([a-zA-Z]*)</a>", 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("rent")) {
				freq = "year";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<h1 class=\"entry-title entry-prop\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(list_price) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="wpestate_property_description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.ListingDescription.includes("||")) {
			var index = mls.ListingDescription.indexOf("||"); // Gets the
																// first
			// index where a
			// space occours
			mls.ListingDescription = mls.ListingDescription.substr(0, index);
		}
		if (isDefined(list_price)) {
			if (list_price.includes("/")) {
				frequency = list_price.split("/");
				if (frequency[1].includes("Yearly") || frequency[1].includes("yearly") || frequency[1].includes("year")) {
					freq = "year";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
				if (frequency[1].includes("Monthly") || frequency[1].includes("monthly") || frequency[1].includes("month")) {
					freq = "month";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
				if (frequency[1].includes("annually") || frequency[1].includes("Annually") || frequency[1].includes("annual")) {
					freq = "year";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
			}
			if (list_price == "") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
			if (list_price.includes("monthly") || list_price.includes("Monthly") || list_price.includes("month")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (list_price.includes("yearly")) {
				freq = "year";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (list_price.includes("annually")) {
				freq = "year";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (list_price.includes("request") || list_price.includes("negotiable")) {
				mls.ListPrice.value = KPRICEONDEMAND;
			} else {
				if (mls.ListPrice.value < 50000) {
					freq = "month";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
				var patt1 = /[0-9]/g;
				if (!(mls.ListPrice.value.match(patt1))) {
					mls.ListPrice.value = KPRICEONDEMAND;
				}
			}
		}
		mls.LotSize.value = get_unique_regex_match(html, "<strong>Property Lot Size:</strong>([ 0-9\,\. ]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>Property Lot Size:</strong>([ 0-9\,\. ]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<i class="fa fa-check"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air conditioning (A/C)") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Bedrooms = get_next_regex_match(html, 0, "Bedrooms:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "<strong>Bathrooms:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "<strong>Property-id:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "TrouveMoi";
		mls.Brokerage.Phone = "(509) 3449-4032";
		mls.Brokerage.Email = "info@trouvemoirealty.com";
		mls.Location.Latitude = get_unique_regex_match(html, '"general_latitude":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '"general_longitude":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>Area:</strong> <a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.City.value)) {
			mls.Address.City.value = get_next_regex_match(html, 0, "<span class=\"adres_area\">[\\s\\t\\n ]*?<a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.Address.Country.value = "Haiti";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a href=\"([^\"]*)\" title=\"\" rel=\"prettyPhoto\" class=\"prettygalery\" > ", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=apartments&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=rental&filter_search_type%5B%5D=apartments&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES",
						KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// // Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=duplex&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=guest-house&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=house&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=townhouse&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=rental&filter_search_type%5B%5D=house&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=rental&filter_search_type%5B%5D=villa&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=hotel&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=rental&filter_search_type%5B%5D=commercial-property&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES",
						KEDIFICIOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Stores
		{
			try {
				// Stores en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=commercial-property&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES",
						KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// //Office
		{
			try {
				// Office en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=office-space&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES",
						KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=beach&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=sale&filter_search_type%5B%5D=land&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://trouvemoirealty.com/advanced-search-2/?filter_search_action%5B%5D=rental&filter_search_type%5B%5D=land&advanced_area=&bedrooms=&bathrooms=&price=&advanced_city=&property-id=&submit=SEARCH+PROPERTIES",
						KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
