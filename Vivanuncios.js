qa_override("[E2592408747]", "Some of the properties have city and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in Vivanuncios.js ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'class="href-link tile-title-text" href="([^\"]*)"', category, "https://www.vivanuncios.com.mx", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in Vivanuncios.js on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, 'class="arrows icon-right-arrow" href="([^\"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.vivanuncios.com.mx" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.LotSize.value = get_unique_regex_match(html,
				'<i class="category-icon icon-area"></i></span><div class="category-inner-container"><span class="pri-props-name">Metros Cuadrados<span class="colon">:</span></span><span class="pri-props-value">([^\<]*)<span>([a-zA-Z]*)', 1,
				KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html,
				'<i class="category-icon icon-area"></i></span><div class="category-inner-container"><span class="pri-props-name">Metros Cuadrados<span class="colon">:</span></span><span class="pri-props-value">([^\<]*)<span>([a-zA-Z]*)', 2,
				KDONOTNOTIFYERROR)
				+ get_unique_regex_match(
						html,
						'<i class="category-icon icon-area"></i></span><div class="category-inner-container"><span class="pri-props-name">Metros Cuadrados<span class="colon">:</span></span><span class="pri-props-value">[^\<]*<span>[a-zA-Z]*<span class="square">([0-9]*)',
						1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<span class="pri-props-name">Rec?mara[^\<]*s[^\<]*<span class="colon">:</span></span><span class="pri-props-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, 'Rec[^\:]*mara[^\:]*s[^\:]*: <span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.Bathrooms = get_unique_regex_match(html, '<span class="pri-props-name">Ba[^\<]*os<span class="colon">:</span></span><span class="pri-props-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, 'Ba[^\:]*os: <span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Garage<span class="colon">:</span></span><span class="pri-props-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.NumParkingSpaces)) {
			mls.NumParkingSpaces = get_unique_regex_match(html, 'Garage: <span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="description-content">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="Agent-contactName">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '"telephone":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="nd-content hidden"><h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_next_regex_match(html, 0, '<div class="revip-seo-content" style="position: absolute; left:10000px;"><h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.ListPrice.value = get_unique_regex_match(html, '</span><span><span class="value"><span class="ad-price">[\\s\\t\\n ]*?([\$]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '</span><span><span class="value"><span class="ad-price">[\\s\\t\\n ]*?([\$]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		forRent = get_unique_regex_match(html, '<span class="PriceLabel">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListingTitle)) {
			if (mls.ListingTitle.includes("Renta")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		var features = get_all_regex_matched(html, '<div class="amenities-icon[^\"]*"></div><div class="amenities-label">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.Country.value = "Mexico";
		mls.Address.City.value = get_next_regex_match(html, 0, '"addressLocality":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '"latitude":([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"longitude":([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = mls.Location.Longitude.replace("}}", "").trim();
		var imageCount = 0;
		var images;

		images = get_all_regex_matched(html, '<img class="lazyload" src="[^\"]*" data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Building
		{
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-venta-inmuebles/departamento/v1c1097a1dwp1", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Building en rent
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-renta-inmuebles/departamento/v1c1098a1pop1", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-venta-inmuebles/casa/v1c1097a1dwp1", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-venta-inmuebles/villas/v1c1097a1dwp1", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-renta-inmuebles/casa/v1c1098a1pop1", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-renta-inmuebles/villas/v1c1098a1pop1", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-venta-inmuebles/haciendas/v1c1097a1dwp1", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-venta-inmuebles/quintas/v1c1097a1dwp1", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-venta-inmuebles/vinedos/v1c1097a1dwp1", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-renta-inmuebles/haciendas/v1c1098a1pop1", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-renta-inmuebles/quintas/v1c1098a1pop1", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.vivanuncios.com.mx/s-renta-inmuebles/vinedos/v1c1098a1pop1", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in Vivanuncios.js required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (16 passed, 13 warning, 13 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S07053/ 2. No JSON
 * was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08020/ 3.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08037/ 4.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08031/ 5.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08040/ 6.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S01727/ 7. No JSON
 * was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08009/ 8.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08014/ 9.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08003/ 10.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08008/ 11.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08018/ 12.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08060/ 13.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08055/
 * 
 * Regular-expressions/xpath: get_next_regex_match - Reference
 * Number&lt;/span&gt;[\s\t\n ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*)
 * (1) (OK) get_next_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Description&lt;/span&gt;(.+?)&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - &lt;span class=&quot;label&quot;&gt;Area
 * [(]sq.m.[)]&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Bedrooms&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * Reference Number&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_next_regex_match -
 * !3d([^!]*) (1) (OK) get_next_regex_match - !2d([^!]*) (1) (OK)
 * get_unique_regex_match - Period&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Land size [(]sq.m.[)]&lt;/span&gt;[\s\t\n
 * ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK)
 * get_unique_regex_match - &lt;span class=&quot;label&quot;&gt;Price [(]US
 * [$][)]&lt;/span&gt;[\s\t\n ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*)
 * (1) (OK) get_unique_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Location&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Airconditioned&lt;/span&gt;[\s\t\n
 * ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK)
 * get_unique_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Bathrooms&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_all_regex_matched -
 * &lt;span class=&quot;label&quot;&gt;([^&lt;]*)&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;[\s\t\n ]*?Yes[\s\t\n ]*?&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - &lt;div class=&quot;wrapper&quot;&gt;[\s\t\n
 * ]*?&lt;h1&gt;([^&lt;]*) (1) (OK) HTTP response codes: HTTP200 (13/13) (OK)
 * Javascript Exceptions: ReferenceError: s is not defined (13) (Error)
 * 
 * 
 * 1. http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S07053/ 2.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08020/ 3.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08037/ 4.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08031/ 5.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08040/ 6.
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S01727/ 7.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08009/ 8.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08014/ 9.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08003/ 10.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08008/ 11.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08018/ 12.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08060/ 13.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08055/
 * 
 * QA executed on 10-Feb-2020 at 11:01:34 PM
 *  <<< Extraction QA Results <<< END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */

