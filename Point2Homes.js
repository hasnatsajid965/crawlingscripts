const useIncapsulaCookies = false;


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KNOTIFYERROR);
	while (isDefined(html)) {
		while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"item-address\" data-url=\"([^\"]*)\"", category, "https://www.point2homes.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
			tracer++;
			actualCount++;
			cumulatedCount++;
			if (passedMaxPublications()) {
				break;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		url = url.replace("&page=" + page + "&", "&page=" + (page + 1) + "&");
		html = gatherContent_url(url, KNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(true);
	var html = gatherContent_url(url, KNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^<>]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<strong>Precio</strong><br>[ \\t\\r\\n]*([A-Z$]*)[ ]?([^<>]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Superficie habitable</strong><br />[ \\t\\r\\n]*([0-9]*)([^<]*)[ \\t\\r\\n]*</p>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Superficie habitable</strong><br />[ \\t\\r\\n]*([0-9]*)([^<]*)[ \\t\\r\\n]*</p>", 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<span class=\"user-name\">([^<]*)</", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<span class=\"phone icon icon-call\">([^<]*)<div class=\"see-phone\"", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = arrayElement(extract_through_DOM_attribute_selector(html, "itemprop=description", KDOMSELECTOR_INNERHTML, KDONOTNOTIFYERROR), 0);
		if (mls.ListingDescription != undefined) {
			mls.ListingDescription = mls.ListingDescription.replaceAll("•", "\n•");
		}
		var imageCount = 0;
		var images;
		for (var pass = 0; pass <= 1; pass++) {
			if (pass == 0) {
				images = get_all_regex_matched(html, "<img[ \\t\\r\\n]*id=\"big_img\"[ \\t\\r\\n]*src=\"([^\"]*)\"", 1);
			} else if (pass == 1) {
				images = get_all_regex_matched(html, "<a[ \\t\\r\\n]*class=\"tn_img js-tn_img[^\"]*\"[ \\t\\r\\n]*data-big=\"([^\"]*)\"", 1);
			}
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Nuevos Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Condos%2FCoops&NewDevelopment=on&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KNUEVOSAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Condos%2FCoops&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Nuevos Apartamentos en alquiler
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Rental-Properties-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Condos%2FCoops&NewDevelopment=on&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KNUEVOSAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Rental-Properties-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Condos%2FCoops&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Nuevas Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&NewDevelopment=on&page=1&sort_by=DESC_listing_created&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KNUEVASCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&page=1&sort_by=DESC_listing_created&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Nuevas multi-familia en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Multi-Family&NewDevelopment=on&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KNUEVASMULTIFAMILIAVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Multi-familia en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Multi-Family&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KMULTIFAMILIAVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Nuevas Casas en alquiler
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Rental-Properties-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Homes&NewDevelopment=on&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KNUEVASCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Rental-Properties-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Homes&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Commercial-Real-Estate.html?location=Dominican+Republic&search_mode=location&page=1&sort_by=DESC_listing_created&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1",
						KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.point2homes.com/DO/Land-For-Sale.html?location=Dominican+Republic&search_mode=location&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Otros
		{
			try {
				// Otros en venta
				cumulatedCount += crawlCategory(
						"https://www.point2homes.com/DO/Real-Estate-Listings.html?location=Dominican+Republic&search_mode=location&PropertyType=Other&page=1&SelectedView=listings&LocationGeoAreaId=15&location_changed=&ajax=1", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
