qa_override("[E2431535312]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html))
					&& (tracer = addNextToPropertiesList(html, tracer, "<li class=\"([^\"]*)\" data-id=\"([^\"]*)\" data-photos=\"([^\"]*)\">(.*?)<a href=\"([^\"]*)\">", category, "https://www.supercasas.com", 5, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href=\"([^\"]*)\">&raquo;</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.supercasas.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		not_available = get_unique_regex_match(html, "<div class=\"error-title\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(not_available)) {
			if (not_available == "Anuncio no disponible") {
				return analyzeOnePublication_return_innactive;
			}
		}
		var code = get_unique_regex_match(html, "<div>Anuncio <b>#([0-9]*)</b>", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		mls.ListPrice.currencyPeriod = get_next_regex_match(html, 0, "<div id=\"detail-ad-header\">[ \\t\\r\\n]*<h2>([^<]*)</h2>[ \\t\\r\\n]*<h3>([A-Z$]*)[ ]?([^/]*)/([^<>]*)</h3>", 4, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.currencyPeriod)) {
			mls.ListPrice.currencyPeriod = undefined;
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div id=\"detail-ad-header\">[ \\t\\r\\n]*<h2>([^<]*)</h2>[ \\t\\r\\n]*<h3>([A-Z$]*)[ ]?([^<>]*)</h3>", 3, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div id=\"detail-ad-header\">[ \\t\\r\\n]*<h2>([^<]*)</h2>[ \\t\\r\\n]*<h3>([A-Z$]*)[ ]?([^<>]*)</h3>", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(mls.ListPrice.currencyPeriod);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div id=\"detail-ad-header\">[ \\t\\r\\n]*<h2>([^<]*)</h2>[ \\t\\r\\n]*<h3>([A-Z$]*)[ ]?([^/]*)/([^<>]*)</h3>", 3, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div id=\"detail-ad-header\">[ \\t\\r\\n]*<h2>([^<]*)</h2>[ \\t\\r\\n]*<h3>([A-Z$]*)[ ]?([^/]*)/([^<>]*)</h3>", 2, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<div id=\"detail-ad-header\">[ \\t\\r\\n]*<h2>([^<]*)</h2>[ \\t\\r\\n]*<h3>([A-Z$]*)[ ]?([^<>]*)</h3>", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<td><label>Terreno:</label></td>[ \\t\\r\\n]*<td>([0-9,\.]*)([^<]*?)</td>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<td><label>Terreno:</label></td>[ \\t\\r\\n]*<td>([0-9,\.]*)([^<]*?)</td>", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<td><label>Construcci.n:</label></td>[ \\t\\r\\n]*<td>([0-9,\.]*)([^<]*?)</td>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<td><label>Construcci.n:</label></td>[ \\t\\r\\n]*<td>([0-9,\.]*)([^<]*?)</td>", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<span>([0-9]*) habitaciones</span>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<span>([0-9\\.]*) ba&ntilde;os</span>", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<span>([0-9\\.]*) parqueos</span>", 1, KDONOTNOTIFYERROR);
		mls.CondoFloorNum = get_unique_regex_match(html, "<td><label>Nivel/Piso:</label></td>[ \\t\\r\\n]*<td>([0-9]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<h2>Agente</h2>(.*?)[ \\t\\r\\n]*<h3>(.*?)</h3>", 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = undefined;
		var phonesList = get_next_regex_match(html, 0, "<h2>Agente</h2>(.*?)[ \\t\\r\\n]*<h3>(.*?)</h3>[ \\t\\r\\n]*<ul>(.*?)</ul>", 3, KDONOTNOTIFYERROR);
		if (isDefined(phonesList)) {
			var phones = get_all_regex_matched(phonesList, "<li><label>(Tel|Cel|Tel 2):</label> ([^<]*)</li>", 2);
			phones.forEach(function(onephone) {
				if (mls.Brokerage.Phone == undefined) {
					mls.Brokerage.Phone = "";
				}
				if (mls.Brokerage.Phone.length > 0) {
					mls.Brokerage.Phone += "/";
				}
				mls.Brokerage.Phone += onephone;
			});
		}
		var periodicityVenta;
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span>Venta:</span> ([A-Z$]*)[ ]?([^<>/]*)/([^<>]*)<", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<span>Venta:</span> ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
		} else {
			periodicityVenta = filterToAllowedFrequency(get_next_regex_match(html, 0, "<span>Venta:</span> ([A-Z$]*)[ ]?([^<>/]*)/([^<>/])<", 3, KDONOTNOTIFYERROR));
		}
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span>Venta:</span> ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
		if ((isPublicationPurchase()) && (isDefined(mls.ListPrice.value)) && (isDefined(mls.ListPrice.currencyCode))) {
			mls.ListPrice.currencyPeriod = periodicityVenta;
		}
		var amueblado;
		var periodicityRenta;
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span>Alquiler( Amueblado)?:</span> ([A-Z$]*)[ ]?([^<>/]*)/([^<>]*)<", 3, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			amueblado = isDefined(get_next_regex_match(html, 0, "<span>Alquiler( Amueblado)?:</span> ([A-Z$]*)[ ]?([^<>/]*)/([^<>]*)<", 1, KDONOTNOTIFYERROR));
			mls.ListPrice.value = get_next_regex_match(html, 0, "<span>Alquiler( Amueblado)?:</span> ([A-Z$]*)[ ]?([^<>]*)<", 3, KDONOTNOTIFYERROR);
		} else {
			periodicityRenta = filterToAllowedFrequency(get_next_regex_match(html, 0, "<span>Alquiler( Amueblado)?:</span> ([A-Z$]*)[ ]?([^<>/]*)/([^<>/])<", 4, KDONOTNOTIFYERROR));
			amueblado = isDefined(get_next_regex_match(html, 0, "<span>Alquiler( Amueblado)?:</span> ([A-Z$]*)[ ]?([^<>/]*)/([^<>/])<", 1, KDONOTNOTIFYERROR));
		}
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span>Alquiler( Amueblado)?:</span> ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
		if ((isPublicationLease()) && (isDefined(mls.ListPrice.value)) && (isDefined(mls.ListPrice.currencyCode))) {
			mls.ListPrice.currencyPeriod = periodicityRenta;
		}
		if ((isDefined(mls.ListPrice.value)) && (isDefined(mls.ListPrice.value))) {
			var operationList = "Renta,Venta".split(",");
			if (operationList.length > 1) {
				operationList.forEach(function(oneOperation) {
					if (isPublicationPurchase()) {
						if (oneOperation == "Renta") {
							if (mls.NonMLSListingData.ListingVariations == undefined) {
								mls.NonMLSListingData.ListingVariations = [];
							}
							var obj = {};
							obj.value = ((isDefined(periodicityRenta)) ? ("ListPrice.currencyPeriod = " + periodicityRenta) + "; " : "") + "ListPrice.value = " + mls.ListPrice.value + "; ListPrice.currencyCode = " + mls.ListPrice.currencyCode
									+ "; NonMLSListingData.category = " + getAlternateCategory("LEASE");
							mls.NonMLSListingData.ListingVariations.push(obj);
						}
					} else if (isPublicationLease()) {
						if (oneOperation == "Venta") {
							if (mls.NonMLSListingData.ListingVariations == undefined) {
								mls.NonMLSListingData.ListingVariations = [];
							}
							var obj = {};
							obj.value = ((isDefined(periodicityVenta)) ? ("ListPrice.currencyPeriod = " + periodicityVenta) + "; " : "") + "ListPrice.value = " + mls.ListPrice.value + "; ListPrice.currencyCode = " + mls.ListPrice.currencyCode
									+ "; NonMLSListingData.category = " + getAlternateCategory("PURCHASE");
							mls.NonMLSListingData.ListingVariations.push(obj);
						}
					} else if (isPublicationRent()) {
					}
				});
			}
		}
		// mls.ListingDescription = get_next_regex_match(html, 0,
		// "<h3>Observaciones:</h3>[ \\t\\r\\n]*<p>([^<]*)</p>", 1,
		// KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, "<h3>Observaciones:</h3>[\\s\\t\\n ]*?<p>(.+?)</p>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span>Venta:</span>([a-zA-Z\$ ]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span>Venta:</span>([a-zA-Z\$ ]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		// if (isDefined(mls.ListingDescription)) {
		// mls.ListingDescription = mls.ListingDescription.replaceAll("?",
		// "\n?");
		// }
		mls.Address.Country.value = "Dominican Republic";
		mls.Location.Directions = get_next_regex_match(html, 0, "<td><label>Localizaci&oacute;n:</label></td>[ \\t\\r\\n]*<td[^>]*?>([^<]*?)</td>", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var tokens = mls.Location.Directions.split(",");
			if (tokens.length > 1) {
				mls.Address.StateOrProvince.value = tokens[tokens.length - 2].trim();
			}
			if (tokens.length > 0) {
				mls.Address.City.value = tokens[tokens.length - 1].trim();
			}
			if (tokens.length > 2) {
				mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 3].trim();
			}
		}
		var featuresList = get_next_regex_match(html, 0, "<h3>Comodidades:</h3>[ \\t\\r\\n]*<ul>(.*?)</ul>", 1, KDONOTNOTIFYERROR);
		if (isDefined(featuresList)) {
			var features = get_all_regex_matched(featuresList, "<li>([^<]*?)</li>", 1);
			if (isDefined(features)) {
				features.forEach(function(feature) {
					if (isDefined(feature)) {
						if (mls.DetailedCharacteristics == undefined) {
							mls.DetailedCharacteristics = {};
						}
						if (feature === "Control de Acceso") {
							mls.DetailedCharacteristics.hasGatedEntry = true;
						} else if (feature === "Piscina") {
							mls.DetailedCharacteristics.hasPool = true;
						} else {
							if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
								mls.DetailedCharacteristics.AdditionalInformation = {};
							}
							if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
								mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
							}
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				});
			}
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<li><a href=\"https://img.supercasas.com/AdsPhotos/([^\"]*)\" data-photo=\"([^\"]*)\"><img src=\"([^\"]*)\"></a></li>", 3);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Fincas
		{
			try {
				// Fincas en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=126&PriceType=400", KFINCASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=123&PriceType=400", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Penthouse en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=149&PriceType=400", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=123&PriceType=401", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Penthouse en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=149&PriceType=401", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=123&PriceType=4012", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Penthouse en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=149&PriceType=4012", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=124&PriceType=400", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=151&PriceType=400", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=124&PriceType=401", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=151&PriceType=401", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=124&PriceType=4012", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=151&PriceType=4012", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=128&PriceType=400", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Locales - Tiendas en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=128&PriceType=400", KTIENDASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Locales - Tiendas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=128&PriceType=4012", KTIENDASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Naves
		{
			try {
				// Naves en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=127&PriceType=400", KNAVESVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Naves en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=127&PriceType=401", KNAVESALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Edificios
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=148&PriceType=400", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Edificios en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=148&PriceType=401", KEDIFICIOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Edificios en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=148&PriceType=4012", KEDIFICIOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=129&PriceType=400", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=129&PriceType=401", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=129&PriceType=4012", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=125&PriceType=400", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=125&PriceType=401", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Negocios
		{
			try {
				// Negocios en venta
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=150&PriceType=400", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Negocios en alquiler
				cumulatedCount += crawlCategory("https://www.supercasas.com/buscar/?ObjectType=150&PriceType=401", KOTROSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
