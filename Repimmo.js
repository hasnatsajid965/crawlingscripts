qa_override("[E1473228885]", "Some properties has lot size and some not.");
qa_override("[E1907854190]", "Some properties have city and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<h3><a href="([^\"]*)" id="annonce_lnk_[^\"]*" itemprop="name" >', category, "http://www.repimmo.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, 'class="pageindexlink" href="([^\"]*)"  rel="nofollow" >&gt;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://www.repimmo.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_unique_regex_match(html, '<div id="container_body" ><h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span itemprop="price">([0-9\,\. ]*)([^\;]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span itemprop="price">([0-9\,\. ]*)([^\;]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			return analyzeOnePublication_return_unreachable;
		}
		rentValue = mls.ListPrice.value.replace(" ", "");
		if (rentValue < 30000) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		forRent = get_unique_regex_match(html, '<span itemprop="price">[0-9\,\. ]*[^\;]*;([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("month")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (forRent.includes("year")) {
				freq = "year";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		checkCategory = get_unique_regex_match(html, '<div class="annonce_detail_top"><h2>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(checkCategory)) {
			if (checkCategory.includes("land") && checkCategory.includes("sale")) {
				mls.NonMLSListingData.category = getCategory(KTERRENOSVENTAS);
			}
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Land</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Land</span>[^\<]*<span class="surface">([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<span class="label">Surface</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<span class="label">Surface</span>[^\<]*<span class="surface">([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<h3>Further information</h3><br />(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<div class="contact_box_identity">([^\<]*)<br />', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.office.PhoneNumber = get_next_regex_match(html, 0, '<div class="contact_box_phone">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, 'posgps_lat=\'([^\']*)\';', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, 'posgps_lng=\'([^\']*)\';', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "France";
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, "<span itemprop=\"title\">[^\<]*</span></a></li><li itemscope itemtype=\"[^\"]*\" ><a href=\"[^\"]*\" title=\"[^\"]*\" itemprop=\"url\" ><span itemprop=\"title\">([^\<]*)", 1,
				KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(
				html,
				"<span itemprop=\"title\">[^\<]*</span></a></li><li itemscope itemtype=\"[^\"]*\" ><a href=\"[^\"]*\" title=\"[^\"]*\" itemprop=\"url\" ><span itemprop=\"title\">[^\<]*</span></a></li><li itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\" ><a href=\"[^\"]*\" title=\"[^\"]*\" itemprop=\"url\" ><span itemprop=\"title\">([^\<]*)",
				1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'src="([^\"]*)" class="imageImmoProduit"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = "http://www.repimmo.com" + oneImageTag.replace("s_maison", "maison");
				} else {
					obj.MediaURL = "http://www.repimmo.com" + oneImageTag.replace("s_maison", "maison");
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}

}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=2&ctls_pays=1&ctls_cp=971&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=2&ctls_pays=1&ctls_cp=973&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=2&ctls_pays=1&ctls_cp=972&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=2&ctls_pays=1&ctls_cp=973&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=11&ctls_pays=1&ctls_cp=971&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=11&ctls_pays=1&ctls_cp=973&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=11&ctls_pays=1&ctls_cp=972&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=11&ctls_pays=1&ctls_cp=973&ctls_ville=", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=971&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=972&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=971&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=972&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=1&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=971&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=972&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=971&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=972&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=4&ctls_pays=1&ctls_cp=973&ctls_ville=", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=971&ctls_ville=", KNUEVASCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=973&ctls_ville=", KNUEVASCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=972&ctls_ville=", KNUEVASCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=973&ctls_ville=", KNUEVASCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=971&ctls_ville=", KNUEVASCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=973&ctls_ville=", KNUEVASCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=972&ctls_ville=", KNUEVASCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=3&ctls_pays=1&ctls_cp=973&ctls_ville=", KNUEVASCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}

		}

		// stores
		{
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=971&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=973&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=972&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=973&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=971&ctls_ville=", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=973&ctls_ville=", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=972&ctls_ville=", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Stores en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=5&ctls_pays=1&ctls_cp=973&ctls_ville=", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// stores
		{
			try {
				// Office en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=971&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=973&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=972&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=973&ctls_ville=", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=971&ctls_ville=", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=973&ctls_ville=", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=972&ctls_ville=", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=6&ctls_pays=1&ctls_cp=973&ctls_ville=", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Building
		{
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=971&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=973&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=972&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=973&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=971&ctls_ville=", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=973&ctls_ville=", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=972&ctls_ville=", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Buildings en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=20&ctls_pays=1&ctls_cp=973&ctls_ville=", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Land
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=971&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=973&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=972&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=1&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=973&ctls_ville=", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=971&ctls_ville=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=973&ctls_ville=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=972&ctls_ville=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en rent
				cumulatedCount += crawlCategory("http://www.repimmo.com/search_action.php?ctls_type_transaction=2&ctls_type_de_bien=9&ctls_pays=1&ctls_cp=973&ctls_ville=", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
