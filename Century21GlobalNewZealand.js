var countryContext = "New Zealand";

include("base/Century21GlobalFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/New-Zealand?subtype=Apartment", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/New-Zealand?subtype=House", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/New-Zealand?subtype=Townhouse", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/New-Zealand?subtype=Lifestyle", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/New-Zealand?subtype=Other", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
