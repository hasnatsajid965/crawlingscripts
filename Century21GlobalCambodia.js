var countryContext = "Cambodia";

include("base/Century21GlobalFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Cambodia?subtype=Flat", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Cambodia?subtype=Apartment", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Cambodia?subtype=Villa", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Cambodia?subtype=House", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Building
		{
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Cambodia?subtype=113", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
