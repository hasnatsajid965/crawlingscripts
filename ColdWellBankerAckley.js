qa_override("[E1473228885]", "Some properties do have lot data and some do not have");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" title=\"[^\"]*\"><img class=\"thumbnail\"", category, "http://www.coldwellbankerackley.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\"><i class=\"fa fa-forward\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://www.coldwellbankerackley.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// setProxyConditions(true, null);
	// rotateUserAgents(true);
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		list_not_available = get_unique_regex_match(html, '<h1 class="text-really-big p-lg pos-relative">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "404") {
				return analyzeOnePublication_return_innactive;
				;
			}
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"iq-listing-price\">([\$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"iq-listing-price\">([\$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		checkImages = get_next_regex_match(html, 0, "class=\"iq-listing-image\" style=\"background-image:url[(]'([^\']*)'[])]", 1, KDONOTNOTIFYERROR);
		images = get_all_regex_matched(html, "class=\"iq-listing-image\" style=\"background-image:url[(]'([^\']*)'[])]", 1);
		if (isUndefined(mls.ListPrice.value) && isUndefined(checkImages)) {
			return analyzeOnePublication_return_innactive;
			;
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		mls.ListingDescription = get_unique_regex_match(html, '<h3>Description</h3><p>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "MLS#</th><td class=\"col-md-9\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		title = get_unique_regex_match(html, "<h1 class=\"iq-listing-address\">(.+?)</h3>", 1, KNOTIFYERROR);
		if (isDefined(title)) {
			title = title.replace(/(<([^>]+)>)/gi, "");
			mls.ListingTitle = title.replace(/\s+/g, " ").trim();
		}
		mls.LotSize.value = get_unique_regex_match(html, "SQ Feet</th><td class=\"col-md-9\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<tr class=\"iq-listing-features-sq-foot\"><th class=\"col-md-3\" scope=\"row\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, "data-lat=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "data-lon=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.FloorCoverings = get_unique_regex_match(html, "Story[\(]s[]\)]</th><td class=\"col-md-9\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "Year</th><td class=\"col-md-9\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Puerto Rico";
		mls.Address.City.value = get_unique_regex_match(html, 'City</th><td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Coldwell Banker Ackley";
		mls.Brokerage.Phone = "407-846-4040";
		mls.Brokerage.Email = "cbarfrontdesk@gmail.com";
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		// setProxyConditions(true, null);
		// rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.coldwellbankerackley.com/real-estate/state/pr", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
