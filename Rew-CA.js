

// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'href="([^\"]*)"><div class="displaypanel-pic_overlay visible-xs">', category, "https://www.rew.ca", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="paginator-next_page paginator-control"><a rel="next" href="([^\"]*)"><i class="rewicon-play"></i>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.rew.ca" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	url = url.replace(/ /g, "");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<title>([^\<]*)| REW</title>', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size[\s\t\n ]*?</th>[\s\t\n ]*?<td>([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size[\s\t\n ]*?</th>[\s\t\n ]*?<td>([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, 'Property Description:</div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<p class="card-body card-text card-remarks">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<div class="col-xs-8 contact-modal_details">[\s\t\n ]*?<div class="clearfix">[\s\t\n ]*?<a data-tracking-url="[^\"]*" href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, 'tel:([^\"]*)', 1, KDONOTNOTIFYERROR);

		mls.ListPrice.value = get_unique_regex_match(html, '<div class="propertyheader-price">([\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<div class="propertyheader-price">([\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, 'Features</th>[\s\t\n ]*?<td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/['"]+/g, '');
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "AC") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.PropertyType.value = get_unique_regex_match(html, ">Property Type</th>[\s\t\n ]*?<td><a href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "Listing ID</th>[\s\t\n ]*?<td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "Property Age</th>[\s\t\n ]*?<td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<th>Bedrooms</th>[\s\t\n ]*?<td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<th>Bath</th>[\s\t\n ]*?<td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<li><b>L.Space</b>([ 0-9\,\. ]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<li><b>L.Space</b>([ 0-9\,\. ]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '<meta content="([^\"]*)" itemprop="latitude">', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '<meta content="([^\"]*)" itemprop="longitude">', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, 'id="city_search_query" value="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Canada";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img class="lazyload img-responsive" data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://century21aruba.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.rew.ca/properties/areas/langley-bc?only_open_house=0&only_virtual_tour=0&property_type=apartment_condo&searchable_id=154&searchable_type=Geography", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.rew.ca/properties/areas/langley-bc?only_open_house=0&only_virtual_tour=0&property_type=house&searchable_id=154&searchable_type=Geography", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.rew.ca/properties/areas/langley-bc?only_open_house=0&only_virtual_tour=0&property_type=townhouse&searchable_id=154&searchable_type=Geography", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.rew.ca/properties/areas/langley-bc?only_open_house=0&only_virtual_tour=0&property_type=mfd_mobile_home&searchable_id=154&searchable_type=Geography", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.rew.ca/properties/areas/langley-bc?only_open_house=0&only_virtual_tour=0&property_type=duplex&searchable_id=154&searchable_type=Geography", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.rew.ca/properties/areas/langley-bc?only_open_house=0&only_virtual_tour=0&property_type=land_lot&searchable_id=154&searchable_type=Geography", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
