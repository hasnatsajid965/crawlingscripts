qa_override("[E2431535312]", "Some of the properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var checkRepeat = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "href=\"([^\"]*)\" class=\"view_detail\">More Details</a>", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\">Next</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink) || relativeLink == "#") {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.PropertyType.value = get_unique_regex_match(html, "Property Type : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"price_box\"[ ]*?>([\$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"price_box\"[ ]*?>([\$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, "<h1 class=\"title_text\" >([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
			mls.ListingTitle = get_next_regex_match(html, 0, "<h1 class=\"title_text\"[ ]*?></h1><h2 class=\"location_build_up\"[ ]*?><span[ ]*?>([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, 'Property Description</span></div>[\\s\\t\\n ]*?<div class="wpl_prp_show_detail_boxes_cont" >(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, "Lot Area : <span>([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		areauntis = get_unique_regex_match(html, "Lot Area : <span>([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR) + get_unique_regex_match(html, "Lot Area : <span>[0-9\,\. ]*[a-zA-Z]*&sup([0-9]*)", 1, KDONOTNOTIFYERROR);
		if (areauntis !== "undefinedundefined") {
			mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Area : <span>([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR) + get_unique_regex_match(html, "Lot Area : <span>[0-9\,\. ]*[a-zA-Z]*&sup([0-9]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.YearBuilt = get_unique_regex_match(html, "Year Built : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<label>Amenities</label> : <span>(.+?)</span></div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.Bedrooms = get_next_regex_match(html, 0, "Bedrooms : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "Bathrooms : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "MLS Number : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Baja Properties";
		mls.Brokerage.Phone = "011+52 (624)142-0988";
		mls.Brokerage.Email = "contact@bajaproperties.com";
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, "class=\"rows location Area\">Area : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, 'ws_lat = \'([^\']*)\'', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, 'ws_lon = \'([^\']*)\'', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "Zone : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_next_regex_match(html, 0, "Country : <span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "src=\"([^\"]*)\" data-hover-title=\"Click to see gallery\"", 1);
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://bajaproperties.com/properties/?widget_id=2&kind=0&sf_unit_price=260&sf_select_property_type=19&sf_unit_field_3039=11", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://bajaproperties.com/properties/?widget_id=2&kind=0&sf_unit_price=260&sf_select_property_type=20&sf_unit_field_3039=11", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://bajaproperties.com/properties/?widget_id=2&kind=0&sf_unit_price=260&sf_select_property_type=21&sf_unit_field_3039=11", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://bajaproperties.com/properties/?widget_id=2&kind=0&sf_unit_price=260&sf_select_property_type=22&sf_unit_field_3039=11", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
