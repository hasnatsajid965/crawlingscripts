qa_override("[E2592408747]", "Some properties does not contain city.");
qa_override("[E2140387965]", "Some properties does not contain currencyCode value.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href, 'https://www.islandrealestate.com/view') and contains(text(),'details')])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "(//div[text()='Next'])[1]", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KNOTIFYERROR)) {
						loadedMore = false;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="wpp_the_content"><div class="at-above-post addthis_tool" data-url="[^\"]*"></div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "Price:</span>[ ]*?<span class=\"value\">([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "Currency:</span>[ ]*?<span class=\"value\">([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		list_price = get_next_regex_match(html, 0, "Price:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			if (mls.ListPrice.value === 0) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
			for_rent = get_unique_regex_match(html, "Rental Terms:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(for_rent)) {
				if (for_rent.includes('Per Night')) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
				} else if (for_rent.includes("Per Month")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
				}
			} else {
				for_rent = get_unique_regex_match(html, "Listing:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
				if (for_rent.includes("Long-term Rental")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
				}
			}
		} else {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"property-title entry-title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size:</span>[ ]*?<span class=\"value\">([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size:</span>[ ]*?<span class=\"value\">([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Total Area:</span>[ ]*?<span class=\"value\">([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "Total Area:</span>[ ]*?<span class=\"value\">([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		}
		mls.Location.Latitude = get_unique_regex_match(html, '"latitude":"([^\"]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'longitude":"([^\"]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "Island:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = mls.Address.Country.value.replace('&nbsp;', '');
		mls.Address.City.value = get_unique_regex_match(html, "Locale:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = mls.Address.City.value.replace('&nbsp;', '');
		if (isUndefined(mls.Address.City.value)) {
			mls.Address.City.value = get_unique_regex_match(html, "City/Town:</span> <span class=\"value\">([^a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.Address.FullStreetAddress = get_unique_regex_match(html, "Location:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Franchise.Name = get_unique_regex_match(html, "Agency Name::</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "Agent Name::</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = mls.Brokerage.Name.replace('&nbsp;', '');
		mls.Brokerage.Phone = get_unique_regex_match(html, "Telephone:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = mls.Brokerage.Phone.replace('&nbsp;', '');
		mls.PropertyType = get_unique_regex_match(html, "Property:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType = mls.PropertyType.replace('&nbsp;', '');
		mls_id = get_unique_regex_match(html, "MLS ID:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = mls_id.replace(/[^0-9,]/g, "");
		mls_number = get_unique_regex_match(html, "Property ID:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsNumber = mls_number.replace('&nbsp;', '');
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div class=\"sidebar_gallery_item\">[ ]*?<a href=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			oneImageTag = oneImageTag.replace("-600x450", "");
			oneImageTag = oneImageTag.replace("-600x400", "");
			oneImageTag = oneImageTag.replace("-600x424", "");
			oneImageTag = oneImageTag.replace("-600x405", "");
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Apartment&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=Long-term+Rental&wpp_search%5Btype_of_property%5D=Apartment&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Commercial&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Condo&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Townhome&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=Long-term+Rental&wpp_search%5Btype_of_property%5D=House-Villa&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Private+Island&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Condo+Hotel&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Hotel&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.islandrealestate.com/view/?wpp_search%5Bpagination%5D=on&wpp_search%5Bper_page%5D=10&wpp_search%5Bstrict_search%5D=false&wpp_search%5Bproperty_type%5D=apartment%2Ccommercial_property%2Ccondo%2Ccondo_hotel%2Ccottage%2Cfractional%2Chotel%2Chouse__villa%2Cland%2Cprivate_island%2Cresort_property%2Ctimeshare%2Ctownhome%2Crandom_525&wpp_search%5Bmls_id%5D=&wpp_search%5Bisland%5D=-1&wpp_search%5Blisting_type%5D=For+Sale&wpp_search%5Btype_of_property%5D=Cottage&wpp_search%5Bprice%5D%5Bmin%5D=&wpp_search%5Bprice%5D%5Bmax%5D=",
				KOTROSVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
