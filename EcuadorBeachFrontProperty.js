qa_override("[E1473228885]", "Some properties have lotsize and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html)
					&& (tracer = addNextToPropertiesList(html, tracer, '<a title="Property details" href=\"([^\"]*)\" class=\"os-propertytitle\">', category, "https://www.ecuadorbeachfrontproperty.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="pager-next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_unique_regex_match(html, '<div class="span12 property-title" style="border-left:10px solid #eb9a03;">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="status-price" id="currency_div">[\s\t\n ]*?([\&#36;]+)([ 0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="status-price" id="currency_div">[\s\t\n ]*?([\&#36;]+)([ 0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Lot Size:[\s\t\n ]*?<span>[\s\t\n ]*?([0-9\,\. ]+)&nbsp;([a-zA-Z\,\.]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Lot Size:[\s\t\n ]*?<span>[\s\t\n ]*?([0-9\,\. ]+)&nbsp;([a-zA-Z\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedroom:[\s\t\n ]*?<span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathroom:[\s\t\n ]*?<span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Parking:[\s\t\n ]*?<span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, 'Floors:[ ]*?</strong><span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<div class="span4">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Swimming Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air Conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						if (obj !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					if (obj !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="os_property_content clearfix">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Ecuador Beachfront Property S.A.";
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.YearBuilt = get_unique_regex_match(html, " Built on: ([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Category:[ ]*?</strong><span><a href='[^\']*'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, 'new google.maps.LatLng[\(]([^\,]*), ([^\;]*)[\)]', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, 'new google.maps.LatLng[\(]([^\,]*), ([^\;]*)[\)]', 2, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Ecuador";
		mls.Location.Directions = get_unique_regex_match(html, "<span class=\"address_details\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			address = mls.Location.Directions.split(",");
			if (isDefined(address[1])) {
				mls.Address.City.value = address[1].trim();
			}
			if (isDefined(address[2])) {
				mls.Address.StateOrProvince.value = address[2].trim();
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'class="propertyphotogroup" href="([^\"]*)"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=3&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=country%3A55_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=4&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&currency_item=&live_site=https%3A%2F%2Fwww.ecuadorbeachfrontproperty.com%2Flistings%2F&process_element=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=catid%3A3_type%3A1_type%3A1_country%3A55_max_price%3A1000000_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=6&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&currency_item=&live_site=https%3A%2F%2Fwww.ecuadorbeachfrontproperty.com%2Flistings%2F&process_element=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=catid%3A4_type%3A1_type%3A1_country%3A55_max_price%3A1000000_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Land for sale
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=7&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&currency_item=&live_site=https%3A%2F%2Fwww.ecuadorbeachfrontproperty.com%2Flistings%2F&process_element=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=catid%3A6_type%3A1_type%3A1_country%3A55_max_price%3A1000000_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=12&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&currency_item=&live_site=https%3A%2F%2Fwww.ecuadorbeachfrontproperty.com%2Flistings%2F&process_element=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=catid%3A9_type%3A1_type%3A1_country%3A55_max_price%3A1000000_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=7&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&currency_item=&live_site=https%3A%2F%2Fwww.ecuadorbeachfrontproperty.com%2Flistings%2F&process_element=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=catid%3A12_type%3A1_type%3A1_country%3A55_max_price%3A1000000_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others for Sale
		{
			try {
				// Othera en venta
				cumulatedCount += crawlCategory(
						"https://www.ecuadorbeachfrontproperty.com/listings/index.php/browse-properties/advanced-search?category_ids%5B%5D=11&property_type=1&min_price=0&max_price=1000000&keyword=&sortby=a.id&orderby=desc&address=&state_id=&postcode=&se_geoloc=&radius_search=5&nbath=&nbed=&nfloors=&sqft_min=&sqft_max=&lotsize_min=&lotsize_max=&advfieldLists=&currency_item=&live_site=https%3A%2F%2Fwww.ecuadorbeachfrontproperty.com%2Flistings%2F&process_element=&option=com_osproperty&task=property_advsearch&show_more_div=0&Itemid=225&search_param=catid%3A8_type%3A1_type%3A1_country%3A55_max_price%3A1000000_sortby%3Aa.id_orderby%3Adesc&list_id=0&adv_type=1&show_advancesearchform=1&advtype_id_1=&advtype_id_2=&advtype_id_3=&advtype_id_4=&advtype_id_5=&advtype_id_6=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
