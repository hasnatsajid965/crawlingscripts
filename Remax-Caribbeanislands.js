function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" class=\"underline underline2\">", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" class=\"next\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		// list_price = get_next_regex_match(html, 0, "<div
		// class=\"details-parameters-price\" onclick=\"[^\"]*\">([^\<]*)", 1,
		// KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"details-parameters-price\" onclick=\"[^\"]*\">[\s\t\n ]*?([\$ ]+)([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"details-parameters-price\" onclick=\"[^\"]*\">[\s\t\n ]*?([\$ ]+)([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);

		mls.ListingTitle = get_unique_regex_match(html, "<div class=\"details-title pull-left\">[\\s\\t\\n ]*?<h3>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<p class="details-desc" style="text-align:justify;">[\\s\\t\\n ]*?<strong>Listing ID:</strong>[^\<]*(.+?)</div>', 1, KDONOTNOTIFYERROR);
		// if (isDefined(list_price)) {
		// mls.ListPrice.value = list_price.replace(/[^0-9,]/g, "");
		// mls.ListPrice.currencyCode = list_price.replace(/[^a-zA-Z\$]/g, "");
		// }
		mls.LotSize.value = get_unique_regex_match(html,
				"Lot size:</div>[\s\t\n ]*?<div class=\"clearfix\"></div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class=\"details-parameters-cont\">[\s\t\n ]*?<div class=\"details-parameters-val\" style=\"width:100%!important;\">([0-9\.\, ]*)([^\<]*)", 1,
				KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html,
				"Lot size:</div>[\s\t\n ]*?<div class=\"clearfix\"></div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class=\"details-parameters-cont\">[\s\t\n ]*?<div class=\"details-parameters-val\" style=\"width:100%!important;\">([0-9\.\, ]*)([^\<]*)", 2,
				KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "Year built</div>[\\s\\t\\n ]*?<div class=\"details-parameters-val\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms</div>[\\s\\t\\n ]*?<div class=\"details-parameters-val\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms</div>[\\s\\t\\n ]*?<div class=\"details-parameters-val\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.DetailedCharacteristics.FloorCoverings = get_unique_regex_match(html, "Stories/Floors</div>[\\s\\t\\n ]*?<div class=\"details-parameters-val\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "Listing ID:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<h3 class=\"title-negative-margin\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'mapInit[\(]([^\,]*),([^\,]*),', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'mapInit[\(]([^\,]*),([^\,]*),', 2, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<h3>[^\<]*<span class=\"special-color\">.</span></h3>[\\s\\t\\n ]*?<br />[\\s\\t\\n ]*?<h4>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var tokens = mls.Location.Directions.split(",");
			if (tokens.length > 0) {
				mls.Address.City.value = tokens[tokens.length - 2].trim();
			}
			if (tokens.length > 1) {
				mls.Address.Country.value = tokens[tokens.length - 1].trim();
			}
		}
		var features = get_all_regex_matched(html, '<i class="jfont[ ]*?">&#xe815;</i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air conditioning (A/C)") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "class=\"slide-bg swiper-lazy\" data-background=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (oneImageTag !== "/Content/nslide/2.jpg") {
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			}
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.remax-caribbeanislands.com/searchByCountry/st-vincent-and-the-grenadines/FilterByState/All/HavingLocations/All/typeOf/Apartment_Condo/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sales%20and%20Leases",
						KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.remax-caribbeanislands.com/searchByCountry/st-vincent-and-the-grenadines/FilterByState/All/HavingLocations/All/typeOf/Multi-family-(duplexx123x-triplexx123x-aptpontintx-buildingsx123x-etcpontintx)/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sales%20and%20Leases",
						KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.remax-caribbeanislands.com/searchByCountry/st-vincent-and-the-grenadines/FilterByState/All/HavingLocations/All/typeOf/House_Villa/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sales%20and%20Leases",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en venta
				cumulatedCount += crawlCategory(
						"https://www.remax-caribbeanislands.com/searchByCountry/st-vincent-and-the-grenadines/FilterByState/All/HavingLocations/All/typeOf/Townhouse/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sales%20and%20Leases",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.remax-caribbeanislands.com/searchByCountry/st-vincent-and-the-grenadines/FilterByState/All/HavingLocations/All/typeOf/Commercial-building-or-spacex123x-officex123x-warehousex123x-hospitalityx123x-etcpontintx/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sales%20and%20Leases",
						KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.remax-caribbeanislands.com/searchByCountry/st-vincent-and-the-grenadines/FilterByState/All/HavingLocations/All/typeOf/Lot_Land/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sales%20and%20Leases",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
