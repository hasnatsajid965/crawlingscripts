qa_override("[E1473228885]", "Some of the properties have lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" class=\"blue-button\">View Details", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" aria-label=\"Next page\">Next", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.belizeproperty.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		// mls.PropertyType.value = get_unique_regex_match(html, "Property
		// Type</option>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, "class=\"small-12 medium-8 large-9 column main-content\">[\\s\\t\\n ]*?<div><h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "ID #</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<strong>Price: </strong>([\$a-zA-Z ]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<strong>Price: </strong>([\$a-zA-Z ]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		if (mls.ListPrice.value == "0") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<p><p><strong>(.+?)<p></p>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, '<p><h1><strong>(.+?)<p></p>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, '</strong></p>[\s\t\n ]*?<p><strong>(.+?)<p><div class=\'embed-container\'>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription) || mls.ListingDescription == "") {
			mls.ListingDescription = get_unique_regex_match(html, '<h1>Property Information for ID[^\<]*</h1>(.+?)<p></p>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription) || mls.ListingDescription == "") {
			mls.ListingDescription = get_unique_regex_match(html, '<p><p><strong>(.+?)</p>[\s\t\n ]*?</p>', 1, KDONOTNOTIFYERROR);
		}
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>Lot Size</strong>:([ 0-9x\,\.,\/ ]*)([a-zA-Z\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<strong>Lot Size</strong>:([ 0-9x\,\.,\/ ]*)([a-zA-Z\. ]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li>([^\<]*)<br /></li>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Swimming Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Bedrooms = get_next_regex_match(html, 0, "Beds</strong>:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "Baths</strong>:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.FloorCoverings = get_next_regex_match(html, 0, "Floors</strong>:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "Agent:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = "1-250-361-7159";
		mls.Address.City.value = get_next_regex_match(html, 0, "City/Town/Village</strong>:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Belize";
		mls.Location.Latitude = get_unique_regex_match(html, '!3d([^\!]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '!2d([^\!]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img src='([^\']*)' alt='' data-caption='' />", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Condos&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Homes&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Beach+%26+Lagoon+Lots&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Farm+%26+River+Property&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Development+Property&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Residential+Lots&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Business+For+Sale&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Income+Property&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.belizeproperty.com/index.php?main_category_or%5B%5D=Commercial+Property&price-min=&price-max=&district_or%5B%5D=&city_town_village_or%5B%5D=&neighborhood_or%5B%5D=&action=searchresults",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
