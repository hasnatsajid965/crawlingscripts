

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in Lamudi.js ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html)
					&& (tracer = addNextToPropertiesList(html, tracer, 'href="([^\"]*)"[\s\t\n ]*?title="[^\"]*"[\s\t\n ]*?target="_blank"[\s\t\n ]*?data-sku="[^\"]*"[\s\t\n ]*?data-position="[^\"]*"[\s\t\n ]*?class="js-listing-link"', category, "",
							1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in Lamudi.js on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a href="([^\"]*)">[\s\t\n ]*?<span class="icon-navigate-right">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.LotSize.value = get_unique_regex_match(html, '<i class="icon-land-size"></i>[\s\t\n ]*?<span class="Overview-attribute">[\s\t\n ]*?([0-9\,\. ]+)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<i class="icon-land-size"></i>[\s\t\n ]*?<span class="Overview-attribute">[\s\t\n ]*?([0-9\,\. ]+)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Rec[^\<]*maras[\s\t\n ]*?</div>[\s\t\n ]*?<div class="last">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Ba[^\<]*os[\s\t\n ]*?</div>[\s\t\n ]*?<div class="last">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, 'Estacionamientos[\s\t\n ]*?</div>[\s\t\n ]*?<div class="last">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="ViewMore-text-description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.ListingDescription.includes("FEATURES")) {
			var index = mls.ListingDescription.indexOf("FEATURES");
			mls.ListingDescription = mls.ListingDescription.substring(0, index);
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="Agent-contactName">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="Header-title-block small-12 columns">[\\s\\t\\n ]*?<h1>([^\<]*)', 1, KNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="Overview-main FirstPrice">([\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="Overview-main FirstPrice">([\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		forRent = get_unique_regex_match(html, '<span class="PriceLabel">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent) || forRent !== "") {
			if (forRent == "Mensual") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, 'listing_city": "([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Mexico";
		mls.Location.Latitude = get_next_regex_match(html, 0, '"location_latitude": "([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"location_longitude": "([^\"]*)"', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;

		images = get_all_regex_matched(html, '<img class="swiper-lazy"[\\s\\t\\n ]*?data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("774x491", "1024x991");
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apaprtment
		{
			try {
				// Apaprtment en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/departamento/for-sale/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Apaprtment en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/departamento/for-sale/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Apaprtment en rent
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/departamento/for-rent/", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Apaprtment en rent
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/departamento/for-rent/", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Office
		{
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/offices/for-sale/", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/offices/for-sale/", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/offices/for-rent/", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/offices/for-rent/", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/casa/for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/casa/for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/casa/for-rent/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/casa/for-rent/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/terreno/for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/terreno/for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/terreno/for-rent/", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/terreno/for-rent/", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/comercial/for-sale/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/comercial/for-sale/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/comercial/for-rent/", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/comercial/for-rent/", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/foreclosures/for-sale/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/foreclosures/for-sale/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.mx/foreclosures/for-rent/", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://www.lamudi.com.co/foreclosures/for-rent/", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in Lamudi.js required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (16 passed, 13 warning, 13 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S07053/ 2. No JSON
 * was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08020/ 3.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08037/ 4.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08031/ 5.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08040/ 6.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S01727/ 7. No JSON
 * was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08009/ 8.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08014/ 9.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08003/ 10.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08008/ 11.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08018/ 12.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08060/ 13.
 * No JSON was returned from javascript with page
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08055/
 * 
 * Regular-expressions/xpath: get_next_regex_match - Reference
 * Number&lt;/span&gt;[\s\t\n ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*)
 * (1) (OK) get_next_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Description&lt;/span&gt;(.+?)&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - &lt;span class=&quot;label&quot;&gt;Area
 * [(]sq.m.[)]&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Bedrooms&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * Reference Number&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_next_regex_match -
 * !3d([^!]*) (1) (OK) get_next_regex_match - !2d([^!]*) (1) (OK)
 * get_unique_regex_match - Period&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Land size [(]sq.m.[)]&lt;/span&gt;[\s\t\n
 * ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK)
 * get_unique_regex_match - &lt;span class=&quot;label&quot;&gt;Price [(]US
 * [$][)]&lt;/span&gt;[\s\t\n ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*)
 * (1) (OK) get_unique_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Location&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_unique_regex_match -
 * &lt;span class=&quot;label&quot;&gt;Airconditioned&lt;/span&gt;[\s\t\n
 * ]*?&lt;div class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK)
 * get_unique_regex_match - &lt;span
 * class=&quot;label&quot;&gt;Bathrooms&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;([^&lt;]*) (1) (OK) get_all_regex_matched -
 * &lt;span class=&quot;label&quot;&gt;([^&lt;]*)&lt;/span&gt;[\s\t\n ]*?&lt;div
 * class=&quot;value&quot;&gt;[\s\t\n ]*?Yes[\s\t\n ]*?&lt;/div&gt; (1) (OK)
 * get_unique_regex_match - &lt;div class=&quot;wrapper&quot;&gt;[\s\t\n
 * ]*?&lt;h1&gt;([^&lt;]*) (1) (OK) HTTP response codes: HTTP200 (13/13) (OK)
 * Javascript Exceptions: ReferenceError: s is not defined (13) (Error)
 * 
 * 
 * 1. http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S07053/ 2.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08020/ 3.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08037/ 4.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08031/ 5.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08040/ 6.
 * http://www.arubarealestate.com/aruba-houses-for-sale/HSE-S01727/ 7.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08009/ 8.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08014/ 9.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08003/ 10.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08008/ 11.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08018/ 12.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08060/ 13.
 * http://www.arubarealestate.com/aruba-vacation-homes-for-rent/VAC-R08055/
 * 
 * QA executed on 10-Feb-2020 at 11:01:34 PM
 *  <<< Extraction QA Results <<< END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */

