var countryText = "Venezuela";
include("base/LuxuryEstateFunctions.js");

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Appartment
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=41", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=27", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=37", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=37", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=39", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=39", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=29", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=29", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Lands
	{
       try {
		// Lands en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=45", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Lands en rent
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=45", KTERRENOSALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Offices
	{
	    try {
		// Offices en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=47", KOFICINASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Offices en rent
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=47", KOFICINASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Buildings
	{
	    try {
		// Buildings en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=19", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Buildings en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=19", KEDIFICIOSALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Buildings en venta
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/venezuela?sort=relevance&types=43", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Buildings en rent
		cumulatedCount += crawlCategory("https://www.luxuryestate.com/rent/venezuela?sort=relevance&types=43", KEDIFICIOSALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
