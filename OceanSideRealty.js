qa_override("[E2592408747]", "There is no city data on this website.");


// crawlForPublications crawl-mode: Virtual Browser Crawling

function crawlCategory(browser, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = virtual_browser_html(browser);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" title=\"([^\"]*)\" class=\"ViewListingLink\">", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		cumulatedCount += actualCount;
		var foundIt = false;
		if (actualCount > 0) {
			print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
			if (passedMaxPublications()) {
				break;
			}
			var element = virtual_browser_find_one(browser, "//a[starts-with(text(),'Next')]", KDONOTNOTIFYERROR);
			if (element != undefined) {
				if (virtual_browser_click_element(browser, element, KNOTIFYERROR)) {
					html = virtual_browser_html(browser);
					tracer = 0;
					actualCount = 0;
					page++;
					foundIt = true;
				}
			}
		}
		if (!foundIt) {
			return cumulatedCount;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		if (!html.includes("h3")) {
			return analyzeOnePublication_return_unreachable;
		}
		var dVideo = get_unique_regex_match(html, "<a href=\"([^\"]*)\" rel=\"([^\"]*)\"[ \\t\\r\\n]?window_props=\"([^\"]*)\">Virtual Tour</a>", 1, KDONOTNOTIFYERROR);
		if (isDefined(dVideo)) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = "https://www.ocean-side-realty.com" + dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		mls.ListingDescription = get_next_regex_match(html, 0, "<h3 class=\"listing-sec-title\">Description</h3>[ \\t\\r\\n]*<div>[ \\t\\r\\n]*(.*?)</div>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span itemprop=\"price\">([A-Z$]*)[ ]?([0-9,\\.]*)</span>", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span itemprop=\"priceCurrency\">([A-Z$]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size: <b>([0-9,\\.]*?)([^<])</b>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size: <b>([0-9,\\.]*?)([^<])</b>", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, ">Size: <b>([0-9,\\.]*?)([^<])</b>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, ">Size: <b>([0-9,\\.]*?)([^<])</b>", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, ">Bedrooms: <b>([0-9]*?)[ ]?</b>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, ">Bathrooms: <b>([0-9]*?)[ ]?</b>", 1, KDONOTNOTIFYERROR);
		var code = get_unique_regex_match(html, "MLS #: <b>([^<]*)</b>", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		var half = get_unique_regex_match(html, ">Half Bathrooms: <b>([0-9]*?)[ ]?</b>", 1, KDONOTNOTIFYERROR);
		if ((isDefined(half)) && (mls.Bathrooms != undefined)) {
			mls.Bathrooms = Number(mls.Bathrooms) + (Number(half) * 0.5);
		} else if ((isDefined(half)) && (mls.Bathrooms == undefined)) {
			mls.Bathrooms = (Number(half) * 0.5);
		}
		// mls.NumFloors = get_unique_regex_match(html, "<td>Levels:
		// ([0-9]*?)</td>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<span itemprop=\"name\" class=\"contact-agent-name\">([^<]*?)</span>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<span itemprop=\"telephone\" class=\"contact-agent-phone\">([^<]*?)[ ]?<em>\\(Cell\\)</em></span>", 1, KDONOTNOTIFYERROR);
		var otherNumbers = get_all_regex_matched(html, "<span itemprop=\"telephone\" class=\"contact-agent-phone\">([^<]*?)[ ]?<em>\\(Office\\)</em></span>", 1, false);
		var curNumbers = mls.Brokerage.Phone;
		if (isDefined(otherNumbers)) {
			for ( var oneNumber in otherNumbers) {
				if (isDefined(curNumbers)) {
					curNumbers += "/" + otherNumbers[oneNumber];
				} else {
					curNumbers = otherNumbers[oneNumber];
				}
			}
		}
		mls.Brokerage.Phone = curNumbers;
		mls.Address.Country.value = "Dominican Republic";
		mls.Address.City.value = get_unique_regex_match(html, "<span itemprop=\"addressLocality\">([^<,]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.Address.PostalCode.value = get_unique_regex_match(html, "<span itemprop=\"postalCode\">([^<,]*)</span>", 1, KDONOTNOTIFYERROR);
		var index = 1;
		while (true) {
			var view = virtual_browser_find_one(browser, "//h4[text()=\"View\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(view)) {
				if (mls.DetailedCharacteristics.ViewTypes == undefined) {
					mls.DetailedCharacteristics.ViewTypes = {};
				}
				if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
					mls.DetailedCharacteristics.ViewTypes.ViewType = [];
				}
				var obj = {};
				obj.value = virtual_browser_element_text(view);
				mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
			} else {
				break;
			}
			index++;
		}
		index = 1;
		while (true) {
			var appliance = virtual_browser_find_one(browser, "//h4[text()=\"Appliances\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(appliance)) {
				var obj = JSON.parse(get_list_empty_variable("appliance"));
				obj.value = virtual_browser_element_text(appliance);
				if (mls.DetailedCharacteristics.Appliances.appliance == undefined) {
					mls.DetailedCharacteristics.Appliances.appliance = [];
				}
				mls.DetailedCharacteristics.Appliances.appliance.push(obj);
			} else {
				break;
			}
			index++;
		}
		index = 1;
		while (true) {
			var cooling = virtual_browser_find_one(browser, "//h4[text()=\"Cooling\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(cooling)) {
				var obj = JSON.parse(get_list_empty_variable("coolingSystem"));
				obj.value = virtual_browser_element_text(cooling);
				if (mls.DetailedCharacteristics.CoolingSystems.coolingSystem == undefined) {
					mls.DetailedCharacteristics.CoolingSystems.coolingSystem = [];
				}
				mls.DetailedCharacteristics.CoolingSystems.coolingSystem.push(obj);
			} else {
				break;
			}
			index++;
		}
		index = 1;
		while (true) {
			var exterior = virtual_browser_find_one(browser, "//h4[text()=\"Exterior Finish\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(exterior)) {
				var obj = JSON.parse(get_list_empty_variable("exteriorType"));
				obj.value = virtual_browser_element_text(exterior);
				if (mls.DetailedCharacteristics.ExteriorTypes.exteriorType == undefined) {
					mls.DetailedCharacteristics.ExteriorTypes.exteriorType = [];
				}
				mls.DetailedCharacteristics.ExteriorTypes.exteriorType.push(obj);
			} else {
				break;
			}
			index++;
		}
		index = 1;
		while (true) {
			var feature = virtual_browser_find_one(browser, "//h4[text()=\"Interior Features\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(feature)) {
				if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
					mls.DetailedCharacteristics.AdditionalInformation = {};
				}
				if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
					mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
				}
				var obj = {};
				obj.value = virtual_browser_element_text(feature);
				mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
			} else {
				break;
			}
			index++;
		}
		index = 1;
		while (true) {
			var feature = virtual_browser_find_one(browser, "//h4[text()=\"Lot Features\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(feature)) {
				if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
					mls.DetailedCharacteristics.AdditionalInformation = {};
				}
				if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
					mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
				}
				var obj = {};
				obj.value = virtual_browser_element_text(feature);
				mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
			} else {
				break;
			}
			index++;
		}
		index = 1;
		while (true) {
			var feature = virtual_browser_find_one(browser, "//h4[text()=\"Extra Features\"]/../ul/li[" + index + "]", KDONOTNOTIFYERROR);
			if (isDefined(feature)) {
				if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
					mls.DetailedCharacteristics.AdditionalInformation = {};
				}
				if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
					mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
				}
				var obj = {};
				obj.value = virtual_browser_element_text(feature);
				mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
			} else {
				break;
			}
			index++;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img alt=\"([^\"]*)\" data-fullsrc=\"([^\"]*)\" data-src=\"([^\"]*)\" data-thumbsrc=\"([^\"]*)\"", 2);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	return crawlCategory(browser, category);
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/HOUSES__VILLAS/page_1944160.html", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/CONDOS__APARTMENTS/page_1944165.html", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/Dominican_Republic_Land_for_Sale/page_2140636.html", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/HOTELS__BUSINESSES/page_2140610.html", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/Beachfront_Properties/page_2251703.html", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/Rentals_and_Relocations/page_2215628.html", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.ocean-side-realty.com/Properties_with_Seller_Financing/page_2672739.html", KCASASVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
