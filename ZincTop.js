qa_override("[E2592408747]", "Some properties have city and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)">View Property', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, 'class=\'rh_pagination__btn current\' >[^\<]*</a> <a href=\'([^\']*)\'', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="rh_page__title">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<p class="price"> [\$][0-9\,\. ]*([a-zA-Z]*)', 1, KDONOTNOTIFYERROR) + get_next_regex_match(html, 0, '<p class="price"> ([\$])[0-9\,\. ]*[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<p class="price"> [\$]([0-9\,\. ]*)[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || isUndefined(mls.ListPrice.currencyCode) || mls.ListPrice.currencyCode == "" || mls.ListPrice.value == 0 || mls.ListPrice.value == ".") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_next_regex_match(html, 0, 'Area </span><div>(.+?)<span class="figure">([^\<]*)</span> <span class="label">([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, 'Area </span><div>(.+?)<span class="figure">([^\<]*)</span> <span class="label">([^\<]*)', 3, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_next_regex_match(html, 0, 'Bedrooms </span><div>(.+?)<span class="figure">([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, 'Bathrooms </span><div>(.+?)<span class="figure">([^\<]*)', 2, KDONOTNOTIFYERROR);
		forRent = get_next_regex_match(html, 0, '<p class="status">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("RENT") || forRent.includes("Rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
			}
		}
		rentCheck = get_unique_regex_match(html, '<p class="price">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(rentCheck)) {
			if (rentCheck.includes("month")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
			}
		}
		mls.ListingDescription = get_next_regex_match(html, 0, 'Description</h4><div class="rh_content">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li class="rh_property__feature" id="[^\"]*"><a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, 'class="rh_property_agent__title">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_next_regex_match(html, 0, 'Office[\s\t\n ]*?:&nbsp;</span> <a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, "\"lat\":\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "\"lng\":\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "Property ID[\s\t\n ]*?:</p><p class=\"id\">&nbsp;([^\<]*)-property", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<p class=\"rh_page__property_address\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		comma = (mls.Location.Directions.match(/,/g) || []).length;
		if (isDefined(comma)) {
			if (comma == "5") {
				address = mls.Location.Directions.split(",");
				if (isDefined(address[3])) {
					mls.Address.City.value = address[3].trim();
				}
				if (isDefined(address[5])) {
					mls.Address.Country.value = address[5].trim();
				}
			}
			if (comma == "3") {
				address = mls.Location.Directions.split(",");
				if (isDefined(address[0])) {
					mls.Address.City.value = address[0].trim();
				}
				if (isDefined(address[3])) {
					mls.Address.Country.value = address[3].trim();
				}

			}
			if (comma == "4") {
				address = mls.Location.Directions.split(",");
				if (isDefined(address[1])) {
					mls.Address.City.value = address[1].trim();
				}
				if (isDefined(address[4])) {
					mls.Address.Country.value = address[4].trim();
				}

			}
			if (comma == "6") {
				address = mls.Location.Directions.split(",");
				if (isDefined(address[2])) {
					mls.Address.City.value = address[2].trim();
				}
				if (isDefined(address[6])) {
					mls.Address.Country.value = address[6].trim();
				}

			}
		} else {
			mls.Address.Country.value = mls.Location.Directions;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img class="lazyload" src=\'[^\']*\' data-src=\"([^\"]*)\"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://zinctop.com/properties-search/?type=residential-sale&status=for-sale", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://zinctop.com/properties-search/?type=residential-rental&status=for-rent", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://zinctop.com/properties-search/?type=land-for-sale&status=for-sale", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://zinctop.com/properties-search/?type=other&status=for-sale", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://zinctop.com/properties-search/?type=commercial-lease&status=for-rent", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://zinctop.com/properties-search/?type=vacation-rental&status=for-rent", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
