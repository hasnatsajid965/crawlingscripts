

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"view-dtl-btn\">[ ]*?<a href=\"([^\"]*)", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" class=\"last\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		description = arrayElement(extract_through_DOM_attribute_selector(html, "class=\"content\"", KDOMSELECTOR_INNERHTML, KDONOTNOTIFYERROR), 0);
		var index = description.indexOf("<strong>"); // Gets the first index
		// where a space occours
		mls.ListingDescription = description.substr(0, index);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"field-content\"><span class=\"money-item[^\"]*\">([0-9\,]*)[\s ]*?([a-zA-Z]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"field-content\"><span class=\"money-item[^\"]*\">([0-9\,]*)[\s ]*?([a-zA-Z]*)</span>", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"price-from-night\">[a-zA-Z]*([ \$ ]*)([0-9\,\.]*)</span>", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"price-from-night\">[a-zA-Z]*([ \$ ]*)([0-9\,\.]*)</span>", 2, KDONOTNOTIFYERROR);
			if (isUndefined(mls.ListPrice.value)) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls.ListingTitle = get_unique_regex_match(html, "<div id=\"overview\" class=\"tab-contents\">[\\s\\t\\n ]*?<h1>([^\<]*)", 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Floor Area</strong>:([0-9\, ]*)([a-zA-Z\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Floor Area</strong>:([0-9\, ]*)([a-zA-Z\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<strong>Land Area</strong>:([0-9\, ]*)([a-zA-Z\.]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>Land Area</strong>:([0-9\, ]*)([a-zA-Z\.]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Bedrooms:[ ]*?</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "strong>Bathrooms:[ ]*?</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var for_rent = get_unique_regex_match(html, "<div style=\"float: left\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent.includes("month")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		var features = get_all_regex_matched(html, '<div class=\"amenities_image amenities-bullet\">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Location.Latitude = get_unique_regex_match(html, 'address = "([^\,]*), ([^\"]*)";', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'address = "([^\,]*), ([^\"]*)";', 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Altmanbarbados";
		mls.Brokerage.Phone = "(246) 432-0840/537-0840";
		mls.Brokerage.Email = "jennifer.straker@altmanbarbados.com";
		mls.Location.Directions = get_next_regex_match(html, 0, "<div id=\"overview\" class=\"tab-contents\">[\\s\\t\\n ]*?<h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var tokens = mls.Location.Directions.split(",");
			if (tokens.length > 1) {
				mls.Address.City.value = tokens[tokens.length - 2].trim();
			}
			if (tokens.length > 0) {
				mls.Address.Country.value = tokens[tokens.length - 1].trim();
			}
			if (tokens.length > 2) {
				mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 3].trim();
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "image : '([^\']*)'", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=261&saleprice=&bedrooms=All&property_parish=All", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=314&saleprice=&bedrooms=All&property_parish=All", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=270&saleprice=&bedrooms=All&property_parish=All", KNUEVOSAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=35&nightly=All&bedrooms=All&property_parish=All&by_name=All", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Villas en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=268&saleprice=&bedrooms=All&property_parish=All", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}

			try {
				// Villas en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=264&saleprice=&bedrooms=All&property_parish=All", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=265&saleprice=&bedrooms=All&property_parish=All", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=274&saleprice=&bedrooms=All&property_parish=All", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=267&saleprice=&bedrooms=All&property_parish=All", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=315&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=148&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=154&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=156&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=157&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=158&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=159&nightly=All&bedrooms=All&property_parish=All&by_name=All", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=29&saleprice=&bedrooms=All&property_parish=All", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en alquiler (amueblado)
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/long-term-rentals/search?property_subtype=149&nightly=All&bedrooms=All&property_parish=All&by_name=All", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.altmanbarbados.com/real-estate-sales/search?island=169&property_subtype=31&saleprice=&bedrooms=All&property_parish=All", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
