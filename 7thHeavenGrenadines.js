function crawlCategory(url, category, stopword) {
    var cumulatedCount = 0;
    print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
    var actualCount = 0;
    var page = 1;
    var tracer = 0;
    var html = gatherContent_url(url, false, KDONOTNOTIFYERROR);
    while (isDefined(html)) {
	try {
	    while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="property property--preview" itemscope itemtype="[^"]*">[\\s\\t\\n ]*?<a href="([^"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
		tracer++;
		actualCount++;
		cumulatedCount++;
	    }
	} catch (e) {
	    if (e === "Duplicate detected.") {
		return cumulatedCount;
	    }
	    throw "error: " + e;
	}
	print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
	var relativeLink = get_unique_regex_match(html, '<a class="next page-numbers page-numbers--navigation" href="([^"]*)"', 1, KDONOTNOTIFYERROR);
	if (isUndefined(relativeLink)) {
	    break;
	}
	html = gatherContent_url(relativeLink, false, KDONOTNOTIFYERROR);
	tracer = 0;
	actualCount = 0;
	page++;
    }
    return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
    // addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
    var html = gatherContent_url(url, false, KDONOTNOTIFYERROR);
    var mls = JSON.parse(mlsJSONString);
    if (isDefined(html)) {
	if (mls.NonMLSListingData == undefined) {
	    mls.NonMLSListingData = {};
	}
	if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
	    mls.NonMLSListingData.lang = "es-do";
	}
	mls.ListingTitle = get_next_regex_match(html, 0, "<title>([^<]*)</title>", 1, KDONOTNOTIFYERROR);
	mls.MlsId = get_next_regex_match(html, 0, '<span class="property__id">Property ID:[ ]?([^<]*)', 1, KDONOTNOTIFYERROR);
	mls.ListPrice.value = get_unique_regex_match(html, '<i class="fa fa-chevron-down"></i></form>[ ]*?</span>[\s ]*?([\$]*)([0-9\s\, ]*)</span>', 2, KDONOTNOTIFYERROR);
	mls.ListPrice.currencyCode = get_unique_regex_match(html, '<i class="fa fa-chevron-down"></i></form>[ ]*?</span>[\s ]*?([\$]*)([0-9\s\, ]*)</span>', 1, KDONOTNOTIFYERROR);
	if(isUndefined(mls.ListPrice.value)){
		mls.ListPrice.value = get_unique_regex_match(html, '<i class="fa fa-chevron-down"></i></form> </span> ([\$]*)([0-9\s\, ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<i class="fa fa-chevron-down"></i></form> </span> ([\$]*)([0-9\s\, ]*)', 1, KDONOTNOTIFYERROR);
	}
	if(isUndefined(mls.ListPrice.value)){
		if (isUndefined(mls.MlsId)) {
			return analyzeOnePublication_return_innactive;
		}
	}
	mls.Brokerage.Name = "7thHeavenGrenadines";
	mls.Brokerage.Phone = "855-364-8776";
	mls.LotSize.value = get_unique_regex_match(html, 'Total area <i class="fa fa-question-circle" data-tooltip="[^"]*"></i></span>[ ]*?<span class="list__item__value">([0-9\,\s ]*)([^\<]*)</span>', 1, KDONOTNOTIFYERROR);
	mls.LotSize.areaUnits = get_unique_regex_match(html, 'Total area <i class="fa fa-question-circle" data-tooltip="[^"]*"></i></span>[ ]*?<span class="list__item__value">([0-9\,\s ]*)([^\<]*)</span>', 2, KDONOTNOTIFYERROR);
	mls.Bedrooms = get_next_regex_match(html, 0, 'Bedrooms</span> <span class="list__item__value">([^<]*)', 1, KNOTIFYERROR);
	mls.Bathrooms = get_next_regex_match(html, 0, 'Bathrooms</span> <span class="list__item__value">([^<]*)', 1, KNOTIFYERROR);
	mls.Location.Latitude = get_unique_regex_match(html, 'data-lat="([^"]*)"', 1, KDONOTNOTIFYERROR);
	mls.Location.Longitude = get_unique_regex_match(html, 'data-lng="([^"]*)"', 1, KDONOTNOTIFYERROR);
	mls.ListingDescription = get_next_regex_match(html, 0, '<div class="property__description" itemprop="description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
	mls.Location.Directions = get_next_regex_match(html, 0, '<h3 class="property__location" itemprop="addressCountry">([^<]*)</h3>', 1, KDONOTNOTIFYERROR);
	if (isDefined(mls.Location.Directions)) {
	    var tokens = mls.Location.Directions.split(",");
	    if (tokens.length > 1) {
		mls.Address.City.value = tokens[tokens.length - 2].trim();
	    }
	    if (tokens.length > 0) {
		mls.Address.Country.value = tokens[tokens.length - 1].trim();
	    }
	}
	var imageCount = 0;
	var images;
	images = get_all_regex_matched(html, '<img width="[^"]*" height="[^"]*" src="([^"]*)"', 1);
	images.forEach(function(oneImageTag) {
	    var obj = JSON.parse(get_list_empty_variable("photo"));
	    obj.MediaURL = oneImageTag;
	    obj.MediaOrderNumber = imageCount;
	    if (mls.Photos.photo == undefined) {
		mls.Photos.photo = [];
	    }
	    mls.Photos.photo.push(obj);
	    imageCount++;
	});
	resulting_json(JSON.stringify(mls));
	return analyzeOnePublication_return_success;
    } else {
	return analyzeOnePublication_return_unreachable;
    }
}

function crawlForListings() {
    if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Appartment
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/beachfront-condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/beachfront-condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/beachfront-condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/beachfront-condos-for-sale/", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/st-vincent-and-the-grenadines/homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/luxury/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/beachfront-homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/beachfront-homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/luxury", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }

	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/st-vincent-and-the-grenadines/luxury/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/luxury/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/luxury/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/beachfront-homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/beachfront-homes-for-sale/", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/st-vincent-and-the-grenadines/land-for-sale/", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/land-for-sale/", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/land-for-sale/", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/land-for-sale/", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/land-for-sale/", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/islands-for-sale/", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Building
	{
	    try {
		// Building en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/honduras-roatan/hotels-for-sale/", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Building en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/mexico/hotels-for-sale/", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Building en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/hotels-for-sale/", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Building en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/st-vincent-and-the-grenadines/hotels-for-sale/", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Building en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/hotels-for-sale/", KEDIFICIOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Other
	{
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/nicaragua/farms-for-sale/", KOTROSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.7thheavenproperties.com/real-estate/belize/farms-for-sale/", KOTROSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	print("crawlForListings in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
	next_crawl_needed(null, false);
    }
}
