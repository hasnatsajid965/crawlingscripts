var countryContext = "USA";

include("base/Century21GlobalFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?subtype=3", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Canada?subtype=apartment", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?subtype=1", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?subtype=4", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Canada?subtype=townHouse", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Canada?subtype=mobile", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?features=luxury", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// New Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?features=newConstruction", KNUEVASCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Land
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?subtype=Residential", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}

		}
		// Other
		{
			try {
				// other en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/usa?subtype=Other", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}

		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
