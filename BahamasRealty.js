qa_override("[E1473228885]", "Some properties contain lot size and some not.");
qa_override("[E4063587116]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="imageMask">[s\t\n ]*?<a href="([^"]*)">', category, "https://www.bahamasrealty.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a id="nextpage" class="button" href="([^"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.bahamasrealty.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	if (html == null) {
		return analyzeOnePublication_return_unreachable;
	}
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var list_not_available = get_unique_regex_match(html, '<div  class="cols8 first main-content">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available.includes("NOT")) {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="property_name">([^<]*)</h1>', 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<td>Living Area</td><td>([ 0-9\,\. ]*)([a-zA-Z\s\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<td>Living Area</td><td>([ 0-9\,\. ]*)([a-zA-Z\s\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, '<td>Lot Size</td><td>([ 0-9\,\. ]*)([a-zA-Z\s\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<td>Lot Size</td><td>([ 0-9\,\. ]*)([a-zA-Z\s\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<div id="bedbath" class="attr">([0-9]*?)[\\s\\t ]*?Bedroom[\\s\\t ]*?([0-9]*?)[\\s\\t ]*?Bathroom', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<div id="bedbath" class="attr">([0-9]*?)[\\s\\t ]*?Bedroom[\\s\\t ]*?([0-9]*?)[\\s\\t ]*?Bathroom', 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, "<td>Amenities</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "A/C Unit") {
						mls.DetailedCharacteristics.hasAC = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		description = get_next_regex_match(html, 0, '<div id="detail_description">[\\s\\t\\n ]*?<h2>Description</h2>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(description)) {
			if (description.includes("This information is protected from unlawful")) {
				var index = description.indexOf("This information is protected from unlawful duplication"); // Gets
				// the
				// first
				// index
				// where
				// a
				// space
				// occours
				mls.ListingDescription = description.substr(0, index);
			} else {
				mls.ListingDescription = description;
			}
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="name">[\\s\\t\\n ]*?<a href="[^"]*">([^<]*)</a>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<div class="cel">Cel:([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, 'href="mailto:([^"]*)">E-mail', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, '<div class="tel">Tel:([^<]*)</div>', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "<td>Listing #</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="price buy">([\$])([0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="price buy">([\$])([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "<td>Property Type</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<td>Location</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "0") {
				mls.Address.City.value = tokens[0];
			}
			mls.Address.Country.value = "Bahamas";
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 2].trim();
					var index = mls.Address.City.value.indexOf("/"); // Gets
					// the
					// first
					// index
					// where
					// a
					// space
					// occours
					mls.Address.City.value = mls.Address.City.value.substr(0, index);
				}
				if (tokens.length > 0) {
					// mls.StreetAdditionalInfo.value = tokens[tokens.length -
					// 1].trim();
				}
			}
			if (mls.Address == undefined)
				mls.Address = {};
			if (total_commas == "2") {
				if (tokens.length > 0) {
					mls.Address.StreetAdditionalInfo.value = tokens[tokens.length - 1].trim();
				}
				if (tokens.length > 2) {
					mls.Address.City.value = tokens[tokens.length - 3].trim();
					var index = mls.Address.City.value.indexOf("/"); // Gets
					// the
					// first
					// index
					// where
					// a
					// space
					// occours
					mls.Address.City.value = mls.Address.City.value.substr(0, index);
				}
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<div class="cols2 onethumb">[\\s\\t\\n ]*?<a href="([^"]*)" data-sjs="[^"]*?">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.bahamasrealty.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=662&property_type%5B%5D=694",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// temporal en venta
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=716", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=664", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=656&property_type%5B%5D=664",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=654&property_type%5B%5D=664",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=659&property_type%5B%5D=691",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.bahamasrealty.com/search.php?cmd=search&formcat=residential&island=&bedrooms=&bathrooms=&minprice=&maxprice=&lot_size_min=&lot_size_max=&property_type%5B%5D=692", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
