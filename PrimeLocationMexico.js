var countryText = "Mexico";
include("base/PrimeLocationFunctions.js");
function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Appartment
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/flats/mexico/?category=residential&country_code=mx&currency=gbp&results_sort=highest_price&search_source=refine&view_type=grid", KAPTOVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/houses/mexico/?category=residential&country_code=mx&currency=gbp&results_sort=highest_price&search_source=refine&view_type=grid", KCASASVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/property/mexico/?category=residential&country_code=mx&currency=gbp&property_type=land&results_sort=highest_price&search_source=refine&view_type=grid", KTERRENOSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Other
	{
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/commercial/mexico/?category=residential&country_code=mx&currency=gbp&results_sort=highest_price&search_source=refine&view_type=grid", KOTROSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
