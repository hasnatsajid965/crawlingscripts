var countryContext = "";
include("base/Encuentra24Functions.js");
function crawlForListings() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	setProxyConditions(true, null);
	rotateUserAgents(false);
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Fincas
	{
	    try {
		// Fincas en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-fincas#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KFINCASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Apartamentos
	{
	    try {
		// Apartamentos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-apartamentos#search=f_currency.USD|number.50&page=1", KAPTOVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Apartamentos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-apartamentos", KAPTOVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Apartamentos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-apartments-condos", KAPTOVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Apartamentos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties", KAPTOVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Apartamentos en alquiler
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-apartamentos#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KAPTOALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-apartamentos-amueblados#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KAPTOALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Apartamentos vacacionales
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-vacaciones#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KAPTOTEMPORAL, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Casas
	{
	    // Casas en venta
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-casas#search=f_currency.USD&sort=f_added&dir=desc&page=1", KCASASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    // Casas en venta
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-casas", KCASASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-houses-homes", KCASASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/real-estate-for-sale-houses-homes?sort=f_added&dir=desc", KCASASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-proyectos-nuevos#search=f_currency.USD|number.50&sort=f_added&dir=desc&page=1", KCASASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    // Casas en alquiler
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-casas-en-el-interior#search=f_currency.DOP&sort=f_added&dir=desc&page=1", KCASASALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-casas#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KCASASALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-casas-de-playa#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KCASASALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Buildings
	{
	    // Buildings on venta
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-buildings", KEDIFICIOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    // Buildings on venta
	    try {
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-edificios", KEDIFICIOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Locales - Tiendas
	{
	    try {
		// Locales - Tiendas en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-comercios#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KTIENDASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Locales - Tiendas en alquiler
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-comercios#search=f_currency.DOP|number.50&page=1", KTIENDASALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Officinas
	{
	    try {
		// Officinas en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-oficinas#search=f_currency.DOP|number.50&page=1", KOFICINASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Officinas en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-buildings,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-island-properties", KOFICINASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Officinas en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties", KOFICINASVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Officinas en alquiler
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-alquiler-de-oficinas#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KOFICINASALQUILER, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-lotes-y-terrenos#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KTERRENOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-en-islas", KTERRENOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-lotes-y-terrenos", KTERRENOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-lots-land", KTERRENOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties", KTERRENOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Edificios
	{
	    try {
		// Edificios en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-edificios#search=f_currency.USD|number.50&sort=f_added&dir=desc&page=1", KEDIFICIOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Edificios en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-oficinas", KEDIFICIOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Edificios en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-offices", KEDIFICIOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Edificios en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-farms,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-preconstruction-new-property,real-estate-for-sale-business,real-estate-for-sale-island-properties", KEDIFICIOSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Other
	{
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-lots-land,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-negocios", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-comercios", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-comercios", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-casas-y-terrenos-de-playas", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/colombia-es/bienes-raices-venta-de-propiedades-fincas", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-beachfront-homes-and-lots", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-farms", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-business", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-commercial", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-offices,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-preconstruction-new-property,real-estate-for-sale-buildings,real-estate-for-sale-business,real-estate-for-sale-island-properties", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-island-properties", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.encuentra24.com/panama-en/searchresult/real-estate-for-sale?sort=f_added&dir=desc&q=notcat.real-estate-for-sale-business,real-estate-for-sale-apartments-condos,real-estate-for-sale-houses-homes,real-estate-for-sale-lots-land,real-estate-for-sale-commercial,real-estate-for-sale-beachfront-homes-and-lots,real-estate-for-sale-offices,real-estate-for-sale-farms,real-estate-preconstruction-new-property,real-estate-for-sale-buildings", KOTROSVENTAS, "Hace .* d?as");
	    } catch (e) {
		print("error: "+e);
	    }
	}
	print("crawlForListings in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
