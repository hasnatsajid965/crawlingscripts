
// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {

	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			
			//<div class="f74e80f3"><a href="/Property/abbottabad_ayub_medical_complex_12_marla_single_storey_house_for_sale-20296315-1352-1.html" class="_7ac32433" title="12 Marla Single Storey House For Sale"><div class="_413d73d2" aria-label="Listing link"></div></a></div>

			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a target=\"([^\"]*)\" href=\"https:\\/\\/www.facebook.com\\/sharer\\/sharer.php\\?u=([^\"]*)\" title=\"([^\"]*)\" class=\"([^\"]*)\">", category, "", 2, KDONOTNOTIFYERROR, false)) > 0) {

				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<li><a href=\"([^\"]*)\" title=\"Next\" class=\"([^\"]*)\"><div title=\"Next\" class=\"([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = "https://zameen.com" + relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		
		// LISTING BROKER CODE -------------------------------------------------------
		//<span itemscope="" itemProp="itemListElement" itemType="http://schema.org/ListItem" dir="auto"><span itemProp="name" class="c5051fb4">House 23517282</span><meta itemProp="position" content="4"/></span>
		var reg_exp = "<span itemscope=\"\" itemProp=\"itemListElement\" itemType=\"([^\"]*)\" dir=\"auto\"><span itemProp=\"name\" class=\"([^\"]*)\">([^ ]*) ([0-9]*)</span><meta itemProp=\"position\" content=\"([0-9]*)\"/></span>";
		var code_part1 = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		var code_part2 = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		if (isDefined(code_part2)){
			if(mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code_part2);
			mls.NonMLSListingData.brokerCodes.push("ID" + code_part2);
			if (isDefined(code_part1)){
				mls.NonMLSListingData.brokerCodes.push(code_part1 + " " + code_part2);
			}
		
		}

		


		// LISTING RENT MATCHING	
		reg_exp = "<li aria-label=\"Property detail purpose\"><span class=\"([^\"]*)\">Purpose</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";
		forRent = get_next_regex_match(html, 0, reg_exp, 3, KDONOTNOTIFYERROR);
		if(forRent.includes("Rent")){
			print("Purpose of listing:	" + forRent);
		}

		if (isDefined(forRent)) {
			if (forRent.includes("Rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} 
		else {
			forRent = get_next_regex_match(html, 0, reg_exp, 3, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Rent")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}
		print("List Price Currency Period:	" + mls.ListPrice.currencyPeriod);


		/*
		forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-122 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (get_next_regex_match(html, 0, "<i id=\"houzez-print\" class=\"fa fa-print\"", 0, KDONOTNOTIFYERROR) != undefined) {
			var pos = get_last_regex_match_end_pos();
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}*/
		
		// LISTING PRICE -------------------------------------------------------

		//<div><div class="_126656cb "><div class="c4fc20ba"><span class="e63a6bfb" aria-label="Currency">PKR</span><span class="_14bafbc4"></span><span class="_105b8a67" aria-label="Price">1.4 Crore</span></div></div>

		reg_exp = "<li aria-label=\"Property detail price\"><span class=\"([^\"]*)\">Price</span><span class=\"([^\"]*)\" aria-label=\"Value\"><div class=\"([^\"]*)\">([A-Za-z]*)<span class=\"([^\"]*)\"></span>([0-9.]*) ([A-Za-z]*)</div></span></li>";

		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_next_regex_match(html, 0, reg_exp, 6, KDONOTNOTIFYERROR);
			var price_multiplier = get_next_regex_match(html, 0, reg_exp, 7, KDONOTNOTIFYERROR);
			if (isDefined(price_multiplier)) {
				switch(price_multiplier.toLowerCase()) {
					case "crore": {
						price_number_value *= 10000000;
					}
					break;
					case "lakh": {
						price_number_value *= 100000;
					}
					break;
					case "thousand": {
						price_number_value *= 1000;
					}
					break;
					default: {
						throw "Unhandled multiplier: " + price_multiplier;
					}
				}
			}
			print("price_number_value: "+ price_number_value);
			mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, reg_exp, 4, KDONOTNOTIFYERROR);
		}
			
		/*
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Renta")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}

		} else {

		} 
		else {

			forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-123 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Renta")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}*/
		
		// LISTING TITLE -------------------------------------------------------
		//<div class="b72558b0"><h1 class="_64bb5b3b">House Available For Sale at Phul Ghulab Road Abbottabad</h1><div class="cbcd1b2b" aria-label="Property header">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</div></div>

		reg_exp = "<h1 class=\"([^\"]*)\">([\\s\\S]*)</h1>";
		
		if (isDefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, reg_exp, 2, KNOTIFYERROR);
		}
		print("Listing title *****" + mls.ListingTitle);
		
		
		
		// LISTING DESCRIPTION -------------------------------------------------------
		//<div class="_066bb126"><h3 class="_95f4723e">Description</h3><div class="_892154cd _6c5bbfd9" style="max-height:110px"><div><div class="_2015cd68" aria-label="Property description"><div dir="auto"><span class="_2a806e1e">Phul Ghulab Abbottabad<br />Please call for more details. Be a lucky buyer to own a House in Abbottabad. Make the most of your purchase with this opportunity. Ideally located on Phul Ghulab Road, this is a rare and golden real estate opportunity. With an area spanning 1350  Square Feet, this will prove to be an ideal place for your family. Plug in to our listings and you'll find out that it has various properties that come at a reasonable price tag such as Rs 14,000,000 in this case. You simply cannot say no to this House. Better yet, it comes with tonnes of exciting features that distinguish it from other properties. <br />Don't hesitate to express your concerns, call us.</span></div></div></div>

		reg_exp = "<h3 class=\"([^\"]*)\">Description</h3><div class=\"([^\"]*) ([^\"]*)\" style=\"([^\"]*)\"><div><div class=\"([^\"]*)\" aria-label=\"Property description\"><div dir=\"([^\"]*)\">([\\s\\S]*)<div class=\"([^\"]*)\"><div class=\"([^\"]*)\"><h2 class=\"([^\"]*)\">Location &amp; Nearby</h2>";

		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 7, KDONOTNOTIFYERROR)
		}
		print("Listing Description *****:	" + mls.ListingDescription);
		
		// LAND SIZE -------------------------------------------------------
		//<li aria-label="Property detail area"><span class="_3af7fa95">Area</span><span class="_812aa185" aria-label="Value"><span>6 Marla</span></span></li>
		reg_exp = "<li aria-label=\"Property detail area\"><span class=\"([^\"]*)\">Area</span><span class=\"([^\"]*)\" aria-label=\"Value\"><span>([0-9\.]*) ([^\"]*)</span></span></li>";
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		print("Land Size Value and Area Units *****:	" + mls.LotSize.value + " " + mls.LotSize.areaUnits);
		
		// BEDROOMS -------------------------------------------------------
		//<li aria-label="Property detail beds"><span class="_3af7fa95">Bedroom(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<li aria-label=\"Property detail beds\"><span class=\"([^\"]*)\">([^\"]*)</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		print("Bedrooms *****:	" + mls.Bedrooms);
		
		// BATHROOMS -------------------------------------------------------
		//<li aria-label="Property detail baths"><span class="_3af7fa95">Bath(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<li aria-label=\"Property detail baths\"><span class=\"([^\"]*)\">([^\"]*)</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		print("Bathrooms *****:	" + mls.Bathrooms);
		
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<li aria-label=\"Property detail type\"><span class=\"([^\"]*)\">Type</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";

 		if (isDefined(mls.PropertyType.value)) {
			mls.PropertyType.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		}
		print("Property type *****:	" + mls.PropertyType.value);
		
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("House")) {
					mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
			}
			if (mls.PropertyType.value.includes("Apartment")) {
				mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
			}
		}

		mls.PropertyType.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR); 
		
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("House")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Sale")) {
						mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
					}
				}
			}
			if (mls.PropertyType.value.includes("Apartment")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Sale")) {
						mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
					}
				}
			}
		}
		print("Property type *****:	" + mls.PropertyType.value);

		print("Listing data category *****:	" + mls.NonMLSListingData.category);
		
		// BROKERAGE NAME -------------------------------------------------------
		//<span class="_725b3e64">Arif Khan</span></div><div class="_6f510616"><a href="/Profile/Abbottabad-Fahad_Real_Estate-179977-1.html" class="_23d69e6d" title="Fahad Real Estate">Agency profile</a></div>
		reg_exp = "<span class=\"([^\"]*)\">([^\"]*)</span></div><div class=\"([^\"]*)\"><a href=\"([^\"]*)\" class=\"([^\"]*)\" title=\"([^\"]*)\">([^\"]*)</a></div>";
		if (isDefined(mls.Brokerage.Name)) {
			mls.Brokerage.Name = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		}
		print("Broker name *****:	" + mls.Brokerage.Name);
		
		// BROKERAGE Phone No. -------------------------------------------------------
		//<div class="_9ff5faa9 _02e9ff75 _27fee0c9">0800-ZAMEEN (92633)</div><div class="_9ff5faa9 _02e9ff75 _27fee0c9">(+92) 42 3256 0445</div>
		reg_exp = ">([^\"]*) (([^\"]*))</div><div class=\"([^\"]*) ([^\"]*) ([^\"]*)\">([^\"]*)</div></div></div><a";
		if (isDefined(mls.Brokerage.Phone)) {
			mls.Brokerage.Phone = get_unique_regex_match(html, reg_exp, 7, KDONOTNOTIFYERROR);

		}
		print("Broker Phone No. *****:	" + mls.Brokerage.Phone);
		
		// BROKERAGE ADDRESS -------------------------------------------------------
		//<li aria-label="Property detail location"><span class="_3af7fa95">Location</span><span class="_812aa185" aria-label="Value">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</span></li>
		//"loc_city_id":"385","loc_city_name":"Abbottabad","loc_id":"15946","loc_name":"Phul Ghulab Road","city_id":"385","city_name":"Abbottabad","listing_type":9,
		reg_exp = "\"loc_city_id\":\"([0-9]*)\",\"loc_city_name\":\"([A-Za-z]*)\",\"loc_id\":\"([0-9]*)\",\"loc_name\":\"([^\"]*)\",\"city_id\":\"([0-9]*)\",\"city_name\":\"([A-Za-z]*)\",\"listing_type\"([^\"]*)";
		if (isDefined(mls.Address.City.value)) {
			var city_name = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);


			mls.Address.City.value = city_name;

		}
		print("Broker Address *****:	" + city_name);
		
		
		/*
		mls.LotSize.value = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Dormitorios:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Ba?os:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<strong>Estacionamientos: </strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Tipo:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Apartamentos")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
					}
				}
			}
			if (mls.PropertyType.value.includes("Casas")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KCASASVENTAS);
					}
				}
			}
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<i class=\"fa fa-user\"></i> ([^<]*)</", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<i class=\"fa fa-mobile\"></i>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<strong>Sector:</strong> ([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>Ciudad:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var operacions = get_next_regex_match(html, 0, "<strong>Operaci?n:</strong> ([^<]*?)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(operacions)) {
			var pos = get_last_regex_match_end_pos();
			var operationList = operacions.split(",");
			if (operationList.length > 1) {
				operationList.forEach(function(oneOperation) {
					if (isPublicationPurchase()) {
						if (oneOperation == "Renta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("LEASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationLease()) {
						if (oneOperation == "Venta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("PURCHASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationRent()) {
					}
				});
			}
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}*/
		
		mls.Address.Country.value = "Pakistan";
		reg_exp = "\"([^\"]*)\":([0-9\.]*),\"latitude\":([0-9\.]*),\"longitude\":([0-9\.]*),\"reference_id\":\"([^\"]*)\"";
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		print("Latitude *****:	" + mls.NonMLSListingData.propertyLocation.Latitude.value);
		print("Longitude *****:	" + mls.NonMLSListingData.propertyLocation.Longitude.value);
		
		//<picture class="_219b7e0a"><source type="image/webp" srcSet="https://media.zameen.com/thumbnails/85464870-120x90.webp"/><img role="presentation" alt="2 " title="2 " src="https://media.zameen.com/thumbnails/85464870-120x90.jpeg"/></picture>
		reg_exp = "<picture class=\"([^\"]*)\"><source type=\"image/webp\" srcSet=\"([^\"]*)\"/><img role=\"presentation\" alt=\"([^\"]*)\" title=\"([^\"]*)\" src=\"([^\ ]*)\"";
		images = get_all_regex_matched(html, reg_exp, 5);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			var increase_pixels = "-800x600.jpeg";
			special = obj.MediaURL.substring(0, obj.MediaURL.indexOf('-'));
			obj.MediaURL = special.concat(increase_pixels);
			if (getFileSizeWithProxyConsiderations (obj.MediaURL) > 0) {
				mls.Photos.photo.push(obj);
							imageCount++;
				}
		});
		
		// VIDEOS --------------------------------------------------------
		//"title":null,"host":"youtube","url":"https:\u002F\u002Fyoutu.be\u002FwgGhdi9Ed7U","orderIndex":0},"createdAt":1591812523,"
		reg_exp = "\"title\":([^\"]*),\"host\":\"youtube\",\"url\":\"([^\ ]*)\",\"orderIndex\":([^\"]*)},\"createdAt\":([^\"]*),\"";
		videos = get_all_regex_matched(html, reg_exp, 2, KDONOTNOTIFYERROR);
		
		var videoCount = 0;
		videos.forEach(function(oneVideoTag) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = oneVideoTag;
			obj.MediaOrderNumber = videoCount;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
				mls.Videos.video.push(obj);
					videoCount++;
				
		});
		/*var imageCount = 0;
		var increase_pixels = "-800x600.jpeg";
		
		for (var image in images) {
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			special = images[image].substring(0, images[image].indexOf('-'));
			var final_image = special.concat(increase_pixels);
			//print(final_image);
			mls.Photos.photo.push(final_image);
			imageCount++;
		}*/
		
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlCities(url, category){
	var count = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	if(isDefined(html)){
		//<li class="_45afd756"><a href="/Houses_Property/Abbottabad-385-1.html" title="Abbottabad">Abbottabad<span class="_19c2a89d">(369)</span></a></li>
		var reg_exp = "<li class=\"([^\"]*)\"><a href=\"([^\"]*)\" title=\"([^\"]*)\">([^<]*)<span class=\"([^\"]*)\">\\Q(\\E([0-9]*)\\Q)\\E</span></a></li>";
		var results = get_all_regex_matched(html, reg_exp, 2);
		for (var oneResult in results){
			print("https://zameen.com" + results[oneResult] + "?sort=date_desc");
			count += crawlCategory("https://zameen.com" + results[oneResult] + "?sort=date_desc", category);
			print("Houses for only one city");

		}
	}
	return count;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Houses
		{
			try {
				// Houses for sale
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-1-9.html", KHOUSEFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Houses for lease
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-2-9.html", KHOUSEFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartments
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-1-8.html", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartments for rent
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-2-8.html", KFLATFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Lands
		{
			try {
				// Plots for sale
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-1-2.html", KLANDFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Shops 
		{
			try {
				// Shops for sale
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-1-15.html", KLOCALFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Offices
		{
			try {
				// Offices for sale
				cumulatedCount += crawlCities("https://www.zameen.com/all-cities/pakistan-1-13.html", KOFFICEFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
