var countryText = "Zambia";
include("base/KNightFrankFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.zm.knightfrank.com/properties/residential/for-sale/zambia-lusaka/bungalow%2Cestate%2Cfarmhouse%2Chouse%2Chouses%20of%20multiple%20occupation%2Cresi%20investment%2Ctown%20house%2Ctownhouse%2Ctownhousevilla%2Cvilla%2Cvillage%20house/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.zm.knightfrank.com/properties/residential/for-sale/zambia-lusaka/farm%2Cland%2Cdevelopment%20plot%2Cgreenfield%20land%2Cbrownfield%20land%2Cnew%20build%20land%2Cdevelopment%20site%2Cfarmestate%2Cfarmhouse%2Cfarmland%2Cacreagesemi-rural/all-beds", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.zm.knightfrank.com/properties/residential/for-sale/zambia-lusaka/development/all-beds", KTERRENOSVENTAS);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
