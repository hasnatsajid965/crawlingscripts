qa_override("[E1473228885]", "Some properties has lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="listing-unit-img-wrapper"><a href="([^\"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<li class=\"roundright\"><a href='([^']*)'>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html) && html !== "") {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="price_area">[\s\t\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="price_area">[\s\t\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, "<!---///// slider-->([^!]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Bedrooms:[ ]*?</strong>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size: </strong>([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Bathrooms:[ ]*?</strong>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "maps.LatLng[(]([^\,]*),([^\)]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "maps.LatLng[(]([^\,]*),([^\)]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		var features = get_all_regex_matched(html, '<i class="fa fa-check"></i>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air Condition") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingTitle = get_unique_regex_match(html, '<h1 class="entry-title entry-prop">([^<]*)</h1>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<div class="listing-cover-title"><a href="#">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, '<i class="fa fa-whatsapp"></i>[ ]*?<a href="tel:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, '<i class="fa fa-envelope-o"></i>[ ]*?<a href="mailto:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_next_regex_match(html, 0, '<i class="fa fa-phone"></i>[ ]*?<a href="tel:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "<strong>Property ID:[ ]*?</strong>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
		list_price = get_unique_regex_match(html, '<span class="price_area">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (list_price === "Price on Request") {
			mls.ListPrice.value = KPRICEONDEMAND;
		} else {
			if (list_price.includes("/")) {
				var freq = list_price.substr(list_price.indexOf("/") + 1);
				if (freq == "mnth") {
					freq = "month";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, '<div class="property_categs">[\s\t\n ]*?<a href="[^"]*" rel="tag">([^<]*)</a>', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '<strong>City:[ ]*?</strong> <a href="#" rel="tag">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "<strong>Country:</strong>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
		street_adress = get_unique_regex_match(html, "<span class=\"adres_area\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.FullStreetAddress = street_adress.replace(/\s+/g, ' ').trim();
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a href="([^"]*)" rel="[^"]*" class="prettygalery">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en rental
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/rentals/search?property_subtype=13", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/sales/search?property_subtype=20", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/sales/search?property_subtype=18", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/sales/search?property_subtype=19", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/rentals/search?property_subtype=12", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/rentals/search?property_subtype=15", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/land/search?property_subtype=11", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/land/search?property_subtype=9", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/land/search?property_subtype=5", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/land/search?property_subtype=8", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/land/search?property_subtype=7", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/land/search?property_subtype=6", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/sales/search?property_subtype=21", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/sales/search?property_subtype=17", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("http://www.grenada-islandreality.com/rentals/search?property_subtype=14", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
