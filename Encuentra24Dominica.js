var countryContext = "Dominica";

include("base/Encuentra24Functions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Fincas
		{
			try {
				// Fincas en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-fincas#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KFINCASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-apartamentos#search=f_currency.USD|number.50&page=1", KAPTOVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-apartamentos#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KAPTOALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-apartamentos-amueblados#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KAPTOALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Apartamentos vacacionales
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-vacaciones#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KAPTOTEMPORAL, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			// Casas en venta
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-casas#search=f_currency.USD&sort=f_added&dir=desc&page=1", KCASASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-proyectos-nuevos#search=f_currency.USD|number.50&sort=f_added&dir=desc&page=1", KCASASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			// Casas en alquiler
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-casas-en-el-interior#search=f_currency.DOP&sort=f_added&dir=desc&page=1", KCASASALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-casas#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KCASASALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-casas-de-playa#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KCASASALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-comercios#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KTIENDASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Locales - Tiendas en alquiler
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-comercios#search=f_currency.DOP|number.50&page=1", KTIENDASALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-oficinas#search=f_currency.DOP|number.50&page=1", KOFICINASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-alquiler-alquiler-de-oficinas#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KOFICINASALQUILER, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-lotes-y-terrenos#search=f_currency.DOP|number.50&sort=f_added&dir=desc&page=1", KTERRENOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Edificios
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/dominicana-es/bienes-raices-venta-de-propiedades-edificios#search=f_currency.USD|number.50&sort=f_added&dir=desc&page=1", KEDIFICIOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

