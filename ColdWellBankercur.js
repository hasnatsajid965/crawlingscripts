qa_override("[E1473228885]", "Some properties do have lot size and some not.");
qa_override("[E2431535312]", "Some properties have description and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"image\">[\\s\\t\\n ]*?<a href='([^']*)'>", category, "https://www.coldwellbankercur.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a href="([^"]*)"  title="">&gt;&gt;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.coldwellbankercur.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var list_not_available = get_unique_regex_match(html, '<a href=\"index.php\">Perhaps([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "you were look for something else?") {
				return analyzeOnePublication_return_unreachable;
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="listing_title"><a href="[^"]*" class="[^"]*"></a>([^<]*)', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<b>Square Metres:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Square Feet:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, "<b>Beds:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<b>Baths:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.FloorCoverings = get_unique_regex_match(html, "<b>Floors:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^<]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<b>([^\:]*):</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\'col-xs-8\'>[\\s\\t\\n ]*?Yes[\\s\\t\\n ]*?</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Garage") {
						mls.DetailedCharacteristics.hasGarage = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		description2 = get_unique_regex_match(html, "<p class='property_feature'>(.+?)</p>", 1, KDONOTNOTIFYERROR);
		description = get_next_regex_match(html, 0, "<h2>Property Description</h2>([^\"]*)", 1, KDONOTNOTIFYERROR);
		if (description2 !== 'undefined') {
			description = description.replace(/(<([^>]+)>)/ig, "");
			description2 = description2.replace(/(<([^>]+)>)/ig, "");
			description2 = description2.replace("<div class=", "");
			description2 = description2.replace(/\s+/g, ' ').trim();
			description = description.replace("<div class=", "");
			description = description.replace(/\s+/g, ' ').trim();
			mls.ListingDescription = description + description2;
		} else {
			description = description.replace(/(<([^>]+)>)/ig, "");
			description = description.replace(/\s+/g, ' ').trim();
			mls.ListingDescription = description.replace("<div class=", "");
		}
		if (mls.office == undefined)
			mls.office = {};
		agent_name = get_unique_regex_match(html, '<a  class="listing_agent_name" href="[^"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = agent_name.replace(/[^a-zA-Z]/g, "");
		mls.Brokerage.Phone = get_unique_regex_match(html, '<b>Mobile:</b>([^"]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, '<span itemprop="email">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, "<b>Phone:</b>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "<b>Ref#:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<b>Price:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>[\\s\\t\\n ]*?([\$])([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "<b>Price:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>[\\s\\t\\n ]*?([\$])([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value == "") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
			if (mls.ListPrice.value == 0) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		} else {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "maps.LatLng[(]([^,]*),([^)]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "maps.LatLng[(]([^,]*),([^)]*)", 2, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, '<b>Age:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\'col-xs-8\'>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Videos = get_unique_regex_match(html, '<iframe width=\'[^\']*\' height=\'[^\']*\' src=\'([^\']*)\'', 1, KDONOTNOTIFYERROR);
		mls.ViewTypes = get_unique_regex_match(html, '<b>View:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\'col-xs-8\'>([^\<]*)', 1, KNOTIFYERROR);
		mls.Address.Country.value = get_next_regex_match(html, 0, "<b>Country:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<b>Location:</b>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class='col-xs-8'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 1].trim();

				}
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img src='([^\']*)' height='400'>", 1);
		if (images == "") {
			images = get_all_regex_matched(html, "<img src='([^\']*)' height='400' data-pagespeed-url-hash=\"[^\"]*\" onload=\"pagespeed.CriticalImages.checkImageForCriticality", 1);
		}
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = "https://www.coldwellbankercur.com" + oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankercur.com/index.php?pclass%5B%5D=16&location=&price-min=0&price-max=6000000&beds-min=1&action=searchresults", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankercur.com/index.php?pclass%5B%5D=15&location=&price-min=0&price-max=6000000&beds-min=1&action=searchresults", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankercur.com/index.php?pclass%5B%5D=17&location=&price-min=0&price-max=6000000&action=searchresults", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.coldwellbankercur.com/index.php?pclass%5B%5D=19&location=&price-min=0&price-max=6000000&beds-min=1&action=searchresults", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
