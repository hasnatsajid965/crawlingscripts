function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'href="([^"]*)" title="[^"]*" role="button">[\\s\\t\\n ]*?<img class="img-fluid"', category, "https://century21agcoplus.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, 'class="page-link" href="([^"]*)" aria-label="Next">[s\t\n ]*?<i class="fa fa-chevron-right"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://century21agcoplus.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<i class="fa fa-angle-double-right" aria-hidden="true"></i>([^<]*)', 1, KNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "<b>Ref#</b>([^<]*)", 1, KNOTIFYERROR);
		bedroom = get_unique_regex_match(html, "<b>Bedrooms</b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		bathroom = get_unique_regex_match(html, "<b>Bathrooms</b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(bathroom)) {
			mls.Bathrooms = bathroom;
		}
		if (isDefined(bedroom)) {
			mls.Bedrooms = bedroom;
		}
		mls.LivingArea.value = get_unique_regex_match(html, "<li><b>Living Space</b>([ 0-9\,\. ]*)([a-zA-Z\.\,]*)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<li><b>Living Space</b>([ 0-9\,\. ]*)([a-zA-Z\.\,]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<b>Land Area</b>([ 0-9\,\. ]*)([a-zA-Z\.\,]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<b>Land Area</b>([ 0-9\,\. ]*)([a-zA-Z\.\,]*)", 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li><b>([^\<]*)</b><span><ul>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ViewTypes = get_unique_regex_match(html, '<b>Property View</b>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<h2 class="card-header">Property Remarks</h2>[\\s\\t\\n ]*?<p class="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.ListingDescription.includes(":")) {
			var index = mls.ListingDescription.indexOf(":");
			mls.ListingDescription = mls.ListingDescription.substring(0, index);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<p class="card-body card-text card-remarks">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<p class="font-weight-bold agent-name">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'title="Mobile"[\\s\\t\\n ]*?href="([^<]*)"', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = "agco.leoncedis@wanadoo.fr";
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="d-block title-h4">[\s\t\n ]*?<small>([a-zA-Z\$\? ]*)([0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="d-block title-h4">[\s\t\n ]*?<small>([a-zA-Z\$\? ]*)([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		currency_period = get_unique_regex_match(html, "<span class=\"d-block title-h4\">[\\s\\t\\n ]*?<small>[^\<]*<small>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(currency_period)) {
			if (currency_period == "/mth" || currency_period == "/Mth") {
				currency_period = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(currency_period);
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<b>Property Class</b>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "\"addressLocality\": \"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "<b>Country</b>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "2d([^!]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "!3d([^!]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'property="og:image" content="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (oneImageTag !== "https://century21agcoplus.com/mls.cdn/images/listings/1144603/f/0.jpg") {
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = "https://century21agcoplus.com" + oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			}
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/residential/condos-apartments/for-sale", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rental
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/residential/condos-apartments/for-rent", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/residential/for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/residential/single-family-homes/for-sale", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/residential/single-family-homes/for-rent", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/residential/vacant-land/for-sale", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// stores en venta
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/commercial/for-sale", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// stores en venta
				cumulatedCount += crawlCategory("https://century21agcoplus.com/en/s/commercial/lease", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
