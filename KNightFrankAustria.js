var countryText = "Austria";
include("base/KNightFrankFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.com/properties/residential/for-sale/austria-vienna/bungalow%2Cestate%2Cfarmhouse%2Chouse%2Chouses%20of%20multiple%20occupation%2Cresi%20investment%2Ctown%20house%2Ctownhouse%2Ctownhousevilla%2Cvilla%2Cvillage%20house/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.com/properties/residential/for-sale/austria-vienna/development/all-beds", KNUEVASCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.com/properties/residential/for-sale/austria-vienna/farm,land,development%20plot,greenfield%20land,brownfield%20land,new%20build%20land,development%20site,farmestate,farmhouse,farmland,acreagesemi-rural/all-beds", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.com/properties/residential/for-sale/austria-vienna/development/all-beds", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.com/properties/residential/for-sale/austria-vienna/land%2Cdevelopment/all-beds", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.com/properties/residential/for-sale/austria-vienna/flat%2Capartment%2Cmasionette%2Cstudio%20flat%2Ccondominium%2Cresi%20investment%2Cblock%2Cportfolio%3A%20flats%2Cportfolio%3A%20houses%2Chouses%20or%20multiple%20occupation%2Cserviced%20residence%2Cunit%2Cblock%20of%20flats%2Coff%20plan%2Ctenanted%20investments%2Cdevelopment%20block%2Cground%20rents/all-beds;%20reversions%2C%20regulated%20tenancies=", KAPTOVENTAS);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
