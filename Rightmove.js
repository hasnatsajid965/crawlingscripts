qa_override("[E4063587116]", "Some of the properties contains description and some does not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[@data-test='property-details' and @class='propertyCard-link'])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					// el1 = "https://clasificados.com.do/"+el1;
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			index1 = 2;
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "//button[@class='pagination-button pagination-direction pagination-direction--next']", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement) && verMasButtonElement !== null) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KDONOTNOTIFYERROR)) {
						loadedMore = false;
						break;
					} else {
						return actualCount;
					}
				} else {
					return actualCount;
				}
				if (count != 2) {
					wait(1000);
				}
				index1++;
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<p itemprop="description">(.+?)</p>', 1, KDONOTNOTIFYERROR);

		mls.Location.Latitude = get_next_regex_match(html, 0, '"latitude":([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"longitude":([^\,]*)[\\}]', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<p id="propertyHeaderPrice" class="property-header-price ">[\s\t\n ]*?<small class="property-header-qualifier">[^\<]*</small>[\s\t\n ]*?<strong>[\s\t\n ]*?([&pound]+);([0-9\,\. ]*)', 2,
				KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0,
				'<p id="propertyHeaderPrice" class="property-header-price ">[\s\t\n ]*?<small class="property-header-qualifier">[^\<]*</small>[\s\t\n ]*?<strong>[\s\t\n ]*?([&pound]+);([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, '<p id="propertyHeaderPrice" class="property-header-price ">[\s\t\n ]*?<strong>[\s\t\n ]*?([&pound]+);([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<p id="propertyHeaderPrice" class="property-header-price ">[\s\t\n ]*?<strong>[\s\t\n ]*?([&pound]+);([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '"masterUrl":"([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://media.rightmove.co.uk" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"fs-22\" itemprop=\"name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListingTitle)) {
			if (mls.ListingTitle.includes("rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<ul class="list-two-col list-style-square">(.+?)</ul>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "AC") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							if (feature.includes("Bedrooms")) {
								mls.Bedrooms = feature.replace(/[0-9]/g, "").trim();
							}
							if (feature.includes("Bathrooms")) {
								mls.Bathrooms = feature.replace(/[0-9]/g, "").trim();
							}
							if (feature.includes("Square Feet")) {
								mls.LotSize.value = get_unique_regex_match(feature, "([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
								mls.LotSize.areaUnits = get_unique_regex_match(feature, "[0-9\,\. ]*([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
							}
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.Address.Country.value = "England";
		mls.Location.Directions = get_unique_regex_match(html, "<meta itemprop=\"streetAddress\" content=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			addressValue = mls.Location.Directions.split(",");
			if (isDefined(addressValue[1])) {
				mls.Address.City.value = addressValue[1];
			}
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<a id=\"aboutBranchLink\" href=\"[^\"]*\"><strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "title=\"Call ([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=detached&secondaryDisplayPropertyType=detachedshouses&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=semi-detached&secondaryDisplayPropertyType=semidetachedhouses&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=terraced&secondaryDisplayPropertyType=terracedhouses&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=bungalow&primaryDisplayPropertyType=bungalows&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=park-home&secondaryDisplayPropertyType=parksandmobilehomes&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=",
				KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=flat&primaryDisplayPropertyType=flats&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E1407&propertyTypes=land&primaryDisplayPropertyType=land&includeSSTC=false&mustHave=&dontShow=&furnishTypes=&keywords=", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-to-rent/find.html?locationIdentifier=REGION%5E1407&propertyTypes=flat&primaryDisplayPropertyType=houses&includeLetAgreed=false&mustHave=&dontShow=&furnishTypes=&keywords=", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser,
				"https://www.rightmove.co.uk/property-to-rent/find.html?locationIdentifier=REGION%5E1407&propertyTypes=bungalow&primaryDisplayPropertyType=flats&includeLetAgreed=false&mustHave=&dontShow=&furnishTypes=&keywords=", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.rightmove.co.uk/property-to-rent/find.html?searchType=RENT&locationIdentifier=REGION%5E1407&insId=1&radius=0.0&minPrice=&maxPrice=&minBedrooms=&maxBedrooms=&displayPropertyType=houses&maxDaysSinceAdded=&sortByPriceDescending=&_includeLetAgreed=on&primaryDisplayPropertyType=&secondaryDisplayPropertyType=&oldDisplayPropertyType=&oldPrimaryDisplayPropertyType=&letType=&letFurnishType=&houseFlatShare=",
				KCASASALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
