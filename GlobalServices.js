const KCASASVENTAS = KHOUSEFORSALE = "Casas/Ventas";
const KNUEVASCASASVENTAS = KHOUSEPROJECTFORSALE = "NuevasCasas/Ventas";
const KNUEVASMULTIFAMILIAVENTAS = KMULTIFAMILIALHOUSEPROJECTFORSALE = "NuevasMultiFamilia/Ventas";
const KMULTIFAMILIAVENTAS = KMULTIFAMILIALHOUSEFORSALE = "MultiFamilia/Ventas";
const KCASASALQUILER = KHOUSEFORLEASE = "Casas/Alquiler";
const KNUEVASCASASALQUILER = KHOUSEPROJECTFORLEASE = "NuevasCasas/Alquiler";
const KCASASTEMPORAL = KHOUSEFORTEMPORARYRENT = "Casas/Temporal";
const KFINCASVENTAS = KCOUNTRYHOUSEFORSALE = "Fincas/Ventas";
const KFINCASALQUILER = KCOUNTRYHOUSEFORLEASE = "Fincas/Alquiler";
const KFINCASTEMPORAL = KCOUNTRYHOUSEFORTEMPORARYRENT = "Fincas/Temporal";
const KAPTOVENTAS = KFLATFORSALE = "Apto/Ventas";
const KNUEVOSAPTOVENTAS = KFLATPROJECTFORSALE = "NuevosApto/Ventas";
const KAPTOALQUILER = KFLATFORLEASE = "Apto/Alquiler";
const KNUEVOSAPTOALQUILER = KFLATPROJECTFORLEASE = "NuevosApto/Alquiler";
const KAPTOTEMPORAL = KFLATFORTEMPORARYRENT = "Apto/Temporal";
const KTIENDASVENTAS = KLOCALFORSALE = "Tiendas/Ventas";
const KTIENDASALQUILER = KLOCALFORLEASE = "Tiendas/Alquiler";
const KOFICINASVENTAS = KOFFICEFORSALE = "Oficinas/Ventas";
const KOFICINASALQUILER = KOFFICEFORLEASE = "Oficinas/Alquiler";
const KTERRENOSVENTAS = KLANDFORSALE = "Terrenos/Ventas";
const KTERRENOSALQUILER = KLANDFORLEASE = "Terrenos/Alquiler";
const KEDIFICIOSVENTAS = KBUILDINGFORSALE = "Edificios/Ventas";
const KEDIFICIOSALQUILER = KBUILDINGFORLEASE = "Edificios/Alquiler";
const KNAVESVENTAS = KWHAREHOUSEFORSALE = "Naves/Ventas";
const KNAVESALQUILER = KWHAREHOUSEFORLEASE = "Naves/Alquiler";
const KOTROSVENTAS = KOTHERFORSALE = "Otros/Ventas";
const KOTROSALQUILER = KOTHERFORLEASE = "Otros/Alquiler";
const KUNCATEGORIZED = "UNCATEGORIZED";

const analyzeOnePublication_return_success = 0;
const analyzeOnePublication_return_unreachable = -1;
const analyzeOnePublication_return_same_hash = -2;
const analyzeOnePublication_return_innactive = -3;
const analyzeOnePublication_return_tech_issue = -99;

const KDOMSELECTOR_INNERHTML = "innerhtml";
const KDOMSELECTOR_OUTERHTML = "outerhtml";
const KDOMSELECTOR_INNERTEXT = "innertext";
const KDOMSELECTOR_TAG = "tag";

const KPRICEONDEMAND = "ONDEMAND";

const KEXCLUDEFROMKPIPREFIX = "[EXCLUDEKPI]/"

const KDELTATIMEWHILECALCULATION = 120 * 60 * 1000;
const KDELTATIMEONEDAY = 24 * 60 * 60 * 1000;

const KNOTIFYERROR = true;
const KDONOTNOTIFYERROR = false;

var lastAssignedCategory;

function isString(value) {
	return typeof value === 'string' || valueinstanceofString;
}

function trim(s, c) {
	if (c === "]")
		c = "\\]";
	if (c === "\\")
		c = "\\\\";
	return s.replace(new RegExp("^[" + c + "]+|[" + c + "]+$", "g"), "");
}

function preformatToString(value, reproduceZero) {
	var dReturn;
	if (isUndefined(value)) {
		dReturn = '';
	} else {
		dReturn = JSON.stringify(value);
		if (isString(value)) {
			dReturn = dReturn.slice(1, -1);
		}
	}
	if ((!reproduceZero) && (value === 0)) {
		dReturn = '';
	}
	return dReturn;
}

function filterToAllowedFrequency(dString) {
	switch (dString.toLowerCase()) {
	case "año":
	case "year":
	case "annually":
		return "annually";
		break;
	case "mes":
	case "meses":
	case "mensual":
	case "monthly":
	case "month":
	case "months":
		return "monthly";
		break;
	case "dia":
	case "día":
	case "días":
	case "dias":
	case "diario":
	case "day":
	case "daily":
	case "days":
		return "daily";
		break;
	case "semana":
	case "semanas":
	case "semanal":
	case "week":
	case "weeks":
	case "weekly":
		return "weekly";
		break;
	default:
		if (isUndefined(dString)) {
			return undefined;
		} else {
			throw "Can't identify a periodicity period for '" + dString + "'";
		}
	}
}

function StringSet() {
	var setObj = {}, val = {};
	this.add = function(str) {
		setObj[str] = val;
	};
	this.contains = function(str) {
		return setObj[str] === val;
	};
	this.remove = function(str) {
		delete setObj[str];
	};
	this.values = function() {
		var values = [];
		for ( var i in setObj) {
			if (setObj[i] === val) {
				values.push(i);
			}
		}
		return values;
	};
}

function arrayElement(array, element) {
	if ((isDefined(array)) && (array.length != undefined) && (array.length > element)) {
		return array[element];
	} else {
		return undefined;
	}
}

String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.replace(new RegExp(search, 'g'), replacement);
};

function evaluate(condition) {
	return eval(condition);
}

function isDefined(value) {
	return ((value != undefined) && (value !== 'undefined') && (value != null) && (value !== 'null') && (value != 'undefined') && (value != 'null'));
}

function isUndefined(value) {
	return ((value == undefined) || (value === 'undefined') || (value == 'undefined') || (value == null) || (value === 'null') || (value == 'null'));
}

function test(value) {
	print('result: ' + preformatToString(JSON.parse(value).publication.imagesList.image, false));
	print('result: ' + preformatToString(JSON.parse(value).property.address, false));
}

function consumeBusinessLogicAssignment(category, value, json, associatedSentenceJson, score, publicationcategory) {
	if (ambiguityManagement(category, "" + score) === "false") {
		lastAssignedCategory = undefined;
		return null;
	}
	value = value.trim();
	var mls = JSON.parse(json);
	var sentence = JSON.parse(associatedSentenceJson);
	switch (category) {
	case "AGENTIDENTIFICATION":
		mls.NonMLSListingData.agentNameNLPDetection = value;
		break;
	case "LOTAREA": {
		if (mls.LotSize == undefined) {
			mls.LotSize = {
				value : undefined,
				areaUnits : undefined
			};
		}
		if ((value != 0) && ((mls.LotSize.value == undefined) || (mls.LotSize.value == 0))) {
			mls.LotSize.value = value;
		} else {
			return null;
		}
		break;
	}
	case "LIVINGAREA": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if (mls.LivingArea == undefined) {
			mls.LivingArea = {
				value : undefined,
				areaUnits : undefined
			};
		}
		if ((value != 0) && ((mls.LivingArea.value == undefined) || (mls.LivingArea.value == 0))) {
			mls.LivingArea.value = value;
		} else {
			return null;
		}
		break;
	}
	case "AREAUNIT": {
		if ((lastAssignedCategory == "LIVINGAREA") && (mls.LivingArea != undefined) && (mls.LivingArea.value != 0)) {
			mls.LivingArea.areaUnits = value;
		} else if ((lastAssignedCategory == "LOTAREA") && (mls.LotSize != undefined) && (mls.LotSize.value != 0)) {
			if (value == "acres") {
				mls.LotSize.areaUnits = "m2";
				mls.LotSize.value = "" + (Number(mls.LotSize.value) * 4046.86);
			} else if ((value == "hectares") || (value == "ha")) {
				mls.LotSize.areaUnits = "m2";
				mls.LotSize.value = "" + (Number(mls.LotSize.value) * 10000);
			} else if (value == "tareas") {
				mls.LotSize.areaUnits = "m2";
				mls.LotSize.value = "" + (Number(mls.LotSize.value) * 628.86);
			} else {
				mls.LotSize.areaUnits = value;
			}
		} else {
			return null;
		}
		break;
	}
	case "HASSECURITYSYSTEM": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if (mls.DetailedCharacteristics.HasSecuritySystem == undefined) {
			mls.DetailedCharacteristics.HasSecuritySystem = value;
		} else {
			return null;
		}
		break;
	}
	case "HASPATIO": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if (mls.DetailedCharacteristics.HasPatio == undefined) {
			mls.DetailedCharacteristics.HasPatio = value;
		} else {
			return null;
		}
		break;
	}
	case "HASPOOL": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if (mls.DetailedCharacteristics.HasPool == undefined) {
			mls.DetailedCharacteristics.HasPool = value;
		} else {
			return null;
		}
		break;
	}
	case "ANODECONSTRUCCION": {
		if ((mls.YearBuilt == undefined) || (mls.YearBuilt == 0)) {
			mls.YearBuilt = value;
		} else {
			return null;
		}
		break
	}
	case "CURRENTFLOORHINT": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if ((mls.DetailedCharacteristics.CondoFloorNum == undefined) || (mls.DetailedCharacteristics.CondoFloorNum == 0)) {
			mls.DetailedCharacteristics.CondoFloorNum = value;
		} else {
			return null;
		}
		break;
	}
	case "TOTALFLOORHINT": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if ((mls.DetailedCharacteristics.NumFloors == undefined) || (mls.DetailedCharacteristics.NumFloors == 0)) {
			mls.DetailedCharacteristics.NumFloors = value;
		} else {
			return null;
		}
		break;
	}
	case "BEDROOMSCOUNT": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if ((mls.Bedrooms == undefined) || (mls.Bedrooms == 0)) {
			mls.Bedrooms = value;
		} else {
			return null;
		}
		break;
	}
	case "BATHROOMSCOUNT": {
		if ((publicationcategory === KTERRENOSVENTAS) || (publicationcategory === KTERRENOSALQUILER)) {
			return null;
		}
		if ((mls.Bathrooms == undefined) || (mls.Bathrooms == 0)) {
			mls.Bathrooms = value;
		} else {
			return null;
		}
		break;
	}
	case "PARKINGCOUNT": {
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if ((mls.DetailedCharacteristics.NumParkingSpaces == undefined) || (mls.DetailedCharacteristics.NumParkingSpaces == 0)) {
			mls.DetailedCharacteristics.NumParkingSpaces = parseInt(value, 10);
			if (mls.DetailedCharacteristics.NumParkingSpaces < value) {
				mls.NonMLSListingData.portionNumParkingSpaces = (value - mls.DetailedCharacteristics.NumParkingSpaces);
			} else {
				mls.NonMLSListingData.portionNumParkingSpaces = undefined;
			}
		} else {
			return null;
		}
		break;
	}
	case "LISTINGPRICE": {
		if (mls.ListPrice == undefined) {
			mls.ListPrice = {};
		}
		if ((mls.ListPrice.value == undefined) || (mls.ListPrice.value == 0)) {
			mls.ListPrice.value = parseInt(value, 10);
			mls.ListPrice.currencyCode = extractCurrencyInSentence(associatedSentenceJson, value, (mls.ListPrice.currencyCode !== undefined) ? mls.ListPrice.currencyCode : "undefined");
		} else {
			return null;
		}
		break;
	}
	case "CURRENCY": {
		if ((mls.ListPrice != undefined) && (mls.ListPrice.value != undefined) && (lastAssignedCategory == "LISTINGPRICE")) {
			if ((mls.ListPrice.currencyCode == undefined) || ((mls.ListPrice.currencyCode === "$") && (value !== "$") && (value != undefined))) {
				mls.ListPrice.currencyCode = value;
			} else {
				return null;
			}
		} else {
			return null;
		}
		break;
	}
	case "BROKERPHONECONTACT": {
		var noMeta = sentence.normalizedSentence.replace(new RegExp("\\{PHONE\\|([^\\}]*)\\}", "g"), "").trim();
		var beforeBrokerage = (mls.Brokerage != undefined) ? JSON.parse(JSON.stringify(mls.Brokerage)) : undefined;
		if ((sentence.referenceCount == 1) && (noMeta.length > 0)) {
			if (mls.Brokerage == undefined) {
				mls.Brokerage = {};
			}
			if (mls.Brokerage.Name == undefined) {
				mls.Brokerage.Name = noMeta;
			}
			if (mls.Brokerage.Phone == undefined) {
				mls.Brokerage.Phone = value;
			}
		}
		if (!(((beforeBrokerage == undefined) || ((beforeBrokerage.Name == undefined) && (beforeBrokerage.Phone == undefined))) && (mls.Brokerage != undefined) && (mls.Brokerage.Name != undefined) && (mls.Brokerage.Phone != undefined))) {
			if (mls.NonMLSListingData.otherListingBrokerInfo == undefined) {
				mls.NonMLSListingData.otherListingBrokerInfo = [];
			}
			var obj = {};
			if ((sentence.referenceCount == 1) && (noMeta.length > 0)) {
				obj.Name = noMeta;
			}
			if (isValidPhoneNumber(value)) {
				obj.Phone = value;
				mls.NonMLSListingData.otherListingBrokerInfo.push(obj);
			}
		}
		break;
	}
	case "BROKERCODE": {
		if ((value.match("\\([0-9]{1,3}\\)")) || (value === "")) {
			return null;
		}
		if (mls.NonMLSListingData.brokerCodes == undefined) {
			mls.NonMLSListingData.brokerCodes = [];
		}
		mls.NonMLSListingData.brokerCodes.push(value);
		break;
	}
	case "FEATUREHINT": {
		if (mls.NonMLSListingData.FeatureList == undefined) {
			mls.NonMLSListingData.FeatureList = [];
		}
		var obj = {};
		obj.value = value;
		mls.NonMLSListingData.FeatureList.push(obj);
		break;
	}
	case "BROKEREMAILCONTACT": {
		var noMeta = sentence.normalizedSentence.replace(new RegExp("\\{EMAIL\\|([^\\}]*)\\}", "g"), "").trim();
		var beforeBrokerage = (mls.Brokerage != undefined) ? JSON.parse(JSON.stringify(mls.Brokerage)) : undefined;
		if ((sentence.referenceCount == 1) && (noMeta.length > 0)) {
			if (mls.Brokerage == undefined) {
				mls.Brokerage = {};
			}
			if (mls.Brokerage.Name == undefined) {
				mls.Brokerage.Name = noMeta;
			}
			if (mls.Brokerage.Email == undefined) {
				mls.Brokerage.Email = value;
			}
		}
		if (!(((beforeBrokerage == undefined) || ((beforeBrokerage.Name == undefined) && (beforeBrokerage.Email == undefined))) && (mls.Brokerage != undefined) && (mls.Brokerage.Name != undefined) && (mls.Brokerage.Email != undefined))) {
			if (mls.NonMLSListingData.otherListingBrokerInfo == undefined) {
				mls.NonMLSListingData.otherListingBrokerInfo = [];
			}
			var obj = {};
			if ((sentence.referenceCount == 1) && (noMeta.length > 0)) {
				obj.Name = noMeta;
			}
			if (isValidEMail(value)) {
				obj.Email = value;
				mls.NonMLSListingData.otherListingBrokerInfo.push(obj);
			}
		}
		break;
	}
	}
	lastAssignedCategory = category;
	return JSON.stringify(mls);
}
