qa_override("[E1473228885]", "Some properties has lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)">SEE more >>', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" data-ci-pagination-page=\"[^\"]*\" rel=\"next\"><i class=\"fa fa-chevron-right\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h4 class="page-title">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<span class=\"beds\"><strong>([^\<]*)</strong>Beds", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "</span> <span class=\"area\"><strong>([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.areaUnits = get_unique_regex_match(html, "</span> <span class=\"area\"><strong>([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		}
		mls.Bathrooms = get_unique_regex_match(html, "<span class=\"baths\"><strong>([^\<]*)</strong>Baths</span>", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "lat: ([^\,]*),", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "lng: ([^\,]*),", 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, "<div id=\"description\" class=\"tab-pane active\">(.+?)</div>", 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Homegofast";
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.ListPrice.value = get_unique_regex_match(html, '<p class="text-right"><strong>[\s\t\n ]*?([^\;]*);([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p class="text-right"><strong>[\s\t\n ]*?([^\;]*);([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		var dVideo = get_unique_regex_match(html, "<iframe src=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if ((dVideo != undefined) && (dVideo != "undefined")) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		cityValue = get_unique_regex_match(html, '<h1 class="page-title">[^\<]*</h1>[\s\t\n ]*?<h2>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			cityValue = cityValue.split("-");
			if (isDefined(cityValue)) {
				mls.Address.City.value = cityValue[1];
			}
		}
		mls.Address.Country.value = "Uruguay";
		var features = get_all_regex_matched(html, '<i class="fa fa-check-square"></i>([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air Condition") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}

		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<meta name="og:image" content="([^\"]*)" />', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Uncategorized
				cumulatedCount += crawlCategory("https://www.homesgofast.com/property/sale/uruguay", KUNCATEGORIZED);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
