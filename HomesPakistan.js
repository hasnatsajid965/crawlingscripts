
// crawlForPublications crawl-mode: Regular-Expression Crawling

function land_units(land_units){
	return "Sq. Yard";
}

function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			var reg_exp = "<div class=\'property-btn\'><div class=\'btn-detail2\' onClick=\"window.location.href=\'([^\"]*)\'\">View Detail </div></div>";
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, reg_exp, category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
        }
		reg_exp = "<li class=\'active\'><a href=\'([^\ ]*)\' aria-label=\'Next\'><span aria-hidden=\'true\'>Next</span></a></li>([\\n\\s]*)</ul></nav>";
		var relativeLink = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		
		// LISTING BROKER CODE -------------------------------------------------------
		var reg_exp = "<div class=\"text-gray2\">Property Ref. No: <span class=\"text-black\">([\\n\\s]*)([^\"]*)([\\n\\s]*)</span></div>";
		var broker_code = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		if (isDefined(broker_code)){
			if(mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(broker_code);
		}
		//print("BROKER CODE *****	:" + broker_code);
		
		// LISTING RENT MATCHING	
		reg_exp = "<title>([^\"]*)</title>";
		forRent = get_next_regex_match(html, 0, reg_exp, 1, KDONOTNOTIFYERROR);
		var rent_checker = forRent.toLowerCase();
		if (isDefined(forRent)) {
			if (rent_checker.includes("rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} 
		else {
			forRent = get_next_regex_match(html, 0, reg_exp, 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("rent")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}
		print("List Price Currency Period:	" + mls.ListPrice.currencyPeriod);
		
		// LISTING PRICE -------------------------------------------------------
		reg_exp = "<div class=\"prop-price\">([\\n\\s]*)([A-Za-z]*) ([^\"]*)([\\n\\s]*)</div>";
		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
			mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		}
		print("Listing Price Value *****:	"  + mls.ListPrice.value);
		print("Listing Price Currency Code ****:	" + mls.ListPrice.currencyCode);
			
		/*
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Renta")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} 
		else {
			forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-123 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Renta")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}*/
		
		// LISTING TITLE -------------------------------------------------------
		reg_exp = "<h1 class=\"green-heading2\">([\\n\\s]*)([^\"]*)([\\n\\s]*)</h1>";
		if (isDefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, reg_exp, 2, KNOTIFYERROR);
		}
		print("Listing title *****:		" + mls.ListingTitle);
						
		// LISTING DESCRIPTION -------------------------------------------------------
		reg_exp = "<h3>Property Details</h3>([\\n\\s]*)<div class=\"border-divider2\"></div>([\\n\\s]*)<div class=\"more-feature-main\">([\\n\\s]*)([\\s\\S]*)([\\n\\s]*)<div class=\"clearfix\"></div>([\\n\\s]*)</div>";
		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		print("Listing Description *****:	" + mls.ListingDescription);
		
		// LAND SIZE -------------------------------------------------------
		reg_exp = "<title>([0-9.]*) ([A-Za-z]*)([^\"]*)</title>";
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
			var lot_units = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
			lot_units  = lot_units.toLowerCase();			
			if (lot_units === "sq"){
				mls.LotSize.areaUnits = land_units(lot_units);
			}
			else{
				mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
			}
		}
		print("Land Size Value and Area Units *****:	" + mls.LotSize.value + " " + mls.LotSize.areaUnits);
		
		// BEDROOMS -------------------------------------------------------
		reg_exp = "<div class=\"key-bed\">([\\n\\s]*)([0-9.]) Bedrooms([\\n\\s]*)</div>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		}
		print("Bedrooms *****:	" + mls.Bedrooms);
		
		// BATHROOMS -------------------------------------------------------
		reg_exp = "<div class=\"key-bath\">([\\n\\s]*)([0-9.]) Bathrooms([\\n\\s]*)</div>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		}
		print("Bathrooms *****:	" + mls.Bathrooms);
		
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<title>([0-9.]*) ([A-Za-z]*) ([^\"]*)</title>";
		mls.PropertyType.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR); 
		
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("House")) {
				mls.PropertyType.value = "House";
				if (isDefined(forRent)) {
					if (rent_checker.includes("sale")) {
						mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
					}
					else{
						mls.NonMLSListingData.category = getCategory(KHOUSEFORLEASE);
					}
				}
			}
			if (mls.PropertyType.value.includes("Apartment")) {
				mls.PropertyType.value = "Apartment";
				if (isDefined(forRent)) {
					if (rent_checker.includes("sale")) {
						mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
					}
					else{
						mls.NonMLSListingData.category = getCategory(KFLATFORLEASE);
					}
				}
			}
		}
		print("Property type *****:	" + mls.PropertyType.value);
		print("Listing data category *****:	" + mls.NonMLSListingData.category);
		
		// BROKERAGE NAME -------------------------------------------------------
		reg_exp = "<div class=\"agent-title\">([\\n\\s]*)([^\"]*)([\\n\\s]*)</div>";
		if (isDefined(mls.Brokerage.Name)) {
			mls.Brokerage.Name = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		}
		print("Broker name *****:	" + mls.Brokerage.Name);
		
		// BROKERAGE Phone No. -------------------------------------------------------
		//<div class="_9ff5faa9 _02e9ff75 _27fee0c9">0800-ZAMEEN (92633)</div><div class="_9ff5faa9 _02e9ff75 _27fee0c9">(+92) 42 3256 0445</div>
		reg_exp = "<h2>Please Contact</h2>([\\n\\s\\t]*)<div class=\"property-phone\"> <i class=\"icon-phone-1\"></i>([\\n\\s]*)([^\"]*)([\\n\\s]*)</div>([\\n\\s\\t]*)<div class=\"property-phone\"> <i class=\"icon-mobile\"></i>([\\n\\s]*)([^\"]*)([\\n\\s]*)</div>([\\n\\s\\t]*)<p>Please mention Homes Pakistan when calling.</p>([\\n\\s]*)";
		phone_number = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		mobile_number = get_unique_regex_match(html, reg_exp, 7, KDONOTNOTIFYERROR);
		if (isDefined(phone_number) && isDefined(mobile_number)) {
				mls.Brokerage.Phone = phone_number + " / " + mobile_number;
			}			

		
		print("Broker Phone No. *****:	" + mls.Brokerage.Phone);
		
		// BROKERAGE ADDRESS -------------------------------------------------------
		reg_exp = "<h1 class=\"green-heading2\">([\\n\\s]*)([^\"]*)([\\n\\s]*)</h1>";
		if (isDefined(mls.Address.City.value)) {
			var city_name = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
			var city = city_name.split(/[\s,]+/);
			mls.Address.City.value = city[city.length-1];
		}
		print("Broker City *****:	" + mls.Address.City.value);
		
		mls.Address.Country.value = "Pakistan";
		//reg_exp = "\"([^\"]*)\":([0-9\.]*),\"latitude\":([0-9\.]*),\"longitude\":([0-9\.]*),\"reference_id\":\"([^\"]*)\"";
		//mls.NonMLSListingData.propertyLocation.Latitude.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		//mls.NonMLSListingData.propertyLocation.Longitude.value = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		//print("Latitude *****:	" + mls.NonMLSListingData.propertyLocation.Latitude.value);
		//print("Longitude *****:	" + mls.NonMLSListingData.propertyLocation.Longitude.value);
		
		reg_exp = "<figure>[ ]?<a href=\"([^\"]*)\" class=\"fancybox-media\" data-rel=\"portfolio\">";
		images = get_all_regex_matched(html, reg_exp, 1);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			if (getFileSizeWithProxyConsiderations (obj.MediaURL) > 0) {
				mls.Photos.photo.push(obj);
							imageCount++;
				}
		});	
		
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Houses
		{
			try {
				// Houses for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/houses-For-Sale", KHOUSEFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Houses for lease
				cumulatedCount += crawlCategory("https://www.homespakistan.com/houses-For-Rent", KHOUSEFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartments
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/apartment-for-sale", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartments for rent
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Apartment-For-Rent", KFLATFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
        }
        // Farmhouses
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/farmhouse-plots-for-sale", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
        }
        // Buildings
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Commercial-Building-For-Sale", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartments for rent
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Commercial-Building-For-Rent", KFLATFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Lands
		{
			try {
				// Plots for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Residential-Plot-For-Sale", KLANDFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Shops
		{
			try {
				// Shops for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Shop-For-Sale", KLOCALFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
            try {
				// Shops for rent
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Shop-For-Rent", KLOCALFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Offices
		{
			try {
				// Offices for sale
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Office-For-Sale", KOFFICEFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Offices for rent
				cumulatedCount += crawlCategory("https://www.homespakistan.com/Office-For-Sale", KOFFICEFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
