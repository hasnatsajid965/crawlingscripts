function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"columnmore\">[\s\t\n ]*?<a href=\"([^\"]*)\">", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a class=\"next page-numbers\" href=\"([^\"]*)\">Siguiente</a>[\\t\\s\\t ]*?</div>[\\s\\t\\n ]*?<ul id=\"inner-columns\"", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<div class=\"left-inner\">[\s\t\n ]*?<h1>([^\<]*)</h1>", 1, KNOTIFYERROR);
		// mls.DetailedCharacteristics = get_all_regex_matched(html,
		// "<div><ul><li>(.*)(?=</li></ul></div>)", 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<div><ul><li>(.*)(?=</li></ul></div>)', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, "<p itemprop=\"description\" id=\"corepropertydescription\">(.*)(?=</p></div>)", 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, "</div>[\t\s\n ]*?<h3>([^\<]*)</h3>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<span>Mobile</span>[\t\s\n ]*:([^\<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, "<li class=\"email\"><a href=\"\">([^\<]*)</a>", 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_unique_regex_match(html, "<span>Office</span>[\t\s\n ]*:([^\<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "<li><b>RefNo#</b>:[\s\t ]*?([^\<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "<meta content=\"([a-zA-Z\$]*)\" itemprop=\"priceCurrency\"/><meta content=\"([0-9\.\,]*)\" itemprop=\"price\"/>", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<meta content=\"([a-zA-Z\$]*)\" itemprop=\"priceCurrency\"/><meta content=\"([0-9\.\,]*)\" itemprop=\"price\"/>", 1, KDONOTNOTIFYERROR);
		for_rent = get_next_regex_match(html, 0, "<span class='per'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "por mes") {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<li><b>Prop.Type</b>:([^\<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<li><b>Living Space</b>: ([0-9\,\.]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<li><b>Living Space</b>: ([0-9\,\.]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<li><b>Land Area</b>: ([0-9\,\.]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<li><b>Land Area</b>: ([0-9\,\.]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "itemprop=\"addressRegion\">([^\<]*)</span>", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "itemprop=\"addressCountry\">([^\<]*)</span>", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "data-large-src=\"([^\"]*)\"[\s\t ]*?/>", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "https://century21numberone.com" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		// print(mls);
		// beep();
		// pause();
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://century21numberone.com/es/curacao-inmobiliaria/residenciales-vacaciones-pisos/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21numberone.com/es/curacao-inmobiliaria/propiedad-vacaciones-nueva/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://century21numberone.com/es/curacao-inmobiliaria/propiedad-vacaciones-nueva/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://century21numberone.com/es/curacao-inmobiliaria/comercial-para-la-venta/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://century21numberone.com/es/curacao-inmobiliaria/comercial-para-la-venta/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
