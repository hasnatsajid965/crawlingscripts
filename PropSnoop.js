qa_override("[E1473228885]", "Some properties contain lot size and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling

function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var check = [];
	var cachedSet = new StringSet();
	while (true) {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		// virtual_browser_sendKeys(browser, "\\\uE010");
		wait(20000);
		do {
			el1 = virtual_browser_find_one(browser, "(//a[@class='property__name'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			check[index] = el1;
			if (check[index] == check[index - 1]) {
				return actualCount;
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//a[@class='next page-numbers']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			index = 1;
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		if (mls.Brokerage == undefined)
			mls.Brokerage = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<div class=\"property-agent__name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		;
		mls.Brokerage.Phone = get_next_regex_match(html, 0,
				"<div class=\"property-agent__details-item-icon\"><i class=\"fas fa-phone-alt\"></i>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<a href=\"[^\"]*\"[\\s\\t\\n ]*?class=\"property-agent__details-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, "<i class=\"far fa-envelope\"></i>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<a href=\"[^\"]*\"[\\s\\t\\n ]*?class=\"property-agent__details-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		;
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"property__title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "Property Id:</strong>[\\s\\t\\n ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KNOTIFYERROR);
		mls.ListingDescription = get_unique_regex_match(html, "<div class=\"text-area\">(.+?)</div>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "Area: </strong>[\\s\\t\\n ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "Price: </strong>[\s\t\n ]*?<span class=\"property__listing-link\">([a-zA-Z\. ]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "Price: </strong>[\s\t\n ]*?<span class=\"property__listing-link\">([a-zA-Z\. ]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		if (mls.ListPrice.value == 1) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, "Property Lot Size:</strong>[\s\t\n ]*?<span class=\"property__listing-link\">([0-9\.\, ]*)[a-zA-Z\.\,]*<sup>2</sup>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Property Lot Size:</strong>[\s\t\n ]*?<span class=\"property__listing-link\">[0-9\.\, ]*([a-zA-Z\.\,]*)<sup>2</sup>", 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, "Property Lot Size:</strong>[\s\t\n ]*?<span class=\"property__listing-link\">[0-9\.\, ]*[a-zA-Z\.\,]*<sup>([0-9]*)</sup>", 1, KDONOTNOTIFYERROR);

		mls.NonMLSListingData.PropertySize.value = get_unique_regex_match(html, "Property Size: </strong>[\s\t\n ]*?<span class=\"property__listing-link\">([0-9\.\, ]*)[a-zA-Z\.\,]*<sup>2</sup>", 1, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.PropertySize.areaUnits = get_unique_regex_match(html, "Property Size: </strong>[\s\t\n ]*?<span class=\"property__listing-link\">[0-9\.\, ]*([a-zA-Z\.\,]*)<sup>2</sup>", 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, "Property Lot Size:</strong>[\s\t\n ]*?<span class=\"property__listing-link\">[0-9\.\, ]*[a-zA-Z\.\,]*<sup>([0-9]*)</sup>", 1, KDONOTNOTIFYERROR);
		property_type = get_next_regex_match(html, 0, "<div class=\"property__category\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(property_type)) {
			if (property_type.includes("Sale") && property_type.includes("Condos")) {
				mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
			}
		}
		forRent = get_next_regex_match(html, 0, "<div class=\"property__category\">(.+?)</div>", 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);

			}
		}

		mls.PropertyType.value = property_type.replace(/\s+/g, " ").trim();
		typee = property_type.split(" ");
		if (isDefined(typee[0])) {
			mls.PropertyType.value = typee[0];
		}
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}

		}
		mls.Bedrooms = get_unique_regex_match(html, "Bedroom: </strong>[\\s\\t\\s ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms: </strong>[\\s\\t\\n ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "data-lat=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "data-lng=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "Country: </strong>[\\s\\t\\n ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "Year Built: </strong>[\\s\\t\\n ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_unique_regex_match(html, "Floors No: </strong>[\\s\\t\\n ]*?<span class=\"property__listing-link\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img class=\"property__gallery-nav-img\"[\\s\\t\\n ]*?src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("-143x83.jpg", ".jpg");
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function housesForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Houses')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function appartmentForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Mixed Use')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function appartmentsForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Apartments')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function officesForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Office Space')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function storesForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Retail Space')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function wareHouseForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Warehouse Space')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function beachHouseForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Beach House')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function condoForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Condos')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function townHouseForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Town House')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function landForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "(//span[contains(text(),' Land  ')])[1]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function agricultureForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Agricultural Use')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function freeHoldLandForSale(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Freehold Land')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

// For Rent
function housesForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Houses')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function appartmentForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Mixed Use')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function appartmentsForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Apartments')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function officesForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Office Space')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function storesForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Retail Space')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function wareHouseForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Warehouse Space')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function beachHouseForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Beach House')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function condoForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Condos')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function townHouseForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Town House')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function landForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "(//span[contains(text(),' Land  ')])[1]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function agricultureForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Agricultural Use')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function freeHoldLandForRent(browser, category) {
	virtual_browser_navigate(browser, "https://propsnoop.com/");
	virtual_browser_navigate(browser, "https://propsnoop.com/action/for-rent/");
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//div[text()='All types']", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//span[contains(text(),'- Freehold Land')]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		virtual_browser_navigate(browser, "https://propsnoop.com/action/for-sale/");
		cumulatedCount += housesForSale(browser, KCASASVENTAS);
		cumulatedCount += appartmentForSale(browser, KAPTOVENTAS);
		cumulatedCount += appartmentsForSale(browser, KAPTOVENTAS);
		cumulatedCount += officesForSale(browser, KOFICINASVENTAS);
		cumulatedCount += storesForSale(browser, KTIENDASVENTAS);
		cumulatedCount += wareHouseForSale(browser, KOTROSVENTAS);
		cumulatedCount += beachHouseForSale(browser, KCASASVENTAS);
		cumulatedCount += condoForSale(browser, KCASASVENTAS);
		cumulatedCount += townHouseForSale(browser, KCASASVENTAS);
		cumulatedCount += landForSale(browser, KTERRENOSVENTAS);
		cumulatedCount += agricultureForSale(browser, KTERRENOSVENTAS);
		cumulatedCount += freeHoldLandForSale(browser, KTERRENOSVENTAS);
		cumulatedCount += housesForRent(browser, KCASASALQUILER);
		cumulatedCount += appartmentForRent(browser, KAPTOALQUILER);
		cumulatedCount += appartmentsForRent(browser, KAPTOALQUILER);
		cumulatedCount += officesForRent(browser, KOFICINASALQUILER);
		cumulatedCount += storesForRent(browser, KTIENDASALQUILER);
		cumulatedCount += wareHouseForRent(browser, KOTROSALQUILER);
		cumulatedCount += beachHouseForRent(browser, KCASASALQUILER);
		cumulatedCount += condoForRent(browser, KCASASALQUILER);
		cumulatedCount += townHouseForRent(browser, KCASASALQUILER);
		cumulatedCount += landForRent(browser, KTERRENOSALQUILER);
		cumulatedCount += agricultureForRent(browser, KTERRENOSALQUILER);
		cumulatedCount += freeHoldLandForRent(browser, KTERRENOSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
