qa_override("[E2592408747]", "Some properties have city and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^"]*)" class="link_inmueble" ', category, "https://www.detrasdelafachada.com/", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "title=\"Go to the next page\">></a></li><li><a href='([^']*)'", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		title = get_next_regex_match(html, 0, "<title>([^<]*)</title>", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = title.replace(/[^a-zA-Z\-\%\s]/g, "");
		mls.Bathrooms = get_unique_regex_match(html, 'Ba[^\:]*os:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Dormitorios:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, 'Precio de venta:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-datos precio\">[\\s\\t\\n ]*?([0-9\.\, ]+)([a-zA-Z]+)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Precio de venta:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-datos precio\">[\\s\\t\\n ]*?([0-9\.\, ]+)([a-zA-Z]+)', 2, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<p class="descripcion">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription) && isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			return analyzeOnePublication_return_unreachable;
		}
		var features = get_all_regex_matched(html, "<li class=\"lista_confort\"><span>[^\<]*</span>([^<]*)", 1);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pets Allowed") {
						mls.DetailedCharacteristics.hasPetAllowed = true;
					} else if (feature === "Furnished") {
						mls.DetailedCharacteristics.hasFurnished = true;
					} else if (feature === "Foundation") {
						mls.DetailedCharacteristics.hasFoundation = true;
					} else if (feature === "Garage") {
						mls.DetailedCharacteristics.hasGarage = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.YearBuilt = get_unique_regex_match(html, 'A[^\:]*o de construcci[^\:]*n:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-datos\">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'Superficie:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-datos\">[\\s\\t\\n ]*?([0-9\.\, ]+)([a-zA-Z0-9]+)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Superficie:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-datos\">[\\s\\t\\n ]*?([0-9\.\, ]+)([a-zA-Z0-9]+)', 2, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, 'Nombre y apellidos:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'Tel[^\:]*fono:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, "<br />[\\s\\t\\n ]*?email:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<div class="col-datos">[\\s\\n\\t ]*?Ref:([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.PropertyType.value = get_unique_regex_match(html, 'Tipo de inmueble:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'property="og:latitude" content="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'property="og:longitude" content="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, 'Locality:[\s\t\n ]*?</div>[\s\t\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Cuba";
		mls.Address.FullStreetAddress = get_unique_regex_match(html, 'Direcci[^\:]*n:[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-datos">([^<]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '</div>[\\s\\t\\n ]*?<img  src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.detrasdelafachada.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// multifamily en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/apartments", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/houses", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/semi-detached-houses", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/villas", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Cases en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/chambers", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Cases en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/mansions", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Cases en venta
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-homes-sale-cuba/penthouses", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-rooms-houses-rent-cuba/homes", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://www.detrasdelafachada.com/list-rooms-houses-rent-cuba/lands", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
