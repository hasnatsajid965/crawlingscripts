qa_override("[E1473228885]", "Some of the properties have lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	// var html = virtual_browser_html(browser);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class='property_meta'>[\\s\\t\\n ]*?<a href=([^\>]*)", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\"><i class=\"fa fa-forward\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		list_not_available = get_unique_regex_match(html, '<h1 class="text-really-big p-lg pos-relative">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "404") {
				return analyzeOnePublication_return_unreachable;
			}
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="property_description cboth">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<h3 class=\"property_price f-right col-sm-6\">([\$]*)([0-9\,\. ]*)", 2, KNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<h3 class=\"property_price f-right col-sm-6\">([\$]*)([0-9\,\. ]*)", 1, KNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			list_price = mls.ListPrice.value.replace(",", "");
			if (list_price < 50000) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
			if (list_price == "") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls.MlsId = get_unique_regex_match(html, "ID#:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h3 class=\"property_name\" style=\"margin-bottom: 10px;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, "<h3 class=\"property_name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, "data-src=\"/wp-content/uploads/2018/12/area.png\" alt=\"sq.ft.\" /><p>([0-9\.\, ]*)([a-zA-Z\.\,]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "data-src=\"/wp-content/uploads/2018/12/area.png\" alt=\"sq.ft.\" /><p>([0-9\.\, ]*)([a-zA-Z\.\,]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "src=\"/wp-content/uploads/2018/12/bedroom.png\" alt=\"Bedrooms\" /><p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = mls.Bedrooms.replace(/[^0-9\.]/g, "");
		mls.Bathrooms = get_unique_regex_match(html, "src=\"/wp-content/uploads/2018/12/bathroom.png\" alt=\"Bathrooms\" /><p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = mls.Bathrooms.replace(/[^0-9\.]/g, "");
		mls.Location.Latitude = get_next_regex_match(html, 0, "var lat = \"([^\"]*)\";[\\s\\t\\n ]*?var lng = \"([^\"]*)\";[\\s\\t\\n ]*?var location", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "var lat = \"([^\"]*)\";[\\s\\t\\n ]*?var lng = \"([^\"]*)\";[\\s\\t\\n ]*?var location", 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, 'Features: </span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Piscina") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var for_rent = get_unique_regex_match(html, 'Tipo de anuncio</span>[\\s\\t\\n ]*?<span class="pull-right font-bold">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "Se vende" || for_rent == "Se alquila") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.Address.City.value = get_unique_regex_match(html, '<p class="property_address">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.Address.City.value === "") {
			mls.Address.City.value = get_unique_regex_match(html, '<p class="property_address"><span style="font-weight: 200;">([^\<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.Address.Country.value = "British Virgin Islands";
		mls.Brokerage.Name = "Coldwell Banker Real Estate BVI";
		mls.Brokerage.Phone = "284-495-3000";
		mls.Brokerage.Email = "info@coldwellbankerbvi.com";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img class='rsTmb' src='([^\']*)'", 1);
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				oneImageTag = oneImageTag.replace("-150x150.jpg", ".jpg");
				oneImageTag = oneImageTag.replace("-150x150.png", ".png");
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		// setProxyConditions(true, null);
		// rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=sales_listings&property_type=Apartment", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en rent
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=long_term_rentals&property_type=Apartment", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=sales_listings&property_type=House", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=sales_listings&property_type=Commercial", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=sales_listings&property_type=Villa", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=long_term_rentals&property_type=Commercial", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=long_term_rentals&property_type=House", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=long_term_rentals&property_type=Villa", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=sales_listings&property_type=Land", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en rent
				cumulatedCount += crawlCategory("https://www.coldwellbankerbvi.com/sales_listings/?post_type=long_term_rentals&property_type=Land", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
