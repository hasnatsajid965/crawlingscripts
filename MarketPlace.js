qa_override("[E1473228885]", "Some of the properties contain lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"btn btn-outline btn-primary hidden-xs\" href=\"([^\"]*)\"", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "class=\"js-search-filter next\" href=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// setProxyConditions(true, null);
	// rotateUserAgents(true);
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		list_not_available = get_unique_regex_match(html, '<h1 class="text-really-big p-lg pos-relative">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "404") {
				return analyzeOnePublication_return_unreachable;
			}
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<p class="item-description m-b-0">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"h2 text-ellipsis m-b-sm item-price\">([0-9\,\. ]*)([a-zA-Z\$ ]*)", 1, KNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"h2 text-ellipsis m-b-sm item-price\">([0-9\,\. ]*)([a-zA-Z\$ ]*)", 2, KNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.MlsId = get_unique_regex_match(html, "Id del anuncio</span>[\\s\\t\\n ]*?<span class=\"pull-right text-muted\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"item-title h2 m-b-sm text-ellipsis\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "Construcci[^\<]*n [\(]([a-zA-Z\.\, ]*)[\)]</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "Construcci[^\<]*n [\(]([a-zA-Z\.\, ]*)[\)]</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Solar [\(]([a-zA-Z\.\, ]*)[\)]</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Solar [\(]([a-zA-Z\.\, ]*)[\)]</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "<span class=\"m-r\">[\\s\\t\\n ]*?<i class=\"ion-bookmark m-r-sm\"></i>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "N?mero de habitaciones</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "N?mero de ba?os</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "N?mero de parqueos</span>[\\s\\t\\n ]*?<span class=\"pull-right font-bold\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<span class="text pull-left">([^\<]*)</span>[\\s\\t\\n ]*?<span class="pull-right font-bold">[\\s\\t\\n ]*?<i class="ion-checkmark-round text-success">', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Piscina") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var for_rent = get_unique_regex_match(html, 'Tipo de anuncio</span>[\\s\\t\\n ]*?<span class="pull-right font-bold">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "Se alquila") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (for_rent == "Se vende" && mls.PropertyType.value.includes("Oficinas")) {
				mls.NonMLSListingData.category = getCategory(KOFICINASVENTAS);
			}
		}
		mls.Location.Directions = get_unique_regex_match(html, "<i class=\"ion-location m-r-sm\"></i>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var comma = mls.Location.Directions.split(",").length;
			var tokens = mls.Location.Directions.split(",");
			if (comma == 3) {
				if (tokens.length > 0) {
					if (isDefined(tokens.length)) {
						mls.Address.StateOrProvince.value = tokens[tokens.length - 3];
					}
				}
				if (tokens.length > 1) {
					if (isDefined(tokens.length)) {
						mls.Address.City.value = tokens[tokens.length - 2].trim();
					}
				}
				if (tokens.length > 2) {
					if (isDefined(tokens.length)) {
						mls.Address.Country.value = tokens[tokens.length - 1].trim();
					}
				}
			}
			if (comma == 2) {
				if (tokens.length > 0) {
					if (isDefined(tokens.length)) {
						mls.Address.City.value = tokens[tokens.length - 2].trim();
					}
				}
				if (tokens.length > 1) {
					if (isDefined(tokens.length)) {
						mls.Address.Country.value = tokens[tokens.length - 1].trim();
					}
				}
			}
		}
		mls.Brokerage.Name = get_unique_regex_match(html, '<a class="link-default" href="[^\"]*" >([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<i class="ion-ios-telephone"></i>&nbsp;([^\<]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "class=\"img-responsive\" data-src=\"[^\"]*\" data-lazy=\"([^\"]*)\"", 1);
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		// setProxyConditions(true, null);
		// rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,104/meta54,Se+vende", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,104/meta54,Se+alquila", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,101/meta54,Se+vende", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,108/meta54,Se+vende", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,107/meta54,Se+vende", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,109/meta54,Se+vende", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,101/meta54,Se+alquila", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,108/meta54,Se+alquila", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,107/meta54,Se+alquila", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,109/meta54,Se+alquila", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Stores
		{
			try {
				// Stores en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,105/meta54,Se+vende", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Stores en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,105/meta54,Se+alquila", KTIENDASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,106/meta54,Se+vende", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,106/meta54,Se+alquila", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,110/meta54,Se+vende", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Other en rent
				cumulatedCount += crawlCategory("https://www.marketplace.com.do/Republica_Dominicana/sOrder,dt_pub_date/sShowAs,list/iOrderType,desc/category,110/meta54,Se+alquila", KOTROSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
