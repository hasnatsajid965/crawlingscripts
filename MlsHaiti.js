qa_override("[E1473228885]", "Lot size does not have areaunits");
qa_override("[E1554112646]", "I am overring this error because images are crawling but showing as an error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"img-clipped img-clipped-y img-clipped-y-4by3\" href=\"([^\"]*)\">", category, "https://www.mlshaiti.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" aria-label=\"Next\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.mlshaiti.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.PropertyType.value = get_unique_regex_match(html, "itemprop=\"longitude\" />[\\s\\t\\n ]*?</span>[\\s\\t\\n ]*?<span class=\"small\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"listing-price text-right\">([\$]*)([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"listing-price text-right\">([\$]*)([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		forTitle = get_unique_regex_match(html, "<span itemprop=\"addressLocality\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<span itemprop=\"streetAddress\">([^\<]*)", 1, KDONOTNOTIFYERROR) + "," + forTitle;
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, '</header>[\\s\\t\\n ]*?<div class="col-xs-12">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListingDescription)) {
			if (mls.ListingDescription.includes("Click") || mls.ListingDescription.includes("click")) {
				var index = mls.ListingDescription.indexOf("Click"); // Gets
				// the
				// first
				// index
				// where
				// a
				// space
				// occours
				mls.ListingDescription = mls.ListingDescription.substr(0, index);
			}
		} else {
		}
		mls.LotSize.value = get_unique_regex_match(html, "<span class=\"stat-value\">([^\<]*)</span>[\\s\\t\\n ]*?<span class=\"stat-label small text-nowrap\">Acre", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<span class=\"stat-value\">([^\<]*)</span>[\\s\\t\\n ]*?<span class=\"stat-label small\">Beds</span>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<span class=\"stat-value\">([^\<]*)</span>[\\s\\t\\n ]*?<span class=\"stat-label small\">Full Baths", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "<small class=\"font-11\">MLS #:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<h4 class=\"m-y-0\"><a href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "Mobile:</td>[\\s\\t\\n ]*?<td class=\"font-11 text-primary-shade7\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '<meta content="([^\"]*)" itemprop="latitude" />', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '<meta content="([^\"]*)" itemprop="longitude" />', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<span itemprop=\"streetAddress\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "<span itemprop=\"addressLocality\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Haiti";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img src=\"([^\"]*)\" data-title=\"\" data-description=\"\">", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			oneImageTag = oneImageTag.replace("Thumb", "Full");
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.mlshaiti.com/Property/List?ForSaleOnly=True&HaitiListingsOnly=True&Location=&PropertyTypeCode=RES&MinPrice=&MaxPrice=&MinBeds=&MinBaths=&MinSqft=&MaxSqft=&MinGarage=&MinAcreage=&MaxAcreage=&MinStories=&MinYearBuilt=&MaxYearBuilt=&Radius=&Keyword=&DateListedDaysBack=&IsWaterfront=false&OpenHousesOnly=false&ShowActiveListings=false&ShowComingSoonListings=false&ShowPendingListings=false&ShowSoldListings=false",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.mlshaiti.com/Property/List?ForSaleOnly=True&HaitiListingsOnly=True&Location=&PropertyTypeCode=IMF&MinPrice=&MaxPrice=&MinBeds=&MinBaths=&MinSqft=&MaxSqft=&MinGarage=&MinAcreage=&MaxAcreage=&MinStories=&MinYearBuilt=&MaxYearBuilt=&Radius=&Keyword=&DateListedDaysBack=&IsWaterfront=false&OpenHousesOnly=false&ShowActiveListings=false&ShowComingSoonListings=false&ShowPendingListings=false&ShowSoldListings=false",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory(
						"https://www.mlshaiti.com/Property/List?ForSaleOnly=True&HaitiListingsOnly=True&Location=&PropertyTypeCode=COM&MinPrice=&MaxPrice=&MinBeds=&MinBaths=&MinSqft=&MaxSqft=&MinGarage=&MinAcreage=&MaxAcreage=&MinStories=&MinYearBuilt=&MaxYearBuilt=&Radius=&Keyword=&DateListedDaysBack=&IsWaterfront=false&OpenHousesOnly=false&ShowActiveListings=false&ShowComingSoonListings=false&ShowPendingListings=false&ShowSoldListings=false",
						KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.mlshaiti.com/Property/List?ForSaleOnly=True&HaitiListingsOnly=True&Location=&PropertyTypeCode=FRM&MinPrice=&MaxPrice=&MinBeds=&MinBaths=&MinSqft=&MaxSqft=&MinGarage=&MinAcreage=&MaxAcreage=&MinStories=&MinYearBuilt=&MaxYearBuilt=&Radius=&Keyword=&DateListedDaysBack=&IsWaterfront=false&OpenHousesOnly=false&ShowActiveListings=false&ShowComingSoonListings=false&ShowPendingListings=false&ShowSoldListings=false",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.mlshaiti.com/Property/List?ForSaleOnly=True&HaitiListingsOnly=True&Location=&PropertyTypeCode=LND&MinPrice=&MaxPrice=&MinBeds=&MinBaths=&MinSqft=&MaxSqft=&MinGarage=&MinAcreage=&MaxAcreage=&MinStories=&MinYearBuilt=&MaxYearBuilt=&Radius=&Keyword=&DateListedDaysBack=&IsWaterfront=false&OpenHousesOnly=false&ShowActiveListings=false&ShowComingSoonListings=false&ShowPendingListings=false&ShowSoldListings=false",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
