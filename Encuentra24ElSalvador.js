var countryContext = "El Salvador";

include("base/Encuentra24Functions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-apartments-condos", KAPTOVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-houses-homes", KCASASVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Buildings
		{
			// Buildings on venta
			try {
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-buildings", KEDIFICIOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-lots-land", KTERRENOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Edificios
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-offices", KEDIFICIOSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-beachfront-homes-and-lots", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-farms", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-business", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-commercial", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.encuentra24.com/el-salvador-en/real-estate-for-sale-island-properties", KOTROSVENTAS, "Hace .* d?as");
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

