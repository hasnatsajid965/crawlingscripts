

// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(10000);
		do {
			el1 = virtual_browser_find_one(browser, "(//div[@class='scrollable']/a[@data-rf-test-id='slider-item-1'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//button[@class='clickable buttonControl button-text']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KDONOTNOTIFYERROR);
						wait(6000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="remarks" id="marketing-remarks-scroll" data-rf-test-id="listingRemarks">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_next_regex_match(html, 0, 'Year Built</span><span class="content text-right">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, 'MLS#</span><span class="content text-right">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span>([0-9\,\. ]*)</span></div></div><span class=\"statsLabel\">Price</span>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"statsValue\"><div><span>([\$]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Bedrooms = get_next_regex_match(html, 0, '<div class="info-block" data-rf-test-id="abp-beds"><div class="statsValue">([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, '<div class="info-block" data-rf-test-id="abp-baths"><div class="statsValue"><span>([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<meta name="twitter:image:photo[^\"]*" content="([^\"]*)">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://ssl.cdn-redfin.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<span data-rf-test-id=\"abp-streetLine\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_next_regex_match(html, 0, 'class="info-block sqft" data-rf-test-id="abp-sqFt"><span><span class="statsValue">([0-9\,\. ]*)</span> <span class="sqft-label">([a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, 'class="info-block sqft" data-rf-test-id="abp-sqFt"><span><span class="statsValue">([0-9\,\. ]*)</span> <span class="sqft-label">([a-zA-Z\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.Location.Directions = mls.ListingTitle;
		mls.Address.City.value = get_next_regex_match(html, 0, '"addressLocality":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.PostalCode.value = get_next_regex_match(html, 0, '"postalCode":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "USA";
		mls.Brokerage.Name = "RedFin";
		mls.Brokerage.Phone = "1-844-759-7732";
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		// setProxyConditions(true, null);
		// rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.redfin.com/neighborhood/496060/FL/Daytona-Beach/Neighborhood-L/filter/property-type=house,viewport=29.2266:29.19011:-81.0231:-81.0788,no-outline", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.redfin.com/neighborhood/496060/FL/Daytona-Beach/Neighborhood-L/filter/property-type=townhouse,viewport=29.2266:29.19011:-81.0231:-81.0788,no-outline", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.redfin.com/neighborhood/496060/FL/Daytona-Beach/Neighborhood-L/filter/property-type=multifamily,viewport=29.2266:29.19011:-81.0231:-81.0788,no-outline", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.redfin.com/neighborhood/496060/FL/Daytona-Beach/Neighborhood-L/filter/property-type=condo,viewport=29.2266:29.19011:-81.0231:-81.0788,no-outline", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.redfin.com/neighborhood/496060/FL/Daytona-Beach/Neighborhood-L/filter/property-type=land,viewport=29.2266:29.19011:-81.0231:-81.0788,no-outline", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.redfin.com/neighborhood/496060/FL/Daytona-Beach/Neighborhood-L/filter/property-type=other,viewport=29.2266:29.19011:-81.0231:-81.0788,no-outline", KOTROSVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
