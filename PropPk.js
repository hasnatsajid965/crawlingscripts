
// crawlForPublications crawl-mode: Regular-Expression Crawling

function crawlCategory(url, category, stopword) {	
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			
			//<a class="block clearfix" href="https://www.prop.pk/property/details-23628440.html"  title="Brand New 10 Marla House For Sale Located In Bahria Town" >
            var reg_exp = "<a class=\"([^\"]*)\" href=\"([^\"]*)\"  title=\"([^\"]*)\" >";
            while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, reg_exp, category, "", 2, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
                }
            }
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
        }
        //NEXT button regex ***************************************************
        //<link rel="next" href="https://www.prop.pk/lahore/houses-for-sale-1/2/" /> 
        reg_exp = "<link rel=\"next\" href=\"([^\"]*)\" />";
		var relativeLink = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
        
        // BROKER CODES ************************************
        reg_exp = "<span>([0-9]*)</span>";
        var broker_code = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
        if (isDefined(broker_code)) {
            if(mls.NonMLSListingData.brokerCodes == undefined) {
                mls.NonMLSListingData.brokerCodes = [];
            }
            mls.NonMLSListingData.brokerCodes.push(broker_code);
            mls.NonMLSListingData.brokerCodes.push("ID" + broker_code);
        }
        print("BROKER CODE ***********************: " + broker_code);
	
		/*
		forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-122 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (get_next_regex_match(html, 0, "<i id=\"houzez-print\" class=\"fa fa-print\"", 0, KDONOTNOTIFYERROR) != undefined) {
			var pos = get_last_regex_match_end_pos();
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}*/
		
		// LISTING PRICE -------------------------------------------------------
		//<div class="left price" id="slider_bottom_price"><span class="unit">PKR</span>1.22 Crore

	/*	reg_exp = "<div class=\"left price\" id=\"slider_bottom_price\">([^\"]*)<span class=\"unit\">([^\"]*)</span>([0-9\.]*) ([A-Za-z]*)";
		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
			var price_multiplier = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
*/
		reg_exp = "<div class=\"fixed-price-wrap\" id=\"pview_fixed_price\">([\\n\\s]*)<div class=\"container\">([\\n\\s]*)<div class=\"left price\">([\\n\\s]*)<span class=\"unit\">([^\"]*)</span> ([0-9.]*)([\\n\\s]*)([A-Za-z]*)([\\n\\s]*)</div>";
		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, reg_exp, 5, KDONOTNOTIFYERROR);
			var price_multiplier = get_unique_regex_match(html, reg_exp, 7, KDONOTNOTIFYERROR);
			print("Price multiplier:	" + price_multiplier);
			print("Price number value:	" + price_number_value);

			if (isDefined(price_multiplier)) {
				switch(price_multiplier.toLowerCase()) {
					case "crore": {
						price_number_value *= 10000000;
					}
					break;
					case "lakh": {
						price_number_value *= 100000;
					}
					break;
					case "thousand": {
						price_number_value *= 1000;
					}

					break;

					

					default: {
						throw "Unhandled multiplier: " + price_multiplier;
					}
				}
			}
			mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, reg_exp, 4, KDONOTNOTIFYERROR);
			print("price_number_value: "+ price_number_value +" " + price_multiplier + " "+ mls.ListPrice.currencyCode);			

		}
		
		/*
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Renta")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} else {
			forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-123 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Renta")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}*/
		
		// LISTING TITLE ***********************************************
		//<h1 class="title">5 Marla house for sale in DHA 9 Town</h1>
		reg_exp = "<h1 class=\"title\">(.*)</h1>";
		
		if (isDefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, reg_exp, 1, KNOTIFYERROR);
		}
		print("Listing title *****: " + mls.ListingTitle);
		
		
		// LISTING DESCRIPTION -------------------------------------------------------
		//<p id="desc_full">(.*)</p>

		reg_exp = "<p id=\"desc_full\">([^\"]*)<\/p>";
		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR)
		}
		print("Listing Description *****:	" + mls.ListingDescription);
		

		reg_exp = "<div id=\"print_data_id\" class=\"content toogle-class\">([\\n\\s]*)<input type=\"hidden\" id=\"chk_for_more_detail_desc\" value=\"([^\"]*)\">([\\n\\s]*)<p id=\"desc_full\"> ([^\"]*) <\/p>([\\n\\s]*)<\/div>";
		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR)
		}
		print("Listing Description *****:	" + mls.ListingDescription);

		// LAND SIZE -------------------------------------------------------
		//<ul class=\"left d-inline-block details\">([\n\s]*)<li class=\"d-inline-block\">([\n\s]*)([0-9]*) Marla([\n\s]*)</li>([\n\s]*)<li class="d-inline-block">([\n\s]*)3 Beds([\n\s]*)</li>([\n\s]*)<li class="d-inline-block">([\n\s]*)4 Baths([\n\s]*)</li>([\n\s]*)</ul>
        reg_exp = "<ul class=\"left d-inline-block details\">([\\n\\s]*)<li class=\"d-inline-block\">([\\n\\s]*)([0-9\.]*) ([A-Za-z]*)([\\n\\s]*)<\/li>([\\n\\s]*)<li class=\"d-inline-block\">([\\n\\s]*)([0-9\.]*) ([A-Za-z]*)([\\n\\s]*)<\/li>([\\n\\s]*)<li class=\"d-inline-block\">([\\n\\s]*)([0-9\.]*) ([A-Za-z]*)([\\n\\s]*)<\/li>([\\n\\s]*)<\/ul>([\\n\\s]*)<\/div>([\\n\\s]*)<\/div>";
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		print("Land Size Value and Area Units *****:	" + mls.LotSize.value + " " + mls.LotSize.areaUnits);
		
		// BEDROOMS -------------------------------------------------------
		//<li aria-label="Property detail beds"><span class="_3af7fa95">Bedroom(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		//reg_exp = "<li aria-label=\"Property detail beds\"><span class=\"([^\"]*)\">([^\"]*)</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 8, KDONOTNOTIFYERROR);
		}
		print("Bedrooms *****:	" + mls.Bedrooms);
		
		// BATHROOMS -------------------------------------------------------
		//<li aria-label="Property detail baths"><span class="_3af7fa95">Bath(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		//reg_exp = "<li aria-label=\"Property detail baths\"><span class=\"([^\"]*)\">([^\"]*)</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 13, KDONOTNOTIFYERROR);
		}
		print("Bathrooms *****:	" + mls.Bathrooms);
		
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<ul class=\"left d-inline-block property-type\">([\\n\\s]*)<li class=\"d-inline-block text-uppercase p-relative\">([\\n\\s]*)([^\"]*)([\\n\\s]*)</li>";
 		if (isDefined(mls.PropertyType.value)) {
			mls.PropertyType.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		}
		print("Property type *****:	" + mls.PropertyType.value);
		
		if (isDefined(mls.PropertyType.value.toLowerCase())) {
			property_type = mls.PropertyType.value.toLowerCase();
			if (property_type.includes("house")) {
					mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
			}
			if (property_type.includes("apartment")) {
				mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
			}
		}
		print("Listing data category *****:	" + mls.NonMLSListingData.category);



		reg_exp = "<a href=\"([^\"]*)\">Prop.pk</a>([\\n\\s]*)<svg class=\"svg-arrow\">([\\n\\s]*)<use xlink:href=\"([^\"]*)\"></use>([\\n\\s]*)</svg>([\\n\\s]*)</li>([\\n\\s]*)<li>([\\n\\s]*)<a href=\"([^\"]*)\">([^\"]*)</a>([\\n\\s]*)<svg class=\"svg-arrow\">";
		if (isDefined(mls.Address.City.value)) {
			var city_name = get_unique_regex_match(html, reg_exp, 10, KDONOTNOTIFYERROR);
			mls.Address.City.value = city_name;
		}
		print("Broker Address *****:	" + city_name);

		
		mls.Address.Country.value = "Pakistan";
		reg_exp = "\"google_map_default_longitude\":\"([0-9\.]*)\",\"google_map_default_latitude\":\"([0-9\.]*)\",\"new_projects_section";
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		print("Latitude *****:	" + mls.NonMLSListingData.propertyLocation.Latitude.value);
		print("Longitude *****:	" + mls.NonMLSListingData.propertyLocation.Longitude.value);


		//<picture class="_219b7e0a"><source type="image/webp" srcSet="https://media.zameen.com/thumbnails/85464870-120x90.webp"/><img role="presentation" alt="2 " title="2 " src="https://media.zameen.com/thumbnails/85464870-120x90.jpeg"/></picture>
		reg_exp = "<a data-flag=\"([^\"]*)\" href=\"([^\"]*)\">([\\n\\s]*)<img data-flag=\"([^\"]*)\" src=\"([^\"]*)\" alt=\"\">([\\n\\s]*)</a>";
		images = get_all_regex_matched(html, reg_exp, 2);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			//var increase_pixels = "-800x600.jpeg";
			//special = obj.MediaURL.substring(0, obj.MediaURL.indexOf('-'));
			//obj.MediaURL = special.concat(increase_pixels);
			if (getFileSizeWithProxyConsiderations (obj.MediaURL) > 0) {
				mls.Photos.photo.push(obj);
							imageCount++;
				}
		});
/*		
		// VIDEOS --------------------------------------------------------
		//"title":null,"host":"youtube","url":"https:\u002F\u002Fyoutu.be\u002FwgGhdi9Ed7U","orderIndex":0},"createdAt":1591812523,"
		reg_exp = "\"title\":([^\"]*),\"host\":\"youtube\",\"url\":\"([^\ ]*)\",\"orderIndex\":([^\"]*)},\"createdAt\":([^\"]*),\"";
		videos = get_all_regex_matched(html, reg_exp, 2, KDONOTNOTIFYERROR);
		
		var videoCount = 0;
		videos.forEach(function(oneVideoTag) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = oneVideoTag;
			obj.MediaOrderNumber = videoCount;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
				mls.Videos.video.push(obj);
					videoCount++;
				
		});
*/
		
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlCities(url, category){
	var count = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	if(isDefined(html)){
		//<a href="https://www.prop.pk/lahore/houses-for-sale-1/" class="d-inline-block">Lahore</a>
		var reg_exp = "<a href=\"([^\"]*)\" class=\"([^\"]*)\">([^\"]*)</a>";
		var results = get_all_regex_matched(html, reg_exp, 1);
		for (var oneResult in results){
			count += crawlCategory(results[oneResult] + "?sort=date_desc", category);
			//print(results[oneResult] + "?sort=date_desc");
            //print("Houses for only one city");
            
		}
	}
	return count;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Houses
		{
			try {
				// Houses for sale
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-houses-buy.html", KHOUSEFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
			try {
				// Houses for lease
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-houses-rentals.html", KHOUSEFORLEASE);
			} catch (e) {
				exceptionprint(e);
            }
        }
		// Apartments
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-flats-apartments-buy.html", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
           try {
				// Apartments for rent
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-flats-apartments-rentals.html", KFLATFORLEASE);
			} catch (e) {
				exceptionprint(e);
            }
        }
		// Lands
		{
			try {
				// Plots for sale
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-residential-plots-buy.html", KLANDFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
        }
        // Commercial Plots
		{
			try {
				// Commercial Plots for sale
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-commercial-plots-buy.html", KBUILDINGFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Commercial Plots for rent
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-commercial-plots-rentals.html", KBUILDINGFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Shops
		{
			try {
				// Shops for sale
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-shops-buy.html", KLOCALFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Shops for rent
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-shops-rentals.html", KLOCALFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Offices
		{
			try {
				// Offices for sale
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-offices-buy.html", KOFFICEFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Offices for rent
				cumulatedCount += crawlCities("https://www.prop.pk/pakistan-offices-rentals.html", KOFFICEFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

