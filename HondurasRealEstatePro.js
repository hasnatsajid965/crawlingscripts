qa_override("[E1473228885]", "There is no lot size data on this website.");
qa_override("[E2592408747]", "Some properties has city and some not.");
qa_override("[E2140387965]", "Some properties has currencycode and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
    var cumulatedCount = 0;
    print("---- Processing category "+category+" in "+getJavascriptFile()+" ----");
    var actualCount = 0;
    var page = 1;
    var tracer = 0;
    var checkRepeat = [];
    var html = gatherContent_url(url, KDONOTNOTIFYERROR);
    while (isDefined(html)) {
	try {
	    while ((isDefined(html))&&(tracer = addNextToPropertiesList(html, tracer, "<h3 class=\"listing-title\"><a href=\"([^\"]*)\">", category, "", 1, KDONOTNOTIFYERROR, false))>0) {
		tracer++;
		actualCount++;
		cumulatedCount++;
		if (passedMaxPublications()) {
			break;
		}
	    }
	} catch (e) {
	    if (e!=="Duplicate detected.") {
		throw("error: "+e);
	    } else {
		return cumulatedCount;
	    }
	}
	print("---- "+actualCount+" found in "+getJavascriptFile()+" on page "+page+" for category "+category+" ("+cumulatedCount+") ----");
	if (passedMaxPublications()) {
		break;
	}
	var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\">Next</a>", 1, KDONOTNOTIFYERROR);
	if (isUndefined(relativeLink)||relativeLink=="#") {
	    break;
	}
	html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
	tracer = 0;
	actualCount = 0;
	page++;
    }
    return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
    addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
    var html = gatherContent_url(url, KDONOTNOTIFYERROR);
    var mls = JSON.parse(mlsJSONString);
    if (isDefined(html)) {
	if (mls.NonMLSListingData==undefined) {
	    mls.NonMLSListingData = {};
	}
	if ((mls.NonMLSListingData.lang==undefined)||(mls.NonMLSListingData.lang=="")) {
	    mls.NonMLSListingData.lang = "en-us";
	}
	mls.ListPrice.value = get_next_regex_match(html, 0, "<li class=\"listing-price\"><span class=\"currency-symbol\">[ ]*?</span>([ \$ ]*)([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
	mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<li class=\"listing-price\"><span class=\"currency-symbol\">[ ]*?</span>([ \$ ]*)([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
	mls.ListingTitle = get_next_regex_match(html, 0, "<h1 class=\"entry-title\">([^\<]*)", 1, KDONOTNOTIFYERROR);
	if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
	    return analyzeOnePublication_return_innactive;
	}
	mls.ListingDescription = get_unique_regex_match(html, '<div id="listing-description" itemprop="description">(.+?)<ul', 1, KDONOTNOTIFYERROR);
	mls.ListingDescription = mls.ListingDescription.replace("#gallery-2 { margin: auto; } #gallery-2 .gallery-item { float: left; margin-top: 10px; text-align: center; width: 16%; } #gallery-2 img { border: 2px solid #cfcfcf; } #gallery-2 .gallery-caption { margin-left: 0; } /* see gallery_shortcode() in wp-includes/media.php */", " ").trim();
	if(isUndefined(mls.ListingDescription) || mls.ListingDescription == ""){
		mls.ListingDescription = get_next_regex_match(html, 0, "<p></p>(.+?)<p></p>", 1, KDONOTNOTIFYERROR);
	}
	if(isUndefined(mls.ListingDescription) || mls.ListingDescription == "" ){
		mls.ListingDescription = get_next_regex_match(html, 0, "</figure></li></ul>(.+?)</div>", 1, KDONOTNOTIFYERROR);
	}
	if(isUndefined(mls.ListPrice.value)){
		mls.ListPrice.value = get_next_regex_match(html, 0, "<ul class=\"listing-meta\"><li class=\"listing-price\"><span class=\"currency-symbol\">[ ]*</span>([ 0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
	}
	if(isUndefined(mls.ListPrice.value)){
		mls.ListPrice.value = KPRICEONDEMAND;
	}
	var prices = mls.ListPrice.value.replace(",","").trim();
	if (prices < 50000) {
		freq = "month";
		mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
	}
	if (prices => 100000 && prices < 800000) {
		freq = "year";
		mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
	}
	mls.Brokerage.Name = "Cesar Castillo";
	mls.Brokerage.Phone = "(504) 3178-6866";
	mls.Brokerage.Email = "disserca@msn.com";
	mls.Address.City.value = get_next_regex_match(html, 0, "Location: </span><a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
	mls.Address.Country.value = "Honduras";
	var imageCount = 0;
	var images;
	images = get_all_regex_matched(html, "<li class=\"blocks-gallery-item\"><figure><a href=\"([^\"]*)\">", 1);
	if (isUndefined(images)||images=="") {
	    images = get_all_regex_matched(html, "<a data-rel=\"iLightbox[^\"]*postimages[^\"]*\" data-title=\"[^\"]*\" data-caption=\"\" href=\'([^\']*)\'>", 1);
	}
	if(isDefined(images) && images !== "") images.forEach(function(oneImageTag) {
	    var obj = JSON.parse(get_list_empty_variable("photo"));
	    obj.MediaURL = oneImageTag;
	    obj.MediaOrderNumber = imageCount;
	    if (mls.Photos.photo==undefined) {
		mls.Photos.photo = [];
	    }
	    mls.Photos.photo.push(obj);
	    imageCount++;
	});
	resulting_json(JSON.stringify(mls));
	return analyzeOnePublication_return_success;
    } else {
	return analyzeOnePublication_return_unreachable;
    }
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://hondurasrealestatepro.com/beachfront-lot-houses/", KTERRENOSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Other
	{
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://hondurasrealestatepro.com/oceanview-and-rainforest/", KTERRENOSALQUILER);
	    } catch (e) {
		exceptionprint(e);
	    }
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://hondurasrealestatepro.com/commercial-and-urban/", KTERRENOSALQUILER);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
