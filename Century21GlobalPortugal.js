var countryContext = "Portugal";

include("base/Century21GlobalFunctions.js");

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?subtype=apartment", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?subtype=condo", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?subtype=1", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?subtype=1", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Multifamily en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?subtype=multiFamily", KNUEVASMULTIFAMILIAVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?features=new", KNUEVASCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Land
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.century21global.com/for-sale-residential/Portugal?subtype=farm", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
