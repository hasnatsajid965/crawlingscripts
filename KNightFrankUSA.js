var countryText = "USA";
include("base/KNightFrankFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.us/properties/residential/for-sale/usa-new-york/House/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.us/properties/residential/for-sale/usa-new-york/Penthouse/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.us/properties/residential/for-sale/usa-new-york/Apartment/all-beds", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.us/properties/residential/for-sale/usa-new-york/Flat/all-beds", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.us/properties/residential/for-sale/usa-new-york/Development/all-beds", KNUEVASCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.knightfrank.us/properties/residential/for-sale/usa-new-york/Land/all-beds", KTERRENOSVENTAS);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
