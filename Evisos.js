

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<h3 class=\"title\"><a href=\"([^\"]*)\" title=\"([^\"]*)\">([^\<]*)</a></h3>", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e === "Duplicate detected.") {
				return cumulatedCount;
			}
			throw ("error: " + e);
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href=\"([^\"]*)\" rel=\"next\">SIGUIENTE <", 1, KDONOTNOTIFYERROR);
		if (relativeLink == undefined) {
			break;
		}
		html = gatherContent_url("https://www.evisos.com.do" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}
function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(false);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span itemprop=\"price\" content=\"[^\"]*\">([0-9,\\.]*)[ ]?([A-Z$]*)</span>", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span itemprop=\"price\" content=\"[^\"]*\">([0-9,\\.]*)[ ]?([A-Z$]*)</span>", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<span itemprop=\"price\" content=\"[^\"]*\">([^<>]*)</span>", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span itemprop=\"priceCurrency\" content=\"([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<a href=\"https://www.evisos.com.do/userads/[^\"]*\" [^>]*?title=\"Tienda Virtual de [^>]*\">([^<]*)</a>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<a href=\"tel:([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if ((isDefined(mls.Brokerage.Phone)) && (mls.Brokerage.Phone.indexOf("/") != -1)) {
			if (mls.Brokerage.Phone.indexOf("/") > (mls.Brokerage.Phone.length() - 9)) {
				mls.Brokerage.Phone = mls.Brokerage.Phone.substring(0, mls.Brokerage.Phone.indexOf("/"));
			}
		}
		mls.ListingDescription = arrayElement(extract_through_DOM_attribute_selector(html, "id=adDescription", KDOMSELECTOR_INNERHTML, KDONOTNOTIFYERROR), 0);
		if (mls.ListingDescription != undefined) {
			mls.ListingDescription = mls.ListingDescription.replaceAll("•", "\n•");
		}
		mls.Address.City.value = get_unique_regex_match(html, "\"addressLocality\":\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_unique_regex_match(html, "\"latitude\":\"([0-9\\.-]*)\"", 1, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_unique_regex_match(html, "\"longitude\":\"([0-9\\.-]*)\"", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img src=\"([^\"]*)\" alt=\"\" data-small=\"([^\"\.]*)\.([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.evisos.com.do/departamentos-en-venta-sc-24", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.evisos.com.do/departamentos-en-alquiler-sc-30", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.evisos.com.do/casas-en-venta-sc-25", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.evisos.com.do/casas-en-alquiler-sc-31", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("https://www.evisos.com.do/locales-oficinas-sc-32", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.evisos.com.do/terrenos-parcelas-sc-27", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Naves industriales
		{
			try {
				// Edificios en venta
				cumulatedCount += crawlCategory("https://www.evisos.com.do/galpones-sc-29", KNAVESVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
