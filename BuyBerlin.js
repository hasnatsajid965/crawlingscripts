
// crawlForPublications crawl-mode: Regular-Expression Crawling

function cityName(){
    return "Berlin";
}

function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			var reg_exp = "<a href=\"property-detail.php\\?id=([0-9.]*)\" class=\"search-result-panel-link\">";
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, reg_exp, category, "https://buyberlin.berlin/property-detail.php?id=", 1, KDONOTNOTIFYERROR, false)) > 0) {

				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href=\"([^\"]*)\" aria-label=\"Next\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = "https://buyberlin.berlin/" + relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		
		// LISTING BROKER CODE -------------------------------------------------------
		//<span itemscope="" itemProp="itemListElement" itemType="http://schema.org/ListItem" dir="auto"><span itemProp="name" class="c5051fb4">House 23517282</span><meta itemProp="position" content="4"/></span>
		reg_exp = "<tr>([\\n\\s]*)<td><strong>Ref:</strong></td>([\\n\\s]*)<td>([^\<]*)</td>([\\n\\s]*)</tr>";
		var ref_no = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		if (isDefined(ref_no)){
			if(mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(ref_no);
			if (isDefined(ref_no)){
				mls.NonMLSListingData.brokerCodes.push(ref_no );
			}
		
		}
		
		// LISTING PRICE -------------------------------------------------------

		reg_exp = "<h3 class=\"pull-right search-price\">([^\ ]*) ([^\ ]*)</h3>";

		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		/*	var price_multiplier = get_next_regex_match(html, 0, reg_exp, 7, KDONOTNOTIFYERROR);
			if (isDefined(price_multiplier)) {
				switch(price_multiplier.toLowerCase()) {
					case "crore": {
						price_number_value *= 10000000;
					}
					break;
					case "lakh": {
						price_number_value *= 100000;
					}
					break;
					case "thousand": {
						price_number_value *= 1000;
					}
					break;
					default: {
						throw "Unhandled multiplier: " + price_multiplier;
					}
				}
			}*/
			print("price_number_value: "+ price_number_value);
			mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		
		// LISTING TITLE -------------------------------------------------------
		
		reg_exp = "<div class=\"col-sm-8 col-md-9\">([\\n\\s]*)<h1>([^\\<]*)</h1>([\\n\\s]*)</div>";
		
		if (isDefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, reg_exp, 2, KNOTIFYERROR);
		}
		print("Listing title *****" + mls.ListingTitle);
		
		
		// LISTING DESCRIPTION -------------------------------------------------------
		
		reg_exp = "<div class=\"col-sm-12 col-md-8\">([\\n\\s]*)<div class=\"property-description\">([\\n\\s]*)([\\s\\S]*)<h2>About</h2>";

		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR)
		}
		print("Listing Description *****:	" + mls.ListingDescription);
		
		// LAND SIZE -------------------------------------------------------
		//<li aria-label="Property detail area"><span class="_3af7fa95">Area</span><span class="_812aa185" aria-label="Value"><span>6 Marla</span></span></li>
		reg_exp = "<tr>([\\n\\s]*)<td><strong>Size:</strong></td>([\\n\\s]*)<td>([0-9.]*) ([^\ ]*)</td>([\\n\\s]*)</tr>";
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		}
		print("Land Size Value and Area Units *****:	" + mls.LotSize.value + " " + mls.LotSize.areaUnits);
		
		// BEDROOMS -------------------------------------------------------
		//<li aria-label="Property detail beds"><span class="_3af7fa95">Bedroom(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<tr>([\\n\\s]*)<td><strong>Bedrooms:</strong></td>([\\n\\s]*)<td>([0-9.]*)</td>([\\n\\s]*)</tr>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		}
		print("Bedrooms *****:	" + mls.Bedrooms);
		
		// BATHROOMS -------------------------------------------------------
		//<li aria-label="Property detail baths"><span class="_3af7fa95">Bath(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<tr>([\\n\\s]*)<td><strong>Bathrooms:</strong></td>([\\n\\s]*)<td>([0-9.]*)</td>([\\n\\s]*)</tr>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		}
		print("Bathrooms *****:	" + mls.Bathrooms);
		
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<tr>([\\n\\s]*)<td><strong>Type:</strong></td>([\\n\\s]*)<td>([A-Za-z]*)</td>([\\n\\s]*)</tr>";

 		if (isDefined(mls.PropertyType.value)) {
			mls.PropertyType.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		}
		print("Property type *****:	" + mls.PropertyType.value);
		
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("House")) {
					mls.NonMLSListingData.category = getCategory(KHOUSEFORSALE);
			}
			if (mls.PropertyType.value.includes("Apartment")) {
				mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
			}
		}

		print("Listing data category *****:	" + mls.NonMLSListingData.category);
		
		// BROKERAGE NAME -------------------------------------------------------
		//<span class="_725b3e64">Arif Khan</span></div><div class="_6f510616"><a href="/Profile/Abbottabad-Fahad_Real_Estate-179977-1.html" class="_23d69e6d" title="Fahad Real Estate">Agency profile</a></div>
		/*reg_exp = "<span class=\"([^\"]*)\">([^\"]*)</span></div><div class=\"([^\"]*)\"><a href=\"([^\"]*)\" class=\"([^\"]*)\" title=\"([^\"]*)\">([^\"]*)</a></div>";
		if (isDefined(mls.Brokerage.Name)) {
			mls.Brokerage.Name = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
        }*/
        mls.Brokerage.Name = "BuyBerlin management";
		print("Broker name *****:	" + mls.Brokerage.Name);
		
		// BROKERAGE Phone No. -------------------------------------------------------
		reg_exp = " <h2>Enquire Now</h2>([\\n\\s]*)<p>([\\n\\s]*)<i class=\"fa fa-phone\" title=\"Phone\"></i><a href=\"tel:([^\"]*)\">([^\"]*)</a><br>([\\n\\s]*)<i class=\"fa fa-phone\" title=\"Phone\"></i><a href=\"tel:([^\"]*)\">([^\"]*)</a><br>";
		if (isDefined(mls.Brokerage.Phone)) {
			phone_1 = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
            phone_2 = get_unique_regex_match(html, reg_exp, 6, KDONOTNOTIFYERROR);
            mls.Brokerage.Phone = phone_1.concat(" / "+ phone_2);
		}
		print("Broker Phone No. *****:	" + mls.Brokerage.Phone);
		
		// BROKERAGE ADDRESS -------------------------------------------------------
		//<li aria-label="Property detail location"><span class="_3af7fa95">Location</span><span class="_812aa185" aria-label="Value">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</span></li>
		//"loc_city_id":"385","loc_city_name":"Abbottabad","loc_id":"15946","loc_name":"Phul Ghulab Road","city_id":"385","city_name":"Abbottabad","listing_type":9,
		//reg_exp = "\"loc_city_id\":\"([0-9]*)\",\"loc_city_name\":\"([A-Za-z]*)\",\"loc_id\":\"([0-9]*)\",\"loc_name\":\"([^\"]*)\",\"city_id\":\"([0-9]*)\",\"city_name\":\"([A-Za-z]*)\",\"listing_type\"([^\"]*)";
		if (isDefined(mls.Address.City.value)) {
			
			mls.Address.City.value = cityName();

		}
		print("Broker Address *****:	" + mls.Address.City.value);
		
		
		/*
		mls.LotSize.value = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Dormitorios:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Ba?os:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<strong>Estacionamientos: </strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Tipo:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Apartamentos")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
					}
				}
			}
			if (mls.PropertyType.value.includes("Casas")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KCASASVENTAS);
					}
				}
			}
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<i class=\"fa fa-user\"></i> ([^<]*)</", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<i class=\"fa fa-mobile\"></i>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<strong>Sector:</strong> ([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>Ciudad:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var operacions = get_next_regex_match(html, 0, "<strong>Operaci?n:</strong> ([^<]*?)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(operacions)) {
			var pos = get_last_regex_match_end_pos();
			var operationList = operacions.split(",");
			if (operationList.length > 1) {
				operationList.forEach(function(oneOperation) {
					if (isPublicationPurchase()) {
						if (oneOperation == "Renta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("LEASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationLease()) {
						if (oneOperation == "Venta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("PURCHASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationRent()) {
					}
				});
			}
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}*/
		
        mls.Address.Country.value = "Germany";
		reg_exp = "\"([^\"]*)\":([0-9\.]*),\"latitude\":([0-9\.]*),\"longitude\":([0-9\.]*),\"reference_id\":\"([^\"]*)\"";
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_unique_regex_match(html, reg_exp, 4, KDONOTNOTIFYERROR);
		print("Latitude *****:	" + mls.NonMLSListingData.propertyLocation.Latitude.value);
		print("Longitude *****:	" + mls.NonMLSListingData.propertyLocation.Longitude.value);
		
		reg_exp = "<img src=\"https://image.onoffice.de/([^\ ]*)\" />";
		images = get_all_regex_matched(html, reg_exp, 1);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			var url_append = "https://image.onoffice.de/";
            //special = obj.MediaURL.substring(0, obj.MediaURL.indexOf('-'));
            obj.MediaURL = url_append.concat(obj.MediaURL);
            print("Media url *****:    " + obj.MediaURL);
			//if (getFileSizeWithProxyConsiderations (obj.MediaURL) > 0) {
				mls.Photos.photo.push(obj);
							imageCount++;
			//	}
		});
		
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// All Listings 
		{
			try {
				// Houses for sale
				cumulatedCount += crawlCategory("https://buyberlin.berlin/listings.php", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
