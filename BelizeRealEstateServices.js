qa_override("[E1473228885]", "Some properties have lot size and some not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			// wait(4000);
			el1 = virtual_browser_find_one(browser, "(//a[@class='ViewListingLink'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				finalUrl = virtual_browser_element_attribute(el1, "href");
				resArray.push(finalUrl);
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//a[text()='Next >>']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KDONOTNOTIFYERROR);
						hrefValue = virtual_browser_element_attribute(array[element], "href");
						if (isUndefined(hrefValue) || hrefValue == "") {
							return cumulatedCount;
						}
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<h3 class="listing-sec-title">Description</h3>[\\s\\t\\n ]*?<div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span itemprop=\"price\">([\$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span itemprop=\"price\">([\$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR) + get_next_regex_match(html, 0, "<span itemprop=\"priceCurrency\">([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_next_regex_match(html, 0, "<span itemprop=\"streetAddress\" class=\"address\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size: <b>([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size: <b>([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<li class=\"col-sm-6\">Bedrooms: <b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<li class=\"col-sm-6\">Bathrooms: <b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, "<span itemprop=\"priceCurrency\">[^\<]*</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "monthly" || for_rent == "Monthly") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			if (mls.ListPrice.value < 2000) {
				freq = "week";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.PropertyType.value = get_next_regex_match(html, 0, "<li class=\"col-sm-6\">Type: <b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<span itemprop=\"name\" class=\"contact-agent-name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = "(001)727-565-1507 / (011)501-804-0195";
		mls.Address.City.value = get_next_regex_match(html, 0, "<span class=\"locality\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Belize";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a href=\"javascript:void[^\"]*\"[^\>]*?>[\\s\\t\\n ]*?<img alt=\"[^\"]*\" data-fullsrc=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "http://www.belizerealestateservices.com/Homes_for_Sale/page_2664641.html", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://www.belizerealestateservices.com/Belize_Condominiums/page_2652827.html", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://www.belizerealestateservices.com/Lots__Land/page_2664643.html", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://www.belizerealestateservices.com/Rentals/page_2664642.html", KOTROSALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
