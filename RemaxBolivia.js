var countryContext = "Bolivia";
include("base/RemaxFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=261&mpts=19418&pt=19418&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=261&mpts=19420&pt=19420&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=261&mpts=19422&pt=19422&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KOTROSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=261&mpts=19430&pt=19430&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=261&mpts=19426&pt=19426&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=260&mpts=19418&pt=19418&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KAPTOALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=260&mpts=19420&pt=19420&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KCASASALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=260&mpts=19422&pt=19422&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KOTROSALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=260&mpts=19430&pt=19430&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KCASASALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.bo/publiclistinglist.aspx#mode=gallery&tt=260&mpts=19426&pt=19426&cur=USD&sb=PriceDecreasing&page=1&sc=120&sid=e1083e16-9f2d-4c67-bab4-45bdb9ec1b0a", KTERRENOSALQUILER);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
