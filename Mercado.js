qa_override("[E1473228885]", "Some properties do have lot size and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var pages = 2;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"titl fl\" href=\"([^\"]*)", category, "https://www.mercado.do", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "href=\"([^\"]*)\" title=\"Pagina " + pages + "\">[^\<]*", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.mercado.do" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
		pages++;
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// setProxyConditions(true, null);
	// rotateUserAgents(false);
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		var element = virtual_browser_find_one(browser, "//span[text()='Solarium\Exception\HttpException']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			return analyzeOnePublication_return_innactive;
		}
		var not_available = get_unique_regex_match(html, '<h1 class="hdri">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(not_available)) {
			if (not_available.includes("mal")) {
				return analyzeOnePublication_return_innactive;
			}
		} else {
			not_available = get_unique_regex_match(html, '<h2>([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(not_available)) {
				if (not_available.includes("error")) {
					return analyzeOnePublication_return_innactive;
				}
			}
		}
		mls.ListingDescription = get_unique_regex_match(html, 'Descripcion</p><p itemprop="description">(.+?)<div', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span itemprop=\"price\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value === "Solicitud" || mls.ListPrice.value < 100) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<meta itemprop=\"priceCurrency\" content=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="sel" itemprop="seller" itemscope itemtype="[^\"]*"><p>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'stroke=\"#1b1c1f\" fill=\"#1b1c1f\"/></g></g></svg>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h1 itemprop=\"name\">([^\<]*)", 1, KNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Camas</strong>:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		mls.Address.City.value = get_unique_regex_match(html, "<span itemprop=\"addressLocality\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, "href=\"/c/bienes-raices/casas-en-venta/departamentos-en-alquiler/\" class=\"clnk\"><span itemprop=\"name\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent === "Departamentos en alquiler") {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		var features = get_all_regex_matched(html, '<div class=\"prc\"><strong>[^\<]*</strong>:([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "href=\"([^\"]*)\" class=\"imfb\">", 1, KDONOTNOTIFYERROR);
		if (isDefined(images)) {
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartment
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/departamentos-en-venta/", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/departamentos-en-alquiler/", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Office
		{
			try {
				// Office en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/otros-alojamientos/", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/casas/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/canje-de-casas/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/tierras/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/comercial/tierras-comerciales/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Other
		{
			try {
				// Other en venta
				cumulatedCount += crawlCategory("https://www.mercado.do/c/bienes-raices/casas-en-venta/otros-alojamientos/", KOTROSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
