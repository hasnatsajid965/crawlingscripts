qa_override("[E2592408747]", "There is no city on this website.");

// crawlForPublications crawl-mode: Virtual Browser Crawling

function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			el1 = virtual_browser_find_one(browser, "(//h5/a[@class='CoveoResultLink'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//span[@class='coveo-pager-next-icon']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						if (virtual_browser_click_element(browser, array[element], KDONOTNOTIFYERROR)) {
							wait(6000);
							html = virtual_browser_html(browser);
							tracer = 0;
							actualCount = 0;
							page++;
							foundIt = true;
							break;
						} else {
							return cumulatedCount;
						}
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	// addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	// if (isUndefined(browser)) {
	// browser = create_virtual_browser("HeadlessChrome");
	// if (isUndefined(browser)) {
	// return analyzeOnePublication_return_tech_issue;
	// }
	// }
	// virtual_browser_navigate(browser, url);
	// var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="page-content-body rich-text">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "Sale Price:</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "Sale Price:</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img class="d-block w-100" src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://img.jamesedition.com/listing_images" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"page-title-main\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'Building Size:</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)([a-zA-Z\.\,]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Building Size:</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)([a-zA-Z\.\,]*)', 2, KDONOTNOTIFYERROR);
		if (mls.LotSize.value == "") {
			print("enter" + mls.LotSize.value);
			mls.LotSize.value = get_unique_regex_match(html, '<dt>Lot Size[^\:]*:</dt>[\s\t\n ]*?<dd>([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.Location.Directions = get_unique_regex_match(html, "<h5 class=\"page-title-sub\">(.+?)</h5>", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			mls.Location.Directions = mls.Location.Directions.replace(/<[^>]*>?/gm, ',');
		}
		mls.Address.City.value = get_unique_regex_match(html, '<h5 class="page-title-sub">[^\<]*<br/>([^\,]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "USA";
		mls.MlsId = get_unique_regex_match(html, '"Listing id":([^\,]*),', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<h6 class="font-weight-bold">[\s\t\n ]*?<a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<a href=\"tel:([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'Year Built:</dt>[\s\t\n ]*?<dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.cushmanwakefield.com/en/united-states/properties/invest/invest-property-search?#f:PropertyType=[Office]&f:Country=[United%20States]", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.cushmanwakefield.com/en/united-states/properties/invest/invest-property-search?#f:PropertyType=[Land]&f:Country=[United%20States]", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.cushmanwakefield.com/en/united-states/properties/invest/invest-property-search?#f:PropertyType=[Warehouse%2FDistribution]&f:Country=[United%20States]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.cushmanwakefield.com/en/united-states/properties/invest/invest-property-search?#f:PropertyType=[Other]&f:Country=[United%20States]", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.cushmanwakefield.com/en/united-states/properties/invest/invest-property-search?#f:PropertyType=[Manufacturing]&f:Country=[United%20States]", KNUEVASCASASVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
