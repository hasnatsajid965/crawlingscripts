

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^"]*)">[\r\n\t ]*?<div class="property-image">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a href="([^"]*)">([^<]*)<i class="fa fa-forward">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://aei.com.do" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<header class="property-title">[\s\t\n ]*?<h1>([^\<]*)', 1, KNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<section id="description">[\r\n\t ]*?<header><h2>DESCRIPCION</h2></header>[\r\n\t ]*?<p>([^<]*)</p>', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, "<li>([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Aire A/C") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, " ").trim();
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}

		mls.Brokerage.Name = get_unique_regex_match(html, '</i>([^"]*)</h3>', 1, KDONOTNOTIFYERROR);
		mls.BusinessAdditionalInformation = get_unique_regex_match(html, '</i>([^"]*)</h3>[\r\n\t ]*?<p>([^"]*)</p>', 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<dt>Tel&eacute;fono:</dt>[\r\n\t ]*?<dd>([^<]*)</dd>", 1, KDONOTNOTIFYERROR);
		mls.BrokerId = get_unique_regex_match(html, '<dt>CBR:</dt>[\r\t\n ]*?<dd><a href="#">Registro[\r\t\n ]*?#([^<]*)</a>', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<dl><dt>MLS[ ]?#:</dt><dd><span class="([^"]*?)"><strong>([^<]*)</strong></span>', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "<dt>Localizaci&oacute;n</dt><dd>([^<]*)</dd>", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '</dt><dd><span class="tag price">([a-zA-Z]*[\$]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '</dt><dd><span class="tag price">([a-zA-Z]*[\$]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, 'Estatus:</dt><dd>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "Alquiler") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			} else if (for_rent == "Venta y Alquiler") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.PropertyType.value = get_next_regex_match(html, 0, "<dt>Tipo Propiedad:</dt><dd>([^<]*)</dd>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<dt>Area:</dt><dd>([0-9]*)[ ]?([a-z]*)<sup>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<dt>Area:</dt><dd>([0-9]*)[ ]?([a-z]*)<sup>", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Area Frente</dt><dd>([0-9]*)[ ]?([a-z]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "Area Frente</dt><dd>([0-9]*)[ ]?([a-z]*)", 2, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, "<dt>Habitaciones:</dt><dd>([^<]*)</dd>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<dt>Ba&ntilde;os:</dt><dd>([^<]*)</dd>", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<div class="property-slide">[\r\n\t ]*?<a[ ]?href="#">[\r\n\t ]*?<div class="overlay"><h3></h3></div>[\r\n\t ]*?<img[ ]?alt="[ ]?"[ ]?src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Appartments
		{
			try {
				// Appartments for Sale
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/apartamentos/venta/habs-1/banos-1/precio-100-9000000000", KHOUSEFORSALE);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartments for Lease
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/apartamentos/alquiler/habs-1/banos-1/precio-1-80000", KHOUSEFORLEASE);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Houses
		{
			try {
				// Houses for Sale
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/casas/venta/habs-1/banos-1/precio-100-9000000000", KHOUSEFORSALE);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Houses for Sale
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/locales/venta/habs-1/banos-1/precio-100-9000000000", KHOUSEFORSALE);
			} catch (e) {
				print("error: " + e);
			}

			try {
				// Houses for Sale
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/casas/alquiler/habs-1/banos-1/precio-1-80000", KHOUSEFORSALE);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Houses for Lease
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/casas/alquiler/habs-1/banos-1/precio-1-80000", KHOUSEFORLEASE);
			} catch (e) {
				print("error: " + e);
			}

			try {
				// Houses for Lease
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/locales/alquiler/habs-1/banos-1/precio-1-80000", KHOUSEFORLEASE);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Land
		{
			try {
				// Land for Sale
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/terrenos/venta/habs-1/banos-1/precio-100-9000000000", KLANDFORSALE);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Buildings for Sale
		{
			try {
				cumulatedCount += crawlCategory("https://aei.com.do/inmuebles/Distrito_Nacional/edificios/venta/habs-1/banos-1/precio-100-9000000000", KBUILDINGFORSALE);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
