qa_override("[E2592408747]", "Some properties have city and some not.");
qa_override("[E1473228885]", "Some properties have lot size and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 2;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)" class="property-result-item" data-id="[^\"]*" data-lat="[^\"]*" data-lng="[^\"]*">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<span><span class="selected">[^\<]*</span><a href="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}
function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.MlsId = get_unique_regex_match(html, '<span>Reference:</span>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType = get_next_regex_match(html, 0, 'Property type:</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_next_regex_match(html, 0, '<strong>Internal parkings</strong> :([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = mls.MlsId;
		mls.ListingDescription = get_next_regex_match(html, 0, '<h4>Description</h4>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, 'div class="labelOdb">Area</div>[\s\t\n ]*?<div><span>([^\<]*)</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'div class="labelOdb">Area</div>[\s\t\n ]*?<div><span>([^\<]*)</span>([^\<]*)', 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<strong>([^\<]*)</strong> : Yes', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "MME Genevi�ve DEBAISIEUX";
		mls.Brokerage.Phone = "+32 2 646 49 49 / +32 2 645 09 14 / +32 2 643 10 23";
		mls.ListPrice.currencyCode = get_unique_regex_match(html,
				'<h1 class="widget-title widget-title-job_listing "><span style="font-size: 21px; color:#1a7db8 !important; margin-top: -20px !important; padding-top: 0px !important;">[^\<]*</span><br>[\s\t\n ]*?([a-zA-Z \s]*[\$])([0-9\,\.]*)', 1,
				KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html,
				'<h1 class="widget-title widget-title-job_listing "><span style="font-size: 21px; color:#1a7db8 !important; margin-top: -20px !important; padding-top: 0px !important;">[^\<]*</span><br>[\s\t\n ]*?([a-zA-Z \s]*[\$])([0-9\,\.]*)', 2,
				KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.City.value = get_next_regex_match(html, 0, 'freeTextZipCodeCity=([^\&]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Belgium";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img src="([^\<]*)" alt="" itemprop=\'image\' />', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "https://www.realestate.bnpparibas.be/" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}
function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Offices en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Boffice%5D=office&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Offices en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bserviced+office%5D=serviced+office&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Offices en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Boffice%5D=office&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Offices en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bserviced+office%5D=serviced+office&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bretail%5D=retail&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bindustrial%5D=industrial&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bleisure%5D=leisure&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bmixed+use%5D=mixed+use&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bco+working%5D=co+working&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bwarehouse%5D=warehouse&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bcar+showroom%5D=car+showroom&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bother%5D=other&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bretail%5D=retail&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bindustrial%5D=industrial&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bleisure%5D=leisure&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bmixed+use%5D=mixed+use&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bco+working%5D=co+working&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bwarehouse%5D=warehouse&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bcar+showroom%5D=car+showroom&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Others en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bother%5D=other&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&trans_type=for_sale&property_type%5Bland%5D=land&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en rent
				cumulatedCount += crawlCategory(
						"https://www.realestate.bnpparibas.co.uk/property/search?location=&location_coords=0&distance=0&property_type%5Bland%5D=land&buy_price_min=0&buy_price_max=max&rental_price_min=0&rental_price_max=max&size_min=0&size_max=",
						KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}

		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
