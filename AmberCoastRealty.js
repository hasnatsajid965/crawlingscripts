qa_override("[E2592408747]", "Some properties have city and some not.");
qa_override("[E2431535312]", "Some properties have description and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"hover-effect\" href=\"([^\"]*)\">", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "rel=\"Next\" href=\"([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, 'Description</h2></div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, 'Description</h2>[\s\t\n ]*?</div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"col-lg-7 col-md-7 col-sm-6 col-xs-12\"><h2 class=\"title-left\">([ \$]*)([0-9\,\. ]*)</h2>", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"col-lg-7 col-md-7 col-sm-6 col-xs-12\"><h2 class=\"title-left\">([ \$]*)([0-9\,\. ]*)</h2>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"col-lg-7 col-md-7 col-sm-6 col-xs-12\">[\s\t\n ]*?<h2 class=\"title-left\"> ([ \$]*)([0-9\,\. ]*)</h2>", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"col-lg-7 col-md-7 col-sm-6 col-xs-12\">[\s\t\n ]*?<h2 class=\"title-left\"> ([ \$]*)([0-9\,\. ]*)</h2>", 1, KDONOTNOTIFYERROR);

		}
		mls.ListingTitle = get_unique_regex_match(html, "<h2 class=\"title-left property-name\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<span class=\"title\">Living Area ([a-zA-Z ]*)</span>([0-9\, ]*)</div>", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<span class=\"title\">Living Area ([a-zA-Z ]*)</span>([0-9\, ]*)</div>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<span class=\"title\">Lot Size ([a-zA-Z ]*)</span>([0-9\, ]*)</div>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<span class=\"title\">Lot Size ([a-zA-Z ]*)</span>([0-9\, ]*)</div>", 2, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<span class="title">Property ID</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, '<span class="title">Property Type</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		forRent = get_unique_regex_match(html, '<span class="title">Property Status</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Rental")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
			}
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, '"property_lat":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '"property_lng":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Videos = get_unique_regex_match(html, '<a data-fancy="property_video" href="([^\"]*)">', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<small>Bed</small>([^\<]*)</div>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<small>Bath</small>([^\<]*)</div>", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<i class="fa fa-check"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.Brokerage.Name = "Coldwell Banker Amber Coast Realty";
		mls.Brokerage.Phone = "(809) 571-3131";
		mls.Brokerage.EMail = "info@ambercoastrealty.com";
		mls.Address.Country.value = "Dominican Republic";
		cityValue = get_next_regex_match(html, 0, '<span class="property-address hide-on-list"><p class="prop-location">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(cityValue)) {
			cityValue = cityValue.split(",");
			if (isDefined(cityValue[0])) {
				mls.Address.City.value = cityValue[0];
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "data-bg=\"([^\"]*)\"", 1);
		if (images == "") {
			images = get_all_regex_matched(html, "src=\"([^\"]*)\" class=\"attachment-houzez-imageSize[^\"]*\"", 1);
		}
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				oneImageTag = oneImageTag.replace("-700x430", "");
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=condos-apartments-sales&location=&min-price=&max-price=&bedrooms=&keyword=", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=condos-apartments&location=&min-price=&max-price=&bedrooms=&keyword=", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{

			try {
				// Hotels en Venta
				cumulatedCount += crawlCategory("http://ambercoastrealty.com/search-result-page/?type=hotels-sales&location=&min-price=&max-price=&bedrooms=&keyword=", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en Venta
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=villas-homes-sales&location=&min-price=&max-price=&bedrooms=&keyword=", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=long-term-rentals&location=&min-price=&max-price=&bedrooms=&keyword=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=villas-homes&location=&min-price=&max-price=&bedrooms=&keyword=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=commercial-buildings-sales&location=&min-price=&max-price=&bedrooms=&keyword=", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=businesses-sales&location=&min-price=&max-price=&bedrooms=&keyword=", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.ambercoastrealty.com/search-result-page/?type=land-lots-sales&location=&min-price=&max-price=&bedrooms=&keyword=", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
