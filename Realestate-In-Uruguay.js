

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="listing-widget-thumb">[\s\t\n ]*?<a href="([^\"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="next page-numbers" href="([^\"]*)">Next &rarr;</a>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="page-title">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Price:</td><td>([a-zA-Z\$]*)([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, 'Price:</td><td>[a-zA-Z\$]*([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Lot Sq M: </span>([ 0-9\,\. ]*)[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Lot Sq M: </span>[ 0-9\,\. ]*([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, 'Sq M: </span>([ 0-9\,\. ]*)[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'Sq M: </span>[ 0-9\,\. ]*([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Beds: </span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Baths: </span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'Year Built:</td><td>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div id="listing-description" class="tab-pane active" itemprop="description">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<a href="https://www.realestate-in-uruguay.com/features/[^\"]*" rel="tag">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Real Estate In Uruguay TEAM HAVERKATE at ENGEL & VOELKERS";
		mls.Brokerage.Email = "bettina@realestate-in-uruguay.com";
		mls.office.PhoneNumber = "+598 94 299 200";
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || mls.ListPrice.value == 1) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.MlsId = get_unique_regex_match(html, 'Property ID:</td><td>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "<a href=\"https://www.realestate-in-uruguay.com/de/property-types/[^\"]*\" rel=\"tag\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, '!3d-([^\!]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, '!2d([^\!]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, '<h3 class="listing-address">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Uruguay";
		mls.Address.City.value = get_unique_regex_match(html, "City:</td><td itemprop=\"addressLocality\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-soliloquy-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=apartments&status=for-sale&locations=0&price-range=0", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=apartments&status=for-rent&locations=0&price-range=0", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Hotels en venta
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=commercial&status=for-sale&locations=0&price-range=0", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Hotels en rent
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=commercial&status=for-rent&locations=0&price-range=0", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=single-family-home&status=for-rent&locations=0&price-range=0", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=single-family-home&status=for-sale&locations=0&price-range=0", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=land&status=for-sale&locations=0&price-range=0", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en rent
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=land&status=for-rent&locations=0&price-range=0", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=modern-architecture&status=for-sale&locations=0&price-range=0", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=beach-property&status=for-sale&locations=0&price-range=0", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=countryside-properties&status=for-sale&locations=0&price-range=0", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=farmsranchesestancias&status=for-sale&locations=0&price-range=0", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=historical&status=for-sale&locations=0&price-range=0", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.realestate-in-uruguay.com/listings/?post_type=listing&property-types=gated-community&status=for-sale&locations=0&price-range=0", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
