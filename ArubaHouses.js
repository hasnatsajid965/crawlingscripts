qa_override("[E4063587116]", "Some of the properties contain description and some not");
qa_override("[E1473228885]", "Some of the properties contain lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html)
					&& (tracer = addNextToPropertiesList(html, tracer, '<a class="d-flex justify-content-center align-items-center h-100 img-filter" href="([^\"]*)"', category, "https://century21aruba.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="page-link" href="([^\"]*)" aria-label="Next">[\s\t\n ]*?<i class="fa fa-chevron-right" aria-hidden="true"></i>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://century21aruba.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	url = url.replace(/ /g, "");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<i class="fa fa-angle-double-right" aria-hidden="true"></i>([^<]*)', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<li><b>Land</b>([ 0-9\,\. ]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<li><b>Land</b>([ 0-9\,\. ]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<h2 class="card-header">Property Remarks</h2>(.+?)</section>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<p class="card-body card-text card-remarks">(.+?)</p>', 1, KDONOTNOTIFYERROR);
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<p class="font-weight-bold agent-name">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, 'class="text-blue" title="Office">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, '</a><br/>[\\s\\t\\n ]*?<a href="mailto:([^?]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="d-block text-bold border-bottom ">([a-zA-Z\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="d-block text-bold border-bottom ">([a-zA-Z\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (mls.ListPrice.value < 50000) {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
		}
		ListPrice = get_unique_regex_match(html, '<span class="d-block text-bold border-bottom ">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(ListPrice)) {
			if (ListPrice.includes("/mth")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<li><b>Category</b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<li><b>Beds</b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<li><b>Baths</b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<li><b>L.Space</b>([ 0-9\,\. ]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<li><b>L.Space</b>([ 0-9\,\. ]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '!2d([^\!]*)!3d([^\!]*)!', 2, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '!2d([^\!]*)!3d([^\!]*)!', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '"addressLocality": "([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Aruba";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img class="img-fluid mx-auto collapse"[\s\t\n ]*?src="[^\"]*"[\s\t\n ]*?data-src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://century21aruba.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://century21aruba.com/en/s/residential/for-sale/condos-apartments", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://century21aruba.com/en/s/residential/for-rent/condos-apartments", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21aruba.com/en/s/residential/for-sale/single-family-homes", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21aruba.com/en/s/residential/for-rent/single-family-homes", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://century21aruba.com/en/s/residential/vacant-land", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
