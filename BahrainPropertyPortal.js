
// crawlForPublications crawl-mode: Regular-Expression Crawling

function land_units(land_units){
	return "Sq. meters";
}

function crawlCategory(url, category, stopword) {

	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<h4>([\\n\\s]*)<a href=\"([^\"]*)\" target=\"_self \">([\\n\\s]*)([^\<]*)([\\n\\s]*)</a>([\\n\\s]*)</h4>", category, "", 2, KDONOTNOTIFYERROR, false)) > 0) {

				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href=\'([^\"]*)\'><i class=\"fas fa-angle-right\"></i></a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		} else {
			relativeLink = relativeLink;
		}
		print("**** crawlCategoryNextButton: " + relativeLink);
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
//	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		
		// LISTING BROKER CODE -------------------------------------------------------
		//<span itemscope="" itemProp="itemListElement" itemType="http://schema.org/ListItem" dir="auto"><span itemProp="name" class="c5051fb4">House 23517282</span><meta itemProp="position" content="4"/></span>
		var reg_exp = "<div class=\"listing_detail col-md-4\" id=\"propertyid_display\"><strong>Property Id :</strong> ([0-9.]*)</div>";
		var code_part1 = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		if (isDefined(code_part1)){
			if(mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code_part1);		
		}

		// LISTING RENT MATCHING	
		reg_exp = "<h1 class=\"entry-title entry-prop\">([^\<]*)</h1>";
		forRent = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);

		if (isDefined(forRent)) {
			if (forRent.includes("Rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} 
		else {
			forRent = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Rent")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}
		print("List Price Currency Period:	" + mls.ListPrice.currencyPeriod);


		/*
		forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-122 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (get_next_regex_match(html, 0, "<i id=\"houzez-print\" class=\"fa fa-print\"", 0, KDONOTNOTIFYERROR) != undefined) {
			var pos = get_last_regex_match_end_pos();
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}*/
		
		// LISTING PRICE -------------------------------------------------------

		//<div><div class="_126656cb "><div class="c4fc20ba"><span class="e63a6bfb" aria-label="Currency">PKR</span><span class="_14bafbc4"></span><span class="_105b8a67" aria-label="Price">1.4 Crore</span></div></div>

		reg_exp = "<span class=\"price_area\">([A-Za-z]*) ([^\ ]*) <span class=\"price_label\"></span></span>";

		if (isDefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			var price_number_value = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);

			print("price_number_value: "+ price_number_value);
			mls.ListPrice.value = price_number_value;
			mls.ListPrice.currencyCode = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
        }
        if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || isUndefined(mls.ListPrice.currencyCode) || mls.ListPrice.currencyCode == "" || mls.ListPrice.value == 0 || mls.ListPrice.value == ".") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
        
		/*
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Renta")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}

		} else {

		} 
		else {

			forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-123 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Renta")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}*/
		
		// LISTING TITLE -------------------------------------------------------
		//<div class="b72558b0"><h1 class="_64bb5b3b">House Available For Sale at Phul Ghulab Road Abbottabad</h1><div class="cbcd1b2b" aria-label="Property header">Phul Ghulab Road, Abbottabad, Khyber Pakhtunkhwa</div></div>

		reg_exp = "<h1 class=\"entry-title entry-prop\">([^\<]*)</h1>";
		
		if (isDefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, reg_exp, 1, KNOTIFYERROR);
		}
		print("Listing title *****" + mls.ListingTitle);
		
		
		
		// LISTING DESCRIPTION -------------------------------------------------------
		//<div class="_066bb126"><h3 class="_95f4723e">Description</h3><div class="_892154cd _6c5bbfd9" style="max-height:110px"><div><div class="_2015cd68" aria-label="Property description"><div dir="auto"><span class="_2a806e1e">Phul Ghulab Abbottabad<br />Please call for more details. Be a lucky buyer to own a House in Abbottabad. Make the most of your purchase with this opportunity. Ideally located on Phul Ghulab Road, this is a rare and golden real estate opportunity. With an area spanning 1350  Square Feet, this will prove to be an ideal place for your family. Plug in to our listings and you'll find out that it has various properties that come at a reasonable price tag such as Rs 14,000,000 in this case. You simply cannot say no to this House. Better yet, it comes with tonnes of exciting features that distinguish it from other properties. <br />Don't hesitate to express your concerns, call us.</span></div></div></div>

		reg_exp = " <div class=\"wpestate_property_description\" id=\"wpestate_property_description_section\">([\\s\\S]*)</div>([\\n\\s]*)<div class=\"panel-group property-panel\" id=\"accordion_prop_addr\">";

		if (isDefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR)
		}
		print("Listing Description *****:	" + mls.ListingDescription);
		
		// LAND SIZE -------------------------------------------------------
		//<li aria-label="Property detail area"><span class="_3af7fa95">Area</span><span class="_812aa185" aria-label="Value"><span>6 Marla</span></span></li>
		reg_exp = "<div class=\"listing_detail col-md-4\"><strong>Property Size:</strong> ([0-9.]*) m<sup>2</sup></div>";
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
            if(isDefined(mls.LotSize.value)) {
                mls.LotSize.areaUnits = land_units();
            }
            
		}
		print("Land Size Value and Area Units *****:	" + mls.LotSize.value + " " + mls.LotSize.areaUnits);
		
		// BEDROOMS -------------------------------------------------------
		//<li aria-label="Property detail beds"><span class="_3af7fa95">Bedroom(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<div class=\"listing_detail col-md-4\"><strong>Bedrooms:</strong> ([0-9.]*)</div>";
		if (isDefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		print("Bedrooms *****:	" + mls.Bedrooms);
		
		// BATHROOMS -------------------------------------------------------
		//<li aria-label="Property detail baths"><span class="_3af7fa95">Bath(s)</span><span class="_812aa185" aria-label="Value">-</span></li>
		reg_exp = "<div class=\"listing_detail col-md-4\"><strong>Bathrooms:</strong> ([0-9.]*)</div>";
		if (isDefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		print("Bathrooms *****:	" + mls.Bathrooms);
		
		// PROPERTY TYPE -------------------------------------------------------
		//<li aria-label="Property detail type"><span class="_3af7fa95">Type</span><span class="_812aa185" aria-label="Value">House</span></li>
		reg_exp = "<li aria-label=\"Property detail type\"><span class=\"([^\"]*)\">Type</span><span class=\"([^\"]*)\" aria-label=\"Value\">([^\"]*)</span></li>";
        property_check = forRent.toLowerCase();
 		/*if (isDefined(mls.PropertyType.value)) {
			mls.PropertyType.value = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);
		}*/
		
		if (isDefined(mls.PropertyType.value)) {
			if (property_check.includes("villa") && property_check.includes("sale")) {
                    mls.NonMLSListingData.category = getCategory(KCOUNTRYHOUSEFORSALE);
                    mls.PropertyType.value = "Villa";
            }
			else if(property_check.includes("apartment") && property_check.includes("sale")) {
                mls.NonMLSListingData.category = getCategory(KFLATFORSALE);
                mls.PropertyType.value = "apartment";
            }
            else if (property_check.includes("building") && property_check.includes("sale")) {
                mls.NonMLSListingData.category = getCategory(KBUILDINGFORSALE);
                mls.PropertyType.value = "building";
            }
            else if (property_check.includes("land") && property_check.includes("sale")) {
				mls.NonMLSListingData.category = getCategory(KLANDFORSALE);
                mls.PropertyType.value = "land";
            }
            else if ((property_check.includes("office") || property_check.includes("plaza")) && property_check.includes("sale")) {
				mls.NonMLSListingData.category = getCategory(KLOCALFORSALE);
                mls.PropertyType.value = "office";
			}
		}
		
		print("Property type *****:	" + mls.PropertyType.value);

		print("Listing data category *****:	" + mls.NonMLSListingData.category);
		
		// BROKERAGE NAME -------------------------------------------------------
		//<span class="_725b3e64">Arif Khan</span></div><div class="_6f510616"><a href="/Profile/Abbottabad-Fahad_Real_Estate-179977-1.html" class="_23d69e6d" title="Fahad Real Estate">Agency profile</a></div>
		reg_exp = "<p>Contact: ([^\<]*)<br />([\\n\\s]*)([^\<]*)</p>";
		if (isDefined(mls.Brokerage.Name)) {
			mls.Brokerage.Name = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		}
		print("Broker name *****:	" + mls.Brokerage.Name);
		
		// BROKERAGE Phone No. -------------------------------------------------------
		if (isDefined(mls.Brokerage.Phone)) {
			mls.Brokerage.Phone = get_unique_regex_match(html, reg_exp, 3, KDONOTNOTIFYERROR);

		}
		print("Broker Phone No. *****:	" + mls.Brokerage.Phone);

        // BROKERAGE ADDRESS -------------------------------------------------------
		
		reg_exp = "<div class=\"listing_detail col-md-4\"><strong>City:</strong> <a href=\"([^\"]*)\" rel=\"tag\">([A-Za-z]*)</a></div>";
		if (isDefined(mls.Address.City.value)) {
			var city_name = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
			mls.Address.City.value = city_name;

		}
		print("Broker Address *****:	" + city_name);
		
		
		/*
		mls.LotSize.value = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Dormitorios:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Ba?os:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<strong>Estacionamientos: </strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Tipo:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Apartamentos")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
					}
				}
			}
			if (mls.PropertyType.value.includes("Casas")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KCASASVENTAS);
					}
				}
			}
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<i class=\"fa fa-user\"></i> ([^<]*)</", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<i class=\"fa fa-mobile\"></i>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<strong>Sector:</strong> ([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>Ciudad:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var operacions = get_next_regex_match(html, 0, "<strong>Operaci?n:</strong> ([^<]*?)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(operacions)) {
			var pos = get_last_regex_match_end_pos();
			var operationList = operacions.split(",");
			if (operationList.length > 1) {
				operationList.forEach(function(oneOperation) {
					if (isPublicationPurchase()) {
						if (oneOperation == "Renta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("LEASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationLease()) {
						if (oneOperation == "Venta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("PURCHASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationRent()) {
					}
				});
			}
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}*/
		
		mls.Address.Country.value = "Bahrain";
		reg_exp = "\"hq_latitude\":\"([0-9.]*)\",\"hq_longitude\":\"([0-9.]*)\"";
		mls.NonMLSListingData.propertyLocation.Latitude.value = get_unique_regex_match(html, reg_exp, 1, KDONOTNOTIFYERROR);
		mls.NonMLSListingData.propertyLocation.Longitude.value = get_unique_regex_match(html, reg_exp, 2, KDONOTNOTIFYERROR);
		print("Latitude *****:	" + mls.NonMLSListingData.propertyLocation.Latitude.value);
		print("Longitude *****:	" + mls.NonMLSListingData.propertyLocation.Longitude.value);
		
		//<picture class="_219b7e0a"><source type="image/webp" srcSet="https://media.zameen.com/thumbnails/85464870-120x90.webp"/><img role="presentation" alt="2 " title="2 " src="https://media.zameen.com/thumbnails/85464870-120x90.jpeg"/></picture>
		reg_exp = "<a href=\"([^\"]*)\" title=\"\" rel=\"prettyPhoto\" class=\"prettygalery\" >";
		images = get_all_regex_matched(html, reg_exp, 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			if (getFileSizeWithProxyConsiderations (obj.MediaURL) > 0) {
				mls.Photos.photo.push(obj);
							imageCount++;
				}
        });
        
		resulting_json(JSON.stringify(mls));
		
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}


function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Villas
		{
			try {
				// Villas for sale
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/villa-for-sale/", KCOUNTRYHOUSEFORSALE );
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas for lease
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/villa-for-rent/", KCOUNTRYHOUSEFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartments
		{
			try {
				// Apartments for sale
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/apartment-for-sale/", KFLATFORSALE);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartments for rent
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/apartment-for-rent/", KFLATFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Buildings for sale
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/building-for-sale/", KBUILDINGFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
            try {
				// Buildings for rent
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/building-for-rent/", KBUILDINGFORLEASE);
			} catch (e) {
				exceptionprint(e);
			}
        }
        // Land
		{
			try {
				// Land for sale
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/land-sale-rent/", KLANDFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
        }
        // Commercial
		{
			try {
				// Commercial for sale
				cumulatedCount += crawlCategory("http://www.bahrainpropertyportal.com/commercial-sale-rent/", KLOCALFORSALE);
			} catch (e) {
				exceptionprint(e);
            }
        }
        

		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
