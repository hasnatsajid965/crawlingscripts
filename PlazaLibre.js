qa_override("[E2870690408]", "Some properties have cities and some not.");
qa_override("[E1473228885]", "Some property does have lot size and some does not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12  cell\">[\\s\\t\\n ]*?<a href='([^\']*)'>", category, "", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a href=\"([^\"]*)\" rel=\"next\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<h2>DESCRIPCI[^\<]*N DEL CLASIFICADO</h2>[\\s\\t\\n ]*?<hr>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<!-- Go to www.addthis.com/dashboard to customize your tools -->[\s\t\n ]*?<h2>([a-zA-Z\$ ]*)([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<!-- Go to www.addthis.com/dashboard to customize your tools -->[\s\t\n ]*?<h2>([a-zA-Z\$ ]*)([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h2 class=\"title\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<span>([ 0-9\,\. ]*)</span> ([a-zA-Z0-9]*) del terreno</li>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<span>([ 0-9\,\. ]*)</span> ([a-zA-Z0-9]*) del terreno</li>", 2, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "<span>([^\<]*)</span> A?o de construcci?n</li>", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<span>([^\<]*)</span>[ ]*?Habitaciones</li>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<span>([^\<]*)</span> Ba?os </li>", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "<span>Clasificado ID</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<span> Nombre</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<span>Residencial </span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, "id=\"uemail\" name=\"uemail\" value=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<span>([^\<]*)</span> Parqueos</li>", 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_unique_regex_match(html, '<span>Provincia</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '<span>Sector</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		if (mls.ListPrice.value == 1 || mls.ListPrice.value == 0) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a href=\"([^\"]*)\" class=\"images\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=39&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=43&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=40&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=41&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=643&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=669&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=671&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=42&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=44&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=45&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=84&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory(
						"https://www.plazalibre.com/categoria/bienes-raices?equal%5Bsubcategory_id%5D=694&equal%5Bad_action%5D=all&realstate%5Bequal%5D%5Brooms%5D=all&realstate%5Bequal%5D%5Bbath%5D=all&realstate%5Bequal%5D%5Bparking%5D=all&min%5Bad_price%5D=&max%5Bad_price%5D=&equal%5Bad_province%5D=all&equal%5Bsector_id%5D=all&SearchApply=1&OrderBy%5B%5D=desc-billing_date_from&view_plazalibre=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
