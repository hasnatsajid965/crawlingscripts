qa_override("[E1473228885]", "Some properties have lot size and some not");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html)
					&& (tracer = addNextToPropertiesList(html, tracer, '<div class="views-field views-field-title">[\\s\\t\\n ]*?<span class="field-content"><a href="([^"]*)"', category, "https://www.barbadospropertysearch.com", 1,
							KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="pager-next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.barbadospropertysearch.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	if (url !== "https://www.barbadospropertysearch.com//for-sale/heron-court-under-offer" || url !== "https://www.barbadospropertysearch.com/for-sale/the-one-at-the-st-james") {
		var html = gatherContent_url(url, KDONOTNOTIFYERROR);
		var mls = JSON.parse(mlsJSONString);
		if (isDefined(html)) {
			if (mls.NonMLSListingData == undefined) {
				mls.NonMLSListingData = {};
			}
			if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
				mls.NonMLSListingData.lang = "en-us";
			}
			//
			var title = get_next_regex_match(html, 0, 'class=\"block block-views contextual-links-region even\"><h2 class=\"block-title\">([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(title)) {
				mls.ListingTitle = title;
			}
			if (!isUndefined(title)) {
				var title2 = get_next_regex_match(html, 0, 'id="block-views-image-slideshows-block" class="block block-views even">[\\s\\t\\n ]*?<h2 class="block-title">([^\<]*)', 1, KDONOTNOTIFYERROR);
				if (isDefined(title2)) {
					mls.ListingTitle = title2;
				}
			}
			if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
				mls.ListingTitle = get_next_regex_match(html, 0, '<div id="block-views-image-slideshows-block" class="block block-views even">[\s\t\n ]*?<h2 class="block-title">([^\<]*)', 1, KDONOTNOTIFYERROR);

			}
			mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="label">Sale Price:</span>([ \$ ]*)[0-9\,\. ]*[a-zA-Z]*', 1, KDONOTNOTIFYERROR)
					+ get_unique_regex_match(html, '<span class="label">Sale Price:</span>[ \$ ]*[0-9\,\. ]*([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_unique_regex_match(html, '<span class="label">Sale Price:</span>[ \$ ]*([0-9\,\. ]*)[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
			if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
				return analyzeOnePublication_return_unreachable;
			}
			mls.LotSize.value = get_unique_regex_match(html, 'class="label">Land Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, 'class="label">Land Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.LivingArea.value = get_unique_regex_match(html, '<span class="label">Floor Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
			mls.LivingArea.areaUnits = get_unique_regex_match(html, '<span class="label">Floor Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 2, KDONOTNOTIFYERROR);
			mls.Bedrooms = get_unique_regex_match(html, '<span class="label">Bedrooms:</span>([^<]*)</div>', 1, KDONOTNOTIFYERROR);
			mls.Bathrooms = get_unique_regex_match(html, '<span class="label">Bathrooms:</span>([^<]*)</div>', 1, KDONOTNOTIFYERROR);
			var all_features = "";
			var features_split = "";
			all_features = get_unique_regex_match(html, '<span class=\"label\">Amenities:</span>[\\s\\t\\n ]*?<p>([^<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(all_features)) {
				features_split = all_features.split(",");
			}
			if (isDefined(features_split) && features_split.length !== 0) {
				features_split.forEach(function(feature) {
					if (isDefined(feature)) {
						if (mls.DetailedCharacteristics == undefined) {
							mls.DetailedCharacteristics = {};
						}
						if (feature === "Pool") {
							mls.DetailedCharacteristics.hasPool = true;
						} else if (feature === "AC") {
							mls.DetailedCharacteristics.hasAirCondition = true;
						} else {
							if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
								mls.DetailedCharacteristics.AdditionalInformation = {};
							}
							if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
								mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
							}
							feature = feature.replace(/\s+/g, " ").trim();
							if (feature !== "") {
								var obj = {};
								obj.Description = feature;
								mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
							}
						}
					}
					if (feature.endsWith("View")) {
						if (mls.DetailedCharacteristics.ViewTypes == undefined) {
							mls.DetailedCharacteristics.ViewTypes = {};
						}
						if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
							mls.DetailedCharacteristics.ViewTypes.ViewType = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.value = feature;
							mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
						}
					}
				});
			}
			mls.ListingDescription = get_next_regex_match(html, 0, '<div class="block" id="full-description"><h3>Description</h3><p>(.+?)</div>', 1, KDONOTNOTIFYERROR);
			if (mls.office == undefined)
				mls.office = {};
			mls.Brokerage.Name = get_next_regex_match(html, 0, 'Realtor:</span>[\\s\\t\\n ]*?<a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (mls.Brokerage.Name == undefined) {
				mls.Brokerage.Name = [];
			}
			mls.Brokerage.Email = get_next_regex_match(html, 0, '<a href="mailto[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (mls.Brokerage.Email == undefined) {
				mls.Brokerage.Email = [];
			}
			mls.office.PhoneNumber = get_next_regex_match(html, 0, 'Tel:</span>([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (mls.office.PhoneNumber == undefined) {
				mls.office.PhoneNumber = [];
			}
			if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || mls.ListPrice.value == 1) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
			mls.MlsId = get_unique_regex_match(html, '<span class="label">Property Reference:</span>([^<]*)</div> ', 1, KDONOTNOTIFYERROR);
			mls.PropertyType.value = get_unique_regex_match(html, "Property Type:</span>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
			mls.Location.Latitude = get_next_regex_match(html, 0, '"latitude":([^,]*),"longitude":([^,]*),"markername"', 1, KDONOTNOTIFYERROR);
			mls.Location.Longitude = get_next_regex_match(html, 0, '"latitude":([^,]*),"longitude":([^,]*),"markername"', 2, KDONOTNOTIFYERROR);
			mls.Address.Country.value = "Barbados";
			mls.Address.City.value = get_unique_regex_match(html, "<span class=\"label\">Location:</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
			var imageCount = 0;
			var images;
			images = get_all_regex_matched(html, '<meta property="og:image" content="([^"]*)" ', 1);
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (oneImageTag !== "https://www.barbadospropertysearch.com/sites/default/files/styles/medium/public") {
					if (/(http(s?)):\/\//gi.test(oneImageTag)) {
						obj.MediaURL = oneImageTag;
					} else {
						obj.MediaURL = oneImageTag;
					}
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
				}
			});
			resulting_json(JSON.stringify(mls));
			return analyzeOnePublication_return_success;
		} else {
			return analyzeOnePublication_return_unreachable;
		}
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// building en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Apartment&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Beach+House&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Condo%2FTownhouse&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=House&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Plantation+House&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Villa&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Land&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-sale?search=&type=Commercial&bedrooms=All&sale_price_min=&sale_price_max=&sort_by=created&sort_order=DESC&parish=All", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://www.barbadospropertysearch.com/for-rent?sort_by=field_rental_price_value&sort_order=ASC", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
