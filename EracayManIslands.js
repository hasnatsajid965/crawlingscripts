

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="property-image ">[\\s\\t\\n ]*?<a href=([^\>]*)>', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a class="next page-numbers" href="([^\"]*)">Next page', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	if (url !== "https://www.eracaymanislands.com/properties/spotts-duplex/") {
		url = url.replace(/['"]+/g, '')
		var html = gatherContent_url(url, KDONOTNOTIFYERROR);
		var mls = JSON.parse(mlsJSONString);
		if (isDefined(html)) {
			if (mls.NonMLSListingData == undefined) {
				mls.NonMLSListingData = {};
			}
			if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
				mls.NonMLSListingData.lang = "en-us";
			}
			mls.ListingTitle = get_next_regex_match(html, 0, '<div class="property-title">[\\s\\t\\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (isUndefined(mls.ListingTitle)) {
				mls.ListingTitle = get_next_regex_match(html, 0, '<div class=property-title>[\\s\\t\\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
			}
			if (isUndefined(mls.ListingTitle)) {
				return analyzeOnePublication_return_innactive;
			}
			mls.ViewTypes = get_unique_regex_match(html, "<span>View</span><strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
			mls.ParkingTypes = get_unique_regex_match(html, "<span>Garage</span><strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.value = get_unique_regex_match(html, "<div class=\"withicon\"><span class=\"fas fa-expand\"></span><div class=\"property-info-value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isUndefined(mls.LotSize.value)) {
				mls.LotSize.value = get_unique_regex_match(html, "<li class=\"main-detail-_lot_size\"><i class=\"flaticon-select-1\"></i>[\\s\\t\\n ]*?<span>([^\<]*)", 1, KDONOTNOTIFYERROR);
			}
			mls.Bedrooms = get_unique_regex_match(html, "<span>Beds</span><strong>([^<]*)</strong>", 1, KDONOTNOTIFYERROR);
			mls.Bathrooms = get_unique_regex_match(html, "<span>Baths</span><strong>([^<]*)</strong>", 1, KDONOTNOTIFYERROR);
			var features = get_all_regex_matched(html, "<span>([^\<]*)</span><strong>Yes", 1);
			if (isDefined(features)) {
				features.forEach(function(feature) {
					if (isDefined(feature)) {
						if (mls.DetailedCharacteristics == undefined) {
							mls.DetailedCharacteristics = {};
						}
						if (feature === "Pets Allowed") {
							mls.DetailedCharacteristics.hasPetAllowed = true;
						} else if (feature === "Furnished") {
							mls.DetailedCharacteristics.hasFurnished = true;
						} else if (feature === "Foundation") {
							mls.DetailedCharacteristics.hasFoundation = true;
						} else if (feature === "Garage") {
							mls.DetailedCharacteristics.hasGarage = true;
						} else {
							if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
								mls.DetailedCharacteristics.AdditionalInformation = {};
							}
							if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
								mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
							}
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				});
			}
			mls.ListingDescription = get_next_regex_match(html, 0, "<h2>Property Description</h2>[\s\t\n ]*?<p>(.+?)</p>", 1, KDONOTNOTIFYERROR);
			if (mls.office == undefined)
				mls.office = {};
			mls.Brokerage.Name = get_next_regex_match(html, 0, '\<figcaption class="elementor-image-carousel-caption\">([^\<]*)', 1, KDONOTNOTIFYERROR);
			mls.Brokerage.Phone = get_next_regex_match(html, 0, '<span class=\"elementor-icon-list-text\">([^\<]*)', 1, KDONOTNOTIFYERROR);
			mls.Brokerage.Email = "info@eracaymanislands.com";
			mls.MlsId = get_unique_regex_match(html, '<span>MLS #</span><strong class="emphasize">([^<]*)</strong>', 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_unique_regex_match(html, 'Price</strong>[\s\t\n ]*?<span class="ere-property-price">[\s\t\n ]*?<span class="property-price-prefix">[a-zA-Z ]*</span>[\s\t\n ]*?[\$]([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Price</strong>[\s\t\n ]*?<span class="ere-property-price">[\s\t\n ]*?<span class="property-price-prefix">([a-zA-Z ]*)</span>[\s\t\n ]*?[\$][0-9\,\.]*', 1, KDONOTNOTIFYERROR)
					+ get_unique_regex_match(html, 'Price</strong>[\s\t\n ]*?<span class="ere-property-price">[\s\t\n ]*?<span class="property-price-prefix">[a-zA-Z ]*</span>[\s\t\n ]*?([\$])[0-9\,\.]*', 1, KDONOTNOTIFYERROR);
			mls.PropertyType.value = get_unique_regex_match(html, "<span>Property Type</span><strong>([^<]*)", 1, KDONOTNOTIFYERROR);
			if (mls.PropertyType.value == undefined) {
				mls.PropertyType.value = [];
			}
			mls.Location.Latitude = get_unique_regex_match(html, "lat:([^,]*)", 1, KDONOTNOTIFYERROR);
			mls.Location.Longitude = get_unique_regex_match(html, "lng:([^}]*)", 1, KDONOTNOTIFYERROR);
			mls.Address.Country.value = "Cayman Islands";
			var location_city = "";
			location_city = get_unique_regex_match(html, "<span>Location</span><strong>([^<]*)", 1, KDONOTNOTIFYERROR);
			if (location_city == 'undefined') {
				mls.Address.City.value = [];
			} else {
				mls.Address.City.value = location_city;
			}
			var imageCount = 0;
			var images;
			images = get_all_regex_matched(html, '<li data-target="#slider-property" data-slide-to="[^\"]*">[\\s\\t\\n ]*?<img src="([^\"]*)"', 1);
			if (isDefined(images)) {
				images = images;
			} else {
				images = get_all_regex_matched(html, '<li data-target=#slider-property data-slide-to=[^\>]*>[\\s\\t\\n ]*?<img src=([^\"]*) alt="">', 1);
			}
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = "https://www.eracaymanislands.com/" + oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
			resulting_json(JSON.stringify(mls));
			return analyzeOnePublication_return_success;
		} else {
			return analyzeOnePublication_return_unreachable;
		}
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.eracaymanislands.com/properties/?filter-contract=SALE&filter-property-type=121&filter-property-title=&filter-id=&filter-location=&filter-material=&filter-price-from=&filter-price-to=&filter-baths=&filter-beds=",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Locales - Tiendas
		// {
		try {
			// Locales - Tiendas en venta
			cumulatedCount += crawlCategory(
					"https://www.eracaymanislands.com/properties/?filter-contract=SALE&filter-property-type=55&filter-property-title=&filter-id=&filter-location=&filter-material=&filter-price-from=&filter-price-to=&filter-baths=&filter-beds=",
					KEDIFICIOSVENTAS);
		} catch (e) {
			print("error: " + e);
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.eracaymanislands.com/properties/?filter-contract=SALE&filter-property-type=143&filter-property-title=&filter-id=&filter-location=&filter-material=&filter-price-from=&filter-price-to=&filter-baths=&filter-beds=",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory(
						"https://www.eracaymanislands.com/properties/?filter-contract=SALE&filter-property-type=127&filter-property-title=&filter-id=&filter-location=&filter-material=&filter-price-from=&filter-price-to=&filter-baths=&filter-beds=",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				cumulatedCount += crawlCategory(
						"https://www.eracaymanislands.com/properties/?filter-contract=SALE&filter-property-type=134&filter-property-title=&filter-id=&filter-location=&filter-material=&filter-price-from=&filter-price-to=&filter-baths=&filter-beds=",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
