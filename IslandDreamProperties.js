

// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			el1 = virtual_browser_find_one(browser, "(//figure[@class='item-thumb']/a[starts-with(@href, 'http://islanddreamproperties.com/property/') and @class='hover-effect'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "(//*[@class='fa fa-angle-right'])[1]", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(6000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	// if (isUndefined(browser)) {
	// browser = create_virtual_browser("HeadlessChrome");
	// if (isUndefined(browser)) {
	// return analyzeOnePublication_return_tech_issue;
	// }
	// }
	// virtual_browser_navigate(browser, url);
	// var html = virtual_browser_html(browser);
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	// var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		notAvailable = get_unique_regex_match(html, "<div class=\"error-404-page text-center\">[\s\t\n ]*?<h1>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(notAvailable)) {
			return analyzeOnePublication_return_unreachable;
			;
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<li class=\"prop_type\"><strong>[^\<]*</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		propertyStatus = get_unique_regex_match(html, '<strong>Property Status:</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(propertyStatus)) {
			if (propertyStatus.includes("Sale") && mls.PropertyType.value.includes("Villa")) {
				mls.NonMLSListingData.category = KCASASVENTAS;
			}
		}
		mls.ListingTitle = get_unique_regex_match(html, "<div class=\"table-cell\"><h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = mls.ListingTitle.replace(/\s+/g, " ").trim();
		mls.ListingDescription = get_unique_regex_match(html, '<!-- end NEW -->(.+?)<h3', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription) || mls.ListingDescription == "" || mls.ListingTitle == "") {
			return analyzeOnePublication_return_unreachable;
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"item-price\"><span class=\"price-start\">[^\<]*</span>([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"item-price\"><span class=\"price-start\">[^\<]*</span>([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		list_price = get_next_regex_match(html, 0, "<span class=\"item-price\"><span class=\"price-start\">[^\<]*</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			if (list_price.includes("night") || list_price.includes("Night")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
			}
		} else {
			list_price = get_next_regex_match(html, 0, "<span class=\"item-price\"><span class=\"price-start\">[^\<]*</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"item-price\"><span class=\"price-start\">[^\<]*</span>([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"item-price\"><span class=\"price-start\">[^\<]*</span>([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(list_price)) {
				if (list_price.includes("night") || list_price.includes("Night")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
				}
			} else {
				list_price = get_next_regex_match(html, 0, "<span class=\"item-price item-price-text price-single-listing-text\">([^\<]*)", 1, KDONOTNOTIFYERROR);
				mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"item-price item-price-text price-single-listing-text\">([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
				mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"item-price item-price-text price-single-listing-text\">([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
				if (isDefined(list_price)) {
					if (list_price.includes("night") || list_price.includes("Night")) {
						mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
					}
				} else {
					list_price = get_next_regex_match(html, 0, "<span class=\"item-price\">([^\<]*)</span>[\\s\\t\\n ]*?</div>", 1, KDONOTNOTIFYERROR);
					mls.ListPrice.value = get_next_regex_match(html, 0, "<span class=\"item-price\">([\$])([0-9\,\. ]*)</span>[\\s\\t\\n ]*?</div>", 2, KDONOTNOTIFYERROR);
					mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<span class=\"item-price\">([\$])([0-9\,\. ]*)</span>[\\s\\t\\n ]*?</div>", 1, KDONOTNOTIFYERROR);
					if (isDefined(list_price)) {
						if (list_price.includes("night") || list_price.includes("Night")) {
							mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
						}
					}
				}
			}
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_unique_regex_match(html, "<strong>Land Area:</strong>([0-9\.\, ]*)([a-zA-Z\.\, ]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>Land Area:</strong>([0-9\.\, ]*)([a-zA-Z\.\, ]*)", 2, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "<strong>Year Built:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "<li class=\"prop_type\"><strong>[^\<]*</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Bedrooms:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, "<li id=\"menu-item-10186\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-10186\"><a href=\"[^\"]*\" data-ps2id-api=\"true\">([^\<]*)Bedroom</a></li>", 1,
					KDONOTNOTIFYERROR);
		}
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Bathrooms:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Island Dream Properties";
		mls.Brokerage.Phone = "+12644983200";
		mls.Brokerage.Email = "info@islanddreamproperties.com";
		mls.MlsId = get_unique_regex_match(html, "MLS #([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '"property_lat":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '"property_lng":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "<address class=\"property-address\">([^\<]*)", 1, KNOTIFYERROR);
		mls.Address.Country.value = "Anguilla";
		var features = get_all_regex_matched(html, '<li><a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div class=\"item\"><img src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			oneImageTag = oneImageTag.replace("-150x110.jpg", ".jpg")
			obj.MediaURL = oneImageTag;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function buildingForSale(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='0'][1])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function commercialForSale(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='1'][1])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function landForSale(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='3'][1])[1]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function houseForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	wait(1000);
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='2'][1])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	wait(8000);
	element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='4'][1])[1]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function buildingForRent(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[1]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	wait(2000);
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='2'][1])[1]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	wait(2000);
	element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='0'][1])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function commercialForRent(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='1'][1])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function landForRent(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='3'][1])[1]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function houseForRent(browser, category) {
	var cumulatedCount = 0;
	wait(2000);
	var element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='2'][1])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	wait(8000);
	element = virtual_browser_find_one(browser, "(//button[@class='btn dropdown-toggle btn-default'])[2]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	element = virtual_browser_find_one(browser, "(//li[@data-original-index='4'][1])[1]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(10000);
	}
	cumulatedCount += crawlCategory(browser, category);
	return cumulatedCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		virtual_browser_navigate(browser, "https://islanddreamproperties.com/search/?keyword=&type=villa&status=real-estate");
		cumulatedCount += buildingForSale(browser, KEDIFICIOSVENTAS);
		cumulatedCount += commercialForSale(browser, KOTROSVENTAS);
		cumulatedCount += landForSale(browser, KTERRENOSVENTAS);
		cumulatedCount += houseForSale(browser, KCASASVENTAS);
		cumulatedCount += buildingForRent(browser, KEDIFICIOSALQUILER);
		cumulatedCount += commercialForRent(browser, KOTROSALQUILER);
		cumulatedCount += landForRent(browser, KTERRENOSALQUILER);
		cumulatedCount += houseForRent(browser, KCASASALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
