var countryText = "British Virgin Islands";

include("base/ChristiesRealEstateFunctions.js")

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/vgb", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
	}
}

