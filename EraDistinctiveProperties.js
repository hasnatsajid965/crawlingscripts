DISABLE_JAVASCRIPT("Images are not crawling due to iframe.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, "<figure class='item3-thumb'>[\s\t\n ]*?<a href='([^\']*)'", category, "https://www.eradistinctiveproperties.com/", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<li class='active'><a href='#'>[^\<]*<span class='sr-only'>[^\<]*</span></a></li>  <li><a href='([^\']*)'", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.eradistinctiveproperties.com/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	url = url.replace(/ /g, "");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.LotSize.value = get_unique_regex_match(html, '<br /><strong><span id="lblSquare" style="color:#fff;">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'style=\'color:#fff;\'>([^\<]*)<br /><strong><span id="lblSquare" style="color:#fff;">', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, "<h4 class='position' style='margin-top: -25px;color:#fff;'>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, 'MLS:([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_next_regex_match(html, 0, 'MLS:[^\<]*<br>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class=\'col-xs-12 text-center\'><p style=\'color:#fff;\'>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<h3 style='font-size: 18px;color:#fff;'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = "400-041-7515";
		mls.ListPrice.value = get_unique_regex_match(html, '<a href=\'#\' style=\'font-size: 30px;color: #fff;\'>([\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="d-block text-bold border-bottom ">([\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Bedrooms = get_unique_regex_match(html, "Bedrooms<br /><strong> <span id=\"lblBedRoom\" style=\"color:#fff;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Bathrooms<br /><strong><span id=\"lblBathRoom\" style=\"color:#fff;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "Garage<br /><strong><span id=\"lblGarage\" style=\"color:#fff;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'var property_LAT =([^\;]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'var property_LONG =([^\;]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '所在城市：</span><span class="pvalue">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, '所在国家：</span><span class="pvalue">([^\<]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img style="display:none" class="lazy" data-original="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://century21aruba.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.eradistinctiveproperties.com/PropertyLifeStyleSearch?keyword=lake#", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
