qa_override("[E1473228885]", "Some properties contain lot size and some not.");
qa_override("[E2592408747]", "Some properties have city and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="listing-item__details-link" href="([^"]*)"', category, "https://www.bvisothebysrealty.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a href="([^"]*)" onclick="[^"]*" class="paging__item  paging__item--next  js-at-icon-paging  js-paging-next"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.bvisothebysrealty.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="main-address">([^<]*)</div>', 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<dd itemprop="value" class="listing-info__value">([0-9\,\. ]*)[\s\t\n ]*?([a-zA-Z\.\, ]*)[\s\t\n ]*?</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Interior', 1,
				KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<dd itemprop="value" class="listing-info__value">([0-9\,\. ]*)[\s\t\n ]*?([a-zA-Z\.\, ]*)[\s\t\n ]*?</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Interior', 2,
				KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'class="listing-info__value">([0-9\,\. ]*)([a-zA-Z\.\, ]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Exterior', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'class="listing-info__value">([0-9\,\. ]*)([a-zA-Z\.\, ]*)</dd><dt itemprop="name" class="listing-info__title">[\s\t\n ]*?Exterior', 2, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?YEAR BUILT', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?Full Baths', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '>([^\<]*)</dd><dt itemprop=\"name\" class=\"listing-info__title\">Web Id', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<span class="prop-description__amenities-list-item-text" itemprop="value">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="p">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, 'itemprop="url"><span itemprop="name">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, 'Phone: </span><a class="o-phone-number  tel  phone_block  phone-num-1  icon-text  js-phone-link" href="tel:([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Address.FullStreetAddress = get_unique_regex_match(html, '<div class=\"street-address\">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Address.City.value = get_unique_regex_match(html, '<div class=\"c-address  u-text-ellipsis\"><span class=\"address\"></span>[^\<]*<span class=\"locality\">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Address.PostalCode.value = get_unique_regex_match(
				html,
				'<div class="c-address  u-text-ellipsis"><span class="address"></span>[\\s\\t\\n ]*?<span class="locality">[^\<]*</span><span class="separator">,[\\s\\t\\n ]*?</span>[\\s\\t\\n ]*?<span class="region">[^\<]*</span>[\\s\\t\\n ]*?<span class="postal-code">([^\<]*)</span>[\\s\\t\\n ]*?<span class="country-name">([^\<]*)</span>',
				1, KDONOTNOTIFYERROR);
		mls.Brokerage.Address.Country.value = get_unique_regex_match(
				html,
				'<div class="c-address  u-text-ellipsis"><span class="address"></span>[\\s\\t\\n ]*?<span class="locality">[^\<]*</span><span class="separator">,[\\s\\t\\n ]*?</span>[\\s\\t\\n ]*?<span class="region">[^\<]*</span>[\\s\\t\\n ]*?<span class="postal-code">([^\<]*)</span>[\\s\\t\\n ]*?<span class="country-name">([^\<]*)</span>',
				2, KDONOTNOTIFYERROR);
		mls.DetailedCharacteristics.ArchitectureStyle.value = get_unique_regex_match(html, 'Style<!----></dt><dd itemprop="name" class="prop-description__value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<span class="price__value  u-ignore-phonenumber" itemprop="price" content="([^\"]*)">', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<meta itemprop="priceCurrency" content="([^\"]*)" />', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			list_price = get_next_regex_match(html, 0, '<span class="price__value  price__upon-request">([^<]*)', 1, KDONOTNOTIFYERROR);
			if (list_price == "Price Upon Request") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, 'class="listing-info__value">([^<]*)</dd><dt itemprop="name" class="listing-info__title">[\\s\\t\\n ]*?Property Type', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '#detailMap",Latitude:"([^"]*)",Longitude:"([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '#detailMap",Latitude:"([^"]*)",Longitude:"([^"]*)"', 2, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, '<span itemprop="addressLocality" class="locality">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, '<span itemprop="addressRegion" class="region">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_next_regex_match(html, 0, '<span itemprop="addressCountry" class="country-name">([^<]*)', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<noscript><img src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.bvisothebysrealty.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// multifamily en venta
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/condominium-type", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// multifamily en venta
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/apartment-type", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// multifamily en venta
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/multi-family-home-type", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/single-family-home-type", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/townhouse-type", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/land-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			// stores
			try {
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/commercial-type", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.bvisothebysrealty.com/eng/sales/to-vgb/other-residential-type", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
