DISABLE_JAVASCRIPT("It is failed because it is not returning HTMl.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


// qa_override("[E1473228885]", "Some properties have lot size and some not");

// function crawlCategory(url, category, stopword) {
// var cumulatedCount = 0;
// print("---- Processing category " + category + " in " + getJavascriptFile() +
// " ----");
// var actualCount = 0;
// var page = 1;
// var tracer = 0;
// var html = gatherContent_url(url, KDONOTNOTIFYERROR);
// while (isDefined(html)) {
// try {
// while (isDefined(html)
// && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)"
// itemprop="streetAddress" title="7071 Maple Road">', category,
// "https://www.era.com", 1,
// KDONOTNOTIFYERROR, true)) > 0) {
// tracer++;
// actualCount++;
// cumulatedCount++;
// }
// } catch (e) {
// if (e !== "Duplicate detected.") {
// throw "error: " + e;
// }
// }
// print("---- " + actualCount + " found in " + getJavascriptFile() + " on page
// " + page + " for category " + category + " (" + cumulatedCount + ") ----");
// var relativeLink = get_unique_regex_match(html, 'aria-label="Next Page"
// class="js-ada-next btn-secondary btn-secondary--small btn-secondary--paginate
// paginate--single" onclick="[^\"]*" href="([^\"]*)"', 1, KDONOTNOTIFYERROR);
// if (isUndefined(relativeLink)) {
// break;
// }
// html = gatherContent_url("https://www.era.com" + relativeLink, false,
// KDONOTNOTIFYERROR);
// tracer = 0;
// actualCount = 0;
// page++;
// }
// return cumulatedCount;
// }

// function analyzeOnePublication(url, mlsJSONString) {
// var html = gatherContent_url(url, KDONOTNOTIFYERROR);
// var mls = JSON.parse(mlsJSONString);
// if (isDefined(html)) {
// if (mls.NonMLSListingData == undefined) {
// mls.NonMLSListingData = {};
// }
// if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang ==
// "") {
// mls.NonMLSListingData.lang = "en-us";
// }
// //
// var title = get_next_regex_match(html, 0, 'class=\"block block-views
// contextual-links-region even\"><h2 class=\"block-title\">([^\<]*)', 1,
// KDONOTNOTIFYERROR);
// if (isDefined(title)) {
// mls.ListingTitle = title;
// }
// if (!isUndefined(title)) {
// var title2 = get_next_regex_match(html, 0,
// 'id="block-views-image-slideshows-block" class="block block-views
// even">[\\s\\t\\n ]*?<h2 class="block-title">([^\<]*)', 1, KDONOTNOTIFYERROR);
// if (isDefined(title2)) {
// mls.ListingTitle = title2;
// }
// }
// if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
// mls.ListingTitle = get_next_regex_match(html, 0, '<div
// id="block-views-image-slideshows-block" class="block block-views
// even">[\s\t\n ]*?<h2 class="block-title">([^\<]*)', 1, KDONOTNOTIFYERROR);

// }
// mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span
// class="label">Sale Price:</span>([ \$ ]*)[0-9\,\. ]*[a-zA-Z]*', 1,
// KDONOTNOTIFYERROR)
// + get_unique_regex_match(html, '<span class="label">Sale Price:</span>[ \$
// ]*[0-9\,\. ]*([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
// mls.ListPrice.value = get_unique_regex_match(html, '<span class="label">Sale
// Price:</span>[ \$ ]*([0-9\,\. ]*)[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
// if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" &&
// isUndefined(mls.ListPrice.value)) {
// return analyzeOnePublication_return_unreachable;
// }
// mls.LotSize.value = get_unique_regex_match(html, 'class="label">Land
// Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1, KDONOTNOTIFYERROR);
// mls.LotSize.areaUnits = get_unique_regex_match(html, 'class="label">Land
// Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 2, KDONOTNOTIFYERROR);
// mls.LivingArea.value = get_unique_regex_match(html, '<span
// class="label">Floor Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 1,
// KDONOTNOTIFYERROR);
// mls.LivingArea.areaUnits = get_unique_regex_match(html, '<span
// class="label">Floor Area:</span>([ 0-9\,\. ]*)([ a-zA-Z\,\. ]*)', 2,
// KDONOTNOTIFYERROR);
// mls.Bedrooms = get_unique_regex_match(html, '<span
// class="label">Bedrooms:</span>([^<]*)</div>', 1, KDONOTNOTIFYERROR);
// mls.Bathrooms = get_unique_regex_match(html, '<span
// class="label">Bathrooms:</span>([^<]*)</div>', 1, KDONOTNOTIFYERROR);
// var all_features = "";
// var features_split = "";
// all_features = get_unique_regex_match(html, '<span
// class=\"label\">Amenities:</span>[\\s\\t\\n ]*?<p>([^<]*)', 1,
// KDONOTNOTIFYERROR);
// if (isDefined(all_features)) {
// features_split = all_features.split(",");
// }
// if (isDefined(features_split) && features_split.length !== 0) {
// features_split.forEach(function(feature) {
// if (isDefined(feature)) {
// if (mls.DetailedCharacteristics == undefined) {
// mls.DetailedCharacteristics = {};
// }
// if (feature === "Pool") {
// mls.DetailedCharacteristics.hasPool = true;
// } else if (feature === "AC") {
// mls.DetailedCharacteristics.hasAirCondition = true;
// } else {
// if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
// mls.DetailedCharacteristics.AdditionalInformation = {};
// }
// if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation
// == undefined) {
// mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
// }
// feature = feature.replace(/\s+/g, " ").trim();
// if (feature !== "") {
// var obj = {};
// obj.Description = feature;
// mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
// }
// }
// }
// if (feature.endsWith("View")) {
// if (mls.DetailedCharacteristics.ViewTypes == undefined) {
// mls.DetailedCharacteristics.ViewTypes = {};
// }
// if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
// mls.DetailedCharacteristics.ViewTypes.ViewType = [];
// }
// feature = feature.replace(/\s+/g, " ").trim();
// if (feature !== "") {
// var obj = {};
// obj.value = feature;
// mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
// }
// }
// });
// }
// mls.ListingDescription = get_next_regex_match(html, 0, '<div class="block"
// id="full-description"><h3>Description</h3><p>(.+?)</div>', 1,
// KDONOTNOTIFYERROR);
// if (mls.office == undefined)
// mls.office = {};
// mls.Brokerage.Name = get_next_regex_match(html, 0, 'Realtor:</span>[\\s\\t\\n
// ]*?<a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
// if (mls.Brokerage.Name == undefined) {
// mls.Brokerage.Name = [];
// }
// mls.Brokerage.Email = get_next_regex_match(html, 0, '<a
// href="mailto[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
// if (mls.Brokerage.Email == undefined) {
// mls.Brokerage.Email = [];
// }
// mls.office.PhoneNumber = get_next_regex_match(html, 0, 'Tel:</span>([^\<]*)',
// 1, KDONOTNOTIFYERROR);
// if (mls.office.PhoneNumber == undefined) {
// mls.office.PhoneNumber = [];
// }
// if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" ||
// mls.ListPrice.value == 1) {
// mls.ListPrice.value = KPRICEONDEMAND;
// }
// mls.MlsId = get_unique_regex_match(html, '<span class="label">Property
// Reference:</span>([^<]*)</div> ', 1, KDONOTNOTIFYERROR);
// mls.PropertyType.value = get_unique_regex_match(html, "Property
// Type:</span>([^<]*)</div>", 1, KDONOTNOTIFYERROR);
// mls.Location.Latitude = get_next_regex_match(html, 0,
// '"latitude":([^,]*),"longitude":([^,]*),"markername"', 1, KDONOTNOTIFYERROR);
// mls.Location.Longitude = get_next_regex_match(html, 0,
// '"latitude":([^,]*),"longitude":([^,]*),"markername"', 2, KDONOTNOTIFYERROR);
// mls.Address.Country.value = "Barbados";
// mls.Address.City.value = get_unique_regex_match(html, "<span
// class=\"label\">Location:</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
// var imageCount = 0;
// var images;
// images = get_all_regex_matched(html, '<meta property="og:image"
// content="([^"]*)" ', 1);
// images.forEach(function(oneImageTag) {
// var obj = JSON.parse(get_list_empty_variable("photo"));
// if (oneImageTag !==
// "https://www.barbadospropertysearch.com/sites/default/files/styles/medium/public")
// {
// if (/(http(s?)):\/\//gi.test(oneImageTag)) {
// obj.MediaURL = oneImageTag;
// } else {
// obj.MediaURL = oneImageTag;
// }
// obj.MediaOrderNumber = imageCount;
// if (mls.Photos.photo == undefined) {
// mls.Photos.photo = [];
// }
// mls.Photos.photo.push(obj);
// imageCount++;
// }
// });
// resulting_json(JSON.stringify(mls));
// return analyzeOnePublication_return_success;
// } else {
// return analyzeOnePublication_return_unreachable;
// }
// }

// function crawlForPublications() {
// if (next_crawl_needed((new Date().getTime() +
// KDELTATIMEWHILECALCULATION).toString(), true)) {
// var cumulatedCount = 0;
// var startTime = new Date().getTime();
// {
// try {
// // Appartment en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/California-City-CA-1611c/propertytype_CONDO",
// KAPTOVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// }
// // Casas
// {
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Alabama-NY-510c/propertytype_SFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Alabama-NY-510c/propertytype_MFD",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Alabama-NY-510c/propertytype_MFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/California-City-CA-1611c/propertytype_SFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/California-City-CA-1611c/propertytype_MFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Colorado-City-CO-2159c/propertytype_SFR,TOWNHOUSE,MFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Arizona-City-AZ-716c/propertytype_SFR,TOWNHOUSE,MFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Delaware-City-DE-2517c/propertytype_SFR,TOWNHOUSE,MFR",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Casas en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Florida-City-FL-3229c/propertytype_SFR,TOWNHOUSE",
// KCASASVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Multifamily en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Florida-City-FL-3229c/propertytype_MFR",
// KMULTIFAMILIAVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// }
// // Terrenos
// {
// try {
// // Terrenos en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/California-City-CA-1611c/propertytype_LAND",
// KTERRENOSVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Terrenos en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Colorado-City-CO-2159c/propertytype_LAND",
// KTERRENOSVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Terrenos en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Arizona-City-AZ-716c/propertytype_LAND,FARM",
// KTERRENOSVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// // Terrenos en venta
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Florida-City-FL-3229c/propertytype_LAND,FARM",
// KTERRENOSVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// }
// // Others/sales
// {
// try {
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/California-City-CA-1611c/propertytype_MFD",
// KOTROSVENTAS);
// } catch (e) {
// print("error: " + e);
// }

// try {
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Arizona-City-AZ-716c/propertytype_MFD",
// KOTROSVENTAS);
// } catch (e) {
// print("error: " + e);
// }
// try {
// cumulatedCount +=
// crawlCategory("https://www.era.com/for-sale-homes/Florida-City-FL-3229c/propertytype_MFD",
// KOTROSVENTAS);
// } catch (e) {
// print("error: " + e);
// }

// }
// print("crawlForPublications in " + getJavascriptFile() + " required " +
// formattedTime(new Date().getTime() - startTime) + " to gather " +
// cumulatedCount + " listing references.");
// next_crawl_needed(null, false);
// }
// }

// qa_override("[E4063587116]", "Some of the properties contains description and
// some does not");
function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(5000);
		do {
			el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href, '/property/') and @itemprop='streetAddress'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "//a[@class='js-ada-next btn-secondary btn-secondary--small btn-secondary--paginate paginate--single']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KDONOTNOTIFYERROR);
						wait(6000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	// addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	// if (isUndefined(browser)) {
	// browser = create_virtual_browser("HeadlessChrome");
	// if (isUndefined(browser)) {
	// return analyzeOnePublication_return_tech_issue;
	// }
	// }
	// virtual_browser_navigate(browser, url);
	// var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="je2-listing-page__content-box-text collapsed js-listing-description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"je2-listing-page__intro-price\">[\s\t\n ]*?([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"je2-listing-page__intro-price\">[\s\t\n ]*?([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Bedrooms = get_unique_regex_match(html, '<div class="je2-listing-page__intro-feature-data">([^\<]*)</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">[\s\t\n ]*?Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<div class="je2-listing-page__intro-feature-data">([^\<]*)</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">[\s\t\n ]*?Bathrooms', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-src="([^\"]*)" alt="[^\"]*" loading="lazy"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://img.jamesedition.com/listing_images" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"je2-listing-page__intro-title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, '<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#land" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">([^\<]*)', 1,
				KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(
				html,
				'<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#land" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">[^\<]*</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#house" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(
				html,
				'<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#house" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">[^\<]*</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_unique_regex_match(html, "<h2 class=\"property-address\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '"Listing city":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, '"Listing country":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Address.City.value)) {
			mls.Address.City.value = get_unique_regex_match(html, '"city":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.Address.Country.value)) {
			mls.Address.Country.value = get_unique_regex_match(html, '"country":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		}
		mls.MlsId = get_unique_regex_match(html, '"Listing id":([^\,]*),', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<div class="je2-contact-form__agent-name">([^\<]*)', 1, KDONOTNOTIFYERROR);
		// mls.Brokerage.Phone = get_next_regex_match(html, 0, "<a
		// href=\"tel:([^\"]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType = get_unique_regex_match(html, 'Property type</div>[\s\t\n ]*?<div class="je2-listing-page__building-property-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.era.com/for-sale-homes/Alabama-NY-510c/propertytype_SFR,TOWNHOUSE", KCASASVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}