qa_override("[E4063587116]", "Some of the properties contains description and some does not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		try {
			var loadedMore = false;
			var el1;
			var index = 1;
			var resArray = [];
			do {
				el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href, 'veranuncio.aspx?') and @class='btn btn-cart'])[" + index + "]", KDONOTNOTIFYERROR);
				if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
					// el1 = "https://clasificados.com.do/"+el1;
					resArray.push(virtual_browser_element_attribute(el1, "href"));
				}
				index++;
			} while (isDefined(el1));
			for ( var element in resArray) {
				if (!cachedSet.contains(resArray[element])) {
					cachedSet.add(resArray[element]);
					if (addUrl(resArray[element], category)) {
						actualCount++;
						print("" + actualCount + " - " + resArray[element]);
					} else {
						return actualCount;
					}
					if (passedMaxPublications()) {
						break;
					}
				}
			}
			if (passedMaxPublications()) {
				break;
			}
			index1 = 2;
			for (var count = 0; count <= 2; count++) {
				var verMasButtonElement = virtual_browser_find_one(browser, "(//a[starts-with(@href,'javascript:function')])[" + index1 + "]", KDONOTNOTIFYERROR);
				if (isDefined(verMasButtonElement)) {
					if (virtual_browser_click_element(browser, verMasButtonElement, KNOTIFYERROR)) {
						loadedMore = false;
						break;
					}
				}
				if (count != 2) {
					wait(1000);
				}
				index1++;
			}
			if (!loadedMore) {
				return actualCount;
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return actualCount;
			}
		}
	}
	return actualCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, 'DETALLES DEL ANUNCIO</div><h3>(.+?)</h3>', 1, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, "Rental Terms:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent.includes('Per Night')) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
			}
		} else if (for_rent) {
			for_rent = get_unique_regex_match(html, "Listing:</span>[ ]*?<span class=\"value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (for_rent.includes("Long-term Rental")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("month");
			}
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "Precio:</td><td>([a-zA-Z\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "Precio:</td><td>([a-zA-Z\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		if (mls.ListPrice.value < 500) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div u='image' style='text-align:center'><a href='([^\']*)'", 1);
		var tempImage = get_next_regex_match(html, 0, "<div u='image' style='text-align:center'><a href='([^\']*)'", 1, KDONOTNOTIFYERROR);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "https://clasificados.com.do/" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(tempImage) && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_innactive;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class='page-title'>([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "</i> Solar:</td><td>([0-9 ]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "</i> Solar:</td><td>([0-9 ]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, "<i class='fa fa-check-square-o'></i>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "A/C Unit") {
						mls.DetailedCharacteristics.hasAC = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.Bedrooms = get_unique_regex_match(html, "Habitaciones:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Ba?os:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, "Pais:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "Ciudad:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_unique_regex_match(html, "Sector:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<strong class='name'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "Celular:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType = get_unique_regex_match(html, "Categoria:</td><td>([^\<]*)", 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=101", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=102", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=103", KEDIFICIOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=104", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=105", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=107", KTIENDASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=108", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=109", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=110", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://clasificados.com.do/buscar.aspx?ca=112", KOTROSVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
