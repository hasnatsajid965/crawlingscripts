qa_override("[E1473228885]", "Some properties have lotSize and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var temp = 0;
	var check = [];
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^\"]*)"><img width="525" height="328"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="roundright"><a href=\'([^\']*)\'>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		check.push(relativeLink);
		if (isDefined(check[temp - 1])) {
			if (check[temp] == check[temp - 1]) {
				return cumulatedCount;
			}
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		temp++;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		//
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="entry-title">([^\&]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<span class="h3"><span class="price_label price_label_before"></span>([ a-zA-Z ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR)
				+ get_next_regex_match(html, 0, '<p class="price"> ([\$])[0-9\,\. ]*[a-zA-Z]*', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<span class="h3"><span class="price_label price_label_before"></span>([ a-zA-Z ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) || mls.ListingTitle == "" && isUndefined(mls.ListPrice.value)) {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "" || isUndefined(mls.ListPrice.currencyCode) || mls.ListPrice.currencyCode == "" || mls.ListPrice.value == 0 || mls.ListPrice.value == ".") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.LotSize.value = get_next_regex_match(html, 0, 'Property Lot Size:</strong>[^\,]*,([0-9\,\. ]*)([a-zA-Z]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, 'Property Lot Size:</strong>[^\,]*,([0-9\,\. ]*)([a-zA-Z]*)', 2, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_next_regex_match(html, 0, 'Year Built:</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.NumFloors = get_next_regex_match(html, 0, 'Floors:</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_next_regex_match(html, 0, 'Bedrooms:</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, 'Bathrooms:</strong>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="wpestate_estate_property_details_section  ">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<i class="fa fa-check"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "air conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("view")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, 'My details</div><h3><a href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.office.PhoneNumber = get_next_regex_match(html, 0, '<div class="agent_detail agent_phone_class"><i class="fa fa-phone"></i><a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, '<div class="agent_detail agent_email_class"><i class="fa fa-envelope-o"></i><a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, "\"general_latitude\":\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "\"general_longitude\":\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "<h1 class=\"entry-title\">[^\&]*&#8211;([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "Location: </span><a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.StateOrProvince.value = get_next_regex_match(html, 0, "Department: </span><a href=\"[^\"]*\" rel=\"tag\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Uruguay";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'src="([^\"]*)" data-slider-no="[^\"]*" alt="[^\"]*" class="img-responsive lightbox_trigger"', 1);
		if (isDefined(images) && images !== "")
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();

		// Apartment
		{
			try {
				// Apartment en venta
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/apartments/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}

		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/houses-on-the-coast/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/country-houses/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/chacras/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/building-land/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/rural-land/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://www.van-bevern.com/en/our-properties/ranches/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
