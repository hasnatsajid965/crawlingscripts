qa_override("[E1473228885]", "Some of the properties contain lot size and some not.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = virtual_browser_html(browser);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a href=\"([^\"]*)\" data-href=\"([^\"]*)\" class=\"floatbox orange\"", category, "https://www.drlistings.com", 1, KDONOTNOTIFYERROR, true)) > 0) {
				tracer++;
				actualCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		cumulatedCount += actualCount;
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var array = virtual_browser_find_all(browser, "//form[@name=\"search2\"]/input[@value=\"Next\"]", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(1000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			return cumulatedCount;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		print("checking" + browser);
		if (isUndefined(browser) || browser == null || browser == "null") {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	// var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	// print("html..."+html);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.DetailedCharacteristics == undefined) {
			mls.DetailedCharacteristics = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		forRent = get_next_regex_match(html, 0, "<div class=\"ld location\" style=\"color:#090;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Rental")) {
				forUnit = get_next_regex_match(html, 0, "<div class=\"rd\" style=\"display:inline;color:#009;align:right; margin-right:10px;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
				if (isDefined(forUnit)) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("day");
				} else {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("year");
				}
			}
		} else {
			element = virtual_browser_find_one(browser, "(//span[@itemprop='title'])[2]", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				categoryCheck = virtual_browser_element_text(element);
				if (categoryCheck.includes("villas") || categoryCheck.includes("Villas")) {
					mls.NonMLSListingData.category = getCategory(KCASASVENTAS);
				}
				if (categoryCheck.includes("condos") || categoryCheck.includes("Condos")) {
					mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
				}
				if (categoryCheck.includes("lots") || categoryCheck.includes("Lots")) {
					mls.NonMLSListingData.category = getCategory(KTERRENOSVENTAS);
				}
				if (categoryCheck.includes("Business") || categoryCheck.includes("business")) {
					mls.NonMLSListingData.category = getCategory(KEDIFICIOSVENTAS);
				}
			}
		}
		var dVideo = get_unique_regex_match(html, "\"https://my.matterport.com/show/([^\"]*?)\"", 1, KDONOTNOTIFYERROR);
		if ((dVideo != undefined) && (dVideo != "undefined")) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = "https://my.matterport.com/show/" + dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"rd\">[ \\t\\r\\n]*([A-Z$]*)[ ]?([0-9,]*)([^<]*)", 2, KDONOTNOTIFYERROR);

		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"rd\">[ \\t\\r\\n]*([A-Z$]*)[ ]?([0-9,]*)([^<]*)", 3, KDONOTNOTIFYERROR);
		// for_rent = get_unique_regex_match(html, "<div class=\"rd\"
		// style=\"display:inline;color:#009;align:right;
		// margin-right:10px;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		// if(isDefined(for_rent)){
		// mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
		// }
		if (isUndefined(mls.ListPrice.currencyCode)) {
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"rd\">[ \\t\\r\\n]*([A-Z$]*)[ ]?([0-9,]*)([^<]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size:([0-9\.\, ]*)([a-zA-Z0-9]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size:([0-9\.\, ]*)([a-zA-Z0-9]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Build Size:([0-9\.\, ]*)([a-zA-Z0-9]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "Build Size:([0-9\.\, ]*)([a-zA-Z0-9]*)", 2, KDONOTNOTIFYERROR);
		}
		// mls.LotSize.areaUnits = get_unique_regex_match(html, "<td>Lot Size:([
		// ]*?)([0-9,\\.]*)[ ]?([^0-9][^< ]*?)</td>", 3, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<td>Build Size:([ ]*?)([0-9,\\.]*)[ ]?([^0-9][^< ]*?)</td>", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<td>Build Size:([ ]*?)([0-9,\\.]*)[ ]?([^0-9][^< ]*?)</td>", 3, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<td>Bedrooms: ([0-9]*?)</td>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<td>Bathrooms: ([0-9]*?)</td>", 1, KDONOTNOTIFYERROR);
		mls.DetailedCharacteristics.NumFloors = get_unique_regex_match(html, "<td>Levels: ([0-9]*?)</td>", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "DRListings.com";
		mls.Brokerage.Phone = "809-895-6413/727-375-4781/809-901-3959/809-779-9949";
		var code = get_unique_regex_match(html, ">DRL# ([0-9]{3,6})", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			mls.MlsId = "DRL# " + code;
		}
		if (mls.NonMLSListingData.otherListingBrokerInfo == undefined) {
			mls.NonMLSListingData.otherListingBrokerInfo = [];
		}
		var obj = {};
		obj.Name = get_next_regex_match(html, 0, "<span itemprop=\"name\">([^<]*)</", 1, KDONOTNOTIFYERROR);
		if (isDefined(obj.Name)) {
			mls.NonMLSListingData.otherListingBrokerInfo.push(obj);
		}
		mls.ListingDescription = get_next_regex_match(html, 0, " <div id=\"feature-description\" class=\"feature-tabs-2-content\"><span itemprop=\"description\">(.+?)</span>", 1, KDONOTNOTIFYERROR);
		// }
		if (isUndefined(mls.ListingDescription) && isUndefined(mls.ListPrice.value) || mls.ListingDescription == "") {
			return analyzeOnePublication_return_unreachable;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.Country.value = "Dominican Republic";
		mls.Address.City.value = get_unique_regex_match(html, "<div class=\"ld location\">([^<,]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, "<td>([^:]*): YES</td>", 1);
		if (isDefined(features) && features !== "") {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
						mls.DetailedCharacteristics.AdditionalInformation = {};
					}
					if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
					}
					if (feature === "Gated Community") {
						mls.DetailedCharacteristics.HasGatedEntry = true;
					} else if (feature === "Pool") {
						mls.DetailedCharacteristics.HasPool = true;
					} else {
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
					var obj = {};
					obj.Description = feature;
					mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.viewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.viewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.viewType.push(obj);
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a data-slide-index=\"[^\"]*\" href=\"#\"><img src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			oneImageTag = oneImageTag.replace("thb", "std");
			obj.MediaURL = "https://www.drlistings.com" + oneImageTag;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function housesForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-sale-4\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-1\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function condosForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-sale-4\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-2\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}
function landForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-sale-4\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-3\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function businessForSale(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-sale-4\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-4\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function housesForRentLongTerm(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-rent-1\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-1\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function condosForRentLongTerm(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-rent-1\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-2\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function housesForRentTemporary(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-rent-2\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-1\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function condosForRentTemporary(browser, category) {
	var cumulatedCount = 0;
	var element = virtual_browser_find_one(browser, "//input[@value=\"RESET\"]", KNOTIFYERROR);
	if (isDefined(element)) {
		virtual_browser_click_element(browser, element, KNOTIFYERROR);
		wait(100);
		element = virtual_browser_find_one(browser, "//label[@for=\"for-rent-2\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		element = virtual_browser_find_one(browser, "//label[@id=\"type-of-property-2\"]", KNOTIFYERROR);
		if (isDefined(element)) {
			virtual_browser_click_element(browser, element, KNOTIFYERROR);
			wait(100);
		}
		cumulatedCount += crawlCategory(browser, category);
	}
	return cumulatedCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		virtual_browser_navigate(browser, "https://www.drlistings.com/");
		cumulatedCount += housesForSale(browser, KCASASVENTAS);
		cumulatedCount += housesForRentLongTerm(browser, KCASASALQUILER);
		cumulatedCount += housesForRentTemporary(browser, KCASASTEMPORAL);
		cumulatedCount += condosForSale(browser, KAPTOVENTAS);
		cumulatedCount += condosForRentLongTerm(browser, KAPTOALQUILER);
		cumulatedCount += condosForRentTemporary(browser, KAPTOTEMPORAL);
		cumulatedCount += landForSale(browser, KTERRENOSVENTAS);
		cumulatedCount += businessForSale(browser, KOFICINASVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
