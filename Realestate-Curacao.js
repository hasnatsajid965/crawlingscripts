qa_override("[E2592408747]", "Some properties has city and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, "<h4 class=\"overview_title\"><a href='([^']*)'", category, "https://www.realestate-curacao.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, 'href="([^"]*)">&raquo;', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.realestate-curacao.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var list_not_available = get_unique_regex_match(html, '<div class=\"col-lg-12 block\">[\s\t\n ]*?<h1>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "Not found") {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h2 itemprop="name">([^<]*)', 1, KNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<td>Living space:</td>[\\s\\t\\n ]*?<td >([0-9\,\.]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<td>Living space:</td>[\\s\\t\\n ]*?<td >([0-9\,\.]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<td>Lot size:</td>[\s\t\n ]*?<td >([0-9\,\.]*)([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<td>Lot size:</td>[\s\t\n ]*?<td >([0-9\,\.]*)([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<td>Bedrooms:</td>[\\s\\t\\n ]*?<td itemprop='numberOfRooms'>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<td>Bathrooms:</td>[\\s\\t\\n ]*?<td >([^<]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<td>([^\:]*):</td>[\\s\\t\\n ]*?<td >Yes</td>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Pets allowed") {
						mls.DetailedCharacteristics.hasPetsAllowed = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<p class="lead">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '</table>[\\s\\t\\n ]*?</div>(.+?)</ul>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<p><p>(.+?)</small>[\s\t\n ]*?<br><br>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '</table>[\s\t\n ]*?</div>[\s\t\n ]*?<p>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<strong itemprop="employee">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "class='telephone_number' itemprop=\"telephone\">([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_unique_regex_match(html, "class='hidden-md' itemprop=\"email\">([^<]*)", 1, KDONOTNOTIFYERROR);
		var list_price = "";
		mls.ListPrice.value = get_unique_regex_match(html, '<p itemprop="price" class="lead pull-right ">([^\;]*);([ 0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<p itemprop="price" class="lead pull-right ">([^\;]*);([ 0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		list_price = get_unique_regex_match(html, '<p itemprop="price" class="lead pull-right ">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			if (list_price.includes("request")) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
			if (list_price.includes("/")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		} else {
			list_price = get_unique_regex_match(html, '<p itemprop="price" class="lead pull-right soldprice">([^\<]*)', 1, KDONOTNOTIFYERROR);
			if (isDefined(list_price)) {
				if (list_price.includes("request")) {
					mls.ListPrice.value = KPRICEONDEMAND;
				}
				if (list_price.includes("/")) {
					freq = "month";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
			}
		}
		if (mls.ListPrice.value < 10) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.YearBuilt = get_unique_regex_match(html, "<td>Year built:</td>[\\s\\t\\n ]*?<td >([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Curacao";
		mls.Address.City.value = get_unique_regex_match(html, "<a href='[^\']*' class='anchorlink'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "maps.LatLng[(]([^,]*),([^)]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "maps.LatLng[(]([^,]*),([^)]*)", 2, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "href='([^\']*)' rel='objectimages'", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "http:" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.realestate-curacao.com/en/homes/homes-for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.realestate-curacao.com/en/homes/homes-for-rent/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.realestate-curacao.com/en/commercial/commercial-properties-for-sale/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.realestate-curacao.com/en/commercial/commercial-lots/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://www.realestate-curacao.com/en/commercial/commercial-properties-for-rent/", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.realestate-curacao.com/en/lots/lots/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
