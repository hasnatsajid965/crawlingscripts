qa_override("[E4230575792]", "Some properties contain images and some not.");
qa_override("[E2592408747]", "This website does not contain city");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<p class=\"anuncio-title bold\" style=\"[^\"]*\"><a href=\"([^\"]*)\"", category, "http://plazavirtual.do", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a class=\"current-page\"[ ]*?>[^\<]*</a></li><li><a href=\"([^\"]*)\"[ ]*?>", 1, KDONOTNOTIFYERROR);
		// relativeLink =
		// relativeLink.replace('pag='+page_number,'pag='+page_number+1);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://plazavirtual.do" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="description-text">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div class=\"loading-gif\"></div><img src=\"([^\"]*)\"", 1);
		if (isUndefined(images) || images == "" && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = "http://plazavirtual.do" + oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		mls.ListPrice.value = get_unique_regex_match(html, "<div class=\"price bold\">([^\<]*)", 1, KNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value == "Precio Privado") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		var for_rent = get_unique_regex_match(html, "Tipo Anuncio: </span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent === "Aquilar") {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<div class=\"moneda-tag\">([^\<]*)</div><div class=\"price bold\">", 1, KNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<h3 id=\"anuncio-title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<span class=\"data-title\"> Terreno ([a-zA-Z0-9]*): </span>([^\<]*)", 2, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<span class=\"data-title\"> Terreno ([a-zA-Z0-9]*): </span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Habitaciones: </span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Ba?os: </span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "li id=\"user-nick\">[\\s\\t\\n ]*?<a href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, "<li class=\"user-basic-data\">Celular:([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = "info@onehome.com.do";
		mls.Address.Country.value = "Dominican Republic";
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=9&subcat=9&habs=&banos=&sqmeters=&tsqmeters=&action=Vender&cond=Nuevo&p=", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en Rental
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=18&subcat=9&sqmeters=&tsqmeters=&action=Alquilar&cond=Nuevo&p=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=9&subcat=10&habs=&banos=&sqmeters=&tsqmeters=&action=Vender&cond=Nuevo&p=", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Villas en Rent
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=9&subcat=10&habs=&banos=&sqmeters=&tsqmeters=&action=Alquilar&cond=Nuevo&p=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=10&subcat=11&habs=&banos=&sqmeters=&tsqmeters=&action=Vender&cond=Nuevo&p=", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en rent
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=12&subcat=13&tsqmeters=&action=Alquilar&cond=Nuevo&p=", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=12&subcat=13&tsqmeters=&action=Vender&cond=Nuevo&p=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=13&subcat=14&habs=&banos=&sqmeters=&tsqmeters=&action=Vender&cond=Nuevo&p=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=14&subcat=15&sqmeters=&tsqmeters=&action=Vender&cond=Nuevo&p=", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("http://plazavirtual.do/anuncios.php?cat=3&cat_name=Bienes+Raices&subcat=13&subcat=15&habs=&banos=&sqmeters=&tsqmeters=&action=Alquilar&cond=Nuevo&p=", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
