var countryContext = "Colombia";
include("base/RemaxFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3100&pt=3100&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3110&pt=3110&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3104&pt=3104&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KOTROSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3119&pt=3119&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3101&pt=3101&cur=COP&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KOTROSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3103&pt=3103&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=261&cr=2&mpts=3115&pt=3115&cur=COP&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KEDIFICIOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=260&cr=2&mpts=3100&pt=3100&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KAPTOALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=260&cr=2&mpts=3110&pt=3110&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KCASASALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=260&cr=2&mpts=3104&pt=3104&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KOTROSALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=260&cr=2&mpts=3119&pt=3119&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KCASASALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=4F4A5E9E319F48CAAE82AFB972E415F1#mode=gallery&tt=260&cr=2&mpts=3103&pt=3103&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KTERRENOSALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax.co/PublicListingList.aspx?SearchKey=61C3DBE976E34DC1B91160E5D60F7A81#mode=gallery&tt=260&cr=2&mpts=3115&pt=3115&cur=COP&la=All&sb=PriceIncreasing&page=1&sc=66&rl=1030&lsgeo=1030,0,0,0&sid=3da9d438-feeb-4802-bb20-45b87eac8aec", KEDIFICIOSALQUILER);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
