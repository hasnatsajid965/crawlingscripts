qa_override("[E1473228885]", "There is no lot size data on this website.");

function crawlCategory(url, category, stopword) {
	const ciudad = [ "parish", "Clarendon", "Hanover", "Kingston%20-and-%20St-dot-%20Andrew", "Manchester", "Portland", "St-dot-%20Ann", "St-dot-%20Catherine", "St-dot-%20Elizabeth", "St-dot-%20James", "St-dot-%20Mary", "St-dot-%20Thomas", "Trelawny", "Westmoreland" ];
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	ciudad.forEach(function(city) {
		var actualCount = 0;
		var page = 1;
		var tracer = 0;
		var html = gatherContent_url(url.replace("$+CIUDAD+$", city), KDONOTNOTIFYERROR);
		while (isDefined(html)) {
			try {
				while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="thumbnail recent-properties-box result_item search-result " data-listing="[^"]*" data-mls="[^"]*"  >[s\t\n ]*?<a href="([^"]*)" >', category, "https://century21jm.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
					tracer++;
					actualCount++;
					cumulatedCount++;
					if (passedMaxPublications()) {
						break;
					}
				}
			} catch (e) {
				if (e !== "Duplicate detected.") {
					throw "error: " + e;
				}
			}
			print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + ", city " + city + " (" + cumulatedCount + ") ----");
			if (passedMaxPublications()) {
				break;
			}
			var relativeLink = get_next_regex_match(html, 0, '<li class="paginate_button next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
			if (isUndefined(relativeLink)) {
				break;
			}
			html = gatherContent_url("https://century21jm.com" + relativeLink, KDONOTNOTIFYERROR);
			tracer = 0;
			actualCount = 0;
			page++;
		}
	});
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setCountryPhoneCode("599");
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="col-lg-10 col-lg-offset-1 nopadding">[\\s\\t\\n ]*?<h4 class="theme-black-color">([^\<]*)', 1, KNOTIFYERROR);
		var features = get_all_regex_matched(html, "<ul class=\"amenities\">[\\s\\t\\n ]*?<li>(.+?)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature !== "") {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);

					} else {
					}
				}
				if (feature.startsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class=\"properties-description padding-bottom-25 \">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Century 21";
		mls.Brokerage.Phone = "876 969 2100 / 876 924 1807 / 876 924 4925";
		mls.Brokerage.Email = "c21jam@century21jm.com";
		mls.MlsId = get_unique_regex_match(html, "id=\"mls_id_hidden\" value=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'name=\"latitude[^\"]*\" value=\"([^\"]*)\"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'name=\"longitude[^\"]*\" value=\"([^\"]*)\"', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, 'Mortgage Calculator</h3>[\\s\\t\\n ]*?<h4 class="theme-black-color" >[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, 'Mortgage Calculator</h3>[\\s\\t\\n ]*?<h4 class="theme-black-color" >[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_unique_regex_match(html, 'xs-nopadding">[\\s\\t\\n ]*?<h4 class="theme-black-color" >[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_unique_regex_match(html, 'xs-nopadding">[\\s\\t\\n ]*?<h4 class="theme-black-color" >[\\s\\t\\n ]*?([a-zA-Z\$ ]+)([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_unique_regex_match(html, '<span  class="p-currency">[\s\t\n ]*?<span >[\s\t\n ]*?([a-zA-Z\$ ]+)([0-9\.\, ]*)', 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span  class="p-currency">[\s\t\n ]*?<span >[\s\t\n ]*?([a-zA-Z\$ ]+)([0-9\.\, ]*)', 1, KDONOTNOTIFYERROR);
		}
		for_rent = get_unique_regex_match(html, '<span class="r-s-element">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if ((isDefined(for_rent)) && (for_rent === "For Rent")) {
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<span class=\"theme-black-color font-size-14\">[\\s\\t\\n ]*?<b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (mls.PropertyType.value.includes("Hotel") && for_rent === "For Sale") {
			mls.NonMLSListingData.category = getCategory(KEDIFICIOSVENTAS);
		}
		mls.Bedrooms = get_unique_regex_match(html, "<span class=\"font-size-10 margin-right-5 theme-black-color\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<span class=\"font-size-10 margin-right-5\" >([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, '<div class="col-lg-10 col-lg-offset-1 nopadding">[\\s\\t\\n ]*?<h4 class="theme-black-color">([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_next_regex_match(html, 0, 'Exterior[\\s\\t\\n ]*?</h4>[\\s\\t\\n ]*?<div class="row">[\\s\\t\\n ]*?<div class="col-xs-12"  >[\\s\\t\\n ]*?<ul class="amenities">[\\s\\t\\n ]*?<li>[\\s\\t\\n ]*?Lot Size:([ 0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (total_commas == "1") {
				if (tokens.length > 1) {
					mls.Address.City.value = tokens[tokens.length - 1].trim();
				}
			}
		}
		mls.Address.Country.value = "Jamaica";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img src=\"([^\"]*)\" alt=\"\" class=\"slider-image\">', 1);
		if (isDefined(images))
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				if (/(http(s?)):\/\//gi.test(oneImageTag)) {
					obj.MediaURL = oneImageTag;
				} else {
					obj.MediaURL = "https://century21jm.com" + oneImageTag;
				}
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Apartment/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rental
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/Apartment/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rental
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/Resort%20Apartment-s-Villa/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/House/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Resort%20Apartment-s-Villa/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Townhouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/House/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/Townhouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Commercial%20Bldg-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Commercial%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Commercial%20Spaces-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/Commercial%20Bldg-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/Commercial%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Development%20Land%20%28Commercial%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Development%20Land%20%28Residential%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Residential%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/$+CIUDAD+$/town/town/property-type/Development%20Land%20%28Residential%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Factory/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Farm-s-Agriculture/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://century21jm.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/$+CIUDAD+$/town/town/property-type/Hotel/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/9/range/H", KOTROSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
