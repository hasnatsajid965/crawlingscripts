qa_override("[E2592408747]", "There is no city data on this site");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<h4 class="property-title" ><a href="([^"]*)"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "<a href='([^']*)' class='btn'>&gt;</a>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "es-do";
		}
		mls.Brokerage.Name = get_unique_regex_match(html, '<h3><a href="[^"]*" title="[^"]*" rel="author">([^"]*)</a></h3>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = "+1-268-728-3518 / +1-268-717-1437";
		mls.Brokerage.Email = "info@absolutepropertiesantigua.com";
		mls.ListingTitle = get_next_regex_match(html, 0, '<h3 class="property-single-title">([^<]*)</h3>', 1, KNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '</div></div><p>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<td class="summary_left" valign="top">(.+?)</td>', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<div id="keyfeaturescontainer">(.+?)<!--OPENING FEATURED BLOCK -->', 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0,
					'<img src="https://www.absolutepropertiesantigua.com/wp-content/plugins/custom-share-buttons-with-floating-sidebar/images/reddit.png" alt="Share On Reddit"></a></div></div></div><p style="text-align: left;">(.+?)</div>', 1,
					KDONOTNOTIFYERROR);
		}
		mls.Address.Country.value = "Antigua";
		mls.ListPrice.value = get_next_regex_match(html, 0, '<h5 class="price">([\$]*)([0-9\,\s ]*?)</h5>', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<h5 class="price">([\$]*)([0-9\,\s ]*?)</h5>', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value === "0") {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		rent = get_next_regex_match(html, 0, 'class="property-type" href="[^\"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(rent)) {
			if (rent.includes("Long")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
			if (rent.includes("Short") || rent.includes("Rental")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("week");
			}
		}
		mls.LotSize.value = get_unique_regex_match(html, '<li class="home-size">([0-9,]*)[s\t ]*?([^<]*)</li>', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.LotSize.value)) {
			mls.LotSize.areaUnits = get_unique_regex_match(html, '<li class="home-size">([0-9,]*)[s\t ]*?([^<]*)</li>', 2, KDONOTNOTIFYERROR);
		}
		mls.Bedrooms = get_unique_regex_match(html, '<li class="home-beds">([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<li class="home-baths">([0-9.]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li>[\\s\\t\\n ]*?<a href="[^"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "A/C Unit") {
						mls.DetailedCharacteristics.hasAC = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img width="[^"]*?" height="[^"]*?" src="([^"]*)" class="property-gallery" alt="[^"]*?" />', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("-406x300.jpg", ".jpg");
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.absolutepropertiesantigua.com/property-type/villa/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://www.absolutepropertiesantigua.com/property-type/Short-Term-Rental/", KCASASTEMPORAL);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://www.absolutepropertiesantigua.com/property-type/Long-Term-Rental/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.absolutepropertiesantigua.com/property-type/land-sales/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("http://www.absolutepropertiesantigua.com/property-type/developments/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("http://www.absolutepropertiesantigua.com/property-type/commercial/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
