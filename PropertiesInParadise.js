

// crawlForPublications crawl-mode: Regular-Expression Crawling

function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<h3>[\\s\\t\\n ]*?<a href="([^"]*)">', category, "http://propertiesinparadise.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<div class="page-numbers topPN">[\\s\\t\\n ]*?<ul>[\\s\\t\\n ]*?<li>[^<]*</li>&nbsp;<a href="([^"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("http://propertiesinparadise.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h3 class="fontOv">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="propertyBody row">[\\s\\t\\n ]*?<div class="col-xs-12">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<strong>Plot Size:</strong>[\\s\\t\\n ]*([0-9\.\, ]*)of an ([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<strong>Plot Size:</strong>[\\s\\t\\n ]*([0-9\.\, ]*)of an ([a-zA-Z]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Bedrooms:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Baths:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "<strong>Year Built:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (mls.ListingTitle == "" || isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription) || mls.ListingDescription == "") {
			return analyzeOnePublication_return_unreachable;
		}
		var features = get_all_regex_matched(html, '<strong>([^\:]*):</strong>[ ]*?Yes', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Phone = "+1 264.235.2816";
		mls.Brokerage.Email = "elaine@propertiesinparadise.com";
		mls.ListPrice.value = get_unique_regex_match(html, "Price:</strong>([ \$]*)[\s\t\n ]*([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "Price:</strong>([ \$]*)[\s\t\n ]*([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_next_regex_match(html, 0, "maps.LatLng[(]([^,]*),([^)]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "maps.LatLng[(]([^,]*),([^)]*)", 2, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "<strong>Closest Beach:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Anguilla";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'src="([^"]*)" width="950" height="586"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "http://propertiesinparadise.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://propertiesinparadise.com/forsale", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("http://propertiesinparadise.com/rentals", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
