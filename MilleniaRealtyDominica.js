qa_override("[E1473228885]", "Some properties has lot size and some not.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^"]*)" class="properties__address ">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a href="([^"]*)">[\\s\\t\\n ]*?<span aria-hidden="true" class="glyphicon glyphicon-chevron-right">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="property__title">([^<]*)', 1, KNOTIFYERROR);
		if (isDefined(mls.ListingTitle)) {
			if (mls.ListingTitle.includes("Rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
			for_rent = get_next_regex_match(html, 0, "<div class=\"property__ribon\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (for_rent.includes("Rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.LotSize.value = get_next_regex_match(html, 0, "<li>Land Size:[ ]*?<strong>([0-9\,\. ]*)([a-zA-Z\.\, ]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, "<li>Land Size:[ ]*?<strong>([0-9\,\. ]*)([a-zA-Z\.\, ]*)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms</dd>[\\s\\t\\n ]*?<dd class="property__plan-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms</dd>[\\s\\t\\n ]*?<dd class="property__plan-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<ul class="property__params-list property__params-list--options">[\\s\\t\\n ]*?<li>(.+?)</ul>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="property__description-wrap1">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<h3 class="worker__name fn">[\\s\\t\\n ]*?<a href="[^"]*">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html, '<span class="header__span">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<div class="property__price">[\s\t\n ]*?<strong>[\s\t\n ]*?([\$])([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<div class="property__price">[\s\t\n ]*?<strong>[\s\t\n ]*?([\$])([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Location.Latitude = get_unique_regex_match(html, '"lat":"([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '"lng":"([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, '<div class="property__info-item">Property type:<strong>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominica";
		mls.Location.Directions = get_next_regex_match(html, 0, '<span class="property__city">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			mls.Location.Directions = mls.Location.Directions.replace(/\s+/g, " ").trim();
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			var i = 1;
			if (total_commas >= 1) {
				tokens.forEach(function(values) {
					if (i == 1 && isDefined(values)) {
						mls.Address.City.value = values;
					}
					i++;
				});
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-trigger class="slider__img">[\\s\\t\\n ]*?<img data-lazy="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.kellerwilliamsjamaica.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.milleniarealtydominica.com/property_contract/house/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en rent
				cumulatedCount += crawlCategory("http://www.milleniarealtydominica.com/for-rent/", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("http://www.milleniarealtydominica.com/property_contract/commercial-property/", KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("http://www.milleniarealtydominica.com/property_contract/commercial-rental/", KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.milleniarealtydominica.com/property_contract/land/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
