var countryContext = "Chile";
include("base/LatinCaribFunctions.js")

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	{
	    try {
		// Uncategorized
		cumulatedCount += crawlCategory("https://www.latincarib.com/chile/", KUNCATEGORIZED);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
