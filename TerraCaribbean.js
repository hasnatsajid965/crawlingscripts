

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="listinglink" href="([^"]*)"', category, "https://www.terracaribbean.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a id="pagenext-[^"]*" href="([^"]*)" ', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.terracaribbean.com/St-Lucia/Search/" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	if (html === null || html === "") {
		return analyzeOnePublication_return_innactive;
	}
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="name">([^<]*)', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, '<div class="essline"><h4>Land Area<span>&ndash;</span></h4><span class="esstext">([0-9\,\. ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<div class="essline"><h4>Land Area<span>&ndash;</span></h4><span class="esstext">([0-9\,\. ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, '<h4>Land [(]NET[])]<span>&ndash;</span></h4><span class="esstext">([0-9\,\. ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, '<h4>Land [(]NET[])]<span>&ndash;</span></h4><span class="esstext">([0-9\,\. ]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		}
		var rooms = "";
		rooms = get_unique_regex_match(html, '<div class="essline"><h4>Rooms<span>&ndash;</span></h4><span class="esstext">([^<]*)', 1, KDONOTNOTIFYERROR);
		rooms = rooms.split("/");
		rooms.forEach(function(rooms_data) {
			if (rooms_data.endsWith("Bed")) {
				mls.Bedrooms = rooms_data;
			}
			if (rooms_data.endsWith("Bath")) {
				mls.Bathrooms = rooms_data;
			}
		});
		mls.LivingArea.value = get_unique_regex_match(html, '<div class="essline"><h4>Floor Area<span>&ndash;</span></h4><span class="esstext">([0-9\,\. ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, '<div class="essline"><h4>Floor Area<span>&ndash;</span></h4><span class="esstext">([0-9\,\. ]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '<div class="essline"><h4>Ref #<span>&ndash;</span></h4><span class="esstext">([^<]*)', 1, KDONOTNOTIFYERROR);
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<div id="description-features">[\\s\\t\\n ]*?<ul><li>(.+?)</ul>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div id="description-details">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = "Terra Caribbean";
		mls.Brokerage.Phone = "1 (758) 452-0450 / 1 (758) 452-0071";
		list_price = get_next_regex_match(html, 0, '<span class="price-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, '<span class="price-value">&nbsp;([a-zA-Z]*)&#36;([0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<span class="price-value">&nbsp;([a-zA-Z]*)&#36;([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			if (list_price.includes("Request")) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		if (mls.ListPrice.value < 50000) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		if (isDefined(list_price)) {
			if (list_price.includes('/')) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, '<div class="essline"><h4>Type<span>&ndash;</span></h4><span class="esstext">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, '"latitude": "([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, '"longitude": "([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Videos = get_unique_regex_match(html, '<iframe id="propertyvideo" width="[^"]*" height="[^"]*" src="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, '<h1 class="name">[^<]*</h1><span> -([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			var i = 1;
			if (total_commas >= 1) {
				tokens.forEach(function(values) {
					if (i == 1 && isDefined(values)) {
						mls.Address.City.value = values;
					}
					if (i == 2 && isDefined(values)) {
						mls.Address.Country.value = values;
					}
					i++;
				});
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'class="fullphoto" src="([^"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.terracaribbean.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// multifamily en venta
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=residential&type%5B%5D=apartment+building&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=residential&type%5B%5D=house%2C+house+%26+cottage&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KNUEVASCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=residential&type%5B%5D=townhouse%2Capartment&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=rental&zone=residential&type%5B%5D=house%2Chouse+%26+cottage&currencySearch=USD&minprice=0&maxprice=500000&minbeds=-1&minfloor=-1&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=rental&zone=residential&type%5B%5D=townhouse%2Capartment&currencySearch=USD&minprice=0&maxprice=500000&minbeds=-1&minfloor=-1&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=residential&type%5B%5D=land&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			// stores
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=commercial&type%5B%5D=land&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=commercial&type%5B%5D=apartment+building&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=commercial&type%5B%5D=office+building&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=commercial&type%5B%5D=retail+building&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=commercial&type%5B%5D=warehouse&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=sale&zone=commercial&type%5B%5D=industrial&currencySearch=USD&minprice=0&maxprice=100000000&minbeds=-1&minfloor=-1&minland=-1&maxland=8712000&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=rental&zone=commercial&type%5B%5D=office&currencySearch=USD&minprice=0&maxprice=500000&minbeds=-1&minfloor=-1&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=rental&zone=commercial&type%5B%5D=warehouse&currencySearch=USD&minprice=0&maxprice=500000&minbeds=-1&minfloor=-1&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=rental&zone=commercial&type%5B%5D=retail&currencySearch=USD&minprice=0&maxprice=500000&minbeds=-1&minfloor=-1&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory(
						"https://www.terracaribbean.com/St-Lucia/Search/?country=St.+Lucia&market=rental&zone=commercial&type%5B%5D=industrial&currencySearch=USD&minprice=0&maxprice=500000&minbeds=-1&minfloor=-1&furnishings=Any&btnSubmit=+&currencyMarker=+%24USD",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
