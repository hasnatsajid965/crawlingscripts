var countryContext = "Curacao";
include("base/SotheBysRealtyFunctions.js");

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	{
	    try {
		// multifamily en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/apartment-type", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// multifamily en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/bed-and-breakfast-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// multifamily en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/condominium-type", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// multifamily en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/fractional-ownership-type", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// multifamily en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/multi-family-home-type", KMULTIFAMILIAVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// multifamily en rental
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/rentals/cuw/45-pp/condominium-type", KAPTOALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// multifamily en rental
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/rentals/cuw/45-pp/apartment-type", KAPTOALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/co-op-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/duplex-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/hacienda-estancia-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// temporal en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/farm-ranch-plantation-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/other-residential-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/single-family-home-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/townhouse-type", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en alquiler
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/rentals/cuw/45-pp/duplex-type", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en alquiler
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/rentals/cuw/45-pp/farm-ranch-plantation-type", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en alquiler
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/rentals/cuw/45-pp/single-family-home-type", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/land-type", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/sales/cuw/45-pp/land-type", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en alquiler
		cumulatedCount += crawlCategory("https://www.sothebysrealty.com/eng/rentals/cuw/45-pp/land-type", KTERRENOSALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
