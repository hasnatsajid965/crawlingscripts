var browser;
const maxretriestogetlisthtml = 5;


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	const regex = "<h2 class=\"li-title\">[\\s\\t\\n ]*?<a href=\"([^\"]*)";
	// var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var html = virtual_browser_html(browser);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, regex, category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		element = virtual_browser_find_one(browser, "(//a[@title='Next'])[1]", KDONOTNOTIFYERROR);
		if ((isDefined(element)) && (virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR))) {
			var curretries = 0;
			html = virtual_browser_html(browser);
			while ((!isDefined(get_next_regex_match(html, 0, regex, 0, KDONOTNOTIFYERROR))) && (curretries < maxretriestogetlisthtml)) {
				print("Making sure the html has list content, retry #" + curretries);
				wait(1000);
				html = virtual_browser_html(browser);
				curretries++;
			}
			if (!isDefined(get_next_regex_match(html, 0, regex, 0, KDONOTNOTIFYERROR))) {
				html = undefined;
			}
		} else {
			break;
		}
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setProxyConditions(true, null);
	rotateUserAgents(false);
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		if (!html.includes("h3")) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingDescription = get_unique_regex_match(html, '<h3 class="contact-title">Description</h3>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		list_price = get_next_regex_match(html, 0, "<div class=\"more-info-up\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(list_price)) {
			mls.ListPrice.value = list_price.replace(/[^0-9,]/g, "");
			mls.ListPrice.currencyCode = list_price.replace(/[^a-zA-Z\$]/g, "");
		}
		if (isUndefined(list_price) && isUndefined(description)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h2 class=\"detail_title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "latval = '([^\']*)'", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, "longval = '([^\']*)'", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<i class=\"fa fa-bed\"></i>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<i class=\"fa fa-bath\"></i>([^\<]*)", 1, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, "title=\"For Rent\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "For Rent") {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.PropertyType.value = get_next_regex_match(html, 0, "Type:</span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Cedric Francis";
		mls.Brokerage.Phone = "1284-544-9698";
		mls.Brokerage.Email = "cedric.beaconrealtybvi@gmail.com";
		mls.Location.Directions = get_unique_regex_match(html, "class=\"list-location main-location\"><i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			var directionData = mls.Location.Directions.split(",");
			mls.Address.City.value = directionData[1];
			mls.Address.StateOrProvince.value = directionData[0]
		}
		var all_features = "";
		var features_split = "";
		all_features = get_unique_regex_match(html, '<h3>Property features</h3>[\\s\\t\\n ]*?</div><ul>(.+?)</ul>', 1, KDONOTNOTIFYERROR);
		if (isDefined(all_features)) {
			all_features = all_features.replace(/(<([^>]+)>)/gi, ",");
			features_split = all_features.split(",");
		}
		if (isDefined(features_split) && features_split.length !== 0) {
			features_split.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "AC") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						feature = feature.replace(/\s+/g, " ").trim();
						if (feature !== "") {
							var obj = {};
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					feature = feature.replace(/\s+/g, " ").trim();
					if (feature !== "") {
						var obj = {};
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img class=\"rsTmb\" src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_innactive;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(false);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "http://www.beaconrealtybvi.com/british-virgin-islands-residential-properties-for-sale", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://www.beaconrealtybvi.com/british-virgin-islands-land-for-sale", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "http://www.beaconrealtybvi.com/british-virgin-islands-residential-property-rentalss", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "http://www.beaconrealtybvi.com/british-virgin-islands-commercial-property-rentals", KTIENDASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "http://www.beaconrealtybvi.com/british-virgin-islands-condo-rental", KCASASALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
