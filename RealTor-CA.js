qa_override("[E4063587116]", "Some of the properties contains description and some does not");

DISABLE_JAVASCRIPT("It is failed due to its security.");

function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		// try {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		wait(90000);
		do {
			el1 = virtual_browser_find_one(browser, "(//a[@class='blockLink listingDetailsLink'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "(//i[@class='fa fa-angle-right'])[1]", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KDONOTNOTIFYERROR);
						wait(6000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	// addInternalCodeFormat("\\b[A-Z]{3,3}# [0-9]{2,5}\\b");
	// if (isUndefined(browser)) {
	// browser = create_virtual_browser("HeadlessChrome");
	// if (isUndefined(browser)) {
	// return analyzeOnePublication_return_tech_issue;
	// }
	// }
	// virtual_browser_navigate(browser, url);
	// var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<div class="je2-listing-page__content-box-text collapsed js-listing-description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"je2-listing-page__intro-price\">[\s\t\n ]*?([\$])([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"je2-listing-page__intro-price\">[\s\t\n ]*?([\$])([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Bedrooms = get_unique_regex_match(html, '<div class="je2-listing-page__intro-feature-data">([^\<]*)</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">[\s\t\n ]*?Bedrooms', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<div class="je2-listing-page__intro-feature-data">([^\<]*)</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">[\s\t\n ]*?Bathrooms', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'data-src="([^\"]*)" alt="[^\"]*" loading="lazy"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://img.jamesedition.com/listing_images" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"je2-listing-page__intro-title\">([^\<]*)", 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, '<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#land" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">([^\<]*)', 1,
				KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(
				html,
				'<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#land" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">[^\<]*</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, '<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#house" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(
				html,
				'<svg class="je2-icon" width="20" height="20" viewBox="0 0 20 20"><use xlink:href="#house" /></svg>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-data">[^\<]*</div>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="je2-listing-page__intro-feature-label">([^\<]*)',
				1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_unique_regex_match(html, "<h2 class=\"property-address\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, '"Listing city":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = get_unique_regex_match(html, '"Listing country":"([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, '"Listing id":([^\,]*),', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<div class="je2-contact-form__agent-name">([^\<]*)', 1, KDONOTNOTIFYERROR);
		// mls.Brokerage.Phone = get_next_regex_match(html, 0, "<a
		// href=\"tel:([^\"]*)", 1, KDONOTNOTIFYERROR);
		mls.PropertyType = get_unique_regex_match(html, 'Property type</div>[\s\t\n ]*?<div class="je2-listing-page__building-property-value">([^\<]*)', 1, KDONOTNOTIFYERROR);
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(
				browser,
				"https://www.realtor.ca/map#ZoomLevel=4&Center=54.920828%2C-99.316406&LatitudeMax=65.41716&LongitudeMax=-54.05273&LatitudeMin=40.73893&LongitudeMin=-144.58008&Sort=1-A&PropertyTypeGroupID=1&PropertySearchTypeId=1&TransactionTypeId=2&Currency=CAD",
				KCASASVENTAS);
		// cumulatedCount += categoryLandingPoint(browser,
		// "https://www.realtor.ca/map#PropertyTypeGroupID=1&PropertySearchTypeId=2&TransactionTypeId=2",
		// KCASASVENTAS);
		// cumulatedCount += categoryLandingPoint(browser,
		// "https://www.realtor.ca/map#ZoomLevel=4&Center=54.920828%2C-99.316406&LatitudeMax=65.41716&LongitudeMax=-54.05273&LatitudeMin=40.73893&LongitudeMin=-144.58008&Sort=1-A&PropertyTypeGroupID=1&PropertySearchTypeId=3&TransactionTypeId=2&OwnershipTypeGroupId=2&Currency=CAD",
		// KAPTOVENTAS);
		// cumulatedCount += categoryLandingPoint(browser,
		// "https://www.realtor.ca/map#PropertyTypeGroupID=1&PropertySearchTypeId=8&TransactionTypeId=2",
		// KMULTIFAMILIAVENTAS);
		// cumulatedCount += categoryLandingPoint(browser,
		// "https://www.realtor.ca/map#PropertyTypeGroupID=1&PropertySearchTypeId=4&TransactionTypeId=2",
		// KOTROSVENTAS);
		// cumulatedCount += categoryLandingPoint(browser,
		// "https://www.realtor.ca/map#PropertyTypeGroupID=1&PropertySearchTypeId=5&TransactionTypeId=2",
		// KOTROSVENTAS);
		// cumulatedCount += categoryLandingPoint(browser,
		// "https://www.realtor.ca/map#PropertyTypeGroupID=1&PropertySearchTypeId=6&TransactionTypeId=2",
		// KTERRENOSVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
