var countryContext = "Israel";
include("base/RemaxFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=261&mpts=19418&pt=19418&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=261&mpts=19420&pt=19420&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=261&mpts=19422&pt=19422&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KOTROSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=261&mpts=19430&pt=19430&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=261&mpts=19426&pt=19426&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=260&mpts=19418&pt=19418&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KAPTOALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=260&mpts=19430&pt=19430&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KCASASALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=260&mpts=19420&pt=19420&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KCASASALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=260&mpts=19422&pt=19422&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KOTROSALQUILER);
	cumulatedCount += categoryLandingPoint(browser, "https://www.remax-israel.com/PublicListingList.aspx?OfficeID=120022&Lang=ENU#mode=gallery&tt=260&mpts=19426&pt=19426&cur=USD&sb=MostRecent&page=1&sc=120&sid=5727c1a8-5d8e-4c0e-8728-f2619380cea9&oid=120022", KTERRENOSALQUILER);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
