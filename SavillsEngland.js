var countryText = "England";
include("base/SavillsFunctions.js");
function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_APT&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KAPTOVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Appartment en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_APT&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KAPTOALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_B&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_ND&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KNUEVASCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_D&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_H&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_M&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_PENT&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_B&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_CAST&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_D&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_H&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_M&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Casas en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_PENT&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KCASASALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_BP&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_EST&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_BP&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KTERRENOSALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// New Development
	{
	    try {
		// New Development
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_ND&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KNUEVASCASASVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	// Others
	{
	    try {
		// Others en venta
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_B&SortOrder=SO_PCDD&Currency=USD&PropertyTypes=GRS_PT_STU&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KOTROSVENTAS);
	    } catch (e) {
		print("error: " + e);
	    }
	    try {
		// Others en rent
		cumulatedCount += crawlCategory("https://search.savills.com/com/en/list?SearchList=Id_46001+Category_RegionCountyCountry&Tenure=GRS_T_R&SortOrder=SO_PCDD&Currency=USD&Period=Month&PropertyTypes=GRS_PT_STU&Bedrooms=-1&Bathrooms=-1&CarSpaces=-1&Receptions=-1&ResidentialSizeUnit=SquareMeter&LandAreaUnit=Hectare&Category=GRS_CAT_RES&Shapes=W10", KOTROSALQUILER);
	    } catch (e) {
		print("error: " + e);
	    }
	}
	print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date()
		.getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
	next_crawl_needed(null, false);
    }
}
