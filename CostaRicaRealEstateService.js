qa_override("[E2592408747]", "There is no city on this website.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<div class="post-image property-image alignnone"><a href="([^\"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a class="next page-numbers" href="([^\"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.LotSize.value = get_unique_regex_match(html, 'Plot Size:</span><!-- .property-details_3-label -->[\s\t\n ]*?<span class="property-details-value">([0-9\,\. ]*)([ a-zA-Z ]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Plot Size:</span><!-- .property-details_3-label -->[\s\t\n ]*?<span class="property-details-value">([0-9\,\. ]*)([ a-zA-Z ]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms:</span><!-- .property-details_1-label -->[\s\t\n ]*?<span class="property-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms:</span><!-- .property-details_2-label -->[\s\t\n ]*?<span class="property-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, 'Year Built:</span><!-- .property-details_6-label -->[\s\t\n ]*?<span class="property-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li><a href="[^\"]*">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Split Level A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div id="wpcasa-property-description-2" class="property-description section clearfix clear" style="margin-top:-10px">(.+?)<style type="text/css">', 1, KDONOTNOTIFYERROR);
		if (mls.ListingDescription.includes("Contact")) {
			var index = mls.ListingDescription.indexOf("Contact"); // Gets the
			// first
			// index
			// where a
			// space
			// occours
			mls.ListingDescription = mls.ListingDescription.substr(0, index);
		}
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Phone = "512-600-0767/011-506-2787-0226";
		mls.Brokerage.Name = "Costa Rica Real Estate Service Dominical";
		mls.MlsId = get_unique_regex_match(html, 'Property ID:</span>[\s\t\n ]*?<span class="property-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class=\"entry-title\">([^\<]*)', 1, KNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<span class="property-price-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<span class="property-price-symbol">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == 0.00) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		var dVideo = get_unique_regex_match(html, '<iframe width="[^\"]*" height="[^\"]*" src="([^\"]*)" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>', 1, KDONOTNOTIFYERROR);
		if (isDefined(dVideo)) {
			var obj = JSON.parse(get_list_empty_variable("video"));
			obj.MediaURL = dVideo;
			obj.MediaOrderNumber = 0;
			if (mls.Videos.video == undefined) {
				mls.Videos.video = [];
			}
			mls.Videos.video.push(obj);
		}
		var latlng = get_next_regex_match(html, 0, "latLng:([^\s]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(latlng)) {
			latlng = latlng.replace("[", "");
			latlng = latlng.replace("]", "");
			latlng = latlng.split(",");
		}
		if (isDefined(latlng[0])) {
			mls.Location.Latitude = latlng[0].trim();
		}
		if (isDefined(latlng[1])) {
			mls.Location.Longitude = latlng[1].replace(/(<([^>]+)>)/ig, "");
			mls.Location.Longitude = latlng[1].replace(/\s+/g, ' ').trim();
			mls.Location.Longitude = mls.Location.Longitude.replace("}", "").trim();
		}
		mls.Address.Country.value = "Costa Rica";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<a href="([^\"]*)" title="[^\"]*" class="gallery-link">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "http://www.arubarealestate.com/" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://costaricarealestateservice.com/type/condos-and-townhomes/", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://costaricarealestateservice.com/type/single-family-homes/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://costaricarealestateservice.com/type/luxury-estate-homes-and-mansions/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://costaricarealestateservice.com/type/building-lots/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://costaricarealestateservice.com/type/farms-and-acreage/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others
		{
			try {
				// Others en venta
				cumulatedCount += crawlCategory("https://costaricarealestateservice.com/type/commercial-real-estate/", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

