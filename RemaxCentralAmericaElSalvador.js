var countryContext = "";
include("base/RemaxCentralAmericaFunctions.js")
function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Apartment_Condo/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Appartment en rent
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Apartment_Condo/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Lease", KAPTOALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Multi-family-(duplexx123x-triplexx123x-aptpontintx-buildingsx123x-etcpontintx)/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Townhouse/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KAPTOVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Multi-family-(duplexx123x-triplexx123x-aptpontintx-buildingsx123x-etcpontintx)/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Lease", KAPTOALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/House_Villa/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KCASASVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/House_Villa/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Lease", KCASASALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Lot_Land/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KTERRENOSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Lot_Land/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Lease", KTERRENOSALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	// Others
	{
	    try {
		// Others en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Business-for-sale/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KOTROSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Others en venta
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Commercial-building-or-spacex123x-officex123x-warehousex123x-hospitalityx123x-etcpontintx/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Sale", KOTROSVENTAS);
	    } catch (e) {
		print("error: "+e);
	    }
	    try {
		// Others en rent
		cumulatedCount += crawlCategory("https://www.remax-centralamerica.com/searchByCountry/el-salvador/FilterByState/All/HavingLocations/All/typeOf/Commercial-building-or-spacex123x-officex123x-warehousex123x-hospitalityx123x-etcpontintx/Bedrooms--1-bathrooms--1-from-0-to-0/sortby/Most-recent/page/1/StandardStatus/All/showOnly/Lease", KOTROSALQUILER);
	    } catch (e) {
		print("error: "+e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date().getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
