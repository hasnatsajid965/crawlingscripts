var countryText = "Zimbabwe";
include("base/KNightFrankFunctions.js")
function categoryLandingPoint(browser, url, category) {
    virtual_browser_navigate(browser, url);
    var actualCount = crawlCategory(browser, category);
    print("---- "+actualCount+" found in "+getJavascriptFile()+" for category "+category+" ----");
    return actualCount;
}

function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	var browser = create_virtual_browser("HeadlessChrome");
	if (isUndefined(browser)) {
	    return analyzeOnePublication_return_tech_issue;
	}
	cumulatedCount += categoryLandingPoint(browser, "http://www.knightfrank.co.zw/properties/residential/for-sale/zimbabwe-harare/bungalow%2Cestate%2Cfarmhouse%2Chouse%2Chouses%20of%20multiple%20occupation%2Cresi%20investment%2Ctown%20house%2Ctownhouse%2Ctownhousevilla%2Cvilla%2Cvillage%20house/all-beds", KCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "http://www.knightfrank.co.zw/properties/residential/for-sale/zimbabwe-harare/flat,apartment,masionette,studio%20flat,condominium,resi%20investment,block,portfolio:%20flats,portfolio:%20houses,houses%20or%20multiple%20occupation,serviced%20residence,unit,block%20of%20flats,off%20plan,tenanted%20investments,development%20block,ground%20rents/all-beds;%20reversions,%20regulated%20tenancies=", KAPTOVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "http://www.knightfrank.co.zw/properties/residential/for-sale/zimbabwe-harare/development/all-beds", KNUEVASCASASVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "http://www.knightfrank.co.zw/properties/residential/for-sale/zimbabwe-harare/farm,land,development%20plot,greenfield%20land,brownfield%20land,new%20build%20land,development%20site,farmestate,farmhouse,farmland,acreagesemi-rural/all-beds", KTERRENOSVENTAS);
	cumulatedCount += categoryLandingPoint(browser, "http://www.knightfrank.co.zw/properties/residential/for-sale/zimbabwe-harare/land%2Cdevelopment/all-beds", KTERRENOSVENTAS);
	virtual_browser_close(browser);
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
