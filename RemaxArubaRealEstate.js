function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a class="view" href="([^"]*)">', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<a id="nextpage" class="button" href="([^"]*)">', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="container">[\\s\\t\\n ]*?<h1>([^<]*)</h1>', 1, KNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, '<span class="group">Lot size:</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="col-xs-7">[\s\t\n ]*?([0-9\.\, ]+)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, '<span class="group">Lot size:</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="col-xs-7">[\s\t\n ]*?[0-9\.\, ]+([a-zA-Z]*)<sup>2', 1, KDONOTNOTIFYERROR)
				+ get_unique_regex_match(html, '<span class="group">Lot size:</span>[\s\t\n ]*?</div>[\s\t\n ]*?<div class="col-xs-7">[\s\t\n ]*?[0-9\.\, ]+[a-zA-Z]*<sup>([0-9]*)', 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, '<span class="group">Bedrooms:</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-xs-7">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, '<span class="group">Bathrooms:</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-xs-7">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (mls.Bathrooms == "0") {
			mls.Bathrooms = get_unique_regex_match(html, '<span class="group">Half bathrooms:</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-xs-7">([^<]*)', 1, KDONOTNOTIFYERROR);
		}
		mls.CoolingSystems = get_unique_regex_match(html, '<span class="group">Air-conditioned:</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="col-xs-7">([^<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, "<span class=\"group\">([^\:]*):</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-xs-7\">[\\s\\t\\n ]*?Yes", 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div id="description">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, '<div class="image" style="background-image:url[(]\'[^\']*\'[)]" ></div>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<h4>([^<]*)</h4>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_unique_regex_match(html,
				'<div class="image" style="background-image:url[(]\'[^\']*\'[)]" ></div>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<h4>([^<]*)</h4>[\\s\\t\\n ]*?<h5>[^<]*</h5>[\\s\\t\\n ]*?<div class="">([^<]*)</div>', 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, 'name="agent_email" value="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<div class="price">[\s\t\n ]*?([a-zA-Z ]+)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<div class="price">[\s\t\n ]*?([a-zA-Z ]+)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.PropertyType.value = get_unique_regex_match(html, '<span class="group">Property Type:</span>[s\t\n ]*?</div>[s\t\n ]*?<div class="col-xs-7">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		mls.Location.Latitude = get_unique_regex_match(html, " latLng: [{][\\s\\t\\n ]*?lat: ([^,]*),[\\s\\t\\n ]*?lng:([^}]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, " latLng: [{][\\s\\t\\n ]*?lat: ([^,]*),[\\s\\t\\n ]*?lng:([^}]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<td>Location</td><td>([^<]*)</td>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_unique_regex_match(html, "Area:</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class=\"col-xs-7\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Aruba";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<img class=\"core-image img-responsive lazyload\" style=\"\"[\\s\\t\\n ]*?data-srcset=\"([^\,]*)g", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag + "g";
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=428", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=892", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=79e", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// stores en venta
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=efa", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// stores en venta
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=426", KTIENDASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=8d9", KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Edificios/Ventas
		{
			try {
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=1a2", KEDIFICIOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				cumulatedCount += crawlCategory("https://remax-arubarealestate.com/property/list?search=true&property_name_id=&type=a3c", KEDIFICIOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
