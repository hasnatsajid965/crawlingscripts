var countryText = "France";

include("base/ChristiesRealEstateFunctions.js")

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		setProxyConditions(true, null);
		rotateUserAgents(true);
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/fra/apartments-flats-type", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/fra/villas-townhouses-type", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Land for sale
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/fra/estate-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/fra/farm-ranch-plantation-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/fra/land-lots-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.christiesrealestate.com/sales/fra/vineyard-type", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

