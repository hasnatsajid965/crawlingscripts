qa_override("[E2592408747]", "There is no city on this site");
qa_override("[E1473228885]", "Some properties does not contain lot size.");
qa_override("[E2431535312]", "Some properties does not have description.");
qa_override("[E1473228885]", "Some properties does not have areaUnits.");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var cachedSet = new StringSet();
	while (true) {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		virtual_browser_sendKeys(browser, "\\\uE010");
		wait(20000);
		do {
			el1 = virtual_browser_find_one(browser, "(//div[@class='bottom info']/small)[" + index + "]", KDONOTNOTIFYERROR);
			el2 = virtual_browser_find_one(browser, "(//div[contains(concat(' ', @class, ' '), ' property-item-view ')])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el2)) && (virtual_browser_element_interactable(browser, el2))) {
				data_id = virtual_browser_element_attribute(el2, "data-id");
				el1 = virtual_browser_element_text(el1);
				el1 = el1.split(",");
				if (isDefined(el1[0])) {
					finalText = el1[0].replace(/\s+/g, '-');
				}
				mainUrl = "https://www.puertoricorealtorsmls.com/listing/" + finalText + "/" + data_id + "";
				resArray.push(mainUrl);
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		var array = virtual_browser_find_all(browser, "/html/body/div[3]/section[1]/aside/footer/div/ul/li[@class='pagination-item']/a", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			index = 1;
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	if (url.includes("/404/")) {
		url = url.replace("/404/", "/");
	}
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		var element
		var imageCount = 0;
		var index = 1;
		element = virtual_browser_find_one(browser, "//h1[@class='m-0']", KDONOTNOTIFYERROR);
		if (element !== null) {
			mls.ListingTitle = virtual_browser_element_text(element);
		} else {
			return analyzeOnePublication_return_innactive;
		}
		if (isUndefined(mls.ListingTitle)) {
			// if(mls.ListingTitle == ""){
			// return analyzeOnePublication_return_innactive;
			// }
			mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"m-0\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		}

		element = virtual_browser_find_one(browser, "//h1[text()='Oops! Something Went Wrong.']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			return analyzeOnePublication_return_innactive;
		}
		do {
			element = virtual_browser_find_one(browser, "(//img[@class='flickity-lazyloaded'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(element)) && (virtual_browser_element_interactable(browser, element))) {
				if (virtual_browser_element_attribute(element, "src")) {
					var obj = JSON.parse(get_list_empty_variable("photo"));
					obj.MediaURL = virtual_browser_element_attribute(element, "src");
					obj.MediaOrderNumber = imageCount;
					if (mls.Photos.photo == undefined) {
						mls.Photos.photo = [];
					}
					mls.Photos.photo.push(obj);
					imageCount++;
				}
			}
			index++;
		} while (element != undefined);
		element = virtual_browser_find_one(browser, "//h2[@class='m-0']", KDONOTNOTIFYERROR);
		if (element !== null) {
			var list_price = virtual_browser_element_text(element);
			if (isDefined(list_price)) {
				mls.ListPrice.value = get_next_regex_match(list_price, 0, "([A-Z$]*)[ ]?([0-9,]*)", 2, KDONOTNOTIFYERROR);
				if (isUndefined(mls.ListPrice.value)) {
					mls.ListPrice.value = KPRICEONDEMAND;
				} else {
					mls.ListPrice.currencyCode = get_next_regex_match(list_price, 0, "([A-Z$]*)[ ]?([0-9,]*)", 1, KDONOTNOTIFYERROR);
				}
			} else {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		} else {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.Country.value = "Puerto Rico";
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value == "") {
				return analyzeOnePublication_return_innactive;
			}
		}
		if (isUndefined(mls.Photos.photo)) {
			var imageCount = 0;
			var images;
			images = get_all_regex_matched(html, "src=\"([^\"]*)\"[\\s\\t\\n ]*?title=\"\"[\\s\\t\\n ]*?aria-label=\"Photo of Property\"", 1);
			images.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag;
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		}
		for_rent = virtual_browser_find_one(browser, "//div[@class='col-md-4 mt-md-0 mt-3']", KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			for_rent = virtual_browser_element_text(for_rent);
			if (for_rent.includes("For Rent")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		}
		for_rent = virtual_browser_find_one(browser, "//small[text()='For Lease']", KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			for_rent = virtual_browser_element_text(for_rent);
			if (for_rent.includes("For Lease")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("year");
			}
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "lat:([^\,]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "lng:([^\;]*)[}]", 1, KDONOTNOTIFYERROR);
		element = virtual_browser_find_one(browser, "//h5[@class='mb-0']/a", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Brokerage.Name = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "//ul[@class='list-group']/li", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Brokerage.Phone = virtual_browser_element_text(element);
		}
		element = virtual_browser_find_one(browser, "//span[contains(text(),'Beds:')]", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Bedrooms = virtual_browser_element_text(element);
			mls.Bedrooms = mls.Bedrooms.replace(/[^0-9,]/g, "");
		}
		element = virtual_browser_find_one(browser, "(//span[contains(text(),'Bathrooms:')])[1]", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.Bathrooms = virtual_browser_element_text(element);
			mls.Bathrooms = mls.Bathrooms.replace(/[^0-9,]/g, "");
		}
		element = virtual_browser_find_one(browser, "//div[@class='public-remarks']", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.ListingDescription = virtual_browser_element_text(element);
		}
		lot_size = virtual_browser_find_one(browser, "(//span[contains(text(),'SqFt:')])[1]", KDONOTNOTIFYERROR);
		if (isDefined(lot_size)) {
			lot_sizes = virtual_browser_element_text(lot_size);
			mls.LotSize.value = get_next_regex_match(lot_sizes, 0, "([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);

		} else {
			lot_size = virtual_browser_find_one(browser, "//span[contains(text(),'Lot Size:')]", KDONOTNOTIFYERROR);
			if (isDefined(lot_size)) {
				lot_sizes = virtual_browser_element_text(lot_size);
				mls.LotSize.value = get_next_regex_match(lot_sizes, 0, "([0-9\,\. ]*)[a-zA-Z]*", 1, KDONOTNOTIFYERROR);
				mls.LotSize.areaUnits = get_next_regex_match(lot_sizes, 0, "[0-9\,\. ]*([a-zA-Z]*)", 1, KDONOTNOTIFYERROR);

			}
		}
		if (isUndefined(mls.ListingDescription) || mls.ListingDescription == null || mls.ListingDescription == "" && mls.ListingTitle == null || isUndefined(mls.ListingTitle) || mls.ListingTitle == "") {
			return analyzeOnePublication_return_unreachable;
		}
		element = virtual_browser_find_one(browser, "(//span[contains(text(),'Built:')])", KDONOTNOTIFYERROR);
		if (isDefined(element)) {
			mls.YearBuilt = virtual_browser_element_text(element);
			mls.YearBuilt = mls.YearBuilt.replace(/[^0-9,]/g, "");
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://www.puertoricorealtorsmls.com/for-sale", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.puertoricorealtorsmls.com/for-sale?PropertyType=LAND", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.puertoricorealtorsmls.com/for-sale?PropertyType=RINC", KAPTOVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.puertoricorealtorsmls.com/for-sale?PropertyType=COMS", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://www.puertoricorealtorsmls.com/for-rent", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://www.puertoricorealtorsmls.com/for-rent?PropertyType=COML", KOFICINASALQUILER);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}

/*
 * >>> Extraction QA Results >>> START >>> GENERATED CONTENT, DO NOT ALTER >>>
 * QA Results (12 passed, 20 warning, 0 error):
 * 
 * Messages: 1. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/E108-Calle-Igualdad/PR8800686
 * 2. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/59-Calle-San-Miguel/PR9089772
 * 3. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/Lot-117-12-St-La-Central/PR9090711
 * 4. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/62-Luisa/PR9089923 5. No JSON
 * was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/6020-Campo-Rico-Ave./PR0000371
 * 6. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/3B-Pr-250/PR9088464 7. No JSON
 * was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/KM-1.1-Bo.-Jagual/PR9090537 8.
 * No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/107-Ave.pedro-Albizu-Campos/PR9088616
 * 9. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/88-Calle-Martines-Nadal/PR9088708
 * 10. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/Carr-987-Sardinera-G-302/PR9090142
 * 11. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/CARR-956-Cond.-Lomas-De-Rio-Grande-Apt.-2502/PR8800838
 * 12. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/405-Amanecer-St/PR9089150 13.
 * No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/411-km-2.8-Bo.-Jaguey/PR9090763
 * 14. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/312-Julio-Enrique-Monagas/PR9090147
 * 15. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/26-Santa-Juanita/PR9089967 16.
 * No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/COND.-EL-EMBAJADOR-Cond.-El-Embajador--1702/PR9089167
 * 17. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/PR-494-km-1.1-Arenales-Bajos/PR9089278
 * 18. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/CLL-A-1-Lot-#c8-Ca�aboncito/PR9090545
 * 19. No JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/315-Quebrada/PR9089162 20. No
 * JSON was returned from javascript with page
 * https://www.puertoricorealtorsmls.com/listing/42-Villa-De-Loiza-Hh-4/PR9090113
 * 
 * Regular-expressions/xpath: [W2240006603] virtual_browser_find_one -
 * (//img[@class='flickity-lazyloaded'])[*] (0.7361111) (Warning)
 * virtual_browser_find_one - //ul[@class='list-group']/li (1) (OK)
 * virtual_browser_find_one - (//span[contains(text(),'Bathrooms:')])[*] (1)
 * (OK) virtual_browser_find_one - //h5[@class='mb-0']/a (1) (OK)
 * virtual_browser_find_one - (//span[contains(text(),'SqFt:')])[*] (1) (OK)
 * [E753096733] virtual_browser_find_one - //small[text()='For Lease'] (0)
 * (Error) virtual_browser_find_one - //span[contains(text(),'Beds:')] (1) (OK)
 * virtual_browser_find_one - //h2[@class='m-0'] (1) (OK) [E1375883745]
 * get_all_regex_matched - src=&quot;([^&quot;]*)&quot;[\s\t\n
 * ]*?title=&quot;&quot;[\s\t\n ]*?aria-label=&quot;Photo of Property&quot; (0)
 * (Error) virtual_browser_find_one - //div[@class='col-md-4 mt-md-0 mt-3'] (1)
 * (OK) virtual_browser_find_one - //div[@class='public-remarks'] (1) (OK)
 * get_next_regex_match - lng:([^;]*)[}] (1) (OK) get_next_regex_match -
 * ([A-Z$]*)[ ]?([0-9,]*) (1) (OK) get_next_regex_match - lat:([^,]*) (1) (OK)
 * [E4256100590] virtual_browser_find_one - //h1[text()='Oops! Something Went
 * Wrong.'] (0) (Error) [W1512979441] virtual_browser_find_one -
 * //h1[@class='m-0'] (0.95) (Warning) get_next_regex_match - ([0-9,. ]*) (1)
 * (OK)
 * 
 * 
 * 1.
 * https://www.puertoricorealtorsmls.com/listing/E108-Calle-Igualdad/PR8800686
 * 2.
 * https://www.puertoricorealtorsmls.com/listing/59-Calle-San-Miguel/PR9089772
 * 3.
 * https://www.puertoricorealtorsmls.com/listing/Lot-117-12-St-La-Central/PR9090711
 * 4. https://www.puertoricorealtorsmls.com/listing/62-Luisa/PR9089923 5.
 * https://www.puertoricorealtorsmls.com/listing/6020-Campo-Rico-Ave./PR0000371
 * 6. https://www.puertoricorealtorsmls.com/listing/3B-Pr-250/PR9088464 7.
 * https://www.puertoricorealtorsmls.com/listing/KM-1.1-Bo.-Jagual/PR9090537 8.
 * https://www.puertoricorealtorsmls.com/listing/107-Ave.pedro-Albizu-Campos/PR9088616
 * 9.
 * https://www.puertoricorealtorsmls.com/listing/88-Calle-Martines-Nadal/PR9088708
 * 10.
 * https://www.puertoricorealtorsmls.com/listing/Carr-987-Sardinera-G-302/PR9090142
 * 11.
 * https://www.puertoricorealtorsmls.com/listing/CARR-956-Cond.-Lomas-De-Rio-Grande-Apt.-2502/PR8800838
 * 12. https://www.puertoricorealtorsmls.com/listing/405-Amanecer-St/PR9089150
 * 13.
 * https://www.puertoricorealtorsmls.com/listing/411-km-2.8-Bo.-Jaguey/PR9090763
 * 14.
 * https://www.puertoricorealtorsmls.com/listing/312-Julio-Enrique-Monagas/PR9090147
 * 15. https://www.puertoricorealtorsmls.com/listing/26-Santa-Juanita/PR9089967
 * 16.
 * https://www.puertoricorealtorsmls.com/listing/COND.-EL-EMBAJADOR-Cond.-El-Embajador--1702/PR9089167
 * 17.
 * https://www.puertoricorealtorsmls.com/listing/PR-494-km-1.1-Arenales-Bajos/PR9089278
 * 18.
 * https://www.puertoricorealtorsmls.com/listing/CLL-A-1-Lot-#c8-Ca�aboncito/PR9090545
 * 19. https://www.puertoricorealtorsmls.com/listing/315-Quebrada/PR9089162 20.
 * https://www.puertoricorealtorsmls.com/listing/42-Villa-De-Loiza-Hh-4/PR9090113
 * 
 * QA executed on 20-Mar-2020 at 12:17:40 AM
 *  <<< Extraction QA Results <<< END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
 */
