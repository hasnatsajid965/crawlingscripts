qa_override("[E1554112646]", "I am overriding this error because it is crawling images but showing as an error.");


// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<figure class=\"item-main \">[\\s\\t\\n ]*?<a href=\"([^\"]*)\"", category, "https://www.green-acres.fr", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<a id=\"nextPage\" href=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.green-acres.fr" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingDescription = get_unique_regex_match(html, '<h2 class="title-standard">Property summary</h2>[\\s\\t\\n ]*?<p class="text">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_next_regex_match(html, 0, "<p class=\"form-price\">[\\s\\t\\n ]*?<span class=\"symbol currency-right\">([^\<]*)</span>([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p class=\"form-price\">[\\s\\t\\n ]*?<span class=\"symbol currency-right\">([^\<]*)</span>([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.ListPrice.value)) {
			if (mls.ListPrice.value === 0) {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<p class=\"form-price\">[\s\t\n ]*?<span class=\"symbol currency-left\">([^\<]*)</span>([0-9\,\. ]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p class=\"form-price\">[\s\t\n ]*?<span class=\"symbol currency-left\">([^\<]*)</span>([0-9\,\. ]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<h1 class=\"item-title\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LivingArea.value = get_unique_regex_match(html, "<span class=\"ga-icons ga-icon-house\"></span>([0-9 ]*)([^\<]*) de surface habitable", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<span class=\"ga-icons ga-icon-house\"></span>([0-9 ]*)([^\<]*) de surface habitable", 2, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, "<span class=\"ga-icons ga-icon-territory-square\"></span>([0-9 ]*)([^\<]*) de terrain", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "<span class=\"ga-icons ga-icon-territory-square\"></span>([0-9 ]*)([^\<]*) de terrain", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "<span class=\"ga-icons ga-icon-territory-square\"></span>([0-9 ]*)([^\<]*) of land", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "<span class=\"ga-icons ga-icon-territory-square\"></span>([0-9 ]*)([^\<]*) of land", 2, KDONOTNOTIFYERROR);
		}

		mls.Location.Latitude = get_unique_regex_match(html, 'data-lat="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'data-lng="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "Year built</dt>[\\s\\t\\n ]*?<dd>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<p class=\"details-name\">Rooms</p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<p class=\"details-name\">Bedrooms</p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Guadeloupe";
		mls.Address.City.value = get_unique_regex_match(html, "region <a class='form-reference' href='[^\"]*' >([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, "Reference:([^\<]*)", 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<p>[\s\t\n ]*?<span class="[^\"]*"></span>([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "Air-conditioning") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		var imageCount = 0;
		var images;
		images = get_unique_regex_match(html, "bigPhotos = [\\[]([^\\]]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(images)) {
			temp = images.split(",");
			temp.forEach(function(oneImageTag) {
				var obj = JSON.parse(get_list_empty_variable("photo"));
				obj.MediaURL = oneImageTag.replace(/['"]+/g, '');
				obj.MediaOrderNumber = imageCount;
				if (mls.Photos.photo == undefined) {
					mls.Photos.photo = [];
				}
				mls.Photos.photo.push(obj);
				imageCount++;
			});
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_appartement-on-city_id-dp_gp", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_appartement-on-city_id-dp_gf", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_house-on-city_id-dp_gp", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_house-on-city_id-dp_gf", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_castle-on-city_id-dp_gp", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_castle-on-city_id-dp_gf", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_land-on-city_id-dp_gp", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Building
		{
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_business-on-city_id-dp_gp", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Building en venta
				cumulatedCount += crawlCategory("https://www.green-acres.fr/en/prog_show_properties.html?searchQuery=lg-en-cn-fr-i-24-hab_land-on-city_id-dp_gf", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
