// qa_override("[E4063587116]", "Some of the properties contain description and some not");
// qa_override("[E1473228885]", "Some of the properties contain lot size and some not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


DISABLE_JAVASCRIPT("It is failed at analyzeOnePublication function due to its security.");

function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, 'data-pos="[^\"]*" data-origin="1"[\s\t\n ]*?data-href="([^\"]*)"', category, "https://www.nestoria.com/", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, "class=\"pagination__link active\" data-page=\"[^\"]*\">[^\"]*</a>[\s\t\n ]*?</li><li>[\s\t\n ]*?<a href=\"([^\"]*)\"", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	if (isUndefined(browser)) {
		browser = create_virtual_browser("Chrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	element = virtual_browser_find_one(browser, "//a[@class='buttonView']", KDONOTNOTIFYERROR);
	if ((isDefined(element)) && (virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR))) {
		html = virtual_browser_html(browser);
	} else {
		// break;
	}
	// var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<div class="pull-left">[\s\t\n ]*?<h3>([^<]*)', 1, KNOTIFYERROR);
		mls.ListingDescription = get_next_regex_match(html, 0, '<h3 class="heading-2"> Property Details </h3>(.+?)</p>', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = "Global Properties Innovative Properties Solution";
		mls.Brokerage.Phone = get_next_regex_match(html, 0, 'Call us</strong> - ([^\<]*)', 1, KDONOTNOTIFYERROR);
		var features = get_all_regex_matched(html, '<li> <i class="flaticon-[^\"]*"></i>([^\<]*)</li>', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "A/C Unit") {
						mls.DetailedCharacteristics.hasAC = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						if (feature !== "") {
							obj.Description = feature;
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
						}
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					if (feature !== "") {
						obj.value = feature;
						mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
					}
				}
			});
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, '<h3><span class="text-right">Price: ([\$ ]*)([0-9\,\. ]*)', 2, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, '<h3><span class="text-right">Price: ([\$ ]*)([0-9\,\. ]*)', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.PropertyType.value = get_next_regex_match(html, 0, "<li> <i><img src=\"https://www.globalpropertiesafrica.com/img/resi-icon.png\"></i>[\s\t\n ]*?<p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "Listing Number:</strong> -([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_next_regex_match(html, 0, "<p>([0-9\,\. ]*) Bathroom </p>", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_next_regex_match(html, 0, "<p>([0-9\,\. ]*) Bedrooms </p>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, '<p><i class="fa fa-map-marker"></i>([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Gambia";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img u="image" src="([^\"]*)"', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = "https://www.globalpropertiesafrica.com" + oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// casas en venta
				cumulatedCount += crawlCategory("https://www.nestoria.com.au/forrestdale/houses/sale", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// casas en rent
				cumulatedCount += crawlCategory("https://www.nestoria.com.au/forrestdale/houses/rent", KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory("https://www.nestoria.com.au/forrestdale/apartments/sale", KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rent
				cumulatedCount += crawlCategory("https://www.nestoria.com.au/forrestdale/apartments/rent", KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Land en venta
				cumulatedCount += crawlCategory("https://www.nestoria.com.au/forrestdale/land/sale", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
