qa_override("[E1473228885]", "Some properties does not contain lotsize");
function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<h2 class="post-title entry-title">[\s\t\n ]*?<a href="([^"]*)"', category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, '<a class="next page-numbers" href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	setCountryPhoneCode("599");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<h1 class="entry-title">([^<]*)', 1, KNOTIFYERROR);
		var features = get_all_regex_matched(html, "<li><a href=\"[^\"]*\">([^\<]*)", 1);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Gated Community") {
						mls.DetailedCharacteristics.hasGatedEntry = true;
					} else if (feature === "Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("Views")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="title clearfix">[\\s\\t\\n ]*?<h2>Description</h2>[\\s\\t\\n ]*?</div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_unique_regex_match(html, "<div class=\"listing-agent-bio\"><p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var phonenum = "";
		phonenum = get_unique_regex_match(html, "Phone:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		phonenum = phonenum.replace(/[^0-9or]/g, "");
		mls.Brokerage.Phone = phonenum.replace("or", "/");
		mls.Brokerage.Address.FullStreetAddress = get_unique_regex_match(html, "Email Office</a></strong></p>[\\s\\t\\n ]*?<p>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Videos = get_unique_regex_match(html, '<iframe src=\"([^\"]*)\"', 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_unique_regex_match(html, 'Listing ID:</span>[\\s\\t\\n ]*?<span class="listing-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, "<span class=\"listing-price-value\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, "<span class=\"listing-price-symbol\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		var list_price = "";
		if (isUndefined(mls.ListPrice.value)) {
			list_price = get_unique_regex_match(html, "<span class=\"listing-price-on-request\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(list_price)) {
				if (list_price.includes("Price")) {
					mls.ListPrice.value = KPRICEONDEMAND;
					// mls.ListPrice.currencyCode = "$";
				}
			} else {
				mls.ListPrice.value = KPRICEONDEMAND;
			}
		}
		currency_period = get_unique_regex_match(html, "<span class=\"listing-rental-period\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(currency_period)) {
			if (currency_period.includes("/")) {
				var freq = currency_period.substr(currency_period.indexOf("/") + 1);
				if (freq == "/ per Month") {
					freq = "month";
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
				}
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "<div class=\"title-listing-type\"><a rel=\"tag\" class=\"[^\"]*\" href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.value = get_unique_regex_match(html, 'Lot Size:</span><!-- .listing-[^-]*?-label -->[\s\t\n ]*?<span class="listing-details-value">([0-9\.\, ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, 'Lot Size:</span><!-- .listing-[^-]*?-label -->[\s\t\n ]*?<span class="listing-details-value">([0-9\.\, ]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, 'Living Area:</span><!-- .listing-[^-]*?-label -->[\s\t\n ]*?<span class="listing-details-value">([0-9\.\, ]*)([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, 'Living Area:</span><!-- .listing-[^-]*?-label -->[\s\t\n ]*?<span class="listing-details-value">([0-9\.\, ]*)([^\<]*)', 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, 'Bedrooms:</span><!-- .listing-[^-]*?-label -->[\\s\\t\\n ]*?<span class="listing-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, 'Bathrooms:</span><!-- .listing-[^-]*?-label -->[\\s\\t\\n ]*?<span class="listing-details-value">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.YearBuilt = get_unique_regex_match(html, "<b>Year Built</b>:([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, "latLng:[^d]([^,]*),([^,]*)", 1, KDONOTNOTIFYERROR);
		lng = get_unique_regex_match(html, "latLng:[^d]([^,]*),([^,]*)", 2, KDONOTNOTIFYERROR);
		mls.Location.Longitude = lng.replace("]", "");
		mls.Address.City.value = get_unique_regex_match(html, "<div class=\"title-listing-location\"><a rel=\"tag\" class=\"[^\"]*\" href=\"[^\"]*\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "British Virgin Islands";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, 'src="([^"]*)" class="attachment-post-thumbnail size-post-thumbnail', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("-215x120", "");
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		if (isDefined(list_price)) {
			if (imageCount == 0 && list_price == "Price on request") {
				return analyzeOnePublication_return_innactive;
			}
		}
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://remax-luxury-bvi.com/property-category/property-for-sale/homes-for-sale/", KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://remax-luxury-bvi.com/property-category/property-for-sale/resorts/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Stores
				cumulatedCount += crawlCategory("https://remax-luxury-bvi.com/property-category/property-for-sale/commercial-for-sale/", KTIENDASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://remax-luxury-bvi.com/property-category/property-for-sale/land-for-sale/", KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
