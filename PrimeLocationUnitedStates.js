var countryText = "United States";
include("base/PrimeLocationFunctions.js");
function crawlForPublications() {
    if (next_crawl_needed((new Date().getTime()+KDELTATIMEWHILECALCULATION).toString(), true)) {
	var cumulatedCount = 0;
	var startTime = new Date().getTime();
	// Appartment
	{
	    try {
		// Appartment en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/flats/united-states/?country_code=us&currency=usd&search_source=overseas", KAPTOVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Casas
	{
	    try {
		// Casas en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/houses/united-states/?country_code=us&currency=usd&search_source=overseas", KCASASVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Terrenos
	{
	    try {
		// Terrenos en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/property/united-states/?country_code=us&currency=usd&property_type=land&search_source=overseas", KTERRENOSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	// Other
	{
	    try {
		// Other en venta
		cumulatedCount += crawlCategory("https://www.primelocation.com/overseas/commercial/united-states/?country_code=us&currency=usd&search_source=overseas", KOTROSVENTAS);
	    } catch (e) {
		exceptionprint(e);
	    }
	}
	print("crawlForPublications in "+getJavascriptFile()+" required "+formattedTime(new Date()
		.getTime()-startTime)+" to gather "+cumulatedCount+" listing references.");
	next_crawl_needed(null, false);
    }
}
