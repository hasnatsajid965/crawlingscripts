qa_override("[E1473228885]", "Some of the properties have lot size and some does not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<H2 style='margin-top:0px; line-height:130%;'><a href=\"([^\"]*)\"", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			} else {
				return cumulatedCount;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_next_regex_match(html, 0, "<A HREF=\"([^\"]*)\">More", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	// var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "en-us";
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<font style='font-size:20px; color:#304059; font-weight:bold;'>Price:([0-9\,\. ]*)([a-zA-Z\,\. ]*)", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<font style='font-size:20px; color:#304059; font-weight:bold;'>Price:([0-9\,\. ]*)([a-zA-Z\,\. ]*)", 2, KDONOTNOTIFYERROR);
		mls.ListingTitle = get_unique_regex_match(html, "<TD BGCOLOR='#FFFFFF' valign='top' align='left'>[\s\t\n ]*?<H1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_next_regex_match(html, 0, "<div id=\"description-tab\" style=\"\">[\\s\\t\\n ]*?<table width=\"100%\" style=\"text-align:justify;\"><tbody><tr><td>[\\s\\t\\n ]*?<h3><b>([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) && isUndefined(mls.ListingTitle)) {
			return analyzeOnePublication_return_innactive;
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		if (isDefined(mls.ListingTitle)) {
			mlsid = mls.ListingTitle.split("(");
			var index = mls.ListingTitle.indexOf("("); // Gets the first index
			// where a space occours
			mls.ListingTitle = mls.ListingTitle.substr(0, index);
			if (isDefined(mlsid[1])) {
				mls.MlsId = mlsid[1].replace("(", "");
				mls.MlsId = mlsid[1].replace(")", "");
			}
		}
		mls.PropertyType.value = get_unique_regex_match(html, "Property type</td><td align='right' style='text-align:right'><font style='color:#304059;'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Rent")) {
				freq = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
			}
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<TABLE WIDTH="100%" style=\'text-align:justify;\'><TR><TD>[\\s\\t\\n ]*?<H3><B>[^\<]*</B></H3>(.+?)<BR>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, '<div id=\"description-tab\" style=\"[^\>]*\">[\\s\\t\\n ]*?<table width="100%" style="text-align:justify;"><tbody><tr><td>(.+?)<br><br>', 1, KDONOTNOTIFYERROR);
		}
		mls.LotSize.value = get_unique_regex_match(html, "Land / Lot area:</td><td align='right' style='text-align:right'><font style='color:#304059;'>([0-9\,\. ]*)([a-zA-Z0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Land / Lot area:</td><td align='right' style='text-align:right'><font style='color:#304059;'>([0-9\,\. ]*)([a-zA-Z0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "Land / Lot area:</td><td align=\"right\" style=\"text-align:right\"><font style=\"color:#304059;\">([0-9\,\. ]*)([a-zA-Z0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "Land / Lot area:</td><td align=\"right\" style=\"text-align:right\"><font style=\"color:#304059;\">([0-9\,\. ]*)([a-zA-Z0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		}
		mls.YearBuilt = get_unique_regex_match(html, "Year built</div>[\\s\\t\\n ]*?<div class=\"details-parameters-val\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "Number of Bedrooms</td><td align='right' style='text-align:right'><font style='color:#304059;'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "Number of Bathrooms</td><td align='right' style='text-align:right'><font style='color:#304059;'>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.Bedrooms)) {
			mls.Bedrooms = get_unique_regex_match(html, "Number of Bedrooms</td><td align=\"right\" style=\"text-align:right\"><font style=\"color:#304059;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.Bathrooms)) {
			mls.Bathrooms = get_unique_regex_match(html, "Number of Bathrooms</td><td align=\"right\" style=\"text-align:right\"><font style=\"color:#304059;\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		// mls.MlsId = get_next_regex_match(html,0,"Listing
		// ID:</strong>([^\<]*)",1,KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<B>([^\<]*)</B>[ ]*?<font class=\"text4\">[^\<]*</font> from Haiti</H3>", 1, KDONOTNOTIFYERROR);
		var element = virtual_browser_find_one(browser, "//font[text()='Show Contact Details']", KDONOTNOTIFYERROR);
		if ((isDefined(element)) && (virtual_browser_click_element(browser, element, KDONOTNOTIFYERROR))) {
			wait(2000);
			element = virtual_browser_find_one(browser, "//div[@id='divshowcontactdetailstop']", KDONOTNOTIFYERROR);
			if (isDefined(element)) {
				mls.Brokerage.Phone = virtual_browser_element_text(element);
				mls.Brokerage.Phone = mls.Brokerage.Phone.replace(/[^0-9,]/g, "");
			}
		}
		// mls.Brokerage.Phone = "40 361 407700";
		mls.Location.Latitude = get_unique_regex_match(html, 'id="latitudeMapTop" value="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'id="longitudeMapTop" value="([^\"]*)"', 1, KDONOTNOTIFYERROR);
		getCity = get_all_regex_matched(html, "<a href=\"[^\"]*\" title=\"\">([^\<]*)</a>", 1);
		if (isDefined(getCity)) {
			var i = 0;
			getCity.forEach(function(City) {
				if (i == 2) {
					mls.Address.City.value = City;
				}
				i++;
			});
		}
		mls.Address.Country.value = "Haiti";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "href=\"([^\"]*)\" target=\"_new\" alt=\"Show Original Picture Size\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Apartments_for_Sale/country/Haiti/", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en Rental
				cumulatedCount += crawlCategory("http://www.mondinion.com/Apartments_for_Rent/country/Haiti/", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Houses_for_Sale/country/Haiti/", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Houses_for_Rent/country/Haiti/", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Buildings
		{
			try {
				// Buildings en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Hotels_for_Sale/country/Haiti/", KEDIFICIOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Land_for_Development/country/Haiti/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Developed_Land/country/Haiti/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("http://www.mondinion.com/Islands_for_Sale/country/Haiti/", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
