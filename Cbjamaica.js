

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while (isDefined(html) && (tracer = addNextToPropertiesList(html, tracer, '<a href="([^"]*)" >[s\t\n ]*?<img src="[^"]*" alt="[^"]*" class="img-responsive">', category, "https://www.cbjamaica.com", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw "error: " + e;
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, '<li class="paginate_button next"><a href="([^"]*)"', 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url("https://www.cbjamaica.com" + relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	// addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if (mls.NonMLSListingData.lang == undefined || mls.NonMLSListingData.lang == "") {
			mls.NonMLSListingData.lang = "en-us";
		}
		var list_not_available = get_unique_regex_match(html, '<div class="alert alert-danger">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(list_not_available)) {
			if (list_not_available == "An internal server error occurred.") {
				return analyzeOnePublication_return_unreachable;
			}
		}
		mls.ListingTitle = get_next_regex_match(html, 0, '<span class="theme-primary-color font-weight-600 font-size-18 uppercase-transform">([^<]*)</span>', 1, KNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(
				html,
				'<span class="pull-left theme-primary-color padding-top-10 font-size-28" >([^<]*)</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="nopadding div-float-left bedroom-container-number nopadding line-height-12 padding-top-7 padding-left-4"  >[\\s\\t\\n ]*?<span class="theme-primary-color" >[\\s\\t\\n ]*?<img src="/assets/img/dark-bedroom-icon.svg" class="white-bedroom" >',
				1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(
				html,
				'<span class="pull-left theme-primary-color padding-top-10 font-size-28" >([^<]*)</span>[\\s\\t\\n ]*?</div>[\\s\\t\\n ]*?<div class="nopadding bathroom-container div-float-left line-height-12 padding-top-7 padding-left-4 "  >[\\s\\t\\n ]*?<span class="theme-primary-color" >[s\t\n ]*?<img src="/assets/img/dark-bathroom-icon.svg" class="white-bedroom" >',
				1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Jamaica";
		mls.Location.Directions = get_unique_regex_match(html, "<span class=\"theme-primary-color font-weight-400 font-size-12 uppercase-transform\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.Location.Directions)) {
			total_commas = (mls.Location.Directions.match(/,/g) || []).length;
			var tokens = mls.Location.Directions.split(",");
			if (tokens.length !== 0) {
				mls.Address.City.value = tokens[0];
			}
		}
		var features = get_all_regex_matched(html, '<ul class="amenities">[\s\t\n ]*?<li class="nomargin theme-black-color">([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(features)) {
			features.forEach(function(feature) {
				if (isDefined(feature)) {
					if (mls.DetailedCharacteristics == undefined) {
						mls.DetailedCharacteristics = {};
					}
					if (feature === "Outdoor Pool") {
						mls.DetailedCharacteristics.hasPool = true;
					} else if (feature === "A/C") {
						mls.DetailedCharacteristics.hasAirCondition = true;
					} else {
						if (mls.DetailedCharacteristics.AdditionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation = {};
						}
						if (mls.DetailedCharacteristics.AdditionalInformation.additionalInformation == undefined) {
							mls.DetailedCharacteristics.AdditionalInformation.additionalInformation = [];
						}
						var obj = {};
						obj.Description = feature.replace(/\-\s+/g, ' ').trim();
						;
						mls.DetailedCharacteristics.AdditionalInformation.additionalInformation.push(obj);
					}
				}
				if (feature.endsWith("View")) {
					if (mls.DetailedCharacteristics.ViewTypes == undefined) {
						mls.DetailedCharacteristics.ViewTypes = {};
					}
					if (mls.DetailedCharacteristics.ViewTypes.ViewType == undefined) {
						mls.DetailedCharacteristics.ViewTypes.ViewType = [];
					}
					var obj = {};
					obj.value = feature;
					mls.DetailedCharacteristics.ViewTypes.ViewType.push(obj);
				}
			});
		}
		mls.ListingDescription = get_next_regex_match(html, 0, '<div class="properties-description padding-bottom-20">(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (mls.office == undefined)
			mls.office = {};
		mls.Brokerage.Name = get_next_regex_match(html, 0, '<a class="agent-info agent-name theme-primary-color" href="[^"]*">[\\s\\t\\n ]*?<b>([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, '<a href="tel:([^"]*)', 1, KDONOTNOTIFYERROR);
		email_data = get_next_regex_match(html, 0, '<p class=\"nomargin\">[\\s\\t\\n ]*?Email:([^\<]*)', 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = email_data.replace(/(<([^>]+)>)/ig, "");
		mls.MlsId = get_unique_regex_match(html, '<span class="theme-primary-color pull-left">([^<]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_unique_regex_match(html, '<h1 class="theme-primary-color font-weight-300 uppercase-transform nomargin" >[\s\t\n ]*?([a-zA-Z ]*[\$])([0-9\,\.]*)', 1, KDONOTNOTIFYERROR);
		mls.ListPrice.value = get_unique_regex_match(html, '<h1 class="theme-primary-color font-weight-300 uppercase-transform nomargin" >[\s\t\n ]*?([a-zA-Z ]*[\$])([0-9\,\.]*)', 2, KDONOTNOTIFYERROR);
		for_rent = get_unique_regex_match(html, '<a  href="#" class="theme-white-color text-uppercase" >([^<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(for_rent)) {
			if (for_rent == "For Rent") {
				currency_period = "month";
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency(currency_period);
			}
		}
		mls.LotSize.value = get_unique_regex_match(
				html,
				"Exterior[\\s\\t\\n ]*?</h3>[\\s\\t\\n ]*?<div class=\"row\">[\\s\\t\\n ]*?<div class=\"col-xs-12\"  >[\\s\\t\\n ]*?<ul class=\"amenities\">[\\s\\t\\n ]*?<li class=\"nomargin theme-black-color\">[\\s\\t\\n ]*?[^\:]*Lot Size:([ 0-9\.\,]*)",
				1, KDONOTNOTIFYERROR);
		mls.Location.Latitude = get_unique_regex_match(html, 'name="latitude[^"]*" value="([^"]*)"', 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_unique_regex_match(html, 'name="longitude[^"]*" value="([^"]*)"', 1, KDONOTNOTIFYERROR);
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, '<img src="([^"]*)" class="thumb-preview" alt="">', 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			if (/(http(s?)):\/\//gi.test(oneImageTag)) {
				obj.MediaURL = oneImageTag;
			} else {
				obj.MediaURL = oneImageTag;
			}
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		{
			try {
				// Appartment en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Apartment/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KAPTOVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Appartment en rental
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/parish/town/town/property-type/Apartment/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KAPTOALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		{
			try {
				// Offices en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Commercial%20Bldg-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Office en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Commercial%20Spaces-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Townhouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// temporal en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Warehouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/parish/town/town/property-type/House/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/parish/town/town/property-type/Warehouse/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KCASASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/parish/town/town/property-type/Commercial%20Bldg-s-Offices/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOFICINASALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Development%20Land%20%28Residential%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Residential%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Development%20Land%20%28Commercial%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Resort%20Land/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KTERRENOSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/rent/parish/parish/town/town/property-type/Development%20Land%20%28Residential%29/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KTERRENOSALQUILER);
			} catch (e) {
				print("error: " + e);
			}
		}
		// Others/sales
		{
			try {
				cumulatedCount += crawlCategory(
						"https://www.cbjamaica.com/search/residential-commercial/residential-commercial/rent-sale/sale/parish/parish/town/town/property-type/Commercial%20Lot/currency/currency/id-search/id-search/bed/bed/bath/bath/search/search/from/from/to/to/page/1/limit/12/range/H",
						KOTROSVENTAS);
			} catch (e) {
				print("error: " + e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
