

// crawlForPublications crawl-mode: Regular-Expression Crawling


function crawlCategory(url, category, stopword) {
	var cumulatedCount = 0;
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	while (isDefined(html)) {
		try {
			while ((isDefined(html)) && (tracer = addNextToPropertiesList(html, tracer, "<a class=\"hover-effect\" href=\"([^\"]*)\">", category, "", 1, KDONOTNOTIFYERROR, false)) > 0) {
				tracer++;
				actualCount++;
				cumulatedCount++;
				if (passedMaxPublications()) {
					break;
				}
			}
		} catch (e) {
			if (e !== "Duplicate detected.") {
				throw ("error: " + e);
			}
		}
		print("---- " + actualCount + " found in " + getJavascriptFile() + " on page " + page + " for category " + category + " (" + cumulatedCount + ") ----");
		if (passedMaxPublications()) {
			break;
		}
		var relativeLink = get_unique_regex_match(html, " rel=\"Next\" href=\"([^\"]*)\">", 1, KDONOTNOTIFYERROR);
		if (isUndefined(relativeLink)) {
			break;
		}
		html = gatherContent_url(relativeLink, KDONOTNOTIFYERROR);
		tracer = 0;
		actualCount = 0;
		page++;
	}
	return cumulatedCount;
}

function analyzeOnePublication(url, mlsJSONString) {
	addInternalCodeFormat("([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)");
	var html = gatherContent_url(url, KDONOTNOTIFYERROR);
	var mls = JSON.parse(mlsJSONString);
	if (html != undefined) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		var code = get_unique_regex_match(html, "<strong>C?digo:</strong> ([A-Z]{2,4}-[0-9]{3,3}[A-Z]?)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(code)) {
			if (mls.NonMLSListingData.brokerCodes == undefined) {
				mls.NonMLSListingData.brokerCodes = [];
			}
			mls.NonMLSListingData.brokerCodes.push(code);
		}
		forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-122 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (get_next_regex_match(html, 0, "<i id=\"houzez-print\" class=\"fa fa-print\"", 0, KDONOTNOTIFYERROR) != undefined) {
			var pos = get_last_regex_match_end_pos();
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
			mls.ListPrice.value = get_next_regex_match(html, 0, "<div class=\"lightbox-header hidden-xs\">[\\s\\t\\n ]*?<div class=\"header-title\">[\\s\\t\\n ]*?<p>([a-zA-Z\$ ]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
		} else {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<p><span class=\"price-start\">[^\<]*</span>([ a-zA-Z\$]*)([0-9\.\,]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 2, KDONOTNOTIFYERROR);
			mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "</li>[\s\t\n ]*?</ul>[\s\t\n ]*?<span class=\"item-price\">[^\<]*<br></span><span class=\"item-price\">([a-zA-Z$]*)([0-9\,\.]*)", 1, KDONOTNOTIFYERROR);
		}
		mls.ListingTitle = get_unique_regex_match(html, "<title>([^<]*)</title>", 1, KNOTIFYERROR);
		if (isDefined(forRent)) {
			if (forRent.includes("Renta")) {
				mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
			}
		} else {
			forRent = get_next_regex_match(html, 0, "<span class=\"label-wrap\">[\\s\\t\\n ]*?<span class=\"label-status label-status-123 label label-default\">([^\<]*)", 1, KDONOTNOTIFYERROR);
			if (isDefined(forRent)) {
				if (forRent.includes("Renta")) {
					mls.ListPrice.currencyPeriod = filterToAllowedFrequency("monthly");
				}
			}
		}
		mls.ListingDescription = get_next_regex_match(html, 0, "<p class=\"p-property-description\">(.+?)</p>", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_next_regex_match(html, 0, "<p class=\"p-property-description\" style=\"font-weight: 100 !important;\">(.+?)</p>", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription) || mls.ListingDescription == "") {
			return analyzeOnePublication_return_unreachable;
		}
		mls.LotSize.value = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_next_regex_match(html, 0, "<strong>Construcci[^\:]*n: </strong>([0-9\.\,]*)([^\&]*)", 2, KDONOTNOTIFYERROR);
		mls.LivingArea.value = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 1, KDONOTNOTIFYERROR);
		mls.LivingArea.areaUnits = get_unique_regex_match(html, "<strong>Construcci?n: </strong>([0-9]*)([^<]*?)", 2, KDONOTNOTIFYERROR);
		mls.Bedrooms = get_unique_regex_match(html, "<strong>Dormitorios:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.Bathrooms = get_unique_regex_match(html, "<strong>Ba?os:</strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.NumParkingSpaces = get_unique_regex_match(html, "<strong>Estacionamientos: </strong>[ ]?([0-9]*)<", 1, KDONOTNOTIFYERROR);
		mls.PropertyType.value = get_unique_regex_match(html, "Tipo:</strong>([^<]*)", 1, KDONOTNOTIFYERROR);
		if (isDefined(mls.PropertyType.value)) {
			if (mls.PropertyType.value.includes("Apartamentos")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KAPTOVENTAS);
					}
				}
			}
			if (mls.PropertyType.value.includes("Casas")) {
				if (isDefined(forRent)) {
					if (forRent.includes("Venta")) {
						mls.NonMLSListingData.category = getCategory(KCASASVENTAS);
					}
				}
			}
		}
		mls.Brokerage.Name = get_next_regex_match(html, 0, "<i class=\"fa fa-user\"></i> ([^<]*)</", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "<i class=\"fa fa-mobile\"></i>([^<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_next_regex_match(html, 0, "<strong>Sector:</strong> ([^<]*)</li>", 1, KDONOTNOTIFYERROR);
		mls.Address.City.value = get_next_regex_match(html, 0, "<strong>Ciudad:</strong>([^\<]*)", 1, KDONOTNOTIFYERROR);
		var operacions = get_next_regex_match(html, 0, "<strong>Operaci?n:</strong> ([^<]*?)</li>", 1, KDONOTNOTIFYERROR);
		if (isDefined(operacions)) {
			var pos = get_last_regex_match_end_pos();
			var operationList = operacions.split(",");
			if (operationList.length > 1) {
				operationList.forEach(function(oneOperation) {
					if (isPublicationPurchase()) {
						if (oneOperation == "Renta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">R: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("LEASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationLease()) {
						if (oneOperation == "Venta") {
							var alternateCurrency = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 1, KDONOTNOTIFYERROR);
							var alternateValue = get_next_regex_match(html, pos, ">V: ([A-Z$]*)[ ]?([^<>]*)<", 2, KDONOTNOTIFYERROR);
							if ((isDefined(alternateCurrency)) && (isDefined(alternateValue))) {
								if (mls.NonMLSListingData.ListingVariations == undefined) {
									mls.NonMLSListingData.ListingVariations = [];
								}
								var obj = {};
								obj.value = "ListPrice.value = " + alternateValue + "; ListPrice.currencyCode = " + alternateCurrency + "; NonMLSListingData.category = " + getAlternateCategory("PURCHASE");
								mls.NonMLSListingData.ListingVariations.push(obj);
							}
						}
					} else if (isPublicationRent()) {
					}
				});
			}
		}
		if (isUndefined(mls.ListPrice.value) || mls.ListPrice.value == "") {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.Address.Country.value = "Dominican Republic";
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<div class=\"item\" style=\"background-image: url\\(([^\)]*)\\)\">", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag;
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		// Fincas
		{
			try {
				// Fincas en venta
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=fincas&status=venta&min-price=&max-price=", KFINCASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Apartamentos
		{
			try {
				// Apartamentos en venta
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=apartamentos&status=venta&min-price=&max-price=", KAPTOVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Apartamentos en alquiler
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=apartamentos&status=renta&min-price=&max-price=", KAPTOALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Casas
		{
			try {
				// Casas en venta
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=casas&status=venta&min-price=&max-price=", KCASASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Casas en alquiler
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=casas&status=renta&min-price=&max-price=", KCASASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Locales - Tiendas
		{
			try {
				// Locales - Tiendas en venta
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=locales-comerciales&status=venta&min-price=&max-price=", KTIENDASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Locales - Tiendas en alquiler
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=locales-comerciales&status=renta&min-price=&max-price=", KTIENDASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Officinas
		{
			try {
				// Officinas en venta
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=edificios&status=venta&min-price=&max-price=", KOFICINASVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Officinas en alquiler
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=edificios&status=renta&min-price=&max-price=", KOFICINASALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		// Terrenos
		{
			try {
				// Terrenos en venta
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=terrenos&status=venta&min-price=&max-price=", KTERRENOSVENTAS);
			} catch (e) {
				exceptionprint(e);
			}
			try {
				// Terrenos en alquiler
				cumulatedCount += crawlCategory("https://www.epkasa.com/resultados-busqueda/?keyword=&bedrooms=&bathrooms=&type=terrenos&status=renta&min-price=&max-price=", KTERRENOSALQUILER);
			} catch (e) {
				exceptionprint(e);
			}
		}
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
