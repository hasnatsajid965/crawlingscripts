qa_override("[E1473228885]", "Some of the properties contain lot size and some does not");
qa_override("[E4063587116]", "Some of the properties contain description and some does not");


// crawlForPublications crawl-mode: Virtual Browser Crawling


function crawlCategory(browser, category, stopword) {
	print("---- Processing category " + category + " in " + getJavascriptFile() + " ----");
	var cumulatedCount = 0;
	var actualCount = 0;
	var page = 1;
	var tracer = 0;
	var check = [];
	var cachedSet = new StringSet();
	while (true) {
		var loadedMore = false;
		var el1;
		var onclicks;
		var index = 1;
		var resArray = [];
		// virtual_browser_sendKeys(browser, "\\\uE010");
		wait(20000);
		do {
			el1 = virtual_browser_find_one(browser, "(//a[starts-with(@href, 'https://tobaratos.com/inmuebles/') and @class='title'])[" + index + "]", KDONOTNOTIFYERROR);
			if ((isDefined(el1)) && (virtual_browser_element_interactable(browser, el1))) {
				resArray.push(virtual_browser_element_attribute(el1, "href"));
			}
			check[index] = el1;
			if (check[index] == check[index - 1]) {
				return actualCount;
			}
			index++;
		} while (isDefined(el1));
		for ( var element in resArray) {
			if (!cachedSet.contains(resArray[element])) {
				cachedSet.add(resArray[element]);
				if (addUrl(resArray[element], category)) {
					actualCount++;
					print("" + actualCount + " - " + resArray[element]);
				} else {
					return actualCount;
				}
				if (passedMaxPublications()) {
					break;
				}
			}
		}
		if (passedMaxPublications()) {
			break;
		}
		cumulatedCount += actualCount;
		// var array = virtual_browser_find_all(browser,
		// "(//a[starts-with(@href, 'https://tobaratos.com/inmuebles/') and
		// @class='searchPaginationNext'])", KDONOTNOTIFYERROR);
		var array = virtual_browser_find_all(browser, "//a/i[@class='fa fa-angle-right']", KDONOTNOTIFYERROR);
		if (isDefined(array)) {
			index = 1;
			var foundIt = false;
			for ( var element in array) {
				try {
					if (virtual_browser_element_interactable(browser, array[element])) {
						virtual_browser_click_element(browser, array[element], KNOTIFYERROR);
						wait(3000);
						html = virtual_browser_html(browser);
						tracer = 0;
						actualCount = 0;
						page++;
						foundIt = true;
						break;
					}
				} catch (err) {
				}
			}
			if (!foundIt) {
				return cumulatedCount;
			}
		} else {
			index = 1;
			html = virtual_browser_html(browser);
			tracer = 0;
			actualCount = 0;
			foundIt = true;
			break;
		}
	}
	return cumulatedCount;
}

var browser;

function analyzeOnePublication(url, mlsJSONString) {
	if (isUndefined(browser)) {
		browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
	}
	virtual_browser_navigate(browser, url);
	var html = virtual_browser_html(browser);
	var mls = JSON.parse(mlsJSONString);
	if (isDefined(html)) {
		if (mls.NonMLSListingData == undefined) {
			mls.NonMLSListingData = {};
		}
		if ((mls.NonMLSListingData.lang == undefined) || (mls.NonMLSListingData.lang == "")) {
			mls.NonMLSListingData.lang = "es-do";
		}
		catgoryCheck = get_unique_regex_match(html, '<span class="transaction" title="Transacci?n">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(catgoryCheck)) {
			if (catgoryCheck.includes("Vender")) {
				var element = virtual_browser_find_one(browser, "(//span[@itemprop='title'])[2]", KDONOTNOTIFYERROR);
				if (isDefined(element)) {
					var category = virtual_browser_element_text(element);
					if (category.includes("Apartamentos")) {
						mls.NonMLSListingData.category = KNUEVOSAPTOVENTAS;
					}
					if (category.includes("Casas")) {
						mls.NonMLSListingData.category = KCASASVENTAS;
					}
				}
			}
		}
		not_available = get_unique_regex_match(html, '<h1 class="mbCl">([^\<]*)', 1, KDONOTNOTIFYERROR);
		if (isDefined(not_available)) {
			if (not_available.includes("404")) {
				return analyzeOnePublication_return_innactive;
			}
		}
		mls.ListingDescription = get_unique_regex_match(html, '<img src="[^\"]*" alt="QR CODE" id="qrcode[^\"]*" class="qrcode" />[\\s\\t\\n ]*?</div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingDescription)) {
			mls.ListingDescription = get_unique_regex_match(html, '<img src="https://tobaratos.com/oc-content/uploads/qrcode/[^\"]*" alt="QR CODE" id="[^\"]*" class="qrcode" />[\s\t\n ]*?</div>(.+?)</div>', 1, KDONOTNOTIFYERROR);
		}
		mls.ListPrice.value = get_next_regex_match(html, 0, "<meta itemprop=\"price\" content=\"([^\"]*)\" />", 1, KDONOTNOTIFYERROR);
		mls.ListPrice.currencyCode = get_next_regex_match(html, 0, "<meta itemprop=\"priceCurrency\" content=\"([^\"]*)\" />", 1, KDONOTNOTIFYERROR);
		if (mls.ListPrice.value < 50000) {
			freq = "month";
			mls.ListPrice.currencyPeriod = filterToAllowedFrequency(freq);
		}
		if (isUndefined(mls.ListPrice.value)) {
			mls.ListPrice.value = KPRICEONDEMAND;
		}
		mls.ListingTitle = get_unique_regex_match(html, "<div class=\"basic\">[\\s\\t\\n ]*?<h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		if (isUndefined(mls.ListingTitle)) {
			mls.ListingTitle = get_unique_regex_match(html, "<div class=\"basic\">[\s\t\n ]*?<h1>([^\<]*)", 1, KDONOTNOTIFYERROR);
		}
		if (isUndefined(mls.ListingTitle) && isUndefined(mls.ListingDescription)) {
			return analyzeOnePublication_return_innactive;
		}
		mls.LotSize.value = get_unique_regex_match(html, "Lot Size:</span>[ ]*?<span class=\"value\">([ 0-9\,\.]*)([a-zA-Z0-9]*)", 1, KDONOTNOTIFYERROR);
		mls.LotSize.areaUnits = get_unique_regex_match(html, "Lot Size:</span>[ ]*?<span class=\"value\">([ 0-9\,\.]*)([a-zA-Z0-9]*)", 2, KDONOTNOTIFYERROR);
		if (isUndefined(mls.LotSize.value)) {
			mls.LotSize.value = get_unique_regex_match(html, "[^\:]*rea Construcci[^\:]*n:([ 0-9\,\.]*)([a-zA-Z0-9]*)", 1, KDONOTNOTIFYERROR);
			mls.LotSize.areaUnits = get_unique_regex_match(html, "[^\:]*rea Construcci[^\:]*n:([ 0-9\,\.]*)([a-zA-Z0-9]*)", 2, KDONOTNOTIFYERROR);
		}
		mls.Location.Latitude = get_next_regex_match(html, 0, "LatLng[(]([^\,]*), ([^\,]*)[)]", 1, KDONOTNOTIFYERROR);
		mls.Location.Longitude = get_next_regex_match(html, 0, "LatLng[(]([^\,]*), ([^\,]*)[)]", 2, KDONOTNOTIFYERROR);
		mls.Brokerage.Name = get_unique_regex_match(html, "<div class=\"user-name\">[\\s\\t\\n ]*?<strong>[\\s\\t\\n ]*?<span>([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Phone = get_next_regex_match(html, 0, "class=\"mobile\" data-phone=\"([^\"]*)", 1, KDONOTNOTIFYERROR);
		mls.Brokerage.Email = get_next_regex_match(html, 0, "Email,([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.MlsId = get_next_regex_match(html, 0, "ID: </div>[\s\t\n ]*?<div class=\"info\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Location.Directions = get_unique_regex_match(html, "<span class=\"location\">([^\<]*)", 1, KDONOTNOTIFYERROR);
		mls.Address.Country.value = "Dominican Republic";
		if (isDefined(mls.Location.Directions)) {
			if (mls.Location.Directions !== "Ubicaci?n desconocida") {
				var comma = mls.Location.Directions.split(",").length;
				var tokens = mls.Location.Directions.split(",");
				if (comma == 3) {
					if (tokens.length > 0) {
						if (isDefined(tokens.length)) {
							mls.Address.StateOrProvince.value = tokens[tokens.length - 3];
						}
					}
					if (tokens.length > 1) {
						if (isDefined(tokens.length)) {
							mls.Address.City.value = tokens[tokens.length - 2].trim();
						}
					}
				}
				if (comma == 2) {
					if (tokens.length > 0) {
						if (isDefined(tokens.length)) {
							mls.Address.City.value = tokens[tokens.length - 2].trim();
						}
					}
					if (tokens.length > 2) {
						if (isDefined(tokens.length)) {
							mls.Address.StateOrProvince.value = tokens[tokens.length - 3];
						}
					}
				}
			}
		}
		var imageCount = 0;
		var images;
		images = get_all_regex_matched(html, "<a data-slide-index=\"[^\"]*\" href=\"\" class=\"navi\">[\\s\\t\\n ]*?<img src=\"([^\"]*)\"", 1);
		images.forEach(function(oneImageTag) {
			var obj = JSON.parse(get_list_empty_variable("photo"));
			obj.MediaURL = oneImageTag.replace("_thumbnail.jpg", ".jpg");
			obj.MediaOrderNumber = imageCount;
			if (mls.Photos.photo == undefined) {
				mls.Photos.photo = [];
			}
			mls.Photos.photo.push(obj);
			imageCount++;
		});
		resulting_json(JSON.stringify(mls));
		return analyzeOnePublication_return_success;
	} else {
		return analyzeOnePublication_return_unreachable;
	}
}

function categoryLandingPoint(browser, url, category) {
	virtual_browser_navigate(browser, url);
	var actualCount = crawlCategory(browser, category);
	print("---- " + actualCount + " found in " + getJavascriptFile() + " for category " + category + " ----");
	return actualCount;
}

function crawlForPublications() {
	if (next_crawl_needed((new Date().getTime() + KDELTATIMEWHILECALCULATION).toString(), true)) {
		var cumulatedCount = 0;
		var startTime = new Date().getTime();
		var browser = create_virtual_browser("HeadlessChrome");
		if (isUndefined(browser)) {
			return analyzeOnePublication_return_tech_issue;
		}
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,otros-inmuebles/sTransaction,1", KOTROSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,solares-y-terrenos/sTransaction,3", KTERRENOSALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,solares-y-terrenos/sTransaction,1", KTERRENOSVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,oficinas-y-locales-comerciales/sTransaction,3", KOFICINASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,oficinas-y-locales-comerciales/sTransaction,1", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,habitaciones-y-viviendas-compartidas/sTransaction,3", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,casas-vacacionales-y-villas/sTransaction,3", KCASASALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,casas/sTransaction,3", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,penthouses/sTransaction,1", KOFICINASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,habitaciones-y-viviendas-compartidas/sTransaction,1", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,casas-vacacionales-y-villas/sTransaction,1", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,casas/sTransaction,1", KCASASVENTAS);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,apartamentos/sTransaction,3", KAPTOALQUILER);
		cumulatedCount += categoryLandingPoint(browser, "https://tobaratos.com/buscar/categoria,apartamentos/sTransaction,1", KAPTOVENTAS);
		virtual_browser_close(browser);
		print("crawlForPublications in " + getJavascriptFile() + " required " + formattedTime(new Date().getTime() - startTime) + " to gather " + cumulatedCount + " listing references.");
		next_crawl_needed(null, false);
	}
}
